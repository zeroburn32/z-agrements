$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function () {
    $('input[type=radio][value=inactif]').iCheck('check');
});

// Charger la liste des agrements avec la colonne action
function listAgrements1(code) {
    $('#tabl_dt').DataTable({
        destroy: true,
        dom: 'lBfrtip',
        stateSave: true,
        stateDuration: -1,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tous"]],
        search: {
            'smart': true,
        },
        buttons: [
            'copy', 'excel', 'pdf'
        ],
        //responsive: true,
        language: {
            url: APP_URL + '/backend/js/French.json'
        },
        processing: true,
        serverSide: true,
        ajax: {
            url: "getAgrementsByEtat/" + code,
            type: 'GET'
        },
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', title:'N°' },
            { data: 'raison_sociale', name: 'raison_sociale', title:'Entreprise' },
            { data: 'numero_ifu', name: 'numero_ifu', title:'IFU' },
            { data: 'libelle_type', name: 'libelle_type', title: 'Type Travaux' },
            { data: 'libelle_court', name: 'libelle_court', title: 'Domaine' },
            { data: 'numero_agrement', name: 'numero_agrement', title: 'Numero Agrement' },
            { data: 'date_arrete', name: 'date_arrete', title: 'Date Arrêté'},
            { data: 'categories', name: 'categories', title: 'Catégories' },
            { data: 'action', name: 'action', orderable: false, title: 'Action'},
        ],
        order: [[1, 'asc']]
    });    
}
// Charger la liste des agrements sans la colonne action
function listAgrements2(code) {
    $('#tabl_dt_ret_exp').DataTable({
        destroy: true,
        dom: 'lBfrtip',
        stateSave: true,
        stateDuration: -1,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tous"]],
        search: {
            'smart': true,
        },
        buttons: [
            'copy', 'excel', 'pdf'
        ],
        //responsive: true,
        language: {
            url: APP_URL + '/backend/js/French.json'
        },
        processing: true,
        serverSide: true,
        ajax: {
            url: "getAgrementsByEtat/" + code,
            type: 'GET'
        },
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', title: 'N°' },
            { data: 'raison_sociale', name: 'raison_sociale', title: 'Entreprise' },
            { data: 'numero_ifu', name: 'numero_ifu', title: 'IFU' },
            { data: 'libelle_type', name: 'libelle_type', title: 'Type Travaux' },
            { data: 'libelle_court', name: 'libelle_court', title: 'Domaine' },
            { data: 'numero_agrement', name: 'numero_agrement', title: 'Numero Agrement' },
            { data: 'date_arrete', name: 'date_arrete', title: 'Date Arrêté' },
            { data: 'categories', name: 'categories', title: 'Catégories' },
            //{ data: 'action', name: 'action', orderable: false, title: 'Action'},
        ],
        order: [[1, 'asc']]
    });    
}
$('input[type=radio][name=actif]').on('ifChecked', function () {
    if (this.value == 'actif' || this.value == 'inactif') {
        $('#divAgr').show();
        $('#divAgr2').hide();
        if (this.value == 'inactif') {
            listAgrements1(0);
        } else if (this.value == 'actif'){
            listAgrements1(1);
        }
    } else {
        $('#divAgr').hide();
        $('#divAgr2').show();
        if (this.value == 'retrait') {
            listAgrements2(2);
        } else if(this.value == 'expire') {
            listAgrements2(3);
        } else if(this.value == 'valide') {
            listAgrements2(4);
        }
    }
});

function toggle(id, tableName, attribute, value) {   // Valider ou invalider un agrément
    swal({
        title: 'Êtes-vous sûr ?',
        text: "De vouloir modifier cet agrément ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#115e1e',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Oui, Modifier!',
        cancelButtonText: 'Non, annuler',
        reverseButtons: true,
        showLoaderOnConfirm: true,
        preConfirm: function () {
            return new Promise(function (resolve) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                    $.ajax({
                        type: "POST",
                        url: "toggle",
                        data: { id: id, tableName: tableName, attribute : attribute, value : value},
                        success: function (data) {
                            swal({
                                title: "Succès",
                                text: "Changement effectué!",
                                type: "success",
                                button: "OK !",
                            },
                                function () {
                                    if (value == 0) {
                                        $('input[type=radio][value=actif]').iCheck('check',function () {
                                            listAgrements1(1);
                                        });
                                    } else {
                                        $('input[type=radio][value=inactif]').iCheck('check', function () {
                                            listAgrements1(0);
                                        });
                                    }
                                });
                        },
                        error: function (data) {
                            swal({
                                title: 'Erreur...',
                                text: "Une erreur est survenue. Veuillez reessayer!",
                                type: 'error',
                                timer: '3500'
                            });
                        }
                    });
                });
            }
        });
    }
function retirer(id) {
    swal({
        title: 'CONFIRMATION',
        text: "Voullez-vous retirer cet agrément ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#115e1e',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Oui, Retirer!',
        cancelButtonText: 'Non, annuler',
        reverseButtons: true,
        showLoaderOnConfirm: true,
        preConfirm: function () {
            return new Promise(function (resolve) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                    $.ajax({
                        type: "POST",
                        url: "retirer",
                        data: { id: id},
                        success: function (data) {
                            if(data.rollback !=1) {
                            swal({
                                title: "Succès",
                                text: "Agrement retiré avec succès!",
                                type: "success",
                                button: "OK !",
                            },
                                function () {
                                    $('input[type=radio][value=actif]').iCheck('check', function () {
                                        listAgrements1(1);
                                    });
                                });
                        } else {
                                swal({
                                    title: 'Erreur...',
                                    text: "Transaction annulée. Veuillez reessayer!",
                                    type: 'error',
                                    timer: '3500'
                                });
                        }},
                        error: function (data) {
                            swal({
                                title: 'Erreur...',
                                text: "Une erreur est survenue. Veuillez reessayer!",
                                type: 'error',
                                timer: '3500'
                            });
                        }
                    });
                });
            }
        });
    }

// recupération des categories par domaines et type de travaux
function loadAgrement(id) {
    $.get('getAgrement/' + id , function (data) {
        $('#Ent_id').val(data.Ent_id).trigger('change');
        $('#type_travaux').val(data.Typ_id);
        $('#type_travaux').select2().trigger('select');
    
        
        $('#domaine').val(data.Dom_id).trigger('change');
        
        $('#numero_agrement').val(data.numero_agrement);
        $('#numero_arrete').val(data.numero_arrete);
        $('#date_arrete').val(data.date_arrete);
        $('#agr_id').val(id);
        $('#formMode').val('edit');
       
        checkCategories(id);
    });
        
}
function loadEntreprise(id) {
    $.get('getEntreprise/' + id , function (data) {
        $('#formMode').val('edit');
        $('#ent_id').val(id);

        $('#Reg_id').val(data.Reg_id).select2().trigger('select');
        $('#Pro_id').val(data.Pro_id).select2().trigger('select');
        $('#Com_id').val(data.Com_id).select2().trigger('select');
        
        $('#raison_sociale').val(data.raison_sociale);
        $('#siege_social').val(data.siege_social);
        $('#adresse').val(data.adresse);
        $('#telephone').val(data.telephone);
        $('#registre_commerce').val(data.registre_commerce);
        $('#numero_ifu').val(data.numero_ifu);
        $('#numero_cnss').val(data.numero_cnss);
        $('#personne_responsable').val(data.personne_responsable);

    });
        
}

function checkCategories(id) {
    $.get('getCategoriesByAgrement/' + id , function (data) {
        $.each(data, function (i, o) {
            $(":checkbox[value="+ o.id +"]").prop("checked", "true");  
        });
    });
}


$('#domaine , #type_travaux' ).on('change', function () {
    getCategories();
});

function getCategories () {
    var type_travaux = $("#type_travaux").val();
    var domaine = $("#domaine").val();
    toAppend = '';
    if (domaine != '' && type_travaux != '') {
        $.get('getCategorie/' + domaine + '/' + type_travaux, function (data) {
            $.each(data, function (i, o) {
                toAppend += '<label class="checkbox-inline"><input type="checkbox" name="categories[]" class="square" value="' + o.id + '">&nbsp;' + o.libelle_court + '</label>';
            });
            $('#cb_categorie').empty();
            $('#cb_categorie').append(toAppend);
        });
    }
}
function getChecked() {
    var isChecked = $('[type=checkbox]:checked');
    var names = "";
    isChecked.each(function () {
        names += $(this).val();
    });
    alert(names);
}

$("#frmEntreprise").submit(function (event) {
    event.preventDefault();
    var frm = $('#frmEntreprise'); 
    $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        success: function (data) {
            //console.log(data);
            $('#modal-create-entreprise').hide();
            swal({
                title: "Succès",
                text: "Entreprise "+data.raison_sociale+" ajoutée !",
                icon: "success",
                button: "OK !",
            });
            toAppend = '<option value=' + data.id + '>' + data.raison_sociale + '</option>';
            $('#Ent_id').append(toAppend);
                
            if (data.length != 0) {

            } else {

            }
        }
    });

});
