$(document).ready(function () {
    /* $('#Sta_id', '#Dom_id', '#Typ_id').select2('destroy');
    $('#Sta_id','#Dom_id','#Typ_id').val('').select2({
        placeholder: '-- Choisir une catégorie--',
        language: "fr",
        minimumResultsForSearch: 3
    }); */
});

$('#Dom_id , #Typ_id').on('change', function () {
    getCategories();
});

function getCategories() {
    var type_travaux = $("#Typ_id").val();
    var domaine = $("#Dom_id").val();
    toAppend = '';
    if (domaine != '' && type_travaux != '') {
        $.get('getCategorie/' + domaine + '/' + type_travaux, function (data) {
            var toAppend = '<option value="-1">--Choisir une catégorie--</option>';
            $.each(data, function (i, o) {
                toAppend += '<option value=' + o.id + '>' + o.libelle_court + '</option>';
            });
            var x = document.getElementById("Cat_id");
            while (x.options.length) {
                x.remove(0);
            }
            $('#Cat_id').append(toAppend);
            $('#Cat_id').select2('destroy');
            $('#Cat_id').val('').select2({
                placeholder: '-- Choisir une catégorie--',
                language: "fr",
                minimumResultsForSearch: 3
            });
        });
    }
}