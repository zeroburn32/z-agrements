$(document).ready(function() {
    $("table.example").DataTable({
        responsive: true,
        language: {
            url: APP_URL + '/backend/js/French.json'
        }
    });
    $('#example').DataTable( {
        "scrollX": true,
        language: {
            url: APP_URL + '/backend/js/French.json'
        }
    } );
} );

