function desactiver(id, login) {
    swal({
        title: 'Êtes-vous sûre?',
        text: "De vouloir désactivé le compte de " + login + " ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Oui, Désactiver',
        confirmButtonColor: '#08C832',
        cancelButtonText: 'Non, Annuler',
        cancelButtonColor: '#d33',
        reverseButtons: true,
        showLoaderOnConfirm: true,
        preConfirm: function () {
            return new Promise(function (resolve) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{url('/lockAndUnlockAccount')}}/" + id,
                    type: 'PATCH',
                    data: {id: id, '_token': '{{csrf_token()}}'},
                    success: function (response) {
                        swal({
                            title: "Désactivation !",
                            text: "Compte désactiver avec succès !",
                            type: "success",
                            button: "OK"
                        });
                        window.location.href = "users";
                    },
                    error: function () {
                        swal({
                            title: "Désactivation !",
                            text: "Une erreur est survenue lors de la désactivation !",
                            type: "error",
                            button: "OK"
                        });
                    }
                });
            });
        },
        allowOutsideClick: false
    });
}


function activer(id, login) {
    swal({
        title: 'Êtes-vous sûre?',
        text: "De vouloir activé le compte de " + login + " ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Oui, Activer',
        confirmButtonColor: '#08C832',
        cancelButtonText: 'Non, Annuler',
        cancelButtonColor: '#d33',
        reverseButtons: true,
        showLoaderOnConfirm: true,
        preConfirm: function () {
            return new Promise(function (resolve) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{url('/lockAndUnlockAccount')}}/" + id,
                    type: 'PATCH',
                    data: {id: id, '_token': '{{csrf_token()}}'},
                    success: function (response) {
                        swal({
                            title: "Activation !",
                            text: "Compte activer avec succès !",
                            type: "success",
                            button: "OK"
                        })
                        window.location.href = "users";
                    },
                    error: function () {
                        swal({
                            title: "Activation !",
                            text: "Une erreur est survenue lors de l'activation !",
                            type: "error",
                            button: "OK"
                        });
                    }
                });
            });
        },
        allowOutsideClick: false
    });
}

function reinitialiser(id, login) {
    swal({
        title: 'Êtes-vous sûre ?',
        text: "De vouloir réinitialiser le mot de passe du compte de " + login + " ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Oui, Réinitialiser',
        confirmButtonColor: '#08C832',
        cancelButtonText: 'Non, Annuler',
        cancelButtonColor: '#d33',
        reverseButtons: true,
        showLoaderOnConfirm: true,
        preConfirm: function () {
            return new Promise(function (resolve) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{url('/reinitialiserMotDePasse')}}/" + id,
                    type: 'GET',
                    success: function (data) {
                        swal({
                            title: "Opération reussie !",
                            text: "Votre nouveau mot de passe est : " + data,
                            type: "success",
                            button: "OK"
                        });
                    },
                    error: function (data) {
                        swal({
                            title: "Réinitialisation !",
                            text: "Une erreur est survenue lors de l'envoi du mot de passe par mail. Votre nouveau mot de passe est : " + data,
                            type: "error",
                            button: "OK"
                        });
                    }
                });
            });
        },
        allowOutsideClick: false
    });
}