//initialisation du datatable
$('#tabl').DataTable({
    dom: 'lBfrtip',
    stateSave: true,
    stateDuration: -1,
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tous"]],
    search: {
        'smart': true,
    },
    buttons: [
        'excel', 'pdf', 'print'
    ],
    language: {
        url: APP_URL + '/backend/js/French.json'
    },
});
    //Champs de recherche sur colonnes individuelles
    $('#tabl tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" />' );
    } );

        

        $('#mychartdata').DataTable({
            dom: 'lBfrtip',
            lengthMenu: [[15, 25, 50, -1], [15, 25, 50, "Tous"]],
            search: {
                'smart': true,
              },
            buttons: [
                'excel', 'pdf', 'print'
                ],
            language: {
                url : 'js/French.json'
            },
        });
    
        $('#tablReal').DataTable({
            dom: 'lBfrtip',
            stateSave: true,             
            stateDuration: -1,             
            lengthMenu: [[7,10, 25, 50, -1], [7,10, 25, 50, "Tous"]],
            search: {
                'smart': true,
              },
            buttons: [
                    { extend: 'copy', className: 'btn btn-info btn-sm glyphicon glyphicon-duplicate' },
                    { extend: 'print', className: 'btn btn-info btn-sm glyphicon glyphicon-print' },
                    { extend: 'excel', className: 'btn btn-info btn-sm glyphicon glyphicon-list-alt' },
                    { extend: 'pdf', className: 'btn btn-info btn-sm glyphicon glyphicon-file' }                
                ],
            language: {
                url : 'js/French.json'
            },

            //select input search
            initComplete: function () {
                this.api().columns().every( function () {
                    var column = this;
                    var select = $('<select style="width: 75px;"><option value=""></option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );
     
                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            }
        });

        // Datatables pour le tableau des anciennes réalisations
        $('#tablReal1').DataTable({
            dom: 'lBfrtip',
            stateSave: true,             
            stateDuration: -1 ,            
            lengthMenu: [[7,10, 25, 50, -1], [7,10, 25, 50, "Tous"]],
            search: {
                'smart': true,
              },
            buttons: [
                { extend: 'copy', className: 'btn btn-info btn-sm glyphicon glyphicon-duplicate' },
                { extend: 'print', className: 'btn btn-info btn-sm glyphicon glyphicon-print' },
                { extend: 'excel', className: 'btn btn-info btn-sm glyphicon glyphicon-list-alt' },
                { extend: 'pdf', className: 'btn btn-info btn-sm glyphicon glyphicon-file' }                
            ],
            language: {
                url : 'js/French.json'
            },
            //select input search
            initComplete: function () {
                this.api().columns().every( function () {
                    var column = this;
                    var select = $('<select style="width: 75px;"><option value=""></option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );
     
                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            }
        });

    // Initialisation du select2 -->
    $('select').select2({
         placeholder: 'Choisir un element ...',
         allowClear: true,
         width: '100%',
         minimumResultsForSearch: 3
    });
    

    //Initialisation date et time picker
    $('.datepicker').datepicker(
        {
            format: 'yyyy-mm-dd',
            autoclose: true
        }
        ).on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });
;
    $('.datetimepicker').datetimepicker(
            {
                format: 'yyyy-mm-dd - hh:ii:ss',
                autoclose: true
            }
            ).on('changeDate', function (ev) {
                $(this).datetimepicker('hide');
            });
    ;

    $('.timepicker').timepicker(
            {
                format: 'hh:ii:ss',
                autoclose: true
            }
            ).on('changeDate', function (ev) {
                $(this).timepicker('hide');
            });
    ;

    // Collapse panel dans index réalisation
    $(document).on('click', '.panel-heading span.clickable', function(e){
        var $this = $(this);
        if(!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.box-body').slideUp();
            $this.addClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
            
        } else {
            $this.parents('.panel').find('.box-body').slideDown();
            $this.removeClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
            
        }
    });

    