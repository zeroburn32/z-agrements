// recupération des provinces d'une région
$('#Reg_id').on('change', function () {
    var id = $("#Reg_id").val();
    toAppend = '';
    $.get('getProvinces/' + id, function (data) {
        $.each(data, function (i, o) {
            toAppend += '<option value=' + o.id + '>' + o.libelle_province + '</option>';
        });
        var x = document.getElementById("Pro_id");
        while (x.options.length) {
            x.remove(0);
        }
        $('#Pro_id').append(toAppend); 
        $("#Pro_id").select2("val", ""); 
        //$('#Pro_id').val(k);
    });
});
// recupération des communes d'une province
$('#Pro_id').on('change', function () {
    var id = $("#Pro_id").val();
    console.log(id);
    var toAppend = '';
    $.get('getCommunes/' + id, function (data) {
        $.each(data, function (i, o) {
            toAppend += '<option value=' + o.id + '>' + o.libelle_commune + '</option>';
        });
        var x = document.getElementById("Com_id");
        while (x.options.length) {
            x.remove(0);
        }
        $('#Com_id').append(toAppend);
        $('#Com_id').select2("val", "");
    });
});
// recupération des centres de compilation d'une commune
/* $('#Com_id').on('change', function () {
    var id = $("#Com_id").val();
    $.get('getCentres/' + id, function (data) {

        var toAppend = '';
        $.each(data, function (i, o) {
            toAppend += '<option value=' + o.id_centre + '>' + o.libelle_centre + '</option>';
        });
        var x = document.getElementById("cen_id");
        while (x.options.length) {
            x.remove(0);
        }
        $('#cen_id').append(toAppend);
        $('#cen_id').select2("val", "");
    });
}); */