 // Verif contact form

function surligne(champ, erreur) {
    if (erreur)
        champ.style.backgroundColor = "#fba";
    else
        champ.style.backgroundColor = "";
}
function verifMessage(champ) {
    if (champ.value.length < 2 || champ.value.length > 1000) {
        surligne(champ, true);
        return false;
    }
    else {
        surligne(champ, false);
        return true;
    }
}

function verifEMail(champ) {
    var regex = /^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/;
    if (!regex.test(champ.value)) {
        surligne(champ, true);
        return false;
    }
    else {
        surligne(champ, false);
        return true;
    }
}

function verifObjet(champ) {
    if (champ.value.length < 3 || champ.value.length > 30) {
        surligne(champ, true);
        return false;
    }
    else {
        surligne(champ, false);
        return true;
    }
}

function verifPrenom(champ) {
    if (champ.value.length < 3 || champ.value.length > 50) {
        surligne(champ, true);
        return false;
    }
    else {
        surligne(champ, false);
        return true;
    }
}

function verifNom(champ) {
    if (champ.value.length < 3 || champ.value.length > 25) {
        surligne(champ, true);
        return false;
    }
    else {
        surligne(champ, false);
        return true;
    }
}

//verification du formulaire

function verifForm(f) {
    var NomOk = verifNom(f.name);
    var PrenomOk = verifPrenom(f.prenom);
    var ObjetOk = verifObjet(f.objet);
    var EmailOk = verifEMail(f.email);
    var MessageOk = verifMessage(f.message);

    if (NomOk && PrenomOk && ObjetOk && EmailOk && MessageOk)
        return true;
    else {
        alert("Veuillez remplir correctement tous les champs");
        return false;
    }
}
// Send form data
$(document).ready(function () {
    $('#btnEn').submit(function (event) {
        event.preventDefault();
        if (verifForm(this)) {
            console.log('bonnnnnn');
            var frm = $('#btnEn');
            console.log(frm);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '<?php echo csrf_token();?>'
                }
            });

            $.ajax({
                url: frm.attr('action'),
                type: frm.attr('method'),
                data: frm.serialize(),
                success: function (data) {
                    //$('#formConModal').hide();
                    console.log(data);
                    if(data == 1) {
                        swal.fire({
                            title: "Erreur ",
                            text: "Erreur lors de l'envoi du mail. Veuillez réessayer. ",
                            icon: "error",
                        }).then((result) => {

                        });
                    } else if (data == 2) {
                        swal.fire({
                            title: "Erreur ",
                            text: "Le code ccaptcha est incorrect. Veuillez réessayer! ",
                            icon: "error",
                        }).then((result) => {
                        });
                    } else {
                        swal.fire({
                            title: "Mail Envoyé",
                            text: "Votre message a bien été envoyé.",
                            icon: "success",
                        }).then((result) => {
                            window.location.reload();
                            /* $('#formConModal').modal('hide'); */
                        });
                    }
                    
                },
                error: function (data) {
                    console.log(data);
                    swal.fire({
                        title: "Erreur ",
                        text: "Erreur lors de l'envoi du mail. Veuillez réessayer. ",
                        icon: "error",
                    }).then((result) => {

                    });
                }
            });
        }
    });
});