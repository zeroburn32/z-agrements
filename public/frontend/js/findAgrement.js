$("#searchForm").submit(function (event) {
    event.preventDefault();
    var frm = $('#findAgrementFrm');
    $('#result').addClass('d-none');
    $('#noresult').addClass('d-none');
    $('#btnPrint').addClass('d-none');
    $('.loader').removeClass('d-none');
    $('.progress').removeClass('d-none');
    $.ajax({
         xhr: function () {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    $('.progress').css({
                        width: percentComplete * 100 + '%'
                    });
                    $('.loader').removeClass('d-none');
                    if (percentComplete === 1) {
                        $('.progress').addClass('d-none');
                        $('.loader').addClass('d-none');
                    }
                }
            }, false);
            xhr.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    $('.progress').css({
                        width: percentComplete * 100 + '%'
                    });
                    if (percentComplete === 1) {
                        $('.loader').delay(3000).addClass('d-none');
                        $('.progress').delay(3000).addClass('d-none');
                        
                    }
                }
            }, false);
            return xhr;
        },
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        success: function (data) {           
            if (data.length != 0 ){
            $('#raisonSociale').text(data.raison_sociale);
            $('#numeroIFU').text(data.numero_ifu);
            $('#forIFU1').val(data.numero_ifu);
            $('#rccm').text(data.registre_commerce);
            $('#numeroAgrement').text(data.numero_agrement);
            $('#forAgr1').val(data.numero_agrement);
            $('#typeTravaux').text(data.libelle_type);
            $('#domaine').text(data.libelle_court);
            $('#numeroArrete').text(data.numero_arrete);
            $('#dateArrete').text(data.date_arrete);
            $('#statutAgrement').text(data.statut);
            $('#date_expiration').html(data.expiration + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ( ' + data.date_expiration+' )');
            $('#last_update').text(data.last_update);
                
            var textCat = '';

            $.each(data.categories, function (i, val) {
                textCat = textCat + '&nbsp;&nbsp;&nbsp;&nbsp;<button id="categories" type="button" class="btn btn-secondary">' + '  ' + val.libelle_court.toUpperCase() + '  '+ '</button>'; 
                
            });

            $('#categorie').html(textCat);

            if (data.codeStatut == "VALIDE") {
                $("#tdstatut").css("background-color", "green");
                $("#my_portlet").css("background-color", "green");
                $("#tdstatut").css("color", "white");
            } else {
                $("#tdstatut").css("background-color", "red");
                $("#my_portlet").css("background-color", "red");
                $("#tdstatut").css("color", "white");
            }
                $('#searchForm').animate({ 'padding-top': "0%" });
                $('#result').removeClass('d-none');
                $('#btnPrint').removeClass('d-none');
            
        } else {
                $('#searchForm').animate({ 'padding-top': "0%" });
                $('#noresult').removeClass('d-none');
        }
        },
        error: function (data) {
            console.log(data);
        }
    });

});

