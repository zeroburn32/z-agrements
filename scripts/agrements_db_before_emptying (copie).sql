-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Ven 10 Avril 2020 à 17:41
-- Version du serveur :  5.7.29-0ubuntu0.18.04.1
-- Version de PHP :  7.2.27-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `agrements_db`
--

-- --------------------------------------------------------

--
-- Structure de la table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `log_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_id` bigint(20) UNSIGNED DEFAULT NULL,
  `subject_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `causer_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `properties` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `activity_log`
--

INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
(1, 'default', 'App\\Role model has been created', 1, 'App\\Role', NULL, NULL, '[]', '2019-10-08 12:28:55', '2019-10-08 12:28:55'),
(2, 'default', 'App\\Role model has been created', 2, 'App\\Role', 1, 'App\\User', '[]', '2019-10-08 13:25:25', '2019-10-08 13:25:25'),
(3, 'default', 'App\\Role model has been created', 3, 'App\\Role', 1, 'App\\User', '[]', '2019-11-12 22:21:45', '2019-11-12 22:21:45'),
(4, 'default', 'App\\Role model has been updated', 3, 'App\\Role', 1, 'App\\User', '[]', '2019-12-04 02:41:42', '2019-12-04 02:41:42'),
(5, 'default', 'Changement de statut d\'un agrement', NULL, NULL, 1, 'App\\User', '[]', '2019-12-04 03:15:23', '2019-12-04 03:15:23'),
(6, 'default', 'Entreprise modifiée ', NULL, NULL, 1, 'App\\User', '[]', '2019-12-04 03:16:11', '2019-12-04 03:16:11'),
(7, 'default', 'Agrement modifié ', NULL, NULL, 1, 'App\\User', '[]', '2019-12-04 03:16:38', '2019-12-04 03:16:38'),
(8, 'default', 'Nouvel agrement ajouté', NULL, NULL, 1, 'App\\User', '[]', '2019-12-04 09:09:23', '2019-12-04 09:09:23'),
(9, 'default', 'Changement de statut d\'un agrement', NULL, NULL, 1, 'App\\User', '[]', '2019-12-04 09:11:41', '2019-12-04 09:11:41'),
(10, 'default', 'Agrement retiré', NULL, NULL, 1, 'App\\User', '[]', '2019-12-04 09:12:12', '2019-12-04 09:12:12'),
(11, 'default', 'Nouvel agrement ajouté', NULL, NULL, 3, 'App\\User', '[]', '2019-12-04 10:06:25', '2019-12-04 10:06:25'),
(12, 'default', 'Changement de statut d\'un agrement', NULL, NULL, 1, 'App\\User', '[]', '2019-12-04 10:09:37', '2019-12-04 10:09:37'),
(13, 'default', 'Agrement retiré', NULL, NULL, 1, 'App\\User', '[]', '2019-12-04 10:10:42', '2019-12-04 10:10:42'),
(14, 'default', 'Entreprise modifiée ', NULL, NULL, 1, 'App\\User', '[]', '2020-01-22 13:30:25', '2020-01-22 13:30:25'),
(15, 'default', 'Changement de statut d\'un agrement', NULL, NULL, 1, 'App\\User', '[]', '2020-01-22 15:29:29', '2020-01-22 15:29:29'),
(16, 'default', 'Changement de statut d\'un agrement', NULL, NULL, 1, 'App\\User', '[]', '2020-01-22 15:29:39', '2020-01-22 15:29:39'),
(17, 'default', 'Changement de statut d\'un agrement', NULL, NULL, 1, 'App\\User', '[]', '2020-02-04 17:12:26', '2020-02-04 17:12:26'),
(18, 'default', 'Changement de statut d\'un agrement', NULL, NULL, 1, 'App\\User', '[]', '2020-02-04 17:13:08', '2020-02-04 17:13:08'),
(19, 'default', 'Entreprise modifiée ', NULL, NULL, 1, 'App\\User', '[]', '2020-04-05 09:10:08', '2020-04-05 09:10:08'),
(20, 'default', 'Entreprise modifiée ', NULL, NULL, 1, 'App\\User', '[]', '2020-04-05 09:38:59', '2020-04-05 09:38:59'),
(21, 'default', 'Changement de statut d\'un agrement', NULL, NULL, 1, 'App\\User', '[]', '2020-04-09 12:03:38', '2020-04-09 12:03:38'),
(22, 'default', 'Agrement retiré', NULL, NULL, 1, 'App\\User', '[]', '2020-04-09 23:36:08', '2020-04-09 23:36:08'),
(23, 'default', 'Agrement retiré', NULL, NULL, 1, 'App\\User', '[]', '2020-04-09 23:38:01', '2020-04-09 23:38:01'),
(24, 'default', 'Nouvelle Faq ajoutée', NULL, NULL, 1, 'App\\User', '[]', '2020-04-10 14:33:00', '2020-04-10 14:33:00'),
(25, 'default', 'Changement de statut d\'un agrement', NULL, NULL, 1, 'App\\User', '[]', '2020-04-10 17:25:18', '2020-04-10 17:25:18'),
(26, 'default', 'Agrement retiré', NULL, NULL, 1, 'App\\User', '[]', '2020-04-10 17:28:41', '2020-04-10 17:28:41');

-- --------------------------------------------------------

--
-- Structure de la table `Agrements`
--

CREATE TABLE `Agrements` (
  `id` int(11) NOT NULL,
  `Ent_id` int(11) DEFAULT NULL,
  `Typ_id` int(11) DEFAULT NULL,
  `Dom_id` int(11) DEFAULT NULL,
  `numero_agrement` varchar(254) NOT NULL,
  `numero_arrete` varchar(254) DEFAULT NULL,
  `date_arrete` date DEFAULT NULL,
  `observation` varchar(254) DEFAULT NULL,
  `est_expire` tinyint(1) DEFAULT '0',
  `est_actif` tinyint(1) NOT NULL DEFAULT '0',
  `date_expiration` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Agrements`
--

INSERT INTO `Agrements` (`id`, `Ent_id`, `Typ_id`, `Dom_id`, `numero_agrement`, `numero_arrete`, `date_arrete`, `observation`, `est_expire`, `est_actif`, `date_expiration`, `created_at`, `updated_at`) VALUES
(2, 3, 1, 2, '15-011/W-AEUE', '2015-027/MARHASA/CAB', '2015-12-31', NULL, 0, 1, '2020-12-31', '2019-09-08 16:22:20', '2019-11-18 12:26:41'),
(4, 4, 2, 1, '15-019/W-AEP', '2016-120/MEA/CAB', '2016-12-31', NULL, 0, 0, '2021-12-31', '2019-09-08 16:28:30', '2019-11-18 12:28:12'),
(7, 6, 2, 3, '15-017/W-AH', '2015-021/MARHASA/CAB', '2015-12-31', NULL, 0, 1, '2020-12-31', '2019-09-13 11:14:28', '2019-10-06 13:20:43'),
(14, 3, 2, 2, '25-071/W-AEUE', '201574-127/MARHASA/CAB', '2013-12-31', 'RAS', 1, 1, '2018-12-31', '2019-10-05 01:22:49', '2019-10-05 01:22:49'),
(20, 4, 1, 2, '17-117/ET-AEUE', '2011-030/MEA/CAB', '2011-01-02', NULL, 1, 1, '2016-01-02', '2019-10-06 12:32:04', '2019-11-11 22:50:33'),
(21, 4, 2, 3, 'AZERTY1', 'QZERTY', '2019-10-07', NULL, 0, 1, '2024-10-07', '2019-10-15 15:02:34', '2019-10-15 15:02:34'),
(22, 6, 1, 1, '16-019/E-AEP', '2019-030/MEA/CAB', '2019-11-15', NULL, 0, 1, '2024-11-15', '2019-11-18 09:49:14', '2019-11-18 09:49:14'),
(24, 18, 1, 1, '15-00B/EC-AEP', 'N° 2015-019/MARHASA/CAB', '2015-02-15', NULL, 1, 1, '2020-02-15', '2019-11-19 09:58:40', '2019-11-19 09:58:40'),
(25, 17, 2, 2, 'AZERTY001', '2016-100/MEA/CAB', '2019-11-14', NULL, 0, 0, '2024-11-14', '2019-11-19 10:22:15', '2019-11-19 10:22:15'),
(26, 5, 1, 3, 'QWERTY002', '2016-130/MEA/CAB', '2014-01-01', NULL, 1, 0, '2019-01-01', '2019-11-19 10:41:39', '2019-12-04 00:33:43'),
(27, 15, 1, 3, 'TESTTEST03', 'TESTARRATE', '2016-05-12', NULL, 0, 1, '2021-05-12', '2019-11-19 12:15:40', '2019-12-04 03:16:38'),
(28, 7, 1, 1, '156-00B/EC-AEP', '2016-121/MEA/CAB', '2017-01-01', NULL, 0, 1, '2022-01-01', '2019-11-19 16:14:13', '2019-11-19 16:14:13'),
(30, 7, 1, 2, '14-03/ET-AEUE', '2011-210/MEA/CAB', '2011-01-01', NULL, 1, 1, '2016-01-01', '2019-12-04 00:44:02', '2019-12-04 00:44:02'),
(31, 19, 1, 2, '16-003/ECAEUE', '2016-17/MEA/CAB', '2016-06-15', NULL, 0, 1, '2021-06-15', '2019-12-04 01:06:43', '2019-12-04 01:06:43'),
(32, 20, 2, 1, '21-312/TRV-AEP', '2018-221/MEA/CAB', '2018-05-04', NULL, 0, 1, '2023-05-04', '2019-12-04 09:09:23', '2019-12-04 09:09:23'),
(33, 21, 2, 2, '19-017/W-AEUE', '2019-45/MEA/CAB', '2019-04-10', NULL, 0, 1, '2024-04-10', '2019-12-04 10:06:25', '2019-12-04 10:06:25');

-- --------------------------------------------------------

--
-- Structure de la table `CategorieAgrements`
--

CREATE TABLE `CategorieAgrements` (
  `id` int(11) NOT NULL,
  `Cat_id` int(11) DEFAULT NULL,
  `Agr_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CategorieAgrements`
--

INSERT INTO `CategorieAgrements` (`id`, `Cat_id`, `Agr_id`, `created_at`, `updated_at`) VALUES
(1, 36, 14, NULL, NULL),
(2, 37, 14, NULL, NULL),
(22, 31, 21, NULL, NULL),
(23, 32, 21, NULL, NULL),
(24, 33, 21, NULL, NULL),
(25, 23, 20, NULL, NULL),
(26, 24, 20, NULL, NULL),
(27, 13, 22, NULL, NULL),
(28, 14, 22, NULL, NULL),
(29, 15, 22, NULL, NULL),
(30, 37, 2, NULL, NULL),
(31, 38, 2, NULL, NULL),
(34, 44, 4, NULL, NULL),
(35, 45, 4, NULL, NULL),
(36, 10, 24, NULL, NULL),
(37, 11, 24, NULL, NULL),
(38, 16, 24, NULL, NULL),
(39, 21, 24, NULL, NULL),
(40, 37, 25, NULL, NULL),
(41, 38, 25, NULL, NULL),
(42, 39, 25, NULL, NULL),
(47, 13, 28, NULL, NULL),
(48, 14, 28, NULL, NULL),
(49, 32, 7, NULL, NULL),
(50, 33, 7, NULL, NULL),
(53, 27, 26, NULL, NULL),
(54, 28, 26, NULL, NULL),
(57, 23, 30, NULL, NULL),
(58, 24, 30, NULL, NULL),
(59, 23, 31, NULL, NULL),
(66, 27, 27, NULL, NULL),
(67, 28, 27, NULL, NULL),
(68, 29, 27, NULL, NULL),
(69, 43, 32, NULL, NULL),
(70, 44, 32, NULL, NULL),
(71, 37, 33, NULL, NULL),
(72, 38, 33, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `Categories`
--

CREATE TABLE `Categories` (
  `id` int(11) NOT NULL,
  `Cat_id` int(11) DEFAULT NULL,
  `Dom_id` int(11) DEFAULT NULL,
  `Typ_id` int(11) DEFAULT NULL COMMENT 'Type de travaux -etudes ou Travaux',
  `libelle_court` varchar(20) NOT NULL,
  `libelle_long` varchar(50) NOT NULL,
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `est_actif` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Categories`
--

INSERT INTO `Categories` (`id`, `Cat_id`, `Dom_id`, `Typ_id`, `libelle_court`, `libelle_long`, `description`, `created_at`, `updated_at`, `est_actif`) VALUES
(1, NULL, 1, 1, 'F', 'Groupe F', NULL, '2019-10-01 16:31:48', '2019-10-01 16:34:06', 0),
(2, NULL, 1, 1, 'Eu', 'Groupe Eu', NULL, '2019-10-01 16:32:56', '2019-10-01 16:34:43', 0),
(3, 1, 1, 1, 'Fe', 'Sous Groupe Fe', NULL, '2019-10-01 16:35:30', '2019-10-01 16:47:20', 0),
(4, 1, 1, 1, 'Fs', 'Sous Groupe Fs', NULL, '2019-10-01 16:35:53', '2019-10-01 16:47:02', 0),
(5, 1, 1, 1, 'Fi', 'Sous Groupe Fi', NULL, '2019-10-01 16:36:29', '2019-10-01 16:36:29', 0),
(6, 1, 1, 1, 'Fc', 'Sous Groupe Fc', NULL, '2019-10-01 17:13:10', '2019-10-01 17:13:10', 0),
(7, 1, 1, 1, 'Fsic', 'sous groupe Fsic', NULL, '2019-10-01 17:15:13', '2019-10-01 17:15:13', 0),
(8, 3, 1, 1, 'Fe1', 'Catégorie Fe1', NULL, '2019-10-01 17:15:41', '2019-10-01 17:15:41', 1),
(9, 3, 1, 1, 'Fe2', 'Catégorie Fe2', NULL, '2019-10-01 17:15:54', '2019-10-01 17:17:01', 1),
(10, 4, 1, 1, 'Fs1', 'Catégorie Fs1', NULL, '2019-10-01 17:16:28', '2019-10-01 17:17:11', 1),
(11, 5, 1, 1, 'Fi1', 'Catégorie Fi1', NULL, '2019-10-01 17:18:58', '2019-10-01 17:18:58', 1),
(12, 5, 1, 1, 'Fi2', 'Catégorie Fi2', NULL, '2019-10-01 17:19:20', '2019-10-01 17:19:20', 1),
(13, 5, 1, 1, 'Fi3', 'Catégorie Fi3', NULL, '2019-10-01 17:19:42', '2019-10-01 17:19:42', 1),
(14, 6, 1, 1, 'Fc1', 'Catégorie Fc1', NULL, '2019-10-01 17:21:20', '2019-10-01 17:21:20', 1),
(15, 6, 1, 1, 'Fc2', 'Catégorie Fc2', NULL, '2019-10-01 17:21:35', '2019-10-01 17:21:35', 1),
(16, 6, 1, 1, 'Fc3', 'Catégorie Fc3', NULL, '2019-10-01 17:21:55', '2019-10-01 17:21:55', 1),
(17, 7, 1, 1, 'Fsic1', 'Catégorie Fsic1', NULL, '2019-10-01 17:22:27', '2019-10-01 17:22:27', 1),
(18, 7, 1, 1, 'Fsic2', 'Catégorie Fsic2', NULL, '2019-10-01 17:22:45', '2019-10-01 17:22:45', 1),
(19, 7, 1, 1, 'Fsic3', 'Catégorie Fsic3', NULL, '2019-10-01 17:23:20', '2019-10-01 17:23:20', 1),
(20, 2, 1, 1, 'Eu1', 'Catégorie Eu1', NULL, '2019-10-01 17:24:07', '2019-10-01 17:24:07', 1),
(21, 2, 1, 1, 'Eu2', 'Catégorie Eu2', NULL, '2019-10-01 17:24:21', '2019-10-01 17:24:21', 1),
(22, 2, 1, 1, 'Eu3', 'Catégorie Eu3', NULL, '2019-10-01 17:24:48', '2019-10-01 17:24:48', 1),
(23, NULL, 2, 1, 'Ac', 'Groupe Ac', 'conception de réseaux d’assainissement collectif, semi collectif, de station de traitement et d’épuration des eaux usées, et/ou contrôle de l’exécution des travaux concernés', '2019-10-03 10:05:19', '2019-10-03 10:05:19', 1),
(24, NULL, 2, 1, 'Ap', 'Groupe Ap', 'conception et/ou contrôle de l’exécution des travaux de réalisation d’infrastructures d’assainissement autonome et la promotion des pratiques adéquates d\'hygiène et d\'assainissement', '2019-10-03 10:09:55', '2019-10-03 10:09:55', 1),
(25, NULL, 3, 1, 'EA', 'Catégorie EA', 'Pour des études ou contrôle de travaux dont le coût est inférieur à vingt millions (20.000.000) de francs CFA, disposer d\'un minimum de 	personnel et de matériel conformément à la colonne EA du tableau de l\'article 20', '2019-10-03 10:18:46', '2019-10-03 10:18:46', 1),
(26, NULL, 3, 1, 'EB', 'Catégorie EB', 'Pour des études ou contrôle de travaux dont le coût est supérieur ou 	égal à vingt millions (20.000.000) de francs CFA et inférieur à cent millions (100 000 000) 	de francs CFA, disposer d\'un minimum de personnel et de matériel conformément à la 	colonne EB du tableau de l\'article 20', '2019-10-03 10:19:18', '2019-10-03 10:19:18', 1),
(27, NULL, 3, 1, 'EC', 'Catégorie EC', 'Pour des études ou contrôle de travaux dont le coût est supérieur ou 	égal à cent millions 	(100.000.000) de francs CFA et inférieur cinq cent 	millions (500.000.000) de francs CFA, disposer d\'un minimum de personnel et de matériel conformément à la colonne EC du tableau de l\'article 20', '2019-10-03 10:19:44', '2019-10-03 10:19:44', 1),
(28, NULL, 3, 1, 'ED', 'Catégorie ED', 'Pour des études ou un contrôle de travaux dont le coût est supérieur ou 	égal à cinq cent millions 	(500.000.000) de francs CFA et inférieur à un 	milliard (1.000.000.000) de francs CFA, disposer d\'un minimum de 	personnel et de matériel conformément à la colonne ED du tableau de 	l\'article 20', '2019-10-03 10:20:14', '2019-10-03 10:20:14', 1),
(29, NULL, 3, 1, 'EE', 'Catégorie EE', 'Pour des études ou un contrôle de grands travaux dont le coût est 	supérieur ou égal à un milliard 	(1.000.000.000) de francs CFA, disposer 	d\'un minimum de personnel et de matériel conformément à la colonne EE du tableau de l\'article 20', '2019-10-03 10:20:45', '2019-10-03 10:20:45', 1),
(30, NULL, 3, 2, 'TA', 'Catégorie TA', 'Pour des travaux dont Ie coût est inferieur a vingt millions (20.000.000) de francs CFA, disposer d\'un minimum de personnel et de matériel conformément à la colonne TA du tableau de l\'article 20', '2019-10-03 10:22:55', '2019-10-03 10:22:55', 1),
(31, NULL, 3, 2, 'TB', 'Catégorie TB', 'Pour des travaux dont le coût est supérieur ou égal à vingt millions (20.000.000) de francs CFA et 	inférieur à cent millions (100 000 000) de francs CFA, disposer d\'un minimum de personnel et de matériel conformément à la colonne TB du tableau de l\'article 20', '2019-10-03 10:23:22', '2019-10-03 10:23:22', 1),
(32, NULL, 3, 2, 'TC', 'Catégorie TC', 'Pour des travaux dont le coût est supérieur ou égal à cent millions (100.000.000) de francs CFA et inférieur cinq cent millions (500.000.000) de francs CFA, disposer d\'une (01) brigade conformément à la colonne TC du tableau de l\'article 20', '2019-10-03 10:23:50', '2019-10-03 10:23:50', 1),
(33, NULL, 3, 2, 'TD', 'Catégorie TD', 'Pour des travaux dont le coût est supérieur ou égal à cinq cent millions (500.000.000) de francs CFA et inférieur à un milliard (1.000.000.000) de francs CFA, disposer d\'une (01) brigade conformément à la colonne TC du tableau de l\'article 20', '2019-10-03 10:24:27', '2019-10-03 10:24:27', 1),
(34, NULL, 3, 2, 'TE', 'Catégorie TE', 'Pour les grands travaux dont le coût est supérieur ou égal à un milliard (1.000.000.000) de francs CFA, disposer d\'une (01) brigade conformément à la colonne TE du tableau de l\'article 20', '2019-10-03 10:24:49', '2019-10-03 10:24:49', 1),
(35, NULL, 2, 2, 'R', 'Catégorie R', 'les entreprises ou sociétés de réalisation des travaux de réseaux d\'assainissement collectifs ou semi collectifs (pose des conduites, raccordement aux réseaux, construction de station de traitement des eaux usées et de stations d’épuration)', '2019-10-03 10:28:58', '2019-10-03 10:28:58', 0),
(36, NULL, 2, 2, 'Lp', 'Catégorie Lp', 'les entreprises ou sociétés de réalisation ou de réhabilitation des ouvrages d’assainissement familiaux et/ou communautaires', '2019-10-03 10:29:35', '2019-10-03 10:29:35', 1),
(37, 35, 2, 2, 'R1', 'Catégorie R1', 'Entreprises de raccordement au réseau, de fourniture et pose de canalisation des réseaux tertiaires et des pièces spéciales sur lesdits réseaux', '2019-10-03 10:30:25', '2019-10-03 10:30:25', 1),
(38, 35, 2, 2, 'R2', 'Catégorie R2', 'Entreprises de fourniture et pose de canalisations des réseaux secondaires et primaires et des pièces spéciales sur lesdits réseaux', '2019-10-03 10:30:56', '2019-10-03 10:30:56', 1),
(39, 35, 2, 2, 'R3', 'Catégorie R3', 'Entreprises de construction de réseaux d’égout, de stations de traitement et d’épuration des eaux usées', '2019-10-03 10:31:35', '2019-10-03 10:31:35', 1),
(40, NULL, 1, 2, 'U', 'Groupe U', 'Groupe des entreprises ou sociétés d’adduction d’eau potable qui font : le branchement privé, la pose des canalisations des réseaux primaires, secondaires et tertiaires, la construction de stations de pompage, de stations de traitement d’eau potable et la construction de réservoirs d’eau. (cf articles 30 et 31)', '2019-10-03 10:59:22', '2019-10-03 10:59:22', 0),
(41, NULL, 1, 2, 'P', 'Groupe P', 'Groupe des entreprises ou sociétés de construction de puits modernes qui font : le fonçage, construction du puits, le développement, l’essai de pompage, les aménagements autour des puits. (cf  articles 28 et 29)', '2019-10-03 10:59:48', '2019-10-03 10:59:48', 0),
(42, 41, 1, 2, 'P1', 'Catégorie P1', 'Une (01) brigade de puits', '2019-10-03 11:02:46', '2019-10-03 11:02:46', 1),
(43, 41, 1, 2, 'P2', 'Catégorie P2', 'Deux (02) brigades de puits', '2019-10-03 11:03:20', '2019-10-03 11:03:20', 1),
(44, 40, 1, 2, 'U1', 'Catégorie U1', 'Estimation du plafond (F .CFA) <75.000.000', '2019-10-03 11:05:57', '2019-10-03 11:05:57', 1),
(45, 40, 1, 2, 'U2', 'Catégorie U2', 'Estimation du plafond (F .CFA) <250.000.000', '2019-10-03 11:06:40', '2019-10-03 11:06:40', 1),
(46, 40, 1, 2, 'U3', 'Catégorie U3', '>250.000.000', '2019-10-03 11:07:27', '2019-10-03 11:07:27', 1);

-- --------------------------------------------------------

--
-- Structure de la table `Communes`
--

CREATE TABLE `Communes` (
  `id` int(11) NOT NULL,
  `Pro_id` int(11) DEFAULT NULL,
  `code_commune` varchar(254) DEFAULT NULL,
  `libelle_commune` varchar(254) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Communes`
--

INSERT INTO `Communes` (`id`, `Pro_id`, `code_commune`, `libelle_commune`, `created_at`, `updated_at`) VALUES
(1, 1, 'BAGASSI', 'BAGASSI', NULL, NULL),
(2, 1, 'BANA', 'BANA', NULL, NULL),
(3, 1, 'BOROMO', 'BOROMO', NULL, NULL),
(4, 1, 'FARA', 'FARA', NULL, NULL),
(5, 1, 'OURY', 'OURY', NULL, NULL),
(6, 1, 'PA', 'PA', NULL, NULL),
(7, 1, 'POMPOI', 'POMPOI', NULL, NULL),
(8, 1, 'POURA', 'POURA', NULL, NULL),
(9, 1, 'SIBY', 'SIBY', NULL, NULL),
(10, 1, 'YAHO', 'YAHO', NULL, NULL),
(11, 3, 'BALAVE', 'BALAVE', NULL, NULL),
(12, 3, 'KOUKA', 'KOUKA', NULL, NULL),
(13, 3, 'SAMI', 'SAMI', NULL, NULL),
(14, 3, 'SANABA', 'SANABA', NULL, NULL),
(15, 3, 'SOLENZO', 'SOLENZO', NULL, NULL),
(16, 3, 'TANSILA', 'TANSILA', NULL, NULL),
(17, 18, 'BARANI', 'BARANI', NULL, NULL),
(18, 18, 'BOMBOROKUY', 'BOMBOROKUY', NULL, NULL),
(19, 18, 'BOURASSO', 'BOURASSO', NULL, NULL),
(20, 18, 'DJIBASSO', 'DJIBASSO', NULL, NULL),
(21, 18, 'DOKUY', 'DOKUY', NULL, NULL),
(22, 18, 'DOUMBALA', 'DOUMBALA', NULL, NULL),
(23, 18, 'KOMBORI', 'KOMBORI', NULL, NULL),
(24, 18, 'MADOUBA', 'MADOUBA', NULL, NULL),
(25, 18, 'NOUNA', 'NOUNA', NULL, NULL),
(26, 18, 'SONO', 'SONO', NULL, NULL),
(27, 24, 'BONDOUKUY', 'BONDOUKUY', NULL, NULL),
(28, 24, 'DEDOUGOU', 'DEDOUGOU', NULL, NULL),
(29, 24, 'DOUROULA', 'DOUROULA', NULL, NULL),
(30, 24, 'KONA', 'KONA', NULL, NULL),
(31, 24, 'OUARKOYE', 'OUARKOYE', NULL, NULL),
(32, 24, 'SAFANE', 'SAFANE', NULL, NULL),
(33, 24, 'TCHERIBA', 'TCHERIBA', NULL, NULL),
(34, 27, 'GASSAN', 'GASSAN', NULL, NULL),
(35, 27, 'GOSSINA', 'GOSSINA', NULL, NULL),
(36, 27, 'KOUGNY', 'KOUGNY', NULL, NULL),
(37, 27, 'TOMA', 'TOMA', NULL, NULL),
(38, 27, 'YABA', 'YABA', NULL, NULL),
(39, 27, 'YE', 'YE', NULL, NULL),
(40, 38, 'DI', 'DI', NULL, NULL),
(41, 38, 'GOMBORO', 'GOMBORO', NULL, NULL),
(42, 38, 'KASSOUM', 'KASSOUM', NULL, NULL),
(43, 38, 'KIEMBARA', 'KIEMBARA', NULL, NULL),
(44, 38, 'LANFIERA', 'LANFIERA', NULL, NULL),
(45, 38, 'LANKOUE', 'LANKOUE', NULL, NULL),
(46, 38, 'TOENI', 'TOENI', NULL, NULL),
(47, 38, 'TOUGAN', 'TOUGAN', NULL, NULL),
(48, 8, 'BANFORA', 'BANFORA', NULL, NULL),
(49, 8, 'BEREGADOUGOU', 'BEREGADOUGOU', NULL, NULL),
(50, 8, 'MANGODARA', 'MANGODARA', NULL, NULL),
(51, 8, 'MOUSSODOUGOU', 'MOUSSODOUGOU', NULL, NULL),
(52, 8, 'NIANGOLOKO', 'NIANGOLOKO', NULL, NULL),
(53, 8, 'OUO', 'OUO', NULL, NULL),
(54, 8, 'SIDERADOUGOU', 'SIDERADOUGOU', NULL, NULL),
(55, 8, 'SOUBAKANIEDOUGOU', 'SOUBAKANIEDOUGOU', NULL, NULL),
(56, 8, 'TIEFORA', 'TIEFORA', NULL, NULL),
(57, 22, 'DAKORO', 'DAKORO', NULL, NULL),
(58, 22, 'DOUNA', 'DOUNA', NULL, NULL),
(59, 22, 'KANKALABA', 'KANKALABA', NULL, NULL),
(60, 22, 'LOUMANA', 'LOUMANA', NULL, NULL),
(61, 22, 'NIANKORODOUGOU', 'NIANKORODOUGOU', NULL, NULL),
(62, 22, 'OUELENI', 'OUELENI', NULL, NULL),
(63, 22, 'SINDOU', 'SINDOU', NULL, NULL),
(64, 22, 'WOLOKONTO', 'WOLOKONTO', NULL, NULL),
(65, 14, 'KOMKI-IPALA', 'KOMKI-IPALA', NULL, NULL),
(66, 14, 'KOMSILGA', 'KOMSILGA', NULL, NULL),
(67, 14, 'KOUBRI', 'KOUBRI', NULL, NULL),
(69, 14, 'PABRE', 'PABRE', NULL, NULL),
(70, 14, 'SAABA', 'SAABA', NULL, NULL),
(71, 14, 'TANGHIN-DASSOURI', 'TANGHIN-DASSOURI', NULL, NULL),
(72, 6, 'BAGRE', 'BAGRE', NULL, NULL),
(73, 6, 'BANE', 'BANE', NULL, NULL),
(74, 6, 'BEGUEDO', 'BEGUEDO', NULL, NULL),
(75, 6, 'BISSIGA', 'BISSIGA', NULL, NULL),
(76, 6, 'BITTOU', 'BITTOU', NULL, NULL),
(77, 6, 'BOUSSOUMA', 'BOUSSOUMA', NULL, NULL),
(78, 6, 'GARANGO', 'GARANGO', NULL, NULL),
(79, 6, 'KOMTOEGA', 'KOMTOEGA', NULL, NULL),
(80, 6, 'NIAOGHO', 'NIAOGHO', NULL, NULL),
(81, 6, 'TENKODOGO', 'TENKODOGO', NULL, NULL),
(82, 6, 'ZABRE', 'ZABRE', NULL, NULL),
(83, 6, 'ZOAGA', 'ZOAGA', NULL, NULL),
(84, 6, 'ZONSE', 'ZONSE', NULL, NULL),
(85, 19, 'COMIN-YANGA', 'COMIN-YANGA', NULL, NULL),
(86, 19, 'DOURTENGA', 'DOURTENGA', NULL, NULL),
(87, 19, 'LALGAYE', 'LALGAYE', NULL, NULL),
(88, 19, 'OUARGAYE', 'OUARGAYE', NULL, NULL),
(89, 19, 'SANGHA', 'SANGHA', NULL, NULL),
(90, 19, 'SOUDOUGUI', 'SOUDOUGUI', NULL, NULL),
(91, 19, 'YARGATENGA', 'YARGATENGA', NULL, NULL),
(92, 19, 'YONDE', 'YONDE', NULL, NULL),
(93, 20, 'ANDEMTENGA', 'ANDEMTENGA', NULL, NULL),
(94, 20, 'BASKOURE', 'BASKOURE', NULL, NULL),
(95, 20, 'DIALGAYE', 'DIALGAYE', NULL, NULL),
(96, 20, 'GOUNGHIN', 'GOUNGHIN', NULL, NULL),
(97, 20, 'KANDO', 'KANDO', NULL, NULL),
(98, 20, 'KOUPELA', 'KOUPELA', NULL, NULL),
(99, 20, 'POUYTENGA', 'POUYTENGA', NULL, NULL),
(100, 20, 'TENSOBENTENGA', 'TENSOBENTENGA', NULL, NULL),
(101, 20, 'YARGO', 'YARGO', NULL, NULL),
(102, 2, 'BOURZANGA', 'BOURZANGA', NULL, NULL),
(103, 2, 'GUIBARE', 'GUIBARE', NULL, NULL),
(104, 2, 'KONGOUSSI', 'KONGOUSSI', NULL, NULL),
(105, 2, 'NASSERE', 'NASSERE', NULL, NULL),
(106, 2, 'ROLLO', 'ROLLO', NULL, NULL),
(107, 2, 'ROUKO', 'ROUKO', NULL, NULL),
(108, 2, 'SABCE', 'SABCE', NULL, NULL),
(109, 2, 'TIKARE', 'TIKARE', NULL, NULL),
(110, 2, 'ZIMTANGA', 'ZIMTANGA', NULL, NULL),
(111, 26, 'BOALA', 'BOALA', NULL, NULL),
(112, 26, 'BOULSA', 'BOULSA', NULL, NULL),
(113, 26, 'BOUROUM', 'BOUROUM', NULL, NULL),
(114, 26, 'DARGO', 'DARGO', NULL, NULL),
(115, 26, 'NAGBINGOU', 'NAGBINGOU', NULL, NULL),
(116, 26, 'TOUGOURI', 'TOUGOURI', NULL, NULL),
(117, 26, 'YALGO', 'YALGO', NULL, NULL),
(118, 26, 'ZEGUEDEGUIN', 'ZEGUEDEGUIN', NULL, NULL),
(119, 34, 'BARSALOGHO', 'BARSALOGHO', NULL, NULL),
(120, 34, 'BOUSSOUMA', 'BOUSSOUMA', NULL, NULL),
(121, 34, 'DABLO', 'DABLO', NULL, NULL),
(122, 34, 'KAYA', 'KAYA', NULL, NULL),
(123, 34, 'KORSIMORO', 'KORSIMORO', NULL, NULL),
(124, 34, 'MANE', 'MANE', NULL, NULL),
(125, 34, 'NAMISSIGUIMA', 'NAMISSIGUIMA', NULL, NULL),
(126, 34, 'PENSA', 'PENSA', NULL, NULL),
(127, 34, 'PIBAORE', 'PIBAORE', NULL, NULL),
(128, 34, 'PISSILA', 'PISSILA', NULL, NULL),
(129, 34, 'ZIGA', 'ZIGA', NULL, NULL),
(130, 7, 'BINGO', 'BINGO', NULL, NULL),
(131, 7, 'IMASGO', 'IMASGO', NULL, NULL),
(132, 7, 'KINDI', 'KINDI', NULL, NULL),
(133, 7, 'KOKOLOGHO', 'KOKOLOGHO', NULL, NULL),
(134, 7, 'KOUDOUGOU', 'KOUDOUGOU', NULL, NULL),
(135, 7, 'NANDIALA', 'NANDIALA', NULL, NULL),
(136, 7, 'NANORO', 'NANORO', NULL, NULL),
(137, 7, 'PELLA', 'PELLA', NULL, NULL),
(138, 7, 'POA', 'POA', NULL, NULL),
(139, 7, 'RAMONGO', 'RAMONGO', NULL, NULL),
(140, 7, 'SABOU', 'SABOU', NULL, NULL),
(141, 7, 'SIGLE', 'SIGLE', NULL, NULL),
(142, 7, 'SOAW', 'SOAW', NULL, NULL),
(143, 7, 'SOURGOU', 'SOURGOU', NULL, NULL),
(144, 7, 'THYOU', 'THYOU', NULL, NULL),
(145, 33, 'DASSA', 'DASSA', NULL, NULL),
(146, 33, 'DIDYR', 'DIDYR', NULL, NULL),
(147, 33, 'GODYR', 'GODYR', NULL, NULL),
(148, 33, 'KORDIE', 'KORDIE', NULL, NULL),
(149, 33, 'KYON', 'KYON', NULL, NULL),
(150, 33, 'POUNI', 'POUNI', NULL, NULL),
(151, 33, 'REO', 'REO', NULL, NULL),
(152, 33, 'TENADO', 'TENADO', NULL, NULL),
(153, 33, 'ZAMO', 'ZAMO', NULL, NULL),
(154, 33, 'ZAWARA', 'ZAWARA', NULL, NULL),
(155, 36, 'BIEHA', 'BIEHA', NULL, NULL),
(156, 36, 'BOURA', 'BOURA', NULL, NULL),
(157, 36, 'LEO', 'LEO', NULL, NULL),
(158, 36, 'NEBIELIANAYOU', 'NEBIELIANAYOU', NULL, NULL),
(159, 36, 'NIABOURI', 'NIABOURI', NULL, NULL),
(160, 36, 'SILLY', 'SILLY', NULL, NULL),
(161, 36, 'TO', 'TO', NULL, NULL),
(162, 43, 'BAKATA', 'BAKATA', NULL, NULL),
(163, 43, 'BOUGNOUNOU', 'BOUGNOUNOU', NULL, NULL),
(164, 43, 'CASSOU', 'CASSOU', NULL, NULL),
(165, 43, 'DALO', 'DALO', NULL, NULL),
(166, 43, 'GAO', 'GAO', NULL, NULL),
(167, 43, 'SAPOUY', 'SAPOUY', NULL, NULL),
(168, 4, 'DOULOUGOU', 'DOULOUGOU', NULL, NULL),
(169, 4, 'GAONGO', 'GAONGO', NULL, NULL),
(170, 4, 'IPELCE', 'IPELCE', NULL, NULL),
(171, 4, 'KAYAO', 'KAYAO', NULL, NULL),
(172, 4, 'KOMBISSIRI', 'KOMBISSIRI', NULL, NULL),
(173, 4, 'SAPONE', 'SAPONE', NULL, NULL),
(174, 4, 'TOECE', 'TOECE', NULL, NULL),
(175, 25, 'GUIARO', 'GUIARO', NULL, NULL),
(176, 25, 'PO', 'PO', NULL, NULL),
(177, 25, 'TIEBELE', 'TIEBELE', NULL, NULL),
(178, 25, 'ZECCO', 'ZECCO', NULL, NULL),
(179, 25, 'ZIOU', 'ZIOU', NULL, NULL),
(180, 45, 'BERE', 'BERE', NULL, NULL),
(181, 45, 'BINDE', 'BINDE', NULL, NULL),
(182, 45, 'GOGO', 'GOGO', NULL, NULL),
(183, 45, 'GONBOUSSOUGOU', 'GONBOUSSOUGOU', NULL, NULL),
(184, 45, 'GUIBA', 'GUIBA', NULL, NULL),
(185, 45, 'MANGA', 'MANGA', NULL, NULL),
(186, 45, 'NOBERE', 'NOBERE', NULL, NULL),
(187, 10, 'BILANGA', 'BILANGA', NULL, NULL),
(188, 10, 'BOGANDE', 'BOGANDE', NULL, NULL),
(189, 10, 'COALLA', 'COALLA', NULL, NULL),
(190, 10, 'LIPTOUGOU', 'LIPTOUGOU', NULL, NULL),
(191, 10, 'MANNI', 'MANNI', NULL, NULL),
(192, 10, 'PIELA', 'PIELA', NULL, NULL),
(193, 10, 'THION', 'THION', NULL, NULL),
(194, 11, 'DIABO', 'DIABO', NULL, NULL),
(195, 11, 'DIAPANGOU', 'DIAPANGOU', NULL, NULL),
(196, 11, 'FADA N\'GOURMA', 'FADA N\'GOURMA', NULL, NULL),
(197, 11, 'MATIACOALI', 'MATIACOALI', NULL, NULL),
(198, 11, 'TIBGA', 'TIBGA', NULL, NULL),
(199, 11, 'YAMBA', 'YAMBA', NULL, NULL),
(200, 16, 'BARTHIEBOUGOU', 'BARTHIEBOUGOU', NULL, NULL),
(201, 16, 'FOUTOURI', 'FOUTOURI', NULL, NULL),
(202, 16, 'GAYERI', 'GAYERI', NULL, NULL),
(203, 17, 'KOMPIENGA', 'KOMPIENGA', NULL, NULL),
(204, 17, 'MADJOARI', 'MADJOARI', NULL, NULL),
(205, 17, 'PAMA', 'PAMA', NULL, NULL),
(206, 39, 'BOTOU', 'BOTOU', NULL, NULL),
(207, 39, 'DIAPAGA', 'DIAPAGA', NULL, NULL),
(208, 39, 'KANTCHARI', 'KANTCHARI', NULL, NULL),
(209, 39, 'LOGOBOU', 'LOGOBOU', NULL, NULL),
(210, 39, 'NAMOUNOU', 'NAMOUNOU', NULL, NULL),
(211, 39, 'PARTIAGA', 'PARTIAGA', NULL, NULL),
(212, 39, 'TAMBAGA', 'TAMBAGA', NULL, NULL),
(213, 39, 'TANSARGA', 'TANSARGA', NULL, NULL),
(214, 12, 'BAMA', 'BAMA', NULL, NULL),
(216, 12, 'DANDE', 'DANDE', NULL, NULL),
(217, 12, 'FARAMANA', 'FARAMANA', NULL, NULL),
(218, 12, 'FO', 'FO', NULL, NULL),
(219, 12, 'KARANGASSO SAMBLA', 'KARANGASSO SAMBLA', NULL, NULL),
(220, 12, 'KARANGASSO-VIGUE', 'KARANGASSO-VIGUE', NULL, NULL),
(221, 12, 'KOUNDOUGOU', 'KOUNDOUGOU', NULL, NULL),
(222, 12, 'LENA', 'LENA', NULL, NULL),
(223, 12, 'PADEMA', 'PADEMA', NULL, NULL),
(224, 12, 'PENI', 'PENI', NULL, NULL),
(225, 12, 'SATIRI', 'SATIRI', NULL, NULL),
(226, 12, 'TOUSSIANA', 'TOUSSIANA', NULL, NULL),
(227, 15, 'BANZON', 'BANZON', NULL, NULL),
(228, 15, 'DJIGOUERA', 'DJIGOUERA', NULL, NULL),
(229, 15, 'KANGALA', 'KANGALA', NULL, NULL),
(230, 15, 'KAYAN', 'KAYAN', NULL, NULL),
(231, 15, 'KOLOKO', 'KOLOKO', NULL, NULL),
(232, 15, 'KOURINION', 'KOURINION', NULL, NULL),
(233, 15, 'KOUROUMA', 'KOUROUMA', NULL, NULL),
(234, 15, 'MOROLABA', 'MOROLABA', NULL, NULL),
(235, 15, 'N\'DOROLA', 'N\'DOROLA', NULL, NULL),
(236, 15, 'ORODARA', 'ORODARA', NULL, NULL),
(237, 15, 'SAMOGOHIRI', 'SAMOGOHIRI', NULL, NULL),
(238, 15, 'SAMOROGOUAN', 'SAMOROGOUAN', NULL, NULL),
(239, 15, 'SINDO', 'SINDO', NULL, NULL),
(240, 40, 'BEKUY', 'BEKUY', NULL, NULL),
(241, 40, 'BEREBA', 'BEREBA', NULL, NULL),
(242, 40, 'BONI', 'BONI', NULL, NULL),
(243, 40, 'FOUNZAN', 'FOUNZAN', NULL, NULL),
(244, 40, 'HOUNDE', 'HOUNDE', NULL, NULL),
(245, 40, 'KOTI', 'KOTI', NULL, NULL),
(246, 40, 'KOUMBIA', 'KOUMBIA', NULL, NULL),
(247, 23, 'BANH', 'BANH', NULL, NULL),
(248, 23, 'OUINDIGUI', 'OUINDIGUI', NULL, NULL),
(249, 23, 'SOLLE', 'SOLLE', NULL, NULL),
(250, 23, 'TITAO', 'TITAO', NULL, NULL),
(251, 31, 'ARBOLLE', 'ARBOLLE', NULL, NULL),
(252, 31, 'BAGARE', 'BAGARE', NULL, NULL),
(253, 31, 'BOKIN', 'BOKIN', NULL, NULL),
(254, 31, 'GOMPONSOM', 'GOMPONSOM', NULL, NULL),
(255, 31, 'KIRSI', 'KIRSI', NULL, NULL),
(256, 31, 'LA-TODIN', 'LA-TODIN', NULL, NULL),
(257, 31, 'PILIMPIKOU', 'PILIMPIKOU', NULL, NULL),
(258, 31, 'SAMBA', 'SAMBA', NULL, NULL),
(259, 31, 'YAKO', 'YAKO', NULL, NULL),
(260, 42, 'BARGA', 'BARGA', NULL, NULL),
(261, 42, 'KAIN', 'KAIN', NULL, NULL),
(262, 42, 'KALSAKA', 'KALSAKA', NULL, NULL),
(263, 42, 'KOSSOUKA', 'KOSSOUKA', NULL, NULL),
(264, 42, 'KOUMBRI', 'KOUMBRI', NULL, NULL),
(265, 42, 'NAMISSIGUIMA', 'NAMISSIGUIMA', NULL, NULL),
(266, 42, 'OUAHIGOUYA', 'OUAHIGOUYA', NULL, NULL),
(267, 42, 'OULA', 'OULA', NULL, NULL),
(268, 42, 'RAMBO', 'RAMBO', NULL, NULL),
(269, 42, 'SEGUENEGA', 'SEGUENEGA', NULL, NULL),
(270, 42, 'TANGAYE', 'TANGAYE', NULL, NULL),
(271, 42, 'THIOU', 'THIOU', NULL, NULL),
(272, 42, 'ZOGORE', 'ZOGORE', NULL, NULL),
(273, 44, 'BASSI', 'BASSI', NULL, NULL),
(274, 44, 'BOUSSOU', 'BOUSSOU', NULL, NULL),
(275, 44, 'GOURCY', 'GOURCY', NULL, NULL),
(276, 44, 'LEBA', 'LEBA', NULL, NULL),
(277, 44, 'TOUGO', 'TOUGO', NULL, NULL),
(278, 9, 'BOUDRY', 'BOUDRY', NULL, NULL),
(279, 9, 'KOGHO', 'KOGHO', NULL, NULL),
(280, 9, 'MEGUET', 'MEGUET', NULL, NULL),
(281, 9, 'MOGTEDO', 'MOGTEDO', NULL, NULL),
(282, 9, 'SALOGO', 'SALOGO', NULL, NULL),
(283, 9, 'ZAM', 'ZAM', NULL, NULL),
(284, 9, 'ZORGHO', 'ZORGHO', NULL, NULL),
(285, 9, 'ZOUNGOU', 'ZOUNGOU', NULL, NULL),
(286, 21, 'BOUSSE', 'BOUSSE', NULL, NULL),
(287, 21, 'LAYE', 'LAYE', NULL, NULL),
(288, 21, 'NIOU', 'NIOU', NULL, NULL),
(289, 21, 'SOURGOUBILA', 'SOURGOUBILA', NULL, NULL),
(290, 21, 'TOEGHIN', 'TOEGHIN', NULL, NULL),
(291, 29, 'ABSOUYA', 'ABSOUYA', NULL, NULL),
(292, 29, 'DAPELOGO', 'DAPELOGO', NULL, NULL),
(293, 29, 'LOUMBILA', 'LOUMBILA', NULL, NULL),
(294, 29, 'NAGREONGO', 'NAGREONGO', NULL, NULL),
(295, 29, 'OURGOU-MANEGA', 'OURGOU-MANEGA', NULL, NULL),
(296, 29, 'ZINIARE', 'ZINIARE', NULL, NULL),
(297, 29, 'ZITENGA', 'ZITENGA', NULL, NULL),
(298, 30, 'DEOU', 'DEOU', NULL, NULL),
(299, 30, 'GOROM-GOROM', 'GOROM-GOROM', NULL, NULL),
(300, 30, 'MARKOYE', 'MARKOYE', NULL, NULL),
(301, 30, 'OURSI', 'OURSI', NULL, NULL),
(302, 30, 'TIN-AKOFF', 'TIN-AKOFF', NULL, NULL),
(303, 35, 'BANI', 'BANI', NULL, NULL),
(304, 35, 'DORI', 'DORI', NULL, NULL),
(305, 35, 'FALAGOUNTOU', 'FALAGOUNTOU', NULL, NULL),
(306, 35, 'GORGADJI', 'GORGADJI', NULL, NULL),
(307, 35, 'SAMPELGA', 'SAMPELGA', NULL, NULL),
(308, 35, 'SEYTENGA', 'SEYTENGA', NULL, NULL),
(309, 37, 'ARBINDA', 'ARBINDA', NULL, NULL),
(310, 37, 'BARABOULE', 'BARABOULE', NULL, NULL),
(311, 37, 'DIGUEL', 'DIGUEL', NULL, NULL),
(312, 37, 'DJIBO', 'DJIBO', NULL, NULL),
(313, 37, 'KELBO', 'KELBO', NULL, NULL),
(314, 37, 'KOUTOUGOU', 'KOUTOUGOU', NULL, NULL),
(315, 37, 'NASSOUMBOU', 'NASSOUMBOU', NULL, NULL),
(316, 37, 'POBE-MENGAO', 'POBE-MENGAO', NULL, NULL),
(317, 37, 'TONGOMAYEL', 'TONGOMAYEL', NULL, NULL),
(318, 41, 'BOUNDORE', 'BOUNDORE', NULL, NULL),
(319, 41, 'MANSILA', 'MANSILA', NULL, NULL),
(320, 41, 'SEBBA', 'SEBBA', NULL, NULL),
(321, 41, 'SOLHAN', 'SOLHAN', NULL, NULL),
(322, 41, 'TANKOUGOUNADIE', 'TANKOUGOUNADIE', NULL, NULL),
(323, 41, 'TITABE', 'TITABE', NULL, NULL),
(324, 5, 'BONDIGUI', 'BONDIGUI', NULL, NULL),
(325, 5, 'DIEBOUGOU', 'DIEBOUGOU', NULL, NULL),
(326, 5, 'DOLO', 'DOLO', NULL, NULL),
(327, 5, 'IOLONIORO', 'IOLONIORO', NULL, NULL),
(328, 5, 'TIANKOURA', 'TIANKOURA', NULL, NULL),
(329, 13, 'DANO', 'DANO', NULL, NULL),
(330, 13, 'DISSIN', 'DISSIN', NULL, NULL),
(331, 13, 'GUEGUERE', 'GUEGUERE', NULL, NULL),
(332, 13, 'KOPER', 'KOPER', NULL, NULL),
(333, 13, 'NIEGO', 'NIEGO', NULL, NULL),
(334, 13, 'ORONKUA', 'ORONKUA', NULL, NULL),
(335, 13, 'OUESSA', 'OUESSA', NULL, NULL),
(336, 13, 'ZAMBO', 'ZAMBO', NULL, NULL),
(337, 28, 'BATIE', 'BATIE', NULL, NULL),
(338, 28, 'BOUSSOUKOULA', 'BOUSSOUKOULA', NULL, NULL),
(339, 28, 'KPUERE', 'KPUERE', NULL, NULL),
(340, 28, 'LEGMOIN', 'LEGMOIN', NULL, NULL),
(341, 28, 'MIDEBDO', 'MIDEBDO', NULL, NULL),
(342, 32, 'BOUROUM-BOUROUM', 'BOUROUM-BOUROUM', NULL, NULL),
(343, 32, 'BOUSSERA', 'BOUSSERA', NULL, NULL),
(344, 32, 'DJIGOUE', 'DJIGOUE', NULL, NULL),
(345, 32, 'GAOUA', 'GAOUA', NULL, NULL),
(346, 32, 'GBOMBLORA', 'GBOMBLORA', NULL, NULL),
(347, 32, 'KAMPTI', 'KAMPTI', NULL, NULL),
(348, 32, 'LOROPENI', 'LOROPENI', NULL, NULL),
(349, 32, 'MALBA', 'MALBA', NULL, NULL),
(350, 32, 'NAKO', 'NAKO', NULL, NULL),
(351, 32, 'PERIGBAN', 'PERIGBAN', NULL, NULL),
(352, 14, 'ARRONDISSEMENT 1', 'ARRONDISSEMENT 1', NULL, NULL),
(353, 14, 'ARRONDISSEMENT 10', 'ARRONDISSEMENT 10', NULL, NULL),
(354, 14, 'ARRONDISSEMENT 11', 'ARRONDISSEMENT 11', NULL, NULL),
(355, 14, 'ARRONDISSEMENT 12', 'ARRONDISSEMENT 12', NULL, NULL),
(356, 14, 'ARRONDISSEMENT 2', 'ARRONDISSEMENT 2', NULL, NULL),
(357, 14, 'ARRONDISSEMENT 3', 'ARRONDISSEMENT 3', NULL, NULL),
(358, 14, 'ARRONDISSEMENT 4', 'ARRONDISSEMENT 4', NULL, NULL),
(359, 14, 'ARRONDISSEMENT 5', 'ARRONDISSEMENT 5', NULL, NULL),
(360, 14, 'ARRONDISSEMENT 6', 'ARRONDISSEMENT 6', NULL, NULL),
(361, 14, 'ARRONDISSEMENT 7', 'ARRONDISSEMENT 7', NULL, NULL),
(362, 14, 'ARRONDISSEMENT 8', 'ARRONDISSEMENT 8', NULL, NULL),
(363, 14, 'ARRONDISSEMENT 9', 'ARRONDISSEMENT 9', NULL, NULL),
(364, 12, 'ARRONDISSEMENT 1', 'ARRONDISSEMENT 1', NULL, NULL),
(365, 12, 'ARRONDISSEMENT 2', 'ARRONDISSEMENT 2', NULL, NULL),
(366, 12, 'ARRONDISSEMENT 3', 'ARRONDISSEMENT 3', NULL, NULL),
(367, 12, 'ARRONDISSEMENT 4', 'ARRONDISSEMENT 4', NULL, NULL),
(368, 12, 'ARRONDISSEMENT 5', 'ARRONDISSEMENT 5', NULL, NULL),
(369, 12, 'ARRONDISSEMENT 6', 'ARRONDISSEMENT 6', NULL, NULL),
(370, 12, 'ARRONDISSEMENT 7', 'ARRONDISSEMENT 7', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `Domaines`
--

CREATE TABLE `Domaines` (
  `id` int(11) NOT NULL,
  `libelle_court` varchar(50) NOT NULL,
  `libelle_long` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `est_actif` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Domaines`
--

INSERT INTO `Domaines` (`id`, `libelle_court`, `libelle_long`, `created_at`, `updated_at`, `est_actif`) VALUES
(1, 'AEP', 'Eau Potable', '2018-10-03 12:30:17', '2018-10-03 12:30:17', 1),
(2, 'AEUE', 'Assainissement', '2018-10-03 12:30:30', '2018-10-03 12:30:30', 1),
(3, 'AH', 'Aménagement hydraulique', '2018-10-03 12:30:51', '2018-10-03 12:30:51', 1),
(4, 'GIRE', 'Gestion Intégrée des Ressources en Eau', '2019-01-10 12:03:22', '2019-01-10 12:03:22', 0),
(5, 'PPS', 'Pilotage et Soutien', '2019-01-10 12:04:23', '2019-01-10 12:04:49', 0);

-- --------------------------------------------------------

--
-- Structure de la table `Entreprises`
--

CREATE TABLE `Entreprises` (
  `id` int(11) NOT NULL,
  `Com_id` int(11) DEFAULT NULL,
  `Pro_id` int(11) NOT NULL,
  `Reg_id` int(11) DEFAULT NULL,
  `raison_sociale` varchar(254) DEFAULT NULL,
  `siege_social` varchar(254) DEFAULT NULL,
  `adresse` varchar(254) DEFAULT NULL,
  `telephone` varchar(254) DEFAULT NULL,
  `registre_commerce` varchar(254) DEFAULT NULL,
  `numero_ifu` varchar(254) DEFAULT NULL,
  `personne_responsable` varchar(254) DEFAULT NULL,
  `numero_cnss` varchar(254) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Entreprises`
--

INSERT INTO `Entreprises` (`id`, `Com_id`, `Pro_id`, `Reg_id`, `raison_sociale`, `siege_social`, `adresse`, `telephone`, `registre_commerce`, `numero_ifu`, `personne_responsable`, `numero_cnss`, `created_at`, `updated_at`) VALUES
(3, 5, 1, 5, 'A.Z Consult', 'Ouaga secteur 20, section NP, lot 01, parcelle 08', '07 BP 5136Ouaga 07 Tél: 70 04 90 04        78 80 82 19', '78 80 82 19', 'BF OUA 2014 A 2513', '00047151E', 'Moussa KINDA', '219 543 J', '2019-09-08 16:12:00', '2019-11-15 11:19:00'),
(4, 88, 2, 4, 'IGIP AFRIQUE Sarl', 'Secteur 8 Goughin, section PUH 83 CO, lot 327, parcelle B', '01 BP 4893 Ouaga 01  Tél:50 34 15 29       76 53 55 30', '50 34 15 29', 'BF OUA 2012 M596', '00002701G', 'Mathieu TANKOANO', '17 680 T', '2019-09-13 09:28:59', '2019-11-15 11:19:26'),
(5, 66, 14, 3, 'BKL Services Sarl', 'Secteur 16, section RK, lot 20, parcelle 07,rue 16-318', '01 BP 1985 Ouaga 01 Tél : 50 38 81 45               70 21 15 91', '+226232323', 'BF OUA 2011 M985', '00014221Y', 'Abdoul Aziz Hady BOLY', '72 672 S', '2019-09-13 09:33:10', '2020-04-05 09:38:59'),
(6, 4, 4, 3, 'DECLIQ', 'Ouaga Secteur 29, section KH, lot 06, parcelle 10', '05 BP 6009 Ouaga 05', '78 15 07 53', 'BF OUA 2014 A 0684', '00053214W', 'T. Yves Roland ZOUNGRANA', '227 369 R', '2019-09-13 09:34:48', '2019-11-15 11:20:27'),
(7, 58, 5, 2, 'AMERI TECH', 'Yagna, sect 2', 'Yagna', '+226 78 99 66', 'BF BOB 2561 B 0674', '00033701I', 'ZONGO Madi Too', '245 423 K', '2019-10-06 19:06:16', '2019-12-04 03:16:11'),
(14, 3, 6, 1, 'JonSon Equipement', 'Secteur 16, section RK, lot 20, parcelle 07,rue 16-318', NULL, '78 15 03 02', 'BF GAO 2019 G 11', '00038961L', 'Moustapha KONDE', '321 455 R', '2019-11-18 14:11:15', '2019-11-18 14:11:15'),
(15, 3, 7, 2, 'KTPL', 'Secteur 16, section RK, lot 20, parcelle 07,rue 16-318', '05 BP 6009 Ouaga 05 Tél: 70 08 11 07 / 78 15 07 53', '87 51 03 02', 'AF GAO 2015 G 11', '00428961L', 'ZI Too', '621 125 M', '2019-11-18 14:14:16', '2019-11-18 14:14:16'),
(16, 1, 8, 2, 'JonSon Equipement 02', 'Secteur 16, section RK, lot 20, parcelle 07,rue 16-318', '01 BP 4893 Ouaga 01  Tél:50 34 15 29       76 53 55 30', '78 15 03 02', 'BF OUA 2016 A 0684', '00014221M', 'ZI Madar', '621 352 M', '2019-11-18 14:27:01', '2019-11-18 14:27:01'),
(17, 95, 9, 4, 'EDI Inc', 'Matiakoali', 'hbjlkmlmù', '75858585', 'BF BOB 75B674', '00014111J', 'TOE Madi Sy', '126 125 O', '2019-11-18 14:58:23', '2020-04-05 09:10:08'),
(18, 352, 10, 3, 'BEG Sarl', 'Secteur 28, section. VQ, lot 14, parcelle.Oê', '09 BP 704 Ouaga 09', '70 78 12 84', 'BF OUA 2014 S 1525', '00054423J', 'PascaI OUIYA:', '228998 L', '2019-11-19 09:53:40', '2019-12-03 22:18:12'),
(19, 352, 11, 3, 'HYDRO-GEO SERVICES', 'Secteur 28  Ouagadougou, section 754, lot 25, parcelle 12', 'S/C 01 BP 54 Ouaga 01', '70 33 93 88', 'BF OUA 2013 M 2659', '00047296N', 'T Yacouba TRAORE', '219738 W', '2019-12-04 01:03:57', '2020-01-22 13:30:24'),
(20, 13, 12, 1, 'T-TECK Sarl', 'Ouaga', '05 BP 6119 Ouaga 05', '+226232321', 'BF OUA 2013 S 8456', '00084562K', 'OUALI Idriss', '124 366 R', '2019-12-04 09:07:23', '2019-12-04 09:07:23'),
(21, 352, 13, 3, 'ETK SARL', 'Ouaga, secteur 23', '03 BP 1003 Ouaga 0', '+22678553214', 'BF OUA 2014 P 6333', '00045632B', 'OUATTARA Isidore', '456 322 M', '2019-12-04 10:03:39', '2019-12-04 10:03:39');

-- --------------------------------------------------------

--
-- Structure de la table `faqs`
--

CREATE TABLE `faqs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `titre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contenu` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_create` date NOT NULL,
  `date_modif` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `faqs`
--

INSERT INTO `faqs` (`id`, `titre`, `contenu`, `date_create`, `date_modif`, `created_at`, `updated_at`) VALUES
(3, 'Depot des dossiers', 'IFU CNSS RCCM', '2020-04-09', '2020-04-09', '2020-04-06 00:00:00', '2020-04-06 00:00:00'),
(4, 'Retrait des agrements', 'Retrait des agrements Retrait des agrements Retrait des agrements\r\nRetrait des agrements', '2020-04-08', '2020-04-23', '2020-04-06 00:00:00', '2020-04-06 00:00:00'),
(5, 'fdghfdfghdfhf', 'hgdfghdfhf', '2020-04-04', '2020-04-16', NULL, NULL),
(6, 'Essai', 'OK', '0000-00-00', '0000-00-00', '2020-04-10 14:33:00', '2020-04-10 14:33:00');

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `objet` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `messages`
--

INSERT INTO `messages` (`id`, `name`, `prenom`, `objet`, `email`, `message`, `created_at`, `updated_at`) VALUES
(1, 'COULIBALY', 'Lessi Lassina', 'essai1', 'lessilassina@gmail.com', 'Message essai1', '2020-02-12 13:05:16', '2020-02-12 13:05:16'),
(2, 'COULIBALY', 'Lessi Lassina', 'essai1', 'lessilassina@gmail.com', 'Message essai1', '2020-02-12 13:05:20', '2020-02-12 13:05:20'),
(3, 'COULIBALY', 'Lessi Lassina', 'essai1', 'lessilassina@gmail.com', 'Message essai1', '2020-02-12 13:06:07', '2020-02-12 13:06:07'),
(4, 'COULIBALY', 'Lessi Lassina', 'essai1', 'lessilassina@gmail.com', 'dtucfvmhibjonkm,lùm;ù:*', '2020-02-12 14:06:06', '2020-02-12 14:06:06'),
(5, 'COULIBALY', 'Lessi Lassina', 'fdffdsfsdfdsdf', 'lessi.coulibaly@eau.gov.bf', 'fdhsfgjsftrsgdfsydfsff', '2020-02-12 16:44:57', '2020-02-12 16:44:57'),
(6, 'COULIBALY', 'Lessi Lassina', 'essai1', 'lessi.coulibaly@eau.gov.bf', 'fdqbgcbv', '2020-02-12 16:48:16', '2020-02-12 16:48:16'),
(7, 'COULIBALY', 'Lessi Lassina', 'essai1', 'lessi.coulibaly@eau.gov.bf', 'fdfdfdgfdgg', '2020-02-12 16:49:40', '2020-02-12 16:49:40'),
(8, 'COULIBALY', 'Lessi Lassina', 'dsqdqsf', 'lessilassina@gmail.com', 'sdqfdsghf', '2020-02-12 16:50:47', '2020-02-12 16:50:47'),
(9, 'Lessi Lassina', 'Lessi Lassina', 'fdffdsfsdfdsdf', 'lessilassina@gmail.com', 'dsgqgfdqgqdg', '2020-02-12 16:57:07', '2020-02-12 16:57:07'),
(10, 'COULIBALY', 'Lessi Lassina', 'fdffdsfsdfdsdf', 'lessi.coulibaly@eau.gov.bf', 'vwcxcwxcvwxcvcvwxcvwdfdgfgsdfgd', '2020-02-12 16:59:30', '2020-02-12 16:59:30'),
(11, 'COULIBALY', 'Lessi Lassina', 'fdffdsfsdfdsdf', 'lessilassina@gmail.com', 'dgfsdfghsdfgsdfgfdshsdf', '2020-02-12 17:00:22', '2020-02-12 17:00:22'),
(12, 'COULIBALY', 'Lessi Lassina', 'Demande de renseignement', 'lessilassina@gmail.com', 'Demande de renseignement Demande de renseignementDemande de renseignementDemande de renseignement Demande de renseignement Demande de renseignement  Demande de renseignement  Demande de renseignement \r\nDemande de renseignement\r\nDemande de renseignement\r\nDemande de renseignementDemande de renseignement Demande de renseignement\r\nDemande de renseignement Demande de renseignement Demande de renseignement\r\nDemande de renseignement', '2020-03-11 22:25:51', '2020-03-11 22:25:51'),
(13, 'rrere', 'rererer', 'rerererrer', 'rerererr@rerere.ccc', 'rerrerere', '2020-04-02 09:27:26', '2020-04-02 09:27:26'),
(14, 'rrere', 'rererer', 'rerererrer', 'rerererr@rerere.ccc', 'rerrerere', '2020-04-02 09:27:35', '2020-04-02 09:27:35'),
(15, 'rrere', 'rererer', 'rerererrer', 'rerererr@rerere.ccc', 'rerrerere', '2020-04-02 09:27:45', '2020-04-02 09:27:45'),
(16, 'BARRO', 'Ketandi', 'Essai', 'essai8@eee.ddd', 'Votres messages es là', '2020-04-08 18:19:22', '2020-04-08 18:19:22'),
(17, 'BARRO', 'Ketandi', 'Essai', 'essai8@eee.ddd', 'Votres messages es là', '2020-04-08 18:24:59', '2020-04-08 18:24:59'),
(18, 'fdfdf', 'fdfdfdfdfdf', 'fdfsdffdff', 'est@eau.gov.bf', 'fddsfsdfsdfsdfd', '2020-04-08 18:31:56', '2020-04-08 18:31:56'),
(19, 'dfffdsf', 'fdsfsdf', 'fdsfsd', 'barbe@yahoo.fr', 'fdsdffsfdssf', '2020-04-08 18:35:49', '2020-04-08 18:35:49'),
(20, 'adsrrerfd', 'dsdsd', 'dsdds', 'atata@gmail.com', 'dsdsdssd', '2020-04-08 18:38:25', '2020-04-08 18:38:25'),
(21, '2020-02-15 00:00:00', 'Ketandi', 'dsdds', 'titi1@gmail.com', 'sdqsfdffdfdq', '2020-04-08 18:41:08', '2020-04-08 18:41:08'),
(22, 'COULIBALY', 'rererer', 'essai1', 'titi1@gmail.com', 'dsqdsqd', '2020-04-08 18:42:15', '2020-04-08 18:42:15'),
(23, 'COULIBALY', 'rererer', 'essai1', 'titi1@gmail.com', 'dsqdsqd', '2020-04-08 18:42:17', '2020-04-08 18:42:17'),
(24, 'COULIBALY', 'rererer', 'essai1', 'titi1@gmail.com', 'dsqdsqd', '2020-04-08 18:42:26', '2020-04-08 18:42:26'),
(25, 'adsrrerfd', 'fdfdfdfdfdf', 'Essai', 'emanuel@koko.fr', 'tsrdtfuyiguoip,m;', '2020-04-08 18:43:28', '2020-04-08 18:43:28'),
(26, 'rrere', 'dsdsd', 'Essai', 'titi1@gmail.com', 'dsdsdsdsd', '2020-04-08 19:12:52', '2020-04-08 19:12:52'),
(27, 'rrere', 'dsdsd', 'Essai', 'titi1@gmail.com', 'dsdsdsdsddsds', '2020-04-08 19:13:03', '2020-04-08 19:13:03'),
(28, 'rrereddd', 'dsdsd', 'Essai', 'titi1@gmail.com', 'dsdsdsdsddsds', '2020-04-08 20:26:58', '2020-04-08 20:26:58'),
(29, 'ddsdddsds', 'dsdsdsd', 'dsdsdsdssd', 'titi1@gmail.com', 'ddsdsdsdsds', '2020-04-08 20:27:54', '2020-04-08 20:27:54'),
(30, 'dfsdffsf', 'fsdfdsgdf', 'fdsfdsfffsdf', 'admin@admin.com', 'fsdfsddfffdff', '2020-04-08 20:33:04', '2020-04-08 20:33:04'),
(31, 'dqddqsd', 'dsqdfsdfqs', 'dfsfdsfds', 'barbe@yahoo.fr', 'fsdfsdfdsfds', '2020-04-08 20:37:29', '2020-04-08 20:37:29'),
(32, 'dqddqsd', 'dsqdfsdfqs', 'dfsfdsfds', 'barbe@yahoo.fr', 'fsdfsdfdsfds', '2020-04-08 20:41:18', '2020-04-08 20:41:18'),
(33, 'dsdqsfdfd', 'fdsfsdfdsf', 'fsdfsdfsdffd', 'rolande@rolande.com', 'fsfsdffffffffffffffffffffff', '2020-04-08 20:41:39', '2020-04-08 20:41:39'),
(34, 'dqqsdfq', 'dfqdffdf', 'fdfsdfdf', 'barbe@yahoo.fr', 'fdsfsdfdsfsdfdf', '2020-04-08 20:44:48', '2020-04-08 20:44:48'),
(35, 'dqqsdfqf', 'dfqdffdf', 'fdfsdfdf', 'barbe@yahoo.fr', 'fdsfsdfdsfsdfdf', '2020-04-08 20:47:20', '2020-04-08 20:47:20'),
(36, 'dsdqsdsdd', 'dqfsdfds', 'fdfsdfdfsd', 'emanuel@koko.fr', 'fgfdsfdgsdff', '2020-04-08 20:53:43', '2020-04-08 20:53:43'),
(37, 'dqdfqdsq', 'sfsdfds', 'fsdfdfsd', 'titi1@gmail.com', 'fsdfsdfdsfdsf', '2020-04-08 22:53:55', '2020-04-08 22:53:55'),
(38, 'DREA', 'Ketandi', 'dfsfdsfds', 'atata@gmail.com', 'ffdfsfdfddsffddfsdf', '2020-04-09 12:38:26', '2020-04-09 12:38:26'),
(39, 'DREA', 'Ketandi', 'dfsfdsfds', 'atata@gmail.com', 'ffdfsfdfddsffddfsdf', '2020-04-09 12:38:37', '2020-04-09 12:38:37'),
(40, 'DREA', 'Ketandi', 'dfsfdsfds', 'atata@gmail.com', 'ffdfsfdfddsffddfsdf', '2020-04-09 12:39:54', '2020-04-09 12:39:54'),
(41, 'DREA', 'Ketandi', 'dfsfdsfds', 'atata@gmail.com', 'ffdfsfdfddsffddfsdf', '2020-04-09 12:41:03', '2020-04-09 12:41:03'),
(42, 'alassane', 'Ketandi', 'dfsfdsfds', 'atata@gmail.com', 'ffdfsfdfddsffddfsdf', '2020-04-09 12:42:45', '2020-04-09 12:42:45'),
(43, 'BARRO', 'dfqdffdf', 'fdffdsfsdfdsdf', 'emanuel@koko.fr', 'dfqdqdgfdqdfqfgdd', '2020-04-09 12:44:12', '2020-04-09 12:44:12'),
(44, 'dsddsd', 'dsqdsdfds', 'sddsdqsfsfq', 'barbe@yahoo.fr', 'dqsfsqfdsfqsf', '2020-04-09 12:55:22', '2020-04-09 12:55:22'),
(45, 'dffff', 'fsdfsfsd', 'fdsfsdfds', 'titi1@gmail.com', 'fdssdfdsfsdfsdfd', '2020-04-09 12:57:26', '2020-04-09 12:57:26'),
(46, 'dffff', 'fsdfsfsd', 'fdsfsdfds', 'titi1@gmail.com', 'fdssdfdsfsdfsdfd', '2020-04-09 13:05:24', '2020-04-09 13:05:24');

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_193651_create_roles_permissions_tables', 1),
(4, '2018_08_01_183154_create_pages_table', 1),
(5, '2018_08_04_122319_create_settings_table', 1),
(6, '2019_09_05_111853_create_activity_log_table', 1),
(7, '2020_02_06_164529_create_messages_table', 2),
(8, '2020_02_14_150614_create_faqs_table', 3),
(9, '2020_04_06_141547_create_faqs_table', 4);

-- --------------------------------------------------------

--
-- Structure de la table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `label`, `created_at`, `updated_at`) VALUES
(1, 'VALIDER_AGREMENT', 'Valider une agrément saisi', NULL, NULL),
(2, 'CONSULTER', 'Consulter', NULL, NULL),
(3, 'SAISI', 'Saisir un agrément', NULL, NULL),
(4, 'MODIFIER_AGREMENT', 'Modifier un agrément', NULL, NULL),
(5, 'SUPPRIMER_AGREMENT', 'Supprimer un agrément', NULL, NULL),
(6, 'INVALIDER_AGREMENT', 'Invalider un agrément', NULL, NULL),
(7, 'RETIRER_AGREMENT', 'Retirer un agrément', NULL, NULL),
(8, 'ADMINISTRER', 'Accès à l\'administration', NULL, NULL),
(9, 'MODIFIER_CATEGORIE', 'Modifier les catégories', NULL, NULL),
(10, 'SUPPRIMER_CATEGORIE', 'Supprimer une catégorie', NULL, NULL),
(11, 'AJOUTER_CATEGORIE', 'Ajouter une catégorie', NULL, NULL),
(12, 'PARAMETRER', 'Modifier les paramètres', NULL, NULL),
(13, 'GERER_LOCALITE', 'Gerer les localités', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(2, 2),
(2, 3),
(3, 3),
(4, 3),
(5, 3),
(9, 3),
(10, 3),
(11, 3);

-- --------------------------------------------------------

--
-- Structure de la table `Provinces`
--

CREATE TABLE `Provinces` (
  `id` int(11) NOT NULL,
  `Reg_id` int(11) DEFAULT NULL,
  `code_province` varchar(254) DEFAULT NULL,
  `libelle_province` varchar(254) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Provinces`
--

INSERT INTO `Provinces` (`id`, `Reg_id`, `code_province`, `libelle_province`, `created_at`, `updated_at`) VALUES
(1, 1, 'bf-ba', 'Balé', NULL, NULL),
(2, 5, 'bf-bm', 'Bam', NULL, NULL),
(3, 1, 'bf-bw', 'Banwa', NULL, NULL),
(4, 7, 'bf-bz', 'Bazèga', NULL, NULL),
(5, 13, 'bf-bb', 'Bougouriba', NULL, NULL),
(6, 4, 'bf-bl', 'Boulgou', NULL, NULL),
(7, 6, 'bf-bk', 'Boulkiemdé', NULL, NULL),
(8, 2, 'bf-km', 'Comoé', NULL, NULL),
(9, 11, 'bf-gz', 'Ganzourgou', NULL, NULL),
(10, 8, 'bf-gg', 'Gnagna', NULL, NULL),
(11, 8, 'bf-gm', 'Gourma', NULL, NULL),
(12, 9, 'bf-ho', 'Houet', NULL, NULL),
(13, 13, 'bf-io', 'Ioba', NULL, NULL),
(14, 3, 'bf-ka', 'Kadiogo', NULL, NULL),
(15, 9, 'bf-kn', 'Kénédougou', NULL, NULL),
(16, 8, 'bf-kj', 'Komondjari', NULL, NULL),
(17, 8, 'bf-kp', 'Kompienga', NULL, NULL),
(18, 1, 'bf-ks', 'Kossi', NULL, NULL),
(19, 4, 'bf-kl', 'Koulpélogo', NULL, NULL),
(20, 4, 'bf-kr', 'Kouritenga', NULL, NULL),
(21, 11, 'bf-kw', 'Kourwéogo', NULL, NULL),
(22, 2, 'bf-le', 'Léraba', NULL, NULL),
(23, 10, 'bf-lo', 'Loroum ', NULL, NULL),
(24, 1, 'bf-mo', 'Mouhoun', NULL, NULL),
(25, 7, 'bf-nr', 'Nahouri', NULL, NULL),
(26, 5, 'bf-nm', 'Namentenga', NULL, NULL),
(27, 1, 'bf-ny', 'Nayala', NULL, NULL),
(28, 13, 'bf-7399', 'Noumbiel', NULL, NULL),
(29, 11, 'bf-ob', 'Oubritenga', NULL, NULL),
(30, 12, 'bf-od', 'Oudalan ', NULL, NULL),
(31, 10, 'bf-pa', 'Passoré', NULL, NULL),
(32, 13, 'bf-po', 'Poni', NULL, NULL),
(33, 6, 'bf-sg', 'Sanguié', NULL, NULL),
(34, 5, 'bf-st', 'Sanmatenga ', NULL, NULL),
(35, 12, 'bf-se', 'Séno', NULL, NULL),
(36, 6, 'bf-ss', 'Sissili ', NULL, NULL),
(37, 12, 'bf-sm', 'Soum', NULL, NULL),
(38, 1, 'bf-sr', 'Sourou', NULL, NULL),
(39, 8, 'bf-ta', 'Tapoa', NULL, NULL),
(40, 9, 'bf-tu', 'Tuy', NULL, NULL),
(41, 12, 'bf-yg', 'Yagha', NULL, NULL),
(42, 10, 'bf-yt', 'Yatenga', NULL, NULL),
(43, 6, 'bf-zr', 'Ziro', NULL, NULL),
(44, 10, 'bf-zm', 'Zondoma', NULL, NULL),
(45, 7, 'bf-zw', 'Zoundwéogo', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `Regions`
--

CREATE TABLE `Regions` (
  `id` int(11) NOT NULL,
  `code_region` varchar(254) DEFAULT NULL,
  `libelle_region` varchar(254) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Regions`
--

INSERT INTO `Regions` (`id`, `code_region`, `libelle_region`, `created_at`, `updated_at`) VALUES
(1, '01', 'BOUCLE DU MOUHOUN', NULL, NULL),
(2, '02', 'CASCADES', NULL, NULL),
(3, '03', 'CENTRE', NULL, NULL),
(4, '04', 'CENTRE EST', NULL, NULL),
(5, '05', 'CENTRE NORD', NULL, NULL),
(6, '06', 'CENTRE OUEST', NULL, NULL),
(7, '07', 'CENTRE SUD', NULL, NULL),
(8, '08', 'EST', NULL, NULL),
(9, '09', 'HAUTS-BASSINS', NULL, NULL),
(10, '10', 'NORD', NULL, NULL),
(11, '11', 'PLATEAU CENTRAL', NULL, NULL),
(12, '12', 'SAHEL', NULL, NULL),
(13, '13', 'SUD OUEST', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `roles`
--

INSERT INTO `roles` (`id`, `name`, `label`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrateur', '2019-10-08 12:28:55', '2019-10-08 12:28:55'),
(2, 'invite', 'Invité', '2019-10-08 13:25:25', '2019-10-08 13:25:25'),
(3, 'Saisie', 'Agent de saisie', '2019-11-12 22:21:45', '2019-12-04 02:41:42');

-- --------------------------------------------------------

--
-- Structure de la table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`) VALUES
(1, 1),
(2, 2),
(3, 3),
(3, 4);

-- --------------------------------------------------------

--
-- Structure de la table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`, `label`) VALUES
(1, 'validite_agrement', '5', 'Durée de validité d\'un agrément (an)');

-- --------------------------------------------------------

--
-- Structure de la table `StatutAgrements`
--

CREATE TABLE `StatutAgrements` (
  `id` int(11) NOT NULL,
  `Agr_id` int(11) DEFAULT NULL,
  `Sta_id` int(11) DEFAULT NULL,
  `date_debut_statut` date DEFAULT NULL,
  `date_fin_statut` date DEFAULT NULL COMMENT 'Date de fin cas de suspension d''agrement',
  `observation` varchar(254) DEFAULT NULL,
  `est_actif` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `StatutAgrements`
--

INSERT INTO `StatutAgrements` (`id`, `Agr_id`, `Sta_id`, `date_debut_statut`, `date_fin_statut`, `observation`, `est_actif`, `created_at`, `updated_at`) VALUES
(66, 30, 4, NULL, NULL, NULL, 1, NULL, NULL),
(67, 14, 4, NULL, NULL, NULL, 1, NULL, NULL),
(68, 20, 4, NULL, NULL, NULL, 1, NULL, NULL),
(69, 24, 4, NULL, NULL, NULL, 1, NULL, NULL),
(70, 26, 4, NULL, NULL, NULL, 1, NULL, NULL),
(71, 2, 2, '2020-04-09', NULL, NULL, 1, NULL, NULL),
(72, 7, 2, '2020-04-09', NULL, NULL, 1, NULL, NULL),
(73, 33, 2, '2020-04-10', NULL, NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `Statuts`
--

CREATE TABLE `Statuts` (
  `id` int(11) NOT NULL,
  `code_statut` varchar(20) NOT NULL,
  `libelle_statut` varchar(50) DEFAULT NULL,
  `observation` varchar(254) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `est_actif` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Statuts`
--

INSERT INTO `Statuts` (`id`, `code_statut`, `libelle_statut`, `observation`, `created_at`, `updated_at`, `est_actif`) VALUES
(1, 'VALIDE', 'est valide', 'En cours de validité', '2019-09-13 09:21:57', '2019-10-07 01:22:53', 1),
(2, 'RETIRE', 'est retiré', 'Agrement retiré', '2019-09-13 09:22:27', '2019-10-07 01:23:20', 1),
(3, 'SUSPENDU', 'est suspendu', 'Agrements suspendu', '2019-09-13 09:24:55', '2019-10-07 01:23:38', 1),
(4, 'EXPIRE', 'est expiré', 'Agrements expirés', '2019-11-11 00:00:00', '2020-04-05 10:27:35', 1);

-- --------------------------------------------------------

--
-- Structure de la table `TypeTravaux`
--

CREATE TABLE `TypeTravaux` (
  `id` int(11) NOT NULL,
  `libelle_type` varchar(254) DEFAULT NULL,
  `description` varchar(254) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `est_actif` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `TypeTravaux`
--

INSERT INTO `TypeTravaux` (`id`, `libelle_type`, `description`, `created_at`, `updated_at`, `est_actif`) VALUES
(1, 'Etudes', 'Realisation des etudes', '2019-09-08 15:37:53', '2019-09-08 15:37:53', 1),
(2, 'Travaux', 'Travaux en cours', '2019-09-08 15:38:24', '2019-09-08 15:38:24', 1);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `firstconnect` tinyint(1) DEFAULT '1',
  `active` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `firstconnect`, `active`) VALUES
(1, 'Administrateur', 'admin@admin.com', NULL, '$2y$10$DXWAu5aHXlmuoQ7SGOyxVevDU2aw82.kiG30JyK.zFr7iaEiyAagG', NULL, '2019-10-08 12:45:19', '2019-10-08 23:28:59', 0, 1),
(2, 'User', 'lessilassina@gmail.com', NULL, '$2y$10$Kj8z3AK4YN9zo/IxmfsB/eaivTkC5DqlohfY.Oh5wCmWGdlVk2L0q', NULL, '2019-10-08 12:45:19', '2020-04-08 15:22:52', 0, 1),
(3, 'Agent de saisie', 'agent-saisie@gmail.com', NULL, '$2y$10$QFtQmyImFAX4vjLEG77a0uEJWKteCcF7zNruaXP2TaWhjNHlbmVlu', NULL, '2019-11-12 22:25:23', '2020-04-08 15:06:16', 0, 1),
(4, 'TAO Rasmane', 'rasmane@gmail.com', NULL, '$2y$10$zVctw2ANLZRB.1K3BKzSyuWzgkOcB6Bn8eDgbnMYJg.hkIEfNsfXq', NULL, '2020-04-09 14:38:25', '2020-04-09 14:49:34', 1, 1);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activity_log_log_name_index` (`log_name`),
  ADD KEY `subject` (`subject_id`,`subject_type`),
  ADD KEY `causer` (`causer_id`,`causer_type`);

--
-- Index pour la table `Agrements`
--
ALTER TABLE `Agrements`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Agrements_UN` (`numero_agrement`),
  ADD KEY `FK_Association_13` (`Typ_id`),
  ADD KEY `FK_Association_2` (`Dom_id`),
  ADD KEY `FK_Association_6` (`Ent_id`);

--
-- Index pour la table `CategorieAgrements`
--
ALTER TABLE `CategorieAgrements`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `CategorieAgrements_UN` (`Cat_id`,`Agr_id`),
  ADD KEY `FK_Association_14` (`Agr_id`),
  ADD KEY `FK_Association_15` (`Cat_id`);

--
-- Index pour la table `Categories`
--
ALTER TABLE `Categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Association_5` (`Cat_id`),
  ADD KEY `Dom_id` (`Dom_id`),
  ADD KEY `Typ_id` (`Typ_id`);

--
-- Index pour la table `Communes`
--
ALTER TABLE `Communes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Association_17` (`Pro_id`);

--
-- Index pour la table `Domaines`
--
ALTER TABLE `Domaines`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `libelle_court` (`libelle_court`);

--
-- Index pour la table `Entreprises`
--
ALTER TABLE `Entreprises`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Entreprises_UN` (`numero_ifu`),
  ADD UNIQUE KEY `Raison_sociale_UN` (`raison_sociale`),
  ADD UNIQUE KEY `Entreprises_UN_cnss` (`numero_cnss`),
  ADD UNIQUE KEY `Entreprises_UN_rccm` (`registre_commerce`),
  ADD KEY `FK_Association_11` (`Com_id`),
  ADD KEY `FK_Association_12` (`Reg_id`),
  ADD KEY `Entreprises_Provinces_FK` (`Pro_id`);

--
-- Index pour la table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Index pour la table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Index pour la table `Provinces`
--
ALTER TABLE `Provinces`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Association_16` (`Reg_id`);

--
-- Index pour la table `Regions`
--
ALTER TABLE `Regions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`role_id`,`user_id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`);

--
-- Index pour la table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Index pour la table `StatutAgrements`
--
ALTER TABLE `StatutAgrements`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Agr_id` (`Agr_id`,`Sta_id`),
  ADD KEY `FK_Association_7` (`Sta_id`),
  ADD KEY `FK_Association_8` (`Agr_id`);

--
-- Index pour la table `Statuts`
--
ALTER TABLE `Statuts`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `TypeTravaux`
--
ALTER TABLE `TypeTravaux`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT pour la table `Agrements`
--
ALTER TABLE `Agrements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT pour la table `CategorieAgrements`
--
ALTER TABLE `CategorieAgrements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT pour la table `Categories`
--
ALTER TABLE `Categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT pour la table `Communes`
--
ALTER TABLE `Communes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=371;
--
-- AUTO_INCREMENT pour la table `Domaines`
--
ALTER TABLE `Domaines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `Entreprises`
--
ALTER TABLE `Entreprises`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT pour la table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `Provinces`
--
ALTER TABLE `Provinces`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT pour la table `Regions`
--
ALTER TABLE `Regions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `StatutAgrements`
--
ALTER TABLE `StatutAgrements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT pour la table `Statuts`
--
ALTER TABLE `Statuts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `TypeTravaux`
--
ALTER TABLE `TypeTravaux`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Agrements`
--
ALTER TABLE `Agrements`
  ADD CONSTRAINT `FK_Association_13` FOREIGN KEY (`Typ_id`) REFERENCES `TypeTravaux` (`id`),
  ADD CONSTRAINT `FK_Association_2` FOREIGN KEY (`Dom_id`) REFERENCES `Domaines` (`id`),
  ADD CONSTRAINT `FK_Association_6` FOREIGN KEY (`Ent_id`) REFERENCES `Entreprises` (`id`);

--
-- Contraintes pour la table `CategorieAgrements`
--
ALTER TABLE `CategorieAgrements`
  ADD CONSTRAINT `FK_Association_14` FOREIGN KEY (`Agr_id`) REFERENCES `Agrements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Association_15` FOREIGN KEY (`Cat_id`) REFERENCES `Categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Categories`
--
ALTER TABLE `Categories`
  ADD CONSTRAINT `Categories_ibfk_1` FOREIGN KEY (`Dom_id`) REFERENCES `Domaines` (`id`),
  ADD CONSTRAINT `Categories_ibfk_2` FOREIGN KEY (`Typ_id`) REFERENCES `TypeTravaux` (`id`),
  ADD CONSTRAINT `FK_Association_5` FOREIGN KEY (`Cat_id`) REFERENCES `Categories` (`id`);

--
-- Contraintes pour la table `Communes`
--
ALTER TABLE `Communes`
  ADD CONSTRAINT `FK_Association_17` FOREIGN KEY (`Pro_id`) REFERENCES `Provinces` (`id`);

--
-- Contraintes pour la table `Entreprises`
--
ALTER TABLE `Entreprises`
  ADD CONSTRAINT `Entreprises_Provinces_FK` FOREIGN KEY (`Pro_id`) REFERENCES `Provinces` (`id`),
  ADD CONSTRAINT `FK_Association_11` FOREIGN KEY (`Com_id`) REFERENCES `Communes` (`id`),
  ADD CONSTRAINT `FK_Association_12` FOREIGN KEY (`Reg_id`) REFERENCES `Regions` (`id`);

--
-- Contraintes pour la table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `Provinces`
--
ALTER TABLE `Provinces`
  ADD CONSTRAINT `FK_Association_16` FOREIGN KEY (`Reg_id`) REFERENCES `Regions` (`id`);

--
-- Contraintes pour la table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `StatutAgrements`
--
ALTER TABLE `StatutAgrements`
  ADD CONSTRAINT `FK_Association_7` FOREIGN KEY (`Sta_id`) REFERENCES `Statuts` (`id`),
  ADD CONSTRAINT `FK_Association_8` FOREIGN KEY (`Agr_id`) REFERENCES `Agrements` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
