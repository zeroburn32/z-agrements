<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Communes;

class CommunesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_communes()
    {
        $communes = factory(Communes::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/communes', $communes
        );

        $this->assertApiResponse($communes);
    }

    /**
     * @test
     */
    public function test_read_communes()
    {
        $communes = factory(Communes::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/communes/'.$communes->id
        );

        $this->assertApiResponse($communes->toArray());
    }

    /**
     * @test
     */
    public function test_update_communes()
    {
        $communes = factory(Communes::class)->create();
        $editedCommunes = factory(Communes::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/communes/'.$communes->id,
            $editedCommunes
        );

        $this->assertApiResponse($editedCommunes);
    }

    /**
     * @test
     */
    public function test_delete_communes()
    {
        $communes = factory(Communes::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/communes/'.$communes->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/communes/'.$communes->id
        );

        $this->response->assertStatus(404);
    }
}
