<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Domaines;

class DomainesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_domaines()
    {
        $domaines = factory(Domaines::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/domaines', $domaines
        );

        $this->assertApiResponse($domaines);
    }

    /**
     * @test
     */
    public function test_read_domaines()
    {
        $domaines = factory(Domaines::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/domaines/'.$domaines->id
        );

        $this->assertApiResponse($domaines->toArray());
    }

    /**
     * @test
     */
    public function test_update_domaines()
    {
        $domaines = factory(Domaines::class)->create();
        $editedDomaines = factory(Domaines::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/domaines/'.$domaines->id,
            $editedDomaines
        );

        $this->assertApiResponse($editedDomaines);
    }

    /**
     * @test
     */
    public function test_delete_domaines()
    {
        $domaines = factory(Domaines::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/domaines/'.$domaines->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/domaines/'.$domaines->id
        );

        $this->response->assertStatus(404);
    }
}
