<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Provinces;

class ProvincesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_provinces()
    {
        $provinces = factory(Provinces::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/provinces', $provinces
        );

        $this->assertApiResponse($provinces);
    }

    /**
     * @test
     */
    public function test_read_provinces()
    {
        $provinces = factory(Provinces::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/provinces/'.$provinces->id
        );

        $this->assertApiResponse($provinces->toArray());
    }

    /**
     * @test
     */
    public function test_update_provinces()
    {
        $provinces = factory(Provinces::class)->create();
        $editedProvinces = factory(Provinces::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/provinces/'.$provinces->id,
            $editedProvinces
        );

        $this->assertApiResponse($editedProvinces);
    }

    /**
     * @test
     */
    public function test_delete_provinces()
    {
        $provinces = factory(Provinces::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/provinces/'.$provinces->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/provinces/'.$provinces->id
        );

        $this->response->assertStatus(404);
    }
}
