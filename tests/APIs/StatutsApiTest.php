<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Statuts;

class StatutsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_statuts()
    {
        $statuts = factory(Statuts::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/statuts', $statuts
        );

        $this->assertApiResponse($statuts);
    }

    /**
     * @test
     */
    public function test_read_statuts()
    {
        $statuts = factory(Statuts::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/statuts/'.$statuts->id
        );

        $this->assertApiResponse($statuts->toArray());
    }

    /**
     * @test
     */
    public function test_update_statuts()
    {
        $statuts = factory(Statuts::class)->create();
        $editedStatuts = factory(Statuts::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/statuts/'.$statuts->id,
            $editedStatuts
        );

        $this->assertApiResponse($editedStatuts);
    }

    /**
     * @test
     */
    public function test_delete_statuts()
    {
        $statuts = factory(Statuts::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/statuts/'.$statuts->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/statuts/'.$statuts->id
        );

        $this->response->assertStatus(404);
    }
}
