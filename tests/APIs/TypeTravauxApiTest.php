<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TypeTravaux;

class TypeTravauxApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_type_travaux()
    {
        $typeTravaux = factory(TypeTravaux::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/type_travauxes', $typeTravaux
        );

        $this->assertApiResponse($typeTravaux);
    }

    /**
     * @test
     */
    public function test_read_type_travaux()
    {
        $typeTravaux = factory(TypeTravaux::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/type_travauxes/'.$typeTravaux->id
        );

        $this->assertApiResponse($typeTravaux->toArray());
    }

    /**
     * @test
     */
    public function test_update_type_travaux()
    {
        $typeTravaux = factory(TypeTravaux::class)->create();
        $editedTypeTravaux = factory(TypeTravaux::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/type_travauxes/'.$typeTravaux->id,
            $editedTypeTravaux
        );

        $this->assertApiResponse($editedTypeTravaux);
    }

    /**
     * @test
     */
    public function test_delete_type_travaux()
    {
        $typeTravaux = factory(TypeTravaux::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/type_travauxes/'.$typeTravaux->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/type_travauxes/'.$typeTravaux->id
        );

        $this->response->assertStatus(404);
    }
}
