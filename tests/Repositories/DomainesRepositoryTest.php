<?php namespace Tests\Repositories;

use App\Models\Domaines;
use App\Repositories\DomainesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DomainesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DomainesRepository
     */
    protected $domainesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->domainesRepo = \App::make(DomainesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_domaines()
    {
        $domaines = factory(Domaines::class)->make()->toArray();

        $createdDomaines = $this->domainesRepo->create($domaines);

        $createdDomaines = $createdDomaines->toArray();
        $this->assertArrayHasKey('id', $createdDomaines);
        $this->assertNotNull($createdDomaines['id'], 'Created Domaines must have id specified');
        $this->assertNotNull(Domaines::find($createdDomaines['id']), 'Domaines with given id must be in DB');
        $this->assertModelData($domaines, $createdDomaines);
    }

    /**
     * @test read
     */
    public function test_read_domaines()
    {
        $domaines = factory(Domaines::class)->create();

        $dbDomaines = $this->domainesRepo->find($domaines->id);

        $dbDomaines = $dbDomaines->toArray();
        $this->assertModelData($domaines->toArray(), $dbDomaines);
    }

    /**
     * @test update
     */
    public function test_update_domaines()
    {
        $domaines = factory(Domaines::class)->create();
        $fakeDomaines = factory(Domaines::class)->make()->toArray();

        $updatedDomaines = $this->domainesRepo->update($fakeDomaines, $domaines->id);

        $this->assertModelData($fakeDomaines, $updatedDomaines->toArray());
        $dbDomaines = $this->domainesRepo->find($domaines->id);
        $this->assertModelData($fakeDomaines, $dbDomaines->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_domaines()
    {
        $domaines = factory(Domaines::class)->create();

        $resp = $this->domainesRepo->delete($domaines->id);

        $this->assertTrue($resp);
        $this->assertNull(Domaines::find($domaines->id), 'Domaines should not exist in DB');
    }
}
