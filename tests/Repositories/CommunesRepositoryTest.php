<?php namespace Tests\Repositories;

use App\Models\Communes;
use App\Repositories\CommunesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CommunesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CommunesRepository
     */
    protected $communesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->communesRepo = \App::make(CommunesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_communes()
    {
        $communes = factory(Communes::class)->make()->toArray();

        $createdCommunes = $this->communesRepo->create($communes);

        $createdCommunes = $createdCommunes->toArray();
        $this->assertArrayHasKey('id', $createdCommunes);
        $this->assertNotNull($createdCommunes['id'], 'Created Communes must have id specified');
        $this->assertNotNull(Communes::find($createdCommunes['id']), 'Communes with given id must be in DB');
        $this->assertModelData($communes, $createdCommunes);
    }

    /**
     * @test read
     */
    public function test_read_communes()
    {
        $communes = factory(Communes::class)->create();

        $dbCommunes = $this->communesRepo->find($communes->id);

        $dbCommunes = $dbCommunes->toArray();
        $this->assertModelData($communes->toArray(), $dbCommunes);
    }

    /**
     * @test update
     */
    public function test_update_communes()
    {
        $communes = factory(Communes::class)->create();
        $fakeCommunes = factory(Communes::class)->make()->toArray();

        $updatedCommunes = $this->communesRepo->update($fakeCommunes, $communes->id);

        $this->assertModelData($fakeCommunes, $updatedCommunes->toArray());
        $dbCommunes = $this->communesRepo->find($communes->id);
        $this->assertModelData($fakeCommunes, $dbCommunes->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_communes()
    {
        $communes = factory(Communes::class)->create();

        $resp = $this->communesRepo->delete($communes->id);

        $this->assertTrue($resp);
        $this->assertNull(Communes::find($communes->id), 'Communes should not exist in DB');
    }
}
