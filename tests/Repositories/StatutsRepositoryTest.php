<?php namespace Tests\Repositories;

use App\Models\Statuts;
use App\Repositories\StatutsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class StatutsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var StatutsRepository
     */
    protected $statutsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->statutsRepo = \App::make(StatutsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_statuts()
    {
        $statuts = factory(Statuts::class)->make()->toArray();

        $createdStatuts = $this->statutsRepo->create($statuts);

        $createdStatuts = $createdStatuts->toArray();
        $this->assertArrayHasKey('id', $createdStatuts);
        $this->assertNotNull($createdStatuts['id'], 'Created Statuts must have id specified');
        $this->assertNotNull(Statuts::find($createdStatuts['id']), 'Statuts with given id must be in DB');
        $this->assertModelData($statuts, $createdStatuts);
    }

    /**
     * @test read
     */
    public function test_read_statuts()
    {
        $statuts = factory(Statuts::class)->create();

        $dbStatuts = $this->statutsRepo->find($statuts->id);

        $dbStatuts = $dbStatuts->toArray();
        $this->assertModelData($statuts->toArray(), $dbStatuts);
    }

    /**
     * @test update
     */
    public function test_update_statuts()
    {
        $statuts = factory(Statuts::class)->create();
        $fakeStatuts = factory(Statuts::class)->make()->toArray();

        $updatedStatuts = $this->statutsRepo->update($fakeStatuts, $statuts->id);

        $this->assertModelData($fakeStatuts, $updatedStatuts->toArray());
        $dbStatuts = $this->statutsRepo->find($statuts->id);
        $this->assertModelData($fakeStatuts, $dbStatuts->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_statuts()
    {
        $statuts = factory(Statuts::class)->create();

        $resp = $this->statutsRepo->delete($statuts->id);

        $this->assertTrue($resp);
        $this->assertNull(Statuts::find($statuts->id), 'Statuts should not exist in DB');
    }
}
