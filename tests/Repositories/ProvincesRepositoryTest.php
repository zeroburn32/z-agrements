<?php namespace Tests\Repositories;

use App\Models\Provinces;
use App\Repositories\ProvincesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ProvincesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProvincesRepository
     */
    protected $provincesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->provincesRepo = \App::make(ProvincesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_provinces()
    {
        $provinces = factory(Provinces::class)->make()->toArray();

        $createdProvinces = $this->provincesRepo->create($provinces);

        $createdProvinces = $createdProvinces->toArray();
        $this->assertArrayHasKey('id', $createdProvinces);
        $this->assertNotNull($createdProvinces['id'], 'Created Provinces must have id specified');
        $this->assertNotNull(Provinces::find($createdProvinces['id']), 'Provinces with given id must be in DB');
        $this->assertModelData($provinces, $createdProvinces);
    }

    /**
     * @test read
     */
    public function test_read_provinces()
    {
        $provinces = factory(Provinces::class)->create();

        $dbProvinces = $this->provincesRepo->find($provinces->id);

        $dbProvinces = $dbProvinces->toArray();
        $this->assertModelData($provinces->toArray(), $dbProvinces);
    }

    /**
     * @test update
     */
    public function test_update_provinces()
    {
        $provinces = factory(Provinces::class)->create();
        $fakeProvinces = factory(Provinces::class)->make()->toArray();

        $updatedProvinces = $this->provincesRepo->update($fakeProvinces, $provinces->id);

        $this->assertModelData($fakeProvinces, $updatedProvinces->toArray());
        $dbProvinces = $this->provincesRepo->find($provinces->id);
        $this->assertModelData($fakeProvinces, $dbProvinces->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_provinces()
    {
        $provinces = factory(Provinces::class)->create();

        $resp = $this->provincesRepo->delete($provinces->id);

        $this->assertTrue($resp);
        $this->assertNull(Provinces::find($provinces->id), 'Provinces should not exist in DB');
    }
}
