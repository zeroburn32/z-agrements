<?php namespace Tests\Repositories;

use App\Models\TypeTravaux;
use App\Repositories\TypeTravauxRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TypeTravauxRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TypeTravauxRepository
     */
    protected $typeTravauxRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->typeTravauxRepo = \App::make(TypeTravauxRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_type_travaux()
    {
        $typeTravaux = factory(TypeTravaux::class)->make()->toArray();

        $createdTypeTravaux = $this->typeTravauxRepo->create($typeTravaux);

        $createdTypeTravaux = $createdTypeTravaux->toArray();
        $this->assertArrayHasKey('id', $createdTypeTravaux);
        $this->assertNotNull($createdTypeTravaux['id'], 'Created TypeTravaux must have id specified');
        $this->assertNotNull(TypeTravaux::find($createdTypeTravaux['id']), 'TypeTravaux with given id must be in DB');
        $this->assertModelData($typeTravaux, $createdTypeTravaux);
    }

    /**
     * @test read
     */
    public function test_read_type_travaux()
    {
        $typeTravaux = factory(TypeTravaux::class)->create();

        $dbTypeTravaux = $this->typeTravauxRepo->find($typeTravaux->id);

        $dbTypeTravaux = $dbTypeTravaux->toArray();
        $this->assertModelData($typeTravaux->toArray(), $dbTypeTravaux);
    }

    /**
     * @test update
     */
    public function test_update_type_travaux()
    {
        $typeTravaux = factory(TypeTravaux::class)->create();
        $fakeTypeTravaux = factory(TypeTravaux::class)->make()->toArray();

        $updatedTypeTravaux = $this->typeTravauxRepo->update($fakeTypeTravaux, $typeTravaux->id);

        $this->assertModelData($fakeTypeTravaux, $updatedTypeTravaux->toArray());
        $dbTypeTravaux = $this->typeTravauxRepo->find($typeTravaux->id);
        $this->assertModelData($fakeTypeTravaux, $dbTypeTravaux->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_type_travaux()
    {
        $typeTravaux = factory(TypeTravaux::class)->create();

        $resp = $this->typeTravauxRepo->delete($typeTravaux->id);

        $this->assertTrue($resp);
        $this->assertNull(TypeTravaux::find($typeTravaux->id), 'TypeTravaux should not exist in DB');
    }
}
