ALTER TABLE agrements_db.Entreprises ADD CONSTRAINT Entreprises_UN UNIQUE KEY (numero_ifu);
ALTER TABLE agrements_db.Agrements ADD CONSTRAINT Agrements_UN UNIQUE KEY (numero_agrement);
ALTER TABLE agrements_db.CategorieAgrements ADD CONSTRAINT CategorieAgrements_UN UNIQUE KEY (Cat_id,Agr_id);
ALTER TABLE agrements_db.Domaines ADD est_actif BOOL DEFAULT 0 NOT NULL;
ALTER TABLE agrements_db.Categories ADD est_actif BOOL DEFAULT 1 NOT NULL;
ALTER TABLE agrements_db.Statuts ADD est_actif BOOL DEFAULT 0 NOT NULL;
ALTER TABLE agrements_db.TypeTravaux ADD est_actif BOOL DEFAULT 1 NOT NULL;

ALTER TABLE `CategorieAgrements` DROP  FOREIGN KEY `FK_Association_14` ;
ALTER TABLE `CategorieAgrements` ADD  CONSTRAINT `FK_Association_14` FOREIGN KEY (`Agr_id`) REFERENCES `Agrements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
ALTER TABLE `CategorieAgrements` DROP FOREIGN KEY `FK_Association_15`;
ALTER TABLE `CategorieAgrements` ADD CONSTRAINT `FK_Association_15` FOREIGN KEY (`Cat_id`) REFERENCES `Categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-----************************05/04/2020***************************----------
ALTER TABLE agrements_db.Entreprises ADD CONSTRAINT Entreprises_UN_cnss UNIQUE KEY (numero_cnss);
ALTER TABLE agrements_db.Entreprises ADD CONSTRAINT Entreprises_UN_rccm UNIQUE KEY (registre_commerce);

ALTER TABLE `Entreprises` ADD `Pro_id` INT NOT NULL AFTER `Com_id`; 
ALTER TABLE agrements_db.Entreprises ADD CONSTRAINT Entreprises_Provinces_FK FOREIGN KEY (id) REFERENCES agrements_db.Provinces(id) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE agrements_db.Entreprises DROP FOREIGN KEY Entreprises_Provinces_FK;
ALTER TABLE agrements_db.Entreprises ADD CONSTRAINT Entreprises_Provinces_FK FOREIGN KEY (Pro_id) REFERENCES agrements_db.Provinces(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE agrements_db.settings ADD label varchar(100) NULL;


---******************01/05/2020********************************------------
ALTER TABLE agrements_db.users MODIFY COLUMN active tinyint(1) DEFAULT 1 NULL;
ALTER TABLE agrements_db.Entreprises MODIFY COLUMN Pro_id int(11) NULL;
ALTER TABLE agrements_db.Entreprises MODIFY COLUMN numero_ifu varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE agrements_db.Entreprises MODIFY COLUMN raison_sociale varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `Entreprises` ADD `ville` VARCHAR(100) NULL AFTER `Reg_id`; 

ALTER TABLE agrements_db.Agrements MODIFY COLUMN Ent_id int(11) NOT NULL;
ALTER TABLE agrements_db.Agrements MODIFY COLUMN Dom_id int(11) NOT NULL;
ALTER TABLE agrements_db.Agrements MODIFY COLUMN date_arrete date NOT NULL;
ALTER TABLE agrements_db.Agrements MODIFY COLUMN est_expire tinyint(1) DEFAULT 0 NOT NULL;
ALTER TABLE agrements_db.Agrements MODIFY COLUMN date_expiration date NOT NULL;

ALTER TABLE agrements_db.StatutAgrements MODIFY COLUMN Agr_id int(11) NOT NULL;
ALTER TABLE agrements_db.StatutAgrements MODIFY COLUMN Sta_id int(11) NOT NULL;
ALTER TABLE agrements_db.StatutAgrements MODIFY COLUMN est_actif tinyint(1) DEFAULT 0 NOT NULL;

ALTER TABLE agrements_db.Entreprises DROP KEY Entreprises_UN_cnss;
ALTER TABLE agrements_db.Entreprises DROP KEY Entreprises_UN_rccm;






















