<?php
use  App\Mail\ContactMessageCreated;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

/* Route::get('/', function () {
    return view('welcome');
})->name('/'); */
Route::get('/', 'WelcomeController@index')->name('/');

Route::get('/home', 'HomeController@index')->name('home');

Route::patch('firstConnect/{id}', 'Auth\LoginController@firstConnect')->name('firstConnect');

Route::post('authenticate', 'Auth\LoginController@authenticate')->name('authenticate');

//Route::get('admin', 'Admin\AdminController@index');

Route::resource('admin/roles', 'Admin\RolesController');

Route::resource('admin/permissions', 'Admin\PermissionsController');

Route::resource('admin/users', 'Admin\UsersController');

//Route::resource('admin/pages', 'Admin\PagesController');

Route::resource('admin/activitylogs', 'Admin\ActivityLogsController')->only([
    'index', 'show', 'destroy'
]);

Route::resource('admin/settings', 'Admin\SettingsController');


Route::middleware('auth')->group(function () {

    Route::get('/formUserPassword', 'Auth\AccountController@formPassword')->name('formMyPassword');

    Route::patch('/changeUserPassword', 'Auth\AccountController@changePassword')->name('changeMyPassword');

    Route::patch('/lockAndUnlockAccount/{id}', 'Auth\AccountController@lockAndUnlock')->name('lockAndUnlockAccount');

    Route::get('reinitialiserMotDePasse/{id}', 'Auth\AccountController@reinitialiserMotDePasse')->name('reinitialiserMotDePasse');

    Route::resource('regions', 'RegionsController');

    Route::resource('provinces', 'ProvincesController');

    Route::resource('communes', 'CommunesController');

    Route::resource('agrements', 'AgrementsController');

    Route::resource('agrements', 'AgrementsController');

    Route::resource('categorieAgrements', 'CategorieAgrementsController');

    Route::resource('categories', 'CategoriesController');

    Route::resource('domaines', 'DomainesController');

    Route::resource('entreprises', 'EntreprisesController');

    Route::resource('statutAgrements', 'StatutAgrementsController');

    Route::resource('statuts', 'StatutsController');

    Route::resource('typeTravauxes', 'TypeTravauxController');

    // My Ajax routes
    Route::get('getCategorie/{domaine?}/{type?}', ['uses' => 'AgrementsController@getCategorie']);

    Route::get('getAgrement/{id?}', ['uses' => 'AgrementsController@getAgrement']);

    Route::get('getEntreprise/{id?}', ['uses' => 'EntreprisesController@getEntreprise']);

    Route::get('getCategoriesByAgrement/{id?}', ['uses' => 'AgrementsController@getCategoriesByAgrement']);

    Route::get('getCommunes/{id?}', ['uses' => 'LocalitesController@getCommunes']);

    Route::get('getProvinces/{id?}', ['uses' => 'LocalitesController@getProvinces']);

    Route::get('getRegions/{id?}', ['uses' => 'LocalitesController@getRegions']);

    Route::get('getAgrementsByEtat/{etat?}', ['uses' => 'AgrementsController@getAgrementsByEtat']);

    Route::post('toggle', ['as' => 'toggle', 'uses' => 'AgrementsController@toggle']);

    Route::post('retirer', ['as' => 'retirer', 'uses' => 'AgrementsController@retirer']);

    Route::resource('rapport', 'RapportController');

    Route::post('resultats', ['as' => 'resultat', 'uses' => 'RapportController@resultats']);

    Route::post('importData', 'AgrementsController@importer')->name('importData');

});

/**************** My defined routes ***************************************/
Route::post('findAgrement', ['as' => 'findAgrement', 'uses' => 'FrontendController@findAgrement']);

Route::post('/contact', 'ContactsController@enregister')->name('contact_path');

Route::post('imprimer', ['as' => 'imprimer', 'uses' => 'FrontendController@export_pdf']);

Route::get('/faq', 'FaqController@recupDonnees')->name('faq_path');



