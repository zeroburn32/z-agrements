<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('regions', 'RegionsAPIController');

Route::resource('regions', 'RegionsAPIController');

Route::resource('provinces', 'ProvincesAPIController');

Route::resource('communes', 'CommunesAPIController');

Route::resource('elections', 'ElectionsAPIController');

Route::resource('type_elections', 'TypeElectionsAPIController');

Route::resource('statut_resultats', 'StatutResultatsAPIController');

Route::resource('candidat_elections', 'CandidatElectionsAPIController');

Route::resource('parti_elections', 'PartiElectionsAPIController');

Route::resource('resultats', 'ResultatsAPIController');

Route::resource('candidats', 'CandidatsAPIController');

Route::resource('partis', 'PartisAPIController');

Route::resource('statut_resultats', 'StatutResultatsAPIController');

Route::resource('regions', 'RegionsAPIController');

Route::resource('provinces', 'ProvincesAPIController');

Route::resource('communes', 'CommunesAPIController');

Route::resource('agrements', 'AgrementsAPIController');

Route::resource('agrements', 'AgrementsAPIController');

Route::resource('categorie_agrements', 'CategorieAgrementsAPIController');

Route::resource('categories', 'CategoriesAPIController');

Route::resource('domaines', 'DomainesAPIController');

Route::resource('entreprises', 'EntreprisesAPIController');

Route::resource('statut_agrements', 'StatutAgrementsAPIController');

Route::resource('statuts', 'StatutsAPIController');

Route::resource('type_travauxes', 'TypeTravauxAPIController');
