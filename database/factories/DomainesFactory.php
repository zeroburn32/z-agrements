<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Domaines;
use Faker\Generator as Faker;

$factory->define(Domaines::class, function (Faker $faker) {

    return [
        'sigle_domaine' => $faker->word,
        'libelle_domaine' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
