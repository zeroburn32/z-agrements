<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Statuts;
use Faker\Generator as Faker;

$factory->define(Statuts::class, function (Faker $faker) {

    return [
        'libelle_statut' => $faker->word,
        'observation' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
