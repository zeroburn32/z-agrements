<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Provinces;
use Faker\Generator as Faker;

$factory->define(Provinces::class, function (Faker $faker) {

    return [
        'code_province' => $faker->word,
        'libelle_province' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
