<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Regions;
use Faker\Generator as Faker;

$factory->define(Regions::class, function (Faker $faker) {

    return [
        'code_region' => $faker->word,
        'libelle_region' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
