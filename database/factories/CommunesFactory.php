<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Communes;
use Faker\Generator as Faker;

$factory->define(Communes::class, function (Faker $faker) {

    return [
        'code_commune' => $faker->word,
        'libelle_commune' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
