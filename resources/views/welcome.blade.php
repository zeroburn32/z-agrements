<!DOCTYPE html>
<html>

<head>
    <script type="text/javascript" data-pace-options='{ "ajax": true }' src="{{asset('backend/js/pace.min.js')}}"></script>
    <link href="{{asset('backend/css/pace.css')}}" rel="stylesheet">

    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
    <title>Accueil | SIGAT</title>
    <!--global css starts--> 
    {{-- <script type="text/javascript" src="{{ asset('backend/js/pace.js')}}"></script> --}}
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/pace_blue/pace-theme-loading-bar.css')}}" /> --}}
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('favicon.ico')}}" >
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/jquery-ui.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/app.backend.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/style_frontend.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/my_accordion.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/font-awesome.min.css')}}">
    <link href="{{ captcha_layout_stylesheet_url() }}" type="text/css" rel="stylesheet" />
<style>
    .loader {
        border: 5px solid #f3f3f3;
        border-radius: 50%;
        border-top: 5px solid #3498db;
        width: 40px;
        height: 40px;
        -webkit-animation: spin 2s linear infinite;
        /* Safari */
        animation: spin 2s linear infinite;
    }

    /* Safari */
    @-webkit-keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
        }
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }

        100% {
            transform: rotate(360deg);
        }
    }
</style>

<body style="width:auto; height:auto;" >
    
    <header>
        <div class="container" style="padding-top:8px;" id="monheader">
            <div class="row">
                <div class="col-md-2 col-sm-12" style="text-align:center; ">
                    <img src="frontend/img/armoirie-bf.png" alt="Armoirie du Faso" height="80px" width="80px" />
                </div>
                <div class="col-md-8" id="title" style="text-align:center; font-family:'Times New Roman', Times, serif;">
                    <h2 style="font-weight:bolder; color:cadetblue;">Ministère de l'Eau et de l'Assainissement</h2>
                    <h4 style="color:steelblue">| Plateforme de Vérification des Agréments Techniques |</h4>
                </div>
                <div class="col-md-2  col-sm-12" style="text-align:center;" id="logo2">
                    <img src="frontend/img/armoirie-bf.png" alt="Armoirie du Faso" height="80px" width="80px" />
                </div>
            </div>
            <div class="row">
                <img src="frontend/img/bande-drapeau-bf.jpg" alt="Armoirie du Faso" width="100%" />
            </div>
        </div>
    </header>
    
    <!-- //Header End -->
    <!--Carousel Start -->
    
    <div class="container" id="container">
        <div class="progress d-none" id="progress" style="width:10%; background-color:red;"></div>
        
        <div style="text-align:center; font-family:monospace; padding-top:1%;">
            <div class="loader d-none">  </div>
                <h4 style="color:gray;"> Vérification d'un agrément 
                <button class=" btn btn-info fa fa-question-circle pull-right" type="button" id="#btnHelp" data-toggle="modal" data-target="#myModalHelp"
                    title="Aide" type="submit" style="font-size:20px;"></button>
                <button class=" btn btn-info fa fa-info-circle pull-right" type="button" id="#formFaq" data-toggle="modal" data-target="#formFaq"
                    title="Informations Générales" type="submit" style="font-size:20px;"></button>
                <button class=" btn btn-info fa fa-envelope pull-right" class="btn btn-primary" id="#formCon" data-toggle="modal"
                    data-target="#formConModal" title="Envoyer Mail" type="submit" style="font-size:20px;"></button>
                <button type="submit" id="btnPrint" class=" d-none btn btn-info fa fa-print pull-right" title="Imprimer"
                    style="font-size:14px;"></button>
                    <form class="d-none" id="printFrm" action="{{ route('imprimer') }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="agrement1" id="forAgr1" required>
                        <input type="hidden" name="ifu1" id="forIFU1" required>
                    </form>
                </h4>
                {{-- <button class=" btn btn-info fa fa-print pull-right" class="btn btn-primary" id="btnP" title="Imprimer" type="submit" style="font-size:20px;"></button>
                </h4> --}}
        </div>
        
        <div class="row " id="searchForm" style="padding-top:10%;">
            <div class="" style=" margin:auto; padding-bottom:0px;">
                <form class="form-group col-sm" id="findAgrementFrm" action="{{ route('findAgrement') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="row" style="justify-content:center;">
                        <div class="form-group col-sm">
                            <label>Numéro Agrément <span style="color:red">*</span></label>
                            <input type="text" name="agrement" class="form-control" id="forAgr" size="20px"
                                placeholder="Ex: 15-011/W-AEP" required>
                        </div>
                        <div class="form-group col-sm" >
                            <label>Numéro IFU<span style="color:red">*</span></label>
                            <input type="text" name="ifu" class="form-control" id="forIFU"
                                placeholder="Ex: 00047151 E" required>
                        </div>
                    </div>                         
                    <div class="" style="text-align:center;">
                        <button type="submit" id="btnR" class="btn btn-primary">
                            <i class="livicon" data-name="search" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                                Rechercher 
                            </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="alert fade show d-none col-sm-8" role="alert" id="noresult" style="background-color : moccasin; text-align:center; margin: auto; " >
            <h4 class="alert-heading" style="color : red;" >Aucun resultat trouvé !</h4>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
            </button>
            <p><b>Aucune donnée n'a été retrouvée dans notre base avec les références fournies.</b></p>
            <p>Veuillez vérifier vos critères de recherche puis réessayez.</p>
            <p> Pour une vérification approfondie, veuillez contacter le Ministère de l'Eau et de l'Assainissement.
                <p> Email : contact@mea.gov.bf </p>
                <p> Tel : +226 25 49 89 00 </p>
            </p>
        </div>
        <div class="row d-none" id="result" style="margin-top:0%; padding-top:0px;">
            <div class="col-sm-1"></div>
            <div class="portlet box col-sm-10" id="my_portlet" style="margin-bottom:0px; background-color:green;">
                <div class="portlet-title">
                    <h5><i class="livicon" data-name="comments" data-size="14" data-loop="true" data-c="#fff"
                            data-hc="white"></i>
                        <b>Entreprise de </b> Numero agrement <b><u id="numeroAgrement"></b></u> et de Numero IFU <b><u
                                id="numeroIFU"></u></b>
                    </h5>
                    
                </div>
                
                <div class="portlet-body flip-scroll" style="margin-top:0px; padding-top:0px;padding-bottom:0px; " >
                            <table class="table table-striped table-condensed table-bordered " id="mytable">
                                <thead></thead>
                                <tbody>
                                    <tr>
                                        <td id="td1">Raison Sociale</td>
                                        <td id="raisonSociale" class="td_result" ></td>
                                    </tr>
                                    <tr>
                                        <td id="td1">Type Travaux - Domaine</td>
                                        <td class="td_result"><span id="typeTravaux"></span> - <span id="domaine"></span></td>
                                    </tr>
                                    <tr>
                                        <td id="td1">Arrete</td>
                                        <td class="td_result"><span id="numeroArrete"></span> du <span id="dateArrete"></span></td>
                                    </tr>
                                    <tr>
                                        <td id="td1">Categories</td>
                                        <td class="td_result"><span id="categorie"></span></td>
                                    </tr>
                                    <tr>
                                        <td id="td1">Statut</td>
                                        <td class="td_result" id="tdstatut">
                                            <span id="statutAgrement"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="td1">Date d'expiration</td>
                                        <td class="td_result"><span id="date_expiration"></span></td>
                                    </tr>

                                </tbody>
                                <span class="pull-right" style="font-style:italic;font-size:11px;" >Date de dernière mise à jour :
                                    <span id="last_update" style="font-weight:bolder; font-size:11px;"></span>
                                </span>
                            </table>
                            
                </div>
                {{-- <button type="submit" id="btnP" class="btn btn-primary"> Imprimer le PDF </button> --}}
            </div>
        </div>
    </div>

    <footer class="container" style="" id="monfooter">
        <span>
            <p style="padding-top:1%">
            ©  {{ date("Y")}} Ministère de l'Eau et de l'Assainissement - <a href="/" class="footer-copy">mea.gov.bf</a> - Tous
                droits réservés.&nbsp; &nbsp; &nbsp; <a href="/mentions-legales" class="footer-copy"> Mentions
                    Légales</a>
            </p>
        </span>
    </footer>

    <!-- Modal on page Load begin -->

    <div class="modal fade " id="myModalHelp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="Modallabel3dsign" style="text-align:center;font-weight: bolder; font-size: 18px; color: green">
                        <u>Bienvenu sur SIGAT</u> !
                        <p>La Plateforme Informatisée de Gestion des Agréments Techniques</p>
                    </h5>
                    <br />
                    <div class="box-body" style="text-align:center;" >
                        <p style="font-size: 18px;" > La plateforme de vérification des agréments techniques vous permet de vérifier 
                            les agréments des entreprises réalisant des travaux dans le secteur de 
                            l'Eau et de l'Assainissement.  </p>
                        <p style="font-size: 18px;"> Pour vérifier un agrément technique, vous devez disposer du 
                            <em style="color:blue;" >numéro IFU </em> de l'entreprise et du 
                            <em style="color:blue;" >numéro d'agrément </em> 
                        </p>
                        <p style="font-size: 18px;" > Veuillez cliquer sur continuer pour poursuivre la vérification.
                        </p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Continuer ></button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal for Contact-->
    <div class="modal fade" id="formConModal" role="dialog" aria-labelledby="exampleModalLabel" >
        <div class="modal-dialog" role="document" >
            <div class="modal-content" style="font-size:12px;">
                <div class="modal-header" style="padding: 0.3rem; ">
                    <h3>Nous écrire 
                        <!--à l<a href="mailto:{{ config('mailsupport.admin_support_email') }}">administrateur</a>-->
                    </h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="btnEn" action="{{ route('contact_path') }}" onsubmit="return verifForm(this)" method="POST">
                        {{ @csrf_field() }}
                        <input type="hidden" id="api_url" value="{{ route('contact_path') }}">
                        <div class="form-row">
                        <div class="form-group col-md-5">
                            <label for="name" class="control-label">Nom</label>
                            <input type="text" id="name" name="name" class="form-control" placeholder="NOM"
                                required="required" onblur="verifNom(this)">
                        </div>
                        <div class="form-group col-md-7" >
                            <label for="prenom" class="control-label">Prénom</label>
                            <input type="text" id="prenom" name="prenom" class="form-control" placeholder="Prenom"
                                required="required" onblur="verifPrenom(this)">
                        </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="control-label">Email</label>
                            <input type="text" id="email" name="email" class="form-control" size="30" required="required"
                                placeholder="non@email.com" onblur="verifEMail(this)">
                        </div>
                        <div class="form-group">
                            <label for="objet" class="control-label">Objet</label>
                            <input type="text" id="objet" name="objet" class="form-control" required="required"
                                placeholder="Objet" onblur="verifObjet(this)">
                        </div>
                        <div class="form-group">
                            <label for="message" class="col-form-label" class="control-label sr-only">Message</label>
                            <textarea class="form-control" rows="5" cols="10" required="required" name="message"
                                id="message" placeholder="message" onblur="verifMessage(this)"></textarea>

                        </div>
                        {{-- <div class="form-group {{ $errors->first('CaptchaCode', 'has-error') }}"> --}}
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="CaptchaCode" class="control-label">Entrer le code</label>
                                    <input type="text" class="form-control" id="CaptchaCode" name="CaptchaCode" required></span>
                                    <span class="help-block">{{ $errors->first('CaptchaCode', ':message') }}</span>
                                </div>
                                <div class="form-group col-md-8" >
                                    <label for="CaptchaCode" class="control-label"> &nbsp;</label>
                                    <span class="form-item">{!! captcha_image_html('LoginCaptcha') !!}
                                </div>
                            </div>
                       {{--  </div> --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                            <button type="submit" class="btn btn-primary">Envoyer</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <!-- Modal for FAQ-->
    <div class="modal fade" id="formFaq" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="overflow:scroll; border:#000000 1px solid;">
                <div class="modal-header">
                    <h3>Informations générales</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <div class="accordion" id="accordionExample" style="padding:0px;">
                        @foreach ($faqs as $item)
                        <div class="card" style="padding:0px;">
                        <div class="card-header" id="headingOne{{$item->id}}">
                                <h2 class="mb-0">
                                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne{{$item->id}}"><i
                                            class="fa fa-plus"></i> {{ $item->titre}}</button>
                                </h2>
                            </div>
                            <div id="collapseOne{{$item->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    {{ $item->contenu}}
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
            </div>
        </div>
    </div>

    <!-- Modal on page Load End -->
    <script type="text/javascript" src="frontend/js/jquery.min.js"></script>
    <script type="text/javascript" src="frontend/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="frontend/js/jquery-ui.js"></script>
    <script type="text/javascript" src="frontend/js/app.backend.js"></script>
    <script type="text/javascript" src="frontend/js/livicons-1.4.min.js"></script>
    <script type="text/javascript" src="frontend/js/findAgrement.js"></script>
    <script type="text/javascript" src="frontend/js/my_accordion.js"></script>
    <script type="text/javascript" src="frontend/js/nous_ecrire.js"></script>
    <script type="text/javascript" src="frontend/js/sweetalert2.js"></script>
    <script type="text/javascript" src="frontend/js/html2canvas.js"></script>    
    <!--page level js ends-->
    <script type="text/javascript">
        var APP_URL = {!! json_encode(url('/')) !!};
        
        $(document).ready(function() {
            $('#btnPrint').click(function() {
                $('#printFrm').submit();
            });
        });
    </script>
</body>

</html>