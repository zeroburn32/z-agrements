<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}"/>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>
        SIGAT- 
        @section('title')
        @show
    </title>
    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/bootstrap.min.css')}}">
    {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> --}}
    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/bootstrap-toggle.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/AdminLTE.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/_all-skins.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/_all.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/ionicons.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/DataTables/datatables.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/dataTables.bootstrap4.min.css') }}" />
   <link rel="stylesheet"  type="text/css" href="{{asset('backend/css/jquery-ui.min.css')}}">
        
    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/datepicker.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/bootstrap-datetimepicker.min.css')}}">
    
    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/buttons.css')}}" /> 
    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/advbuttons.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/sweetalert.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/jquery.toast.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/vendor/iCheck/css/all.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/mystyle.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/style-backend.css')}}">

    @yield('css')
</head>

<body class="skin-blue sidebar-mini">
@if (!Auth::guest())
    <div class="wrapper">
        <!-- Main Header -->
        <header class="main-header" id="monheader">
            <!-- Logo -->
            
            <a href="{!! url('/home') !!}" class="logo" id ="monheader" >
                    <img src="{{asset('images/dsilogo.png')}}" max-width="60%" height="50px"
                    class="user-image" alt="logo DSI"/>
                <b> S I G A T</b>
            </a>

            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation" id="monheader">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account Menu -->
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar-->
                                <img src="{{asset('images/dsilogo.jpg')}}"
                                             class="user-image" alt="User Image"/>
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs">{!! Auth::user()->name !!}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header" style="background-image:linear-gradient( #e3f2fd,#e8eaf6);">
                                    <img src="{{asset('images/dsilogo.jpg')}}"
                                             class="img-circle" alt="User Image"/>
                                    <p style=" color:black;">
                                        {!! Auth::user()->name !!}
                                        <small>Membre depuis {!! Auth::user()->created_at->format('M. Y') !!}</small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                    <a href="{{ route('changeMyPassword') }}" class="btn btn-default btn-flat">Change password</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{!! url('/logout') !!}" class="btn btn-default btn-flat"
                                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            Me deconnecter
                                        </a>
                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- Left side column. contains the logo and sidebar -->
        @include('layouts.sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper"  id="moncontent">
                <img src="{{asset('images/bande-drapeau-bf.jpg')}}" width="100%" style="padding:0 px; margin:0 px;" />
            @yield('content')
        </div>

        <!-- Main Footer -->
        <footer class="main-footer" style="max-height: 100px;text-align: center" id="monfooter">
            <strong>Copyright © 2019 <a href="http://www.mea.gov.bf" >DSI-MEA - BF</a>.</strong> Tous droits reservés.
        </footer>

    </div>
@else
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header id="monheader"

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{!! url('/') !!}">
                    Gestion des agrements
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{!! url('/home') !!}">Home</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    <li><a href="{!! url('/login') !!}">Se connecter</a></li>
                    <li><a href="{!! url('/register') !!}">Creer un compte</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div id="page-content-wrapper" id="moncontent">
        <div class="container-fluid">
            <div class="row" >
                <div class="col-lg-12">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    @endif
    <script>
        var APP_URL = {!! json_encode(url('/')) !!};
    </script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- AdminLTE App -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/js/adminlte.min.js"></script>
    
    <script type="text/javascript" src="{{asset('backend/DataTables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/js/jquery-ui.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/DataTables/buttons.print.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/DataTables/Buttons-1.6.1/js/buttons.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/js/jquery-ui.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/DataTables/jszip.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/DataTables/pdfmake.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/DataTables/vfs_fonts.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/DataTables/Buttons-1.6.1/js/buttons.html5.min.js')}}"></script>
   
    <script type="text/javascript" src="{{ asset('backend/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/table-responsive.js') }}"></script>
    {{-- <script type="text/javascript" src="{{asset('backend/js/jquery.min.js')}}" ></script> 
    <script type="text/javascript" src="{{asset('backend/js/bootstrap.min.js')}}" ></script>
    <script type="text/javascript" src="{{asset('backend/js/jquery-ui.js')}}" ></script>  --}}
{{--     <script type="text/javascript" src="{{asset('backend/js/app.min.js')}}"></script> --}}
    {{-- <script type="text/javascript" src="{{ asset('backend/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/dataTables.bootstrap4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/table-responsive.js') }}"></script> --}}
    <script type="text/javascript" src="{{asset('backend/js/select2.min.js')}}" ></script> 
    <script type="text/javascript" src="{{asset('backend/dist/i18n/fr.js')}}" ></script> 
    <script type="text/javascript" src="{{asset('backend/js/bootstrap-datepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/js/bootstrap-datetimepicker.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/js/bootstrap-timepicker.js')}}"></script> 

    <script type="text/javascript" src="{{ asset('backend/js/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/jquery.toast.min.js') }}"></script>
    <script type="text/javascript" src="{{asset('backend/js/raphael.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/js/livicons-1.4.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/vendor/iCheck/js/icheck.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/switchery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/bootstrap-switch.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/radio_checkbox.js') }}"></script>
    <script type="text/javascript" src="{{asset('backend/js/myscripts.js')}}" ></script>
    <script type="text/javascript" src="{{asset('backend/js/agrements.js')}}" ></script>
{{--     <script type="text/javascript" src="{{ asset('backend/js/sweetalert2.all.min.js') }}"></script> --}}
 
    

    @include('flashy::message')

    @yield('scripts')
</body>
</html>
