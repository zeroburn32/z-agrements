

<li class="{{ Request::is('agrements*') ? 'active' : '' }}">
    <a href="{!! route('agrements.index') !!}"><i class="fa fa-check"></i><span>Agrements</span></a>
</li>



<li class="{{ Request::is('entreprises*') ? 'active' : '' }}">
    <a href="{!! route('entreprises.index') !!}"><i class="fa fa-home"></i><span>Entreprises</span></a>
</li>
<li class="{{ Request::is('rapport*') ? 'active' : '' }}">
    <a href="{!! route('rapport.index') !!}"><i class="fa fa-bar-chart"></i><span>Rapport</span></a>
</li>


@can('PARAMETRER')
<li class="treeview">
              <a href="#"><i class="fa fa-cog"></i><span>Paramétrages</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                  
<li class="{{ Request::is('categories*') ? 'active' : '' }}">
    <a href="{!! route('categories.index') !!}"><i class="fa fa-edit"></i><span>Categories</span></a>
</li>

<li class="{{ Request::is('domaines*') ? 'active' : '' }}">
    <a href="{!! route('domaines.index') !!}"><i class="fa fa-edit"></i><span>Domaines</span></a>
</li>
{{-- <li class="{{ Request::is('statutAgrements*') ? 'active' : '' }}">
    <a href="{!! route('statutAgrements.index') !!}"><i class="fa fa-edit"></i><span>Statut Agrements</span></a>
</li> --}}

<li class="{{ Request::is('statuts*') ? 'active' : '' }}">
    <a href="{!! route('statuts.index') !!}"><i class="fa fa-edit"></i><span>Statuts</span></a>
</li>

<li class="{{ Request::is('typeTravauxes*') ? 'active' : '' }}">
    <a href="{!! route('typeTravauxes.index') !!}"><i class="fa fa-edit"></i><span>Nature des Travaux</span></a>
</li>


</ul>
  </li>
  @endcan

@can('GERER_LOCALITE')
    
<li class="treeview">
              <a href="#"><i class="fa fa-flag"></i><span>Localités</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                  
<li class="{{ Request::is('regions*') ? 'active' : '' }}">
    <a href="{!! route('regions.index') !!}"><i class="fa fa-edit"></i><span>Regions</span></a>
</li>

<li class="{{ Request::is('provinces*') ? 'active' : '' }}">
    <a href="{!! route('provinces.index') !!}"><i class="fa fa-edit"></i><span>Provinces</span></a>
</li>

<li class="{{ Request::is('communes*') ? 'active' : '' }}">
    <a href="{!! route('communes.index') !!}"><i class="fa fa-edit"></i><span>Communes</span></a>
</li>
              </ul>
            </li>
@endcan
@if ( Auth::user()->can('ADMINISTRER'))
 <li class="treeview">
              <a href="#"><i class="fa fa-users"></i><span>Administration</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                  <li class="{{ Request::is('users*') ? 'active' : '' }}">
                      <a href="{{ url('/admin/users') }}"><i class="fa fa-bars"></i><span>Utilisateurs</span></a>
                 </li>
                 <li class="{{ Request::is('roles*') ? 'active' : '' }}">
                    <a href="{{ url('/admin/roles') }}"><i class="fa fa-bars"></i><span>Rôles</span></a>
               </li>
                <li class="{{ Request::is('activity*') ? 'active' : '' }}">
                    <a href="{{ url('/admin/activitylogs') }}"><i class="fa fa-bars"></i><span>Logs systèmes</span></a>
               </li>
                <li class="{{ Request::is('settings*') ? 'active' : '' }}">
                    <a href="{{ url('/admin/settings') }}"><i class="fa fa-bars"></i><span>Preferences</span></a>
               </li>
              </ul>
            </li>
    @endif

      <img src="{{asset('images/logo_dsi_mea.png')}}" width="70%" style=" background-color:white; margin-left: 10%; margin-top:20%;"/>

