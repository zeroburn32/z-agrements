@extends('layouts.app')
 @section('title')
Tableau de bord
@endsection
@section('content')
<section class="content-header">
    <h1 style="text-align: center;"><i class="livicon" data-name="barchart" data-size="40" data-c="#0072ff"
            data-hc="#0072ff" data-loop="true" style="float: center;"></i>
        <p style="line-height: 20px;">Tableau de bord</p>
    </h1>
</section>
<div class="content">

    <div class="row">
    <div class="col-12 col-sm-6 col-md-4">
        <div class="info-box mb-4">
            <span class="info-box-icon bg-aqua"><i class="fa fa-thumbs-up"></i></span>    
            <div class="info-box-content">
                <span class="info-box-text">Agrements valides</span>
                <span class="info-box-number">{{$nb_agrements_valide}} <span style="font-size:10px; color:blue"> / {{$nb_agrements}}</span></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <div class="col-12 col-sm-6 col-md-4">
        <div class="info-box mb-4">
            <span class="info-box-icon bg-maroon"><i class="fa fa-power-off"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Agrements Expirés</span>
                <span class="info-box-number">{{$nb_agrements_expire}} <span style="font-size:10px; color:blue" > / {{$nb_agrements}}</span> </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <div class="col-12 col-sm-6 col-md-4">
        <div class="info-box mb-4">
            <span class="info-box-icon bg-red"><i class="fa fa-minus-square"></i></span>    
            <div class="info-box-content">
                <span class="info-box-text">Agrements Retirés</span>
                <span class="info-box-number">{{$nb_agrements_retire}} <span style="font-size:10px; color:blue"> / {{$nb_agrements}}</span></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    
    </div>
    <div class="row">
        <div class="col-12 col-sm-6 col-md-4">
        <div id="agrement_by_domaine_chart" style="min-width: 100px; height: 300px; max-width: 400px; "></div>
        </div>
        <div class="col-12 col-sm-6 col-md-4">
        <div id="agrement_exp_by_domaine_chart" style="min-width: 100px; height: 300px; max-width: 400px; "></div>
        </div>
        <div class="col-12 col-sm-6 col-md-4">
        <div id="agrement_ret_by_domaine_chart" style="min-width: 100px; height: 300px; max-width: 400px; "></div>
        </div>
        {{-- <div id="suffrage_chart" style="min-width: 200px; height: 200px; max-width: 600px; margin: 0 auto"></div> --}}
    </div>
    <div class="row">
        <div class="col-12 col-sm-6 col-md-12">
        <div id="evol_nb_agrements_by_domaine_chart" ></div>
        </div>
        </div>
        <table class="table table-responsive" id="tabl_chart">
            <thead>
                <tr>
                     <th>Annéé</th> 
                    <th>AEP</th>
                    <th>AEUE</th>
                    <th>AH</th>
                </tr>
            </thead>
            <tbody>
                @foreach($nb_agrements_by_annee as $item)
                <tr>
                    <td>{!! $item->annee !!}</td>
                    <td>{!! $item->nb_aep !!}</td>
                    <td>{!! $item->nb_aeue !!}</td>
                    <td>{!! $item->nb_ah !!}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
       {{--  <div class="col-12 col-sm-6 col-md-4">
        <div id="agrement_exp_by_domaine_chart" style="min-width: 100px; height: 300px; max-width: 400px; "></div>
        </div>
        <div class="col-12 col-sm-6 col-md-4">
        <div id="agrement_ret_by_domaine_chart" style="min-width: 100px; height: 300px; max-width: 400px; "></div>
        </div> --}}
        {{-- <div id="suffrage_chart" style="min-width: 200px; height: 200px; max-width: 600px; margin: 0 auto"></div> --}}
    </div>
</div>
@endsection

@section('scripts')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script>
Highcharts.chart('evol_nb_agrements_by_domaine_chart', {
    data: {
        table: 'tabl_chart'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: "Evolution du nombre d'agrements par domaine"
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Nombre agrements'
        }
    },
    tooltip: {
        pointFormat: '{series.name} <b> {point.y} </b>'
        /* formatter: function () {
            return '<b> {series.name} </b><br/>{point.name}.toLowerCase() {point.y}' ;
        } */
    },
    lang : {
            downloadCSV : 'Télécharger CSVs',
            downloadJPEG: 'Télécharger JPEG',
            downloadPDF: ' Télécharger PDF',
            downloadPNG :'Télécharger PNG',
            downloadSVG: ' Télécharger SVG',
            downloadXLS:'Télécharger Excel',
            printChart: 'Imprimer',
            openInCloud: 'Ouvrir en ligne',
            viewData: 'Voir les données'
        },
        exporting: {
            buttons: {
                 contextButton: {
                                    menuItems: [
                                        {
                                        text: 'Télécharger PDF',
                                        onclick: function () {
                                            this.exportChart({
                                                type: 'application/pdf'
                                            });
                                        }
                                    }, {
                                        text: 'Télécharger PNG',
                                        onclick: function () {
                                                this.exportChart({
                                                type: 'application/png'
                                            });
                                        },
                                        separator: false
                                    }, {
                                        text: 'Télécharger XLSX',
                                        onclick: function () {
                                            this.downloadXLS();
                                        }
                                    }                                       
                                ],
                                //symbol:"<i class='fa fa-home'></i>"
                },
                exportButton: {
                    enabled: false
                }   
            }
        }
});
 
 $('#tabl_chart').hide();

    // Arements valides par domaines
    Highcharts.chart('agrement_by_domaine_chart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Agrements valides par domaine'
        }, 
        tooltip: {
            pointFormat: '{series.name} <b> {point.y} => {point.percentage:.1f}%</b>'
        },
        lang : {
            downloadCSV : 'Télécharger CSVs',
            downloadJPEG: 'Télécharger JPEG',
            downloadPDF: ' Télécharger PDF',
            downloadPNG :'Télécharger PNG',
            downloadSVG: ' Télécharger SVG',
            downloadXLS:'Télécharger Excel',
            printChart: 'Imprimer',
            openInCloud: 'Ouvrir en ligne',
            viewData: 'Voir les données'
        },
        exporting: {
            buttons: {
                 contextButton: {
                                    menuItems: [
                                        {
                                        text: 'Télécharger PDF',
                                        onclick: function () {
                                            this.exportChart({
                                                type: 'application/pdf'
                                            });
                                        }
                                    }, {
                                        text: 'Télécharger PNG',
                                        onclick: function () {
                                                this.exportChart({
                                                type: 'application/png'
                                            });
                                        },
                                        separator: false
                                    }, {
                                        text: 'Télécharger XLSX',
                                        onclick: function () {
                                            this.downloadXLS();
                                        }
                                    }                                       
                                ],
                                //symbol:"<i class='fa fa-home'></i>"
                },
                exportButton: {
                    enabled: false
                }   
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: '',
            colorByPoint: true,
            data: [{
                name: 'AH',
                y: {{ $agrementAh[0]->valeur}},
                sliced: false,
                selected: false
            }, {
                    name: 'AEUE',
                    y: {{ $agrementAeue[0]->valeur}}
            }, {
                    name: 'AEP',
                    y: {{ $agrementAep[0]->valeur}}
            }
        ]
        }]
    });

    // Agrements expirés by domaine chart
    Highcharts.chart('agrement_exp_by_domaine_chart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Agrements expirés par domaine'
        }, 
        tooltip: {
            pointFormat: '{series.name} <b>{point.y} =>{point.percentage:.1f}%</b>'
        },
        lang : {
            downloadCSV : 'Télécharger CSV',
            downloadJPEG: 'Télécharger JPEG',
            downloadPDF: ' Télécharger PDF',
            downloadPNG :'Télécharger PNG',
            downloadSVG: ' Télécharger SVG',
            downloadXLS:'Télécharger Excel',
            printChart: 'Imprimer',
            openInCloud: 'Ouvrir en ligne',
            viewData: 'Voir les données'
        },
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: [
                                        {
                                        text: 'Télécharger PDF',
                                        onclick: function () {
                                            this.exportChart({
                                                type: 'application/pdf'
                                            });
                                        }
                                    }, {
                                        text: 'Télécharger PNG',
                                        onclick: function () {
                                                this.exportChart({
                                                type: 'application/png'
                                            });
                                        },
                                        separator: false
                                    }, {
                                        text: 'Télécharger XLSX',
                                        onclick: function () {
                                            this.downloadXLS();
                                        }
                                    }                                       
                                ]
                },
                exportButton: {
                    enabled: false
                }   
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: '',
            colorByPoint: true,
            data: [{
                name: 'AH',
                y: {{ $agrementAh_exp[0]->valeur}},
                sliced: false,
                selected: false
            }, {
                    name: 'AEUE',
                    y: {{ $agrementAeue_exp[0]->valeur}}
            }, {
                    name: 'AEP',
                    y: {{ $agrementAep_exp[0]->valeur}}
            }
        ]
        }]
    });

    // Agrements retirés by domaine chart
    Highcharts.chart('agrement_ret_by_domaine_chart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Agrements retirés par domaine'
        }, 
        tooltip: {
            pointFormat: '{series.name} <b>{point.y} =>{point.percentage:.1f}%</b>'
        },
        lang : {
            downloadCSV : 'Télécharger CSVs',
            downloadJPEG: 'Télécharger JPEG',
            downloadPDF: ' Télécharger PDF',
            downloadPNG :'Télécharger PNG',
            downloadSVG: ' Télécharger SVG',
            downloadXLS:'Télécharger Excel',
            printChart: 'Imprimer',
            openInCloud: 'Ouvrir en ligne',
            viewData: 'Voir les données'
        },
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: [
                                        {
                                        text: 'Télécharger PDF',
                                        onclick: function () {
                                            this.exportChart({
                                                type: 'application/pdf'
                                            });
                                        }
                                    }, {
                                        text: 'Télécharger PNG',
                                        onclick: function () {
                                                this.exportChart({
                                                type: 'application/png'
                                            });
                                        },
                                        separator: false
                                    }, {
                                        text: 'Télécharger XLSX',
                                        onclick: function () {
                                            this.downloadXLS();
                                        }
                                    }                                       
                                ]
                },
                exportButton: {
                    enabled: false
                }   
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: '',
            colorByPoint: true,
            data: [{
                name: 'AH',
                y: {{ $agrementAh_ret[0]->valeur}},
                sliced: false,
                selected: false
            }, {
                    name: 'AEUE',
                    y: {{ $agrementAeue_ret[0]->valeur}}
            }, {
                    name: 'AEP',
                    y: {{ $agrementAep_ret[0]->valeur}}
            }
        ]
        }]
    });
</script>
@endsection