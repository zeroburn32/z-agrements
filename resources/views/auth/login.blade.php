<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login | SIGAT</title>
    <!--global css starts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/bootstrapValidator.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/advbuttons.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/login.css') }}">
    {{-- <link href="{{ captcha_layout_stylesheet_url() }}" type="text/css" rel="stylesheet"/> --}}
    <!--end of page level css-->
    <style>
        body{
            background: url({{ asset('backend/mea.jpg') }}) no-repeat center center fixed;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            overflow-x: hidden;

        }
    </style>

</head>
<body>
<div class="container">
    <div id="notific">
        @include('flashy::message')
    </div>
    <!--Content Section Start -->
    <div class="row">
        <div class="box animation flipInX font_size ">
            <div class="box1">
                <div class="text-center">
                    <a href="{{ route('/') }}" class="nav-link">
                        <img src="{{ asset('backend/img/armoirie.png') }}" alt="logo-MEA" style="max-width:100px;" class="img-fluid mar">
                    </a>
                </div>
                <br/>
                <h3 class="text-primary">Connexion</h3>
                <br/>
                <form action="{{ route('authenticate') }}" class="omb_loginForm"  autocomplete="off" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group {{ $errors->first('email', 'has-error') }}">
                        <label class="sr-only">Email</label>
                        <input type="email" class="form-control" name="email" placeholder="Email"
                               value="{!! old('email') !!}">
                        <span class="help-block">{{ $errors->first('email', ':message') }}</span>
                    </div>
                    <br />
                    <div class="form-group {{ $errors->first('password', 'has-error') }}">
                        <label class="sr-only">Password</label>
                        <input type="password" class="form-control" name="password" placeholder="Mot de passe">
                        <span class="help-block">{{ $errors->first('password', ':message') }}</span>
                    </div>
                    
                    <br />
                    <br />
                    <input type="submit" class="btn btn-block btn-primary" value="Connexion">
                    <br />
                    <br />
                </form>
            </div>
        </div>
    </div>
</div>
<!--global js starts-->
<script type="text/javascript" src="{{ asset('backend/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/bootstrapValidator.min.js') }}" ></script>
<script type="text/javascript" src="{{ asset('backend/js/icheck.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/login_custom.js') }}"></script>
<!--global js end-->
</body>
</html>