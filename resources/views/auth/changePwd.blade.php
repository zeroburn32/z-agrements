@extends('backend.template')
@section('title')
    Utlisateurs
    @parent
@stop
@section('header_styles')
    <link rel="stylesheet" href="{{ asset('backend/css/dataTables.bootstrap4.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('backend/css/sweetalert2.min.css') }}"/>
    <link href="{{ asset('backend/css/tables.min.css') }}" rel="stylesheet" type="text/css"/>
@stop
@section('content')
    <section class="content-header">
        <!--section starts-->
        <h1>Changement de mot de passe</h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="livicon" data-name="home" data-size="18" data-loop="true"></i>
                    Accueil
                </a>
            </li>
            <li>
                <a href="#">Utilisateurs</a>
            </li>
            <li class="active">Changement de mot de passe</li>
        </ol>
    </section>
    <!--section ends-->
    <section class="content user_profile">
        <div class="card">
            <div class="card-heading">
                <h3 class="card-title">Changement de mot de passe</h3>
            </div>
            <div class="card-body">
                {{--{!! Form::model($user, ['method' => 'post']) !!}--}}
                {!! Form::model($user, ['route' => ['changeMyPassword'], 'method' => 'patch']) !!}
                {{--<form action="" method="PATCH" data-toogle="validator" id="formPwd">--}}
                <div class="form-body">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="row">
                            <label for="inputoldpassword" class="col-md-3 control-label">Ancien mot de passe
                                <span class='require text-danger'>*</span>
                            </label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-append"><span class="input-group-text">
                                        <i class="livicon" data-name="key" data-size="16" data-loop="true" data-c="#000"
                                           data-hc="#000"></i></span>
                                    </span>
                                    <input type="password" id="old_password" placeholder="Ancien mot de passe"
                                           name="old_password" class="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="inputpassword" class="col-md-3 control-label">Nouveau mot de passe
                                <span class='require text-danger'>*</span>
                            </label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-append"><span class="input-group-text">
                                            <i class="livicon" data-name="key" data-size="16" data-loop="true"
                                               data-c="#000"
                                               data-hc="#000"></i></span>
                                    </span>
                                    <input type="password" id="password" placeholder="Nouveau mot de passe"
                                           name="password" class="form-control" minlength="8"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="inputnumber" class="col-md-3 control-label">Confirmation du nouveau mot de passe
                                <span class='require text-danger'>*</span>
                            </label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-append"><span class="input-group-text">
                                            <i class="livicon" data-name="key" data-size="16" data-loop="true"
                                               data-c="#000" data-hc="#000"></i></span>
                                    </span>
                                    <input type="password" id="password-confirm"
                                           placeholder="Confirmation du nouveau mot de passe"
                                           name="confirm_password" class="form-control" minlength="8"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="col-md-offset-3 col-md-9 ml-auto">
                        <input type="reset" class="btn btn-sm btn-warning" value="Annuler">&nbsp;
                        <input type="submit" class="btn btn-sm btn-success" value="Changer">
                        {{--<a onclick="changerPassword()" class="btn btn-sm btn-success">Changer</a>--}}
                    </div>
                </div>
                {{--</form>--}}
                {!! Form::close() !!}
            </div>
        </div>

    </section>
@endsection

@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/backend/sweetalert2.all.min.js') }}"></script>
    <script type="text/javascript">
        function changerPassword() {
            var csrf_token = $('meta[name="_token"]').attr('content');
            swal({
                title: 'Confirmation?',
                text: "Confirmez-vous le changement de votre mot de passe ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Oui',
                confirmButtonColor: '#08C832',
                cancelButtonText: 'Non',
                cancelButtonColor: '#d33',
                reverseButtons: true
            }).then(function (e) {
                if (e.value == true) {
//                    $('input[name=_method]').val('PATCH');
                    $.ajax({
                        type: 'POST',
                        url: "{{url('/changerPassword')}}",
                        data: new FormData($("#formPwd form")[0]), '_method': 'PATCH', '_token': csrf_token,
                        dataType: 'JSON',
                        processData: false,
                        contentType: false,
                        success: function (response) {
                            console.log(response);
                            Swal({
                                title: "Changement de mot de passe !",
                                text: "Le changement de votre mot de passe a été effectuer avec succès !",
                                type: "success",
                                button: "OK"
                            })
                        },
                        error: function () {
                            swal({
                             title: "Changement de mot de passe !",
                             text: "Une erreur est survenue lors du changement de votre mot de passe",
                             type: "error",
                             timer: '3000'
                             })
//                            window.location.href = "admin";
                        }
                    });
                }
            });
        }
    </script>
@stop