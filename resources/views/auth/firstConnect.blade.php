<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login | SIGAT</title>
    <!--global css starts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/font-awesome.min.css') }}">
    <link href="{{ asset('backend/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/advbuttons.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/login.css') }}">
    <link href="{{ captcha_layout_stylesheet_url() }}" type="text/css" rel="stylesheet"/>
    <!--end of page level css-->
    <style>
        body {
            background: url({{ asset('images/frontend/Palais_Kossyam.jpg') }}) no-repeat center center fixed;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            overflow-x: hidden;

        }
    </style>

</head>
<body class="bg_firstConnect">
<div class="container">
    <div id="notific">
        @include('flashy::message')
    </div>
    <!--Content Section Start -->
    <div class="row">
        <div class="box animation flipInX font_size ">
            <div class="box1">
                <div class="text-center">
                    <a href="{{ route('/') }}" class="nav-link">
                        <img src="{{ asset('backend/img/armoirie.png') }}" alt="logo-open_elec" style="max-width:100px;" class="img-fluid mar">
                    </a>
                </div>
                <h3 class="text-primary">Changement de mot de passe</h3>
                <h5 class="text-center" style="background-color: #ff4f0e; color: #fff; border-radius: 4px;">
                    Veuillez changer votre mot de passe par mésure de sécurité!</h5>
                <!-- Notifications -->
                <div id="notific"></div>
                {!! Form::model($user, ['route' => ['firstConnect', $user->id], 'method' => 'patch']) !!}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label>Nouveau mot de passe</label>
                    <input type="password" class="form-control" name="newPassword" minlength="8"
                           placeholder="Nouveau mot de passe">
                </div>
                <div class="form-group">
                    <label>Confirmation du nouveau mot de passe</label>
                    <input type="password" class="form-control" name="confirmPassword" minlength="8"
                           placeholder="Confirmation du nouveau mot de passe">
                </div>
                <input type="submit" class="btn btn-block btn-primary btn-sm" value="Changer">
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @include('flashy::message')
</div>
<!--global js starts-->
<script type="text/javascript" src="{{ asset('backend/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('backend/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('backend/js/icheck.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/login_custom.js') }}"></script>
<!--global js end-->
</body>
</html>