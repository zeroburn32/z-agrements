@extends('layouts.app')
@section('title')
Rapport
@endsection

@section('content')
<section class="content-header">
    <h1 class="pull-left">Critères de recherche</h1>
</section>
<div class="content">
    <div class="clearfix"></div>

    @include('flashy::message')

    <div class="clearfix"></div>
   
    <div class="box box-primary">
                <form action="{{ route('rapport.index') }}" method="GET">
                    {{csrf_field()}}
                <div class="box-body">
                    <div class="form-group col-sm-3">
                        {!! Form::label('Sta_id', 'Statut agrément', ['class' => 'col-sm-12 control-label col-form-label']) !!}
                        {!! Form::select('Sta_id', $statuts, null,['class' => 'form-control input-sm','style' =>
                        'width:100%;','placeholder' => '--Statut agrément--', 'required']) !!}
                    </div>
                    <div class="form-group col-sm-3">
                        {!! Form::label('Dom_id', 'Domaine', ['class' => 'col-sm-12 control-label col-form-label']) !!}
                        {!! Form::select('Dom_id', $domaines, null,['class' => 'form-control input-sm','style' =>
                        'width:100%;','placeholder' => '--Domaine--']) !!}
                    </div>
                    <div class="form-group col-sm-2" id="divmilieu">
                        {!! Form::label('Typ_id', 'Type de travaux', ['class' => 'col-sm-12 control-label col-form-label']) !!}
                        {!! Form::select('Typ_id', $typeTravaux, null,['class' => 'form-control input-sm','style' =>
                        'width:100%;','placeholder' => '--Type de travaux--']) !!}
                    </div>
                    <div class="form-group col-sm-2" id="divactivite">
                        {!! Form::label('Cat_id', 'Catégories', ['class' => 'col-sm-4 control-label col-form-label']) !!}
                        <select id="Cat_id" name="Cat_id" class="form-control input-sm" style="width:100%;">
        
                        </select>
                    </div>
        
                    {{-- <div class="form-group col-sm-2">
                            {!! Form::label('Ann_id', 'Année:', ['class' => 'col-sm-4 control-label col-form-label']) !!}
                            {!! Form::select('Ann_id', $annees, null, ['class' => 'form-control input-sm','style' =>
                            'width:100%;' ]) !!}
                        </div> --}}
                    <div class="form-group col-sm-2" style="padding-top: 23px;">
                        {{ Form::button('<i class="glyphicon glyphicon-eye-open">Afficher</i>', ['type' => 'submit', 'class' => 'btn btn-primary btn-sm'] )  }}
        
                    </div>
                    @isset($agrements)
                    <div class="panel-heading">
                        @isset($code_statut)
                        <span>Code statut : </span> <b>{{ $code_statut ?? ''}}</b>
                        @endisset
                        @isset($libelle_domaine )
                        <span>Domaine : </span> <b>{{ $libelle_domaine ?? ''}}</b>
                        @endisset
                        @isset($libelle_type )
                        <span>Nature de travaux : <b></span> {{$libelle_type ?? '' }}</b>
                        @endisset
                        @isset($libelle_categorie )
                        <span>Categorie : </span> <b>{{ $libelle_categorie ?? '' }}</b>
                        @endisset
                    </div>
                    @endisset
                </div>
        </form>
</div>
@isset($agrements)
<div class="box box-primary">
        <div class="box-body">
        
        <div class="box-body table-responsive ">
           
            @include('backend.rapports.resultats')
           
        </div>
       
</div>
</div>
@endisset
</div>

@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('backend/js/rapport.js')}}"></script>

@endsection