          <table class="table table-striped table-responsive" id="tabl">
            <thead>
              <tr>
                <th>ID</th>
                <th>Entreprise</th>
                <th>IFU</th>
                <th>Type Travaux</th>
                <th>Domaine</th>
                <th>Numero Agrement</th>
                <th>Date Arrêté</th>
                <th>Catégories</th>
              </tr>
            </thead>
            <tbody id="mydata">

                    @foreach ($agrements as $i=>$agrements)
                    <tr>
                      <td>{!! $i+1 !!}</td>
                      <td>{!! $agrements->raison_sociale !!}</td>
                      <td>{!! $agrements->numero_ifu !!}</td>
                      <td>{!! $agrements->libelle_type !!}</td>
                      <td>{!! $agrements->libelle_court !!}</td>
                      <td>{!! $agrements->numero_agrement !!}</td>
                      <td>{!! $agrements->date_arrete !!}</td>
                      <td style="color : blue; font-style:bold;">
                        {!! $agrements->categories !!}
                        {{-- @foreach ($agrements->categories as $item)
                        {!! $item->libelle_court !!} ;
                        @endforeach --}}
                      </td>
                    </tr>
                  @endforeach
            </tbody>
        </table>
        {{-- <div id="chartcontainer" style="min-width: 310px; height: 400px; margin: 0 auto"></div> --}}
  