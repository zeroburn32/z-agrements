<!-- Cat Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Cat_id', 'Catégorie') !!}
    {!! Form::number('Cat_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Agr Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Agr_id', 'Agr Id:') !!}
    {!! Form::number('Agr_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('categorieAgrements.index') !!}" class="btn btn-default">Annuler</a>
</div>
