<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $categorieAgrements->id !!}</p>
</div>

<!-- Cat Id Field -->
<div class="form-group">
    {!! Form::label('Cat_id', 'Catégorie') !!}
    <p>{!! $categorieAgrements->Cat_id !!}</p>
</div>

<!-- Agr Id Field -->
<div class="form-group">
    {!! Form::label('Agr_id', 'Agr Id:') !!}
    <p>{!! $categorieAgrements->Agr_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $categorieAgrements->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $categorieAgrements->updated_at !!}</p>
</div>

