<table class="table table-responsive" id="categorieAgrements-table">
    <thead>
        <tr>
            <th>Cat Id</th>
        <th>Agr Id</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($categorieAgrements as $categorieAgrements)
        <tr>
            <td>{!! $categorieAgrements->Cat_id !!}</td>
            <td>{!! $categorieAgrements->Agr_id !!}</td>
            <td>
                {!! Form::open(['route' => ['categorieAgrements.destroy', $categorieAgrements->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('categorieAgrements.show', [$categorieAgrements->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('categorieAgrements.edit', [$categorieAgrements->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Êtes-vous sûr ?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>