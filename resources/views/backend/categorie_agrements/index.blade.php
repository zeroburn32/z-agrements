@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Categorie Agrements</h1>
        <h1 class="pull-right">
           <a class="btn btn-success pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('categorieAgrements.create') !!}">Ajouter</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flashy::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body table-responsive ">
                    @include('backend.categorie_agrements.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

