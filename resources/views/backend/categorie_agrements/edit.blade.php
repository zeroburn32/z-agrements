@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Categorie Agrements
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($categorieAgrements, ['route' => ['categorieAgrements.update', $categorieAgrements->id], 'method' => 'patch']) !!}

                        @include('backend.categorie_agrements.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection