<!-- Libelle Statut Field -->
<div class="form-group col-sm-6">
    {!! Form::label('code_statut', 'Libelle Statut:') !!}
    {!! Form::text('code_statut', null, ['class' => 'form-control', 'readonly'=>'readonly']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('libelle_statut', 'Libelle Statut:') !!}
    {!! Form::text('libelle_statut', null, ['class' => 'form-control']) !!}
</div>

<!-- Observation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('observation', 'Observation:') !!}
    {!! Form::text('observation', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('statuts.index') !!}" class="btn btn-default">Annuler</a>
</div>
