<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $statuts->id !!}</p>
</div>

<!-- Libelle Statut Field -->
<div class="form-group">
    {!! Form::label('libelle_statut', 'Libelle Statut:') !!}
    <p>{!! $statuts->libelle_statut !!}</p>
</div>

<!-- Observation Field -->
<div class="form-group">
    {!! Form::label('observation', 'Observation:') !!}
    <p>{!! $statuts->observation !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $statuts->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $statuts->updated_at !!}</p>
</div>

