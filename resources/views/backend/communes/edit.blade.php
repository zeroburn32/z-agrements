@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Communes
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($communes, ['route' => ['communes.update', $communes->id], 'method' => 'patch']) !!}

                        @include('backend.communes.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection