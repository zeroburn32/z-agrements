@extends('layouts.app')
@section('title')
Communes
@endsection
@section('content')
    <section class="content-header">
        <h1 class="pull-left">Communes</h1>
        <h1 class="pull-right">
           <a class="btn btn-success pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('communes.create') !!}">Ajouter</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flashy::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body table-responsive ">
                    @include('backend.communes.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

