<table class="table table-responsive" id="tabl">
    <thead>
        <tr>
            <th>Region</th>
        <th>Code Commune</th>
        <th>Libelle Commune</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($communes as $communes)
        <tr>
            <td>{!! $communes->Pro_id !!}</td>
            <td>{!! $communes->code_commune !!}</td>
            <td>{!! $communes->libelle_commune !!}</td>
            <td>
                {!! Form::open(['route' => ['communes.destroy', $communes->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('communes.show', [$communes->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('communes.edit', [$communes->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Êtes-vous sûr ?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>