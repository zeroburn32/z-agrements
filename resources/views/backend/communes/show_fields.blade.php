<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $communes->id !!}</p>
</div>

<!-- Region Field -->
<div class="form-group">
    {!! Form::label('Pro_id', 'Province') !!}
    <p>{!! $communes->Pro_id !!}</p>
</div>

<!-- Code Commune Field -->
<div class="form-group">
    {!! Form::label('code_commune', 'Code Commune:') !!}
    <p>{!! $communes->code_commune !!}</p>
</div>

<!-- Libelle Commune Field -->
<div class="form-group">
    {!! Form::label('libelle_commune', 'Libelle Commune:') !!}
    <p>{!! $communes->libelle_commune !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $communes->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $communes->updated_at !!}</p>
</div>

