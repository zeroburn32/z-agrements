<!-- Region Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Pro_id', 'Province') !!}
    {!! Form::number('Pro_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Code Commune Field -->
<div class="form-group col-sm-6">
    {!! Form::label('code_commune', 'Code Commune:') !!}
    {!! Form::text('code_commune', null, ['class' => 'form-control']) !!}
</div>

<!-- Libelle Commune Field -->
<div class="form-group col-sm-6">
    {!! Form::label('libelle_commune', 'Libelle Commune:') !!}
    {!! Form::text('libelle_commune', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('communes.index') !!}" class="btn btn-default">Annuler</a>
</div>
