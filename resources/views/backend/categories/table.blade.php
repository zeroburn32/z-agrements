<table class="table table-responsive" id="tabl">
    <thead>
        <tr>
            <th>Categorie Mère</th>
        <th>Domaine</th>
        <th>Type Travaux</th>
        <th>Libelle Court</th>
        <th>Libelle Long</th>
        <th >Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($categories as $categories)
        <tr>
            <td>{!! $categories->Cat_id !!}</td>
            <td>{!! $categories->dom_court !!}</td>
            <td>{!! $categories->libelle_type !!}</td>
            <td>{!! $categories->libelle_court !!}</td>
            <td>{!! $categories->libelle_long !!}</td>
            <td>
                {!! Form::open(['route' => ['categories.destroy', $categories->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    {{-- <i href="{!! route('categories.show', [$categories->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></i> --}}
                    <a href="{!! route('categories.edit', [$categories->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Êtes-vous sûr ?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>