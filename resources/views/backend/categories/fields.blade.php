<!-- Dom Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('Dom_id', 'Domaine', ['class' => 'control-label col-sm-4']) !!}
    <div class="col-sm-6">
        {!! Form::select('Dom_id', $domaines, ['class' => 'form-control input-sm','style' =>'width:100%;']) !!}
    </div>
</div>

<!-- Typ Id Field -->
<div class="form-group col-sm-12"> 
    {!! Form::label('Typ_id', 'Type de travuax', ['class' => 'control-label col-sm-4']) !!}
    <div class="col-sm-6">
        {!! Form::select('Typ_id', $typetravaux, ['class' => 'form-control input-sm','style' =>'width:100%;']) !!}
    </div>
</div>
<!-- Cat Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('categorie', 'Catégorie Parente', ['class' => 'control-label col-sm-4']) !!}
    <div class="col-sm-6">
        {!! Form::select('Cat_id', $categorieParentes, ['class' => 'form-control input-sm','style' =>
        'width:100%;']) !!}
    </div>
</div>
<!-- Libelle Court Field -->
<div class="form-group col-sm-12">
    {!! Form::label('libelle_court', 'Libelle Court', ['class' => 'control-label col-sm-4']) !!}
    <div class="col-sm-6">
    {!! Form::text('libelle_court', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Libelle Long Field -->
<div class="form-group col-sm-12">
    {!! Form::label('libelle_long', 'Libelle Long', ['class' => 'control-label col-sm-4']) !!}
    <div class="col-sm-6">
    {!! Form::text('libelle_long', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Description Field -->
<div class="form-group col-sm-12">
    {!! Form::label('description', 'Description:', ['class' => 'control-label col-sm-4']) !!}
    <div class="col-sm-6">
        {!! Form::text('description', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-6 col-sm-offset-4">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('categories.index') !!}" class="btn btn-default">Annuler</a>
</div>
