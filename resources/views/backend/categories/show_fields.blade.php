<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $categories->id !!}</p>
</div>

<!-- Cat Id Field -->
<div class="form-group">
    {!! Form::label('Cat_id', 'Cat Id:') !!}
    <p>{!! $categories->Cat_id !!}</p>
</div>

<!-- Dom Id Field -->
<div class="form-group">
    {!! Form::label('Dom_id', 'Dom Id:') !!}
    <p>{!! $categories->Dom_id !!}</p>
</div>

<!-- Typ Id Field -->
<div class="form-group">
    {!! Form::label('Typ_id', 'Typ Id:') !!}
    <p>{!! $categories->Typ_id !!}</p>
</div>

<!-- Libelle Court Field -->
<div class="form-group">
    {!! Form::label('libelle_court', 'Libelle Court:') !!}
    <p>{!! $categories->libelle_court !!}</p>
</div>

<!-- Libelle Long Field -->
<div class="form-group">
    {!! Form::label('libelle_long', 'Libelle Long:') !!}
    <p>{!! $categories->libelle_long !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $categories->description !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $categories->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $categories->updated_at !!}</p>
</div>

