<table class="table table-responsive" id="tabl">
    <thead>
        <tr>
            <th>Libelle Type</th>
        <th>Description</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($typeTravauxes as $typeTravaux)
        <tr>
            <td>{!! $typeTravaux->libelle_type !!}</td>
            <td>{!! $typeTravaux->description !!}</td>
            <td>
                {!! Form::open(['route' => ['typeTravauxes.destroy', $typeTravaux->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('typeTravauxes.show', [$typeTravaux->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('typeTravauxes.edit', [$typeTravaux->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Êtes-vous sûr ?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>