@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Type Travaux
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($typeTravaux, ['route' => ['typeTravauxes.update', $typeTravaux->id], 'method' => 'patch']) !!}

                        @include('backend.type_travauxes.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection