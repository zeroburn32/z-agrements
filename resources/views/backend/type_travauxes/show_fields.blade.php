<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $typeTravaux->id !!}</p>
</div>

<!-- Libelle Type Field -->
<div class="form-group">
    {!! Form::label('libelle_type', 'Libelle Type:') !!}
    <p>{!! $typeTravaux->libelle_type !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $typeTravaux->description !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $typeTravaux->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $typeTravaux->updated_at !!}</p>
</div>

