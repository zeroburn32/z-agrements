<!-- Libelle Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('libelle_type', 'Libelle Type:') !!}
    {!! Form::text('libelle_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('typeTravauxes.index') !!}" class="btn btn-default">Annuler</a>
</div>
