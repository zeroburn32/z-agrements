<!-- Agr Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Agr_id', 'Agr Id:') !!}
    {!! Form::number('Agr_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Sta Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Sta_id', 'Statut') !!}
    {!! Form::number('Sta_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date', 'Date:') !!}
    {!! Form::date('date', null, ['class' => 'form-control','id'=>'date']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Observation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('observation', 'Observation:') !!}
    {!! Form::text('observation', null, ['class' => 'form-control']) !!}
</div>

<!-- Est Actif Field -->
<div class="form-group col-sm-6">
    {!! Form::label('est_actif', 'Est Actif:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('est_actif', 0) !!}
        {!! Form::checkbox('est_actif', '1', null) !!} 1
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('statutAgrements.index') !!}" class="btn btn-default">Annuler</a>
</div>
