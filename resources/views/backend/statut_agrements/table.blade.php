<table class="table table-responsive" id="statutAgrements-table">
    <thead>
        <tr>
            <th>Agr Id</th>
        <th>Sta Id</th>
        <th>Date</th>
        <th>Observation</th>
        <th>Est Actif</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($statutAgrements as $statutAgrements)
        <tr>
            <td>{!! $statutAgrements->Agr_id !!}</td>
            <td>{!! $statutAgrements->Sta_id !!}</td>
            <td>{!! $statutAgrements->date !!}</td>
            <td>{!! $statutAgrements->observation !!}</td>
            <td>{!! $statutAgrements->est_actif !!}</td>
            <td>
                {!! Form::open(['route' => ['statutAgrements.destroy', $statutAgrements->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('statutAgrements.show', [$statutAgrements->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('statutAgrements.edit', [$statutAgrements->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Êtes-vous sûr ?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>