<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $statutAgrements->id !!}</p>
</div>

<!-- Agr Id Field -->
<div class="form-group">
    {!! Form::label('Agr_id', 'Agr Id:') !!}
    <p>{!! $statutAgrements->Agr_id !!}</p>
</div>

<!-- Sta Id Field -->
<div class="form-group">
    {!! Form::label('Sta_id', 'Statut') !!}
    <p>{!! $statutAgrements->Sta_id !!}</p>
</div>

<!-- Date Field -->
<div class="form-group">
    {!! Form::label('date', 'Date:') !!}
    <p>{!! $statutAgrements->date !!}</p>
</div>

<!-- Observation Field -->
<div class="form-group">
    {!! Form::label('observation', 'Observation:') !!}
    <p>{!! $statutAgrements->observation !!}</p>
</div>

<!-- Est Actif Field -->
<div class="form-group">
    {!! Form::label('est_actif', 'Est Actif:') !!}
    <p>{!! $statutAgrements->est_actif !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $statutAgrements->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $statutAgrements->updated_at !!}</p>
</div>

