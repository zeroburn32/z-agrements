
<input type="hidden" id="formMode" name="formMode" value="create"></label>
<input type="hidden" id="ent_id" name="ent_id"></label>
<!-- Region Field -->
<div class="form-group">
    {!! Form::label('Reg_id', 'Région',['class' => 'control-label col-sm-4']) !!}
    <div class="col-sm-6">
        {!! Form::select('Reg_id', [-1 =>'--Regions--' ] + $regions, null,['id' => 'Reg_id','class' =>'form-control input-sm','style' => 'width:100%;']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('Pro_id', 'Province',['class' => 'control-label col-sm-4']) !!}
    <div class="col-sm-6">
        {!! Form::select('Pro_id', [-1 =>'--Provinces--' ] + $provinces, null,['id' => 'Pro_id','class' =>'form-control input-sm','style' => 'width:100%;']) !!}
    </div>
</div>
<!-- Commune Field -->
<div class="form-group">
    {!! Form::label('Com_id', 'Commune',['class' => 'control-label col-sm-4']) !!}
    <div class="col-sm-6">
        {!! Form::select('Com_id', [-1 =>'--Commune--' ] + $communes, null,['id' => 'Com_id','class' =>'form-control
        input-sm','style' => 'width:100%;']) !!}
    </div>
</div>


<!-- Raison Socaile Field -->
<div class="form-group ">
    {!! Form::label('raison_sociale', 'Raison Sociale',['class' => 'control-label col-sm-4']) !!}
    <div class="col-sm-6">
    {!! Form::text('raison_sociale', null, ['id' =>'raison_sociale','class' => 'form-control','required' => 'required']) !!}
    </div>
</div>

<!-- Siege Social Field -->
<div class="form-group ">
    {!! Form::label('siege_social', 'Siège Social',['class' => 'control-label col-sm-4']) !!}
    <div class="col-sm-6">
    {!! Form::text('siege_social', null, ['id' =>'siege_social','class' => 'form-control']) !!}
    </div>
</div>

<!-- Adresse Field -->
<div class="form-group ">
    {!! Form::label('adresse', 'Adresse',['class' => 'control-label col-sm-4']) !!}
    <div class="col-sm-6">
    {!! Form::text('adresse', null, ['id' =>'adresse','class' => 'form-control']) !!}
    </div>
</div>

<!-- Telephone Field -->
<div class="form-group ">
    {!! Form::label('telephone', 'Téléphone',['class' => 'control-label col-sm-4']) !!}
    <div class="col-sm-6">
    {!! Form::text('telephone', null, ['id' =>'telephone','class' => 'form-control','required' => 'required']) !!}
    </div>
</div>

<!-- Registre Commerce Field -->
<div class="form-group ">
    {!! Form::label('registre_commerce', 'Registre de Commerce',['class' => 'control-label col-sm-4']) !!}
    <div class="col-sm-6">
    {!! Form::text('registre_commerce', null, ['id' =>'registre_commerce','class' => 'form-control']) !!}
    </div>
</div>

<!-- Numero Ifu Field -->
<div class="form-group ">
    {!! Form::label('numero_ifu', 'Numéro IFU:',['class' => 'control-label col-sm-4']) !!}
    <div class="col-sm-6">
    {!! Form::text('numero_ifu', null, ['id' =>'numero_ifu','class' => 'form-control','required' => 'required']) !!}
    </div>
</div>

<!-- Personne Responsable Field -->
<div class="form-group ">
    {!! Form::label('personne_responsable', 'Personne Responsable',['class' => 'control-label col-sm-4']) !!}
    <div class="col-sm-6">
    {!! Form::text('personne_responsable', null, ['id' =>'personne_responsable','class' => 'form-control','required' => 'required']) !!}
    </div>
</div>

<!-- Numero Cnss Field -->
<div class="form-group ">
    {!! Form::label('numero_cnss', 'Numero CNSS',['id' =>'', 'class' => 'control-label col-sm-4']) !!}
    <div class="col-sm-6">
    {!! Form::text('numero_cnss', null, ['id' =>'numero_cnss','class' => 'form-control','required' => 'required']) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="form-group" style="text-align: center">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('entreprises.index') !!}" class="btn btn-default">Annuler</a>
</div>
