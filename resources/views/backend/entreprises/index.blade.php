@extends('layouts.app')
@section('title')
Entreprises
@endsection
@section('content')
    <section class="content-header">
        <h1 class="pull-left">Liste des Entreprises</h1>
        @if ( ! Auth::user()->hasRole('invite') )
        <h1 class="pull-right" title="Ajouter une entreprise ou un cabinet" >
           <a class="btn btn-success pull-right" style="margin-top: -10px;margin-bottom: 5px" data-toggle="modal" data-target="#modal-create">
            <i class="fa fa-plus" aria-hidden="true"></i>Ajouter une entreprise</a>
        </h1>
        @endif
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flashy::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body table-responsive ">
                    @include('backend.entreprises.table')
            </div>
        </div>
    </div>
    {{-- Modal Ajout && Modification--}}
    <div class="modal fade pullDown " id="modal-create" role="dialog" aria-labelledby="Modallabel3dsign">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header" style="background-color: forestgreen; font-weight: bolder">
                    <h4 class="modal-title" id="Modallabel3dsign" style="font-weight: bolder; color: white">
                        Ajouter une entreprises
                        <button type="button" class="close" data-dismiss="modal" >×</button>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="box-body table-responsive ">
                        {!! Form::open(['route' => 'entreprises.store', 'class' => 'form-horizontal']) !!}
    
                        @include('backend.entreprises.fields')
    
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="{{ asset('backend/js/localites.js') }}"></script>
@endsection

