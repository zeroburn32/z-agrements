<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $entreprises->id !!}</p>
</div>

<!-- Commune Field -->
<div class="form-group">
    {!! Form::label('Com_id', 'Commune') !!}
    <p>{!! $entreprises->Com_id !!}</p>
</div>

<!-- Region Field -->
<div class="form-group">
    {!! Form::label('Reg_id', 'Région') !!}
    <p>{!! $entreprises->Reg_id !!}</p>
</div>

<!-- Raison Socaile Field -->
<div class="form-group">
    {!! Form::label('raison_socaile', 'Raison Socaile:') !!}
    <p>{!! $entreprises->raison_socaile !!}</p>
</div>

<!-- Siege Social Field -->
<div class="form-group">
    {!! Form::label('siege_social', 'Siege Social:') !!}
    <p>{!! $entreprises->siege_social !!}</p>
</div>

<!-- Adresse Field -->
<div class="form-group">
    {!! Form::label('adresse', 'Adresse:') !!}
    <p>{!! $entreprises->adresse !!}</p>
</div>

<!-- Telephone Field -->
<div class="form-group">
    {!! Form::label('telephone', 'Telephone:') !!}
    <p>{!! $entreprises->telephone !!}</p>
</div>

<!-- Registre Commerce Field -->
<div class="form-group">
    {!! Form::label('registre_commerce', 'Registre Commerce:') !!}
    <p>{!! $entreprises->registre_commerce !!}</p>
</div>

<!-- Numero Ifu Field -->
<div class="form-group">
    {!! Form::label('numero_ifu', 'Numero Ifu:') !!}
    <p>{!! $entreprises->numero_ifu !!}</p>
</div>

<!-- Personne Responsable Field -->
<div class="form-group">
    {!! Form::label('personne_responsable', 'Personne Responsable:') !!}
    <p>{!! $entreprises->personne_responsable !!}</p>
</div>

<!-- Numero Cnss Field -->
<div class="form-group">
    {!! Form::label('numero_cnss', 'Numero Cnss:') !!}
    <p>{!! $entreprises->numero_cnss !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $entreprises->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $entreprises->updated_at !!}</p>
</div>

