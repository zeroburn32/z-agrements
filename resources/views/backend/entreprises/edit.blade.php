@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Entreprises
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($entreprises, ['route' => ['entreprises.update', $entreprises->id], 'method' => 'patch']) !!}

                        @include('backend.entreprises.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection