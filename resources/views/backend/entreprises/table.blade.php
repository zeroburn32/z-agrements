<table class="table table-responsive table-condensed table-striped table-bordered table-hover table-sm table-info" id="tabl">
    <thead>
        <tr class="">
        <th>N°</th>
        {{-- <th>Commune</th> --}}
        {{-- <th>Region</th> --}}
        <th>Responsable</th>
        <th>Nom</th>
        <th style="width:20%;" >Siège Social</th>
        {{-- <th>Adresse</th> --}}
        <th>Telephone</th>
        <th>RCCM</th>
        <th>IFU</th>
        <th>CNSS</th>
        @if ( ! Auth::user()->hasRole('invite') )
            <th>Action</th>
        @endif
       
        </tr>
    </thead>
    <tbody>
    @foreach($entreprises as $i=>$entreprises)
        <tr>
            <td>{!! $i+1 !!}</td>
            <td>{!! $entreprises->personne_responsable !!}</td>
            {{-- <td>{!! $entreprises->libelle_commune !!}</td>
            <td>{!! $entreprises->libelle_region !!}</td> --}}
            <td>{!! $entreprises->raison_sociale !!}</td>
            <td>{!! $entreprises->siege_social !!}</td>
            {{-- <td>{!! $entreprises->adresse !!}</td> --}}
            <td>{!! $entreprises->telephone !!}</td>
            <td>{!! $entreprises->registre_commerce !!}</td>
            <td>{!! $entreprises->numero_ifu !!}</td>
            <td>{!! $entreprises->numero_cnss !!}</td>
            @if ( ! Auth::user()->hasRole('invite') )
            <td>                 
                {!! Form::open(['route' => ['entreprises.destroy', $entreprises->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    {{-- <i href="{!! route('entreprises.show', [$entreprises->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></i> --}}
                    <a href="{!! route('entreprises.edit', [$entreprises->id]) !!}" class='btn btn-primary btn-xs' onclick="loadEntreprise({!!$entreprises->id !!})" data-toggle="modal"
                    data-target="#modal-create" ><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Êtes-vous sûr ?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>