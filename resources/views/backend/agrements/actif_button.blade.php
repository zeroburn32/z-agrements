<div class="input-group select2-bootstrap-append">
    <div class="input-group-btn">
        @if ( Auth::user()->can('RETIRER_AGREMENT') || Auth::user()->can('INVALIDER_AGREMENT') ) 
        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-hand-o-right"> </i>Action
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu ">
            {{-- <li>
                <a href="{!! route('agrements.show', [$id]) !!}" class=' pull-left'><i
                        class="glyphicon glyphicon-eye-open"></i> Plus de détails</a>
            </li> --}}
            @if ( Auth::user()->can('RETIRER_AGREMENT'))
            <li>
                <button onclick="retirer({!! $id !!})" href="#"
                    class='btn-danger'><i class="fa fa-remove " aria-hidden="true"></i> &nbsp; Retirer &nbsp;&nbsp;</button>
            </li>
            @endif
            
            @if ( Auth::user()->can('INVALIDER_AGREMENT'))
            <li>
            <button onclick="toggle({!!$id !!} , 'Agrements','est_actif',0)" href="#"
                    class='btn-warning'><i class="fa fa-minus-circle"></i>&nbsp; &nbsp;Invalider</button>
            </li>
            @endif
        </ul>
        @else 
        -
        @endif
    </div>
</div>