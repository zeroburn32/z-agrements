<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $agrements->id !!}</p>
</div>

<!-- Entreprise Field -->
<div class="form-group">
    {!! Form::label('Ent_id', 'Entreprise:') !!}
    <p>{!! $agrements->Ent_id !!}</p>
</div>

<!-- Type Travaux Field -->
<div class="form-group">
    {!! Form::label('Typ_id', 'Type Travaux') !!}
    <p>{!! $agrements->Typ_id !!}</p>
</div>

<!-- Domaine Field -->
<div class="form-group">
    {!! Form::label('Dom_id', 'Domaine') !!}
    <p>{!! $agrements->Dom_id !!}</p>
</div>

<!-- Numero Agrement Field -->
<div class="form-group">
    {!! Form::label('numero_agrement', 'Numero Agrement:') !!}
    <p>{!! $agrements->numero_agrement !!}</p>
</div>

<!-- Numero Arrete Field -->
<div class="form-group">
    {!! Form::label('numero_arrete', 'Numero Arrete:') !!}
    <p>{!! $agrements->numero_arrete !!}</p>
</div>

<!-- Observation Field -->
<div class="form-group">
    {!! Form::label('observation', 'Observation:') !!}
    <p>{!! $agrements->observation !!}</p>
</div>

<!-- Est Expire Field -->
<div class="form-group">
    {!! Form::label('est_expire', 'Est Expire:') !!}
    <p>{!! $agrements->est_expire !!}</p>
</div>

<!-- Date Expiration Field -->
<div class="form-group">
    {!! Form::label('date_expiration', 'Date Expiration:') !!}
    <p>{!! $agrements->date_expiration !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $agrements->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $agrements->updated_at !!}</p>
</div>

