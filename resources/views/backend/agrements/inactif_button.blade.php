<div class="input-group select2-bootstrap-append">
    <div class="input-group-btn">
        @if ( Auth::user()->can('MODIFIER_AGREMENT') || Auth::user()->can('VALIDER_AGREMENT') || Auth::user()->can('SUPPRIMER_AGREMENT') )
        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-hand-o-right"> </i>Action
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            {{-- <li>
                <a href="{!! route('agrements.show', [$id]) !!}" class=' pull-left'><i
                        class="fa fa-eye-open"></i> Plus de détails</a>
            </li> --}}
            @if ( Auth::user()->can('MODIFIER_AGREMENT'))
            <li>
                <button onclick="loadAgrement({!!$id !!})" class='btn-secondary'
                    data-toggle="modal" data-target="#modal-create"><i class="fa fa-edit"></i>&nbsp;Modifier&nbsp;</button>
            </li>
            @endif
            @if ( Auth::user()->can('VALIDER_AGREMENT'))
            <li>
                <button onclick="toggle({!!$id !!} , 'Agrements','est_actif',1)"
                    class='btn-secondary'> <i class="fa fa-check-square-o"></i>&nbsp;Valider &nbsp;&nbsp;</button>
            </li>
            @endif
            @if ( Auth::user()->can('SUPPRIMER_AGREMENT'))
            <li>
                {!! Form::open(['route' => ['agrements.destroy', $id], 'method' => 'delete']) !!}

                <button class='btn-danger' type="submit"
                    onclick="return confirm('Êtes-vous sûr ?')">
                    <i class="fa fa-trash"></i> Supprimer</button>
                {!! Form::close() !!}
            </li>
            @endif
        </ul>
        @endif
    </div>
</div>