
<!-- Entreprise Field -->
<div class="form-group">
   <label class="control-label col-sm-4">Entreprise</label>
   <div class="col-sm-6"> 
       {!! Form::select('Ent_id', [-1 =>'--Selectionnez l\'entreprise--' ] + $entreprises, null,['id' => 'Ent_id','class' => 'form-control input-sm','style' => 'width:100%;']) !!} 
        
    </div>
    <a class="btn btn-secondary myBtnPlus" style="" data-toggle="modal"
        data-target="#modal-create-entreprise">+</a>
    
</div>

<!-- Type Travaux Field -->
<div class="form-group">
    <label class="control-label col-sm-4">Type de Travaux</label>
    <div class="col-sm-6"> 
        {!! Form::select('Typ_id', [-1 =>'--Selectionnez la nature des travaux--' ] +$typeTravaux, null, ['id' => 'type_travaux', 'class' => 'form-control input-sm','style' => 'width:100%;']) !!} 
    </div>
</div>
<!-- Domaine Field -->
<div class="form-group">
    <label class="control-label col-sm-4">Domaine</label>
    <div class="col-sm-6">
        {!! Form::select('Dom_id', [-1 =>'--Selectionnez le domaine--' ] +$domaines, null,['id' => 'domaine', 'class' => 'form-control input-sm','style' => 'width:100%;']) !!}
    </div>
</div>
<!-- Domaine Field -->
<div class="form-group">
    <label class="control-label col-sm-4">Selectionnez les Catégories</label>
    <div class="col-sm-6" id="cb_categorie">
         {{-- <label class=" checkbox-inline">
            <input type="checkbox" class="square">&nbsp;2</label>
        <label class="checkbox-inline">
            <input type="checkbox" class="square">&nbsp;3</label>  --}}
    </div>
    
</div>

<input type="hidden" id="formMode" name="formMode" value="create"></label>
<input type="hidden" id="agr_id" name="agr_id"></label>

<!-- Numero Agrement Field -->
<div class="form-group">
    <label class="control-label col-sm-4">Numero Agrement</label>
    <div class="col-sm-6">
        {!! Form::text('numero_agrement', null, ['id' => 'numero_agrement','class' => 'form-control','required' => 'required']) !!}
    </div>
</div>

<!-- Numero Arrete Field -->
<div class="form-group">
   {{-- {!! Form::label('numero_arrete', 'Numero Arrete:') !!}--}}
    <label class="control-label col-sm-4">Numero Arrete</label>
    <div class="col-sm-6">{!! Form::text('numero_arrete', null, ['id' => 'numero_arrete','class' => 'form-control']) !!}</div>
</div>
<!-- Date Expiration Field -->
<div class="form-group">
    <label class="control-label col-sm-4">Date de signature</label>
    <div class="col-sm-6">{!! Form::date('date_arrete', null, ['id' => 'date_arrete','class' => 'form-control','id'=>'date_arrete','required' => 'required'])!!} </div>
</div>
{{-- @section('scripts')
    <script type="text/javascript">
        $('#date_arrete').datetimepicker({
            format: 'DD-MM-YYYY',
            useCurrent: false
        })
    </script>
@endsection --}}

<!-- Est Expire Field -->
{{-- <div class="form-group">
    <label class="control-label col-sm-4">Est Expire</label>
    <div class="col-sm-6">
        <label class="checkbox-inline">
            {!! Form::hidden('est_expire', 0) !!}
            {!! Form::checkbox('est_expire', '1', null) !!} 1
        </label>
    </div>
</div> --}}

<!-- Submit Field -->
<div class="form-group col-sm-12" style="text-align: center">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('agrements.index') !!}" class="btn btn-default">Annuler</a>
</div>
