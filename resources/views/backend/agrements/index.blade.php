@extends('layouts.app')
@section('title')
Agrements
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="{{ url('backend/css/sweetalert2.min.css') }}" />
@stop
@section('content')
    <section class="content-header">
        <h1 class="pull-left">Liste des Agrements</h1>
        @can('SAISI')
            <h1 class="pull-right" title="Ajouter un nouvel agrément">
                <a class="btn btn-success pull-right" style="margin-top: -10px;margin-bottom: 5px" data-toggle="modal"
                    data-target="#modal-create">
                    <i class="fa fa-plus" aria-hidden="true"></i> Saisir</a>
                <a class="btn btn-info pull-right" onclick="openModalImport()" style="margin-top: -10px;margin-bottom: 5px" data-toggle="modal"
                    data-target="#modalImport">
                    <i class="fa fa-upload" aria-hidden="true"></i>&nbsp;&nbsp;Importer
                </a>
                
                <a class="btn btn-warning pull-right" style="margin-top: -10px;margin-bottom: 5px" target="_blank"
                    href="{{ url('errors/erreurs_importation.log') }}" title="consulter le log du dernier import">
                    <i class="fa fa-eye fa-lg"></i>&nbsp;&nbsp;Logs
                </a>
            </h1>
        @endcan
    </section>
    <div class="form-group col-sm-offset-4" style="font-size:11px;">
        <label title="Agréments ayant fait l'objet d'un retrait" >
            <input type="radio" name="actif" value="retrait" class="square" />  Retirés
        </label>
        &ensp;&ensp;&ensp;
        <label title="Agréments arrivés à expiration">
            <input type="radio" name="actif" value="expire" class="square" />  Expirés
        </label>
        &ensp;&ensp;&ensp;
        <label title="Agréments en cours de validité dans le système">
            <input type="radio" name="actif" value="valide" class="square" />  Valides
        </label>
        &ensp;&ensp;&ensp;
        <label title="Agréments saisis et dont la saisie a été validée">
            <input type="radio" name="actif" value="actif" class="square" />  Actifs
        </label>
        &ensp;&ensp;&ensp;
        <label title="Agréments saisis mais dont la saisie n'a pas encoré validée">
            <input type="radio" name="actif" value ="inactif" class="square" />  Inactifs
        </label>
    
    </div>
    <div class="content">
        <div class="clearfix"></div>

        @include('flashy::message')

        <div class="clearfix"></div>
        
        <div class="box box-primary">
            
            <div class="box-body table-responsive" id="divAgr" >
                    @include('backend.agrements.table')
            </div>
            <div class="box-body table-responsive" id="divAgr2" >
                    @include('backend.agrements.table_ret_exp')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>

 
     {{-- Modal Ajout && Modification--}}
    
    <div class="modal fade pullDown " id="modal-create" role="dialog" aria-labelledby="Modallabel3dsign">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header" style="background-color: forestgreen; font-weight: bolder">
                    <h4 class="modal-title" id="Modallabel3dsign" style="font-weight: bolder; color: white">
                        Ajouter un Agrement
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="box-body table-responsive ">
                        {{--<form class="form-horizontal" action="{{ route('agrements.store') }}" method="POST"
                        enctype="multipart/form-data">--}}
                        {!! Form::open(['route' => 'agrements.store', 'class' => 'form-horizontal']) !!}
    
                        @include('backend.agrements.fields')
    
                        {!! Form::close() !!}
                        {{--</form>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Ajout && Modification--}}
    <div class="modal fade pullDown " id="modal-create-entreprise" role="dialog" aria-labelledby="Modallabel3dsign">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header" style="background-color: forestgreen; font-weight: bolder">
                    <h4 class="modal-title" id="Modallabel3dsign" style="font-weight: bolder; color: white">
                        Ajouter une entreprises
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="box-body table-responsive ">
                        {!! Form::open(['route' => 'entreprises.store', 'class' => 'form-horizontal', 'id'=>'frmEntreprise']) !!}
                            {{ csrf_field() }}
                         @include('backend.agrements.fields_entreprises') 
    
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal d'importation des agrements --}}

    <div class="modal fade pullDown " id="modalImport" role="dialog" aria-labelledby="Modallabel3dsign">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header" style="background-color: forestgreen; font-weight: bolder">
                    <h4 class="modal-title" id="Modallabel3dsign" style="font-weight: bolder; color: white">
                        Importation des agréments 
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="box-body table-responsive ">
                    <form class="needs-validation" novalidate action="#" method="POST" enctype="multipart/form-data" id="formImport">
                
                        {!! csrf_field() !!}
                        <input id="taille_max" name="taille_max" value="10485760" type="hidden">
                        <div class="form-group">
                            <label for="fichier" class="control-label col-sm-4">Fichier Excel <span class="requit">(xlsx) *</span></label>
                            <div class="col-sm-6">
                                <input id="fichier" type="file" class="form-control input-sm" name="fichier" required
                                    accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                            </div>
                            <span class="btn btn-secondary myBtnPlus" style="font-weight: bold;">Max
                                {{ 10485760 / (1024*1024) }}Mo</span>    
                                </form>                    
                        </div>
                    </div>
                    
                    </div>
                    
                    <div class="modal-footer" style="">
                        <a onclick="importSave()" class="btn btn-sm btn-success pull-right" title="Charger les données dans la base"><i
                                class="fa fa-save" style="padding-right:3px;"></i> Importer</a>
                        <a href="{{ url('imports/canevas.xlsx') }}" target="_blank" title="Télécharger le modèle de données"
                            class="btn btn-sm btn-info pull-right"><i class="fa fa-download"></i>Canevas</a>
                    </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script src="{{ url('backend/js/sweetalert2.all.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/js/localites.js') }}"></script>
<script>
        function openModalImport() {
            $('#modImportTitre').text("Importation des agrements en format Excel");
        }

        function importSave() {
            var tailleMax = parseInt($('#taille_max').val());
            var myFile = document.getElementById("fichier");
            var fsize = myFile.files[0].size,
                fname = myFile.files[0].name,
                fextension = fname.substring(fname.lastIndexOf('.') + 1);
                
            if (fextension.toLowerCase() == "xlsx") {
                if (fsize <= tailleMax) {
                    swal({
                        title: 'CONFIRMATION',
                        text: "Voullez-vous importer ces données ?",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#115e1e',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Oui, importer!',
                        cancelButtonText: 'Non, annuler',
                        reverseButtons: true,
                        showLoaderOnConfirm: true,
                        preConfirm: function () {
                            return new Promise(function (resolve) {
                                $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                                    }
                                });
                                $.ajax({
                                    url: "{{ url('/importData') }}",
                                    type: "POST",
                                    data: new FormData($("#formImport")[0]),
                                    processData: false,
                                    contentType: false,
                                    success: function (data) {
                                        var title = "";
                                        var type = "";
                                        var text = "";
                                        if (data.status == "no-open") {
                                            title = "Erreur";
                                            type = "error";
                                            text = "Echec d'ouverture du fichier";
                                        }
                                        else if (data.status == "no-file") {
                                            title = "Erreur";
                                            type = "error";
                                            text = "Fichier de données non valide";
                                        }
                                        else if (data.status == "success-with-error") {
                                            title = "Opération reussie";
                                            type = "warning";
                                            text = "Importation partielle des données effectuée avec succès.\n Veuillez consulter le fichier log pour plus d'information";
                                        }
                                        else if (data.status == "success") {
                                            title = "Opération reussie";
                                            type = "success";
                                            text = "Agrements importés avec succès !";
                                        }
                                        else if (data.status == "ext-no-valide") {
                                            title = "Erreur";
                                            type = "error";
                                            text = "L'extension du fichier n'est pas valide, veuillez selectionner un fichier .xlsx";
                                        }
                                        else if (data.status == "taille-no-valide") {
                                            console.log(fsize);
                                            title = "Erreur";
                                            type = "error";
                                            text = "La taille du fichier ne doit pas dépassé " + tailleMax / (1024*1024) + "Mo !"
                                        }
                                        swal({
                                            title: title,
                                            type: type,
                                            text: text,
                                            button: "OK !"
                                        }).then(function () {
                                            window.location = "agrements";
                                        });
                                    },
                                    error: function (data) {
                                        swal({
                                            title: 'Erreur',
                                            type: "error",
                                            text: "Une erreure est survenue lors de l'importation des données! Veuillez reessayer!",
                                            button: 'Réessayer !'
                                        });
                                    }
                                });
                            });
                        },
                        allowOutsideClick: false
                    });
                } else {
                    swal({
                        title: 'Erreur',
                        type: "error",
                        text: "La taille du fichier ne doit pas dépassé " + tailleMax / (1024*1024) + "Mo !"
                    });
                }
            } else {
                swal({
                    title: 'Erreur',
                    type: "error",
                    text: "L'extension du fichier n'est pas valide, veuillez selectionner un fichier .xlsx"
                });
            }
        }
</script>


@endsection





