 <!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
    <title>Accueil | SIGAT</title>
    <!--global css starts--> 
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('favicon.ico')}}" >
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/jquery-ui.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/app.backend.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/style_frontend.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/font-awesome.min.css')}}">
    <style>
        
    </style>
</head>

<body style="width:auto; height:auto; margin:2px; background-color:white;">
    <div class="row" id="result" style="">
        <div class="col-sm-1"></div>
        @if ($data->codeStatut == 'VALIDE')
        <div class="portlet box col-sm-12" id="my_portlet" style="margin-bottom:0px; background-color:green;">
            @else
            <div class="portlet box col-sm-10" id="my_portlet" style="margin-bottom:0px; background-color:red;">
                @endif
                <div class="portlet-title">
                    <h5>
                        <i class="livicon" data-name="comments" data-size="14" data-loop="true" data-c="#fff"
                            data-hc="white"></i>
                        <b>Entreprise de </b> Numero agrement <b><u id="numeroAgrement"> {{$data->numero_agrement}}
                        </b></u> et de Numero IFU <b><u id="numeroIFU"> {{$data->numero_ifu}} </u></b>
                    </h5>
                </div>
                <div class="portlet-body flip-scroll" style="margin-top:0px; padding-top:0px;padding-bottom:0px; ">
                    <table class="table table-striped table-condensed table-bordered " id="mytable">
                        <thead></thead>
                        <tbody>
                            <tr>
                                <td id="td1">Raison Sociale</td>
                                <td id="raisonSociale" class="td_result">{{$data->raison_sociale}}</td>
                            </tr>
                            <tr>
                                <td id="td1">Type Travaux - Domaine</td>
                                <td class="td_result">{{$data->libelle_type}} <span id="typeTravaux"></span> - <span
                                        id="domaine">{{$data->libelle_court}}</span></td>
                            </tr>
                            <tr>
                                <td id="td1">Arrete</td>
                                <td class="td_result"><span id="numeroArrete">{{$data->numero_arrete}}</span> du <span
                                        id="dateArrete">{{$data->date_arrete}}</span></td>
                            </tr>
                            <tr>
                                <td id="td1">Categories</td>
                                <td class="td_result"><span id="categorie">
                                        @foreach ($data->categories as $item)
                                        &nbsp;&nbsp;&nbsp;&nbsp;<button id="categories" type="button"
                                            class="btn btn-secondary">{{ $item->libelle_court}}</button>
                                        @endforeach
                                    </span></td>
                            </tr>
                            <tr>
                                <td id="td1">Statut</td>
                                @if ($data->codeStatut == 'VALIDE')
                                <td class="td_result" id="tdstatut" style="background-color:green; color:white">
                                    <span id="statutAgrement">{{$data->statut}}</span>
                                </td>
                                @else
                                <td class="td_result" id="tdstatut" style="background-color:red; color:white">
                                    <span id="statutAgrement">{{$data->statut}}</span>
                                </td>
                                @endif

                            </tr>
                            <tr>
                                <td id="td1">Date d'expiration</td>
                                <td class="td_result"><span id="date_expiration">
                                        {{$data->expiration}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        ({{$data->date_expiration}}) </span></td>
                            </tr>
                        </tbody>
                        <span class="pull-right" style="font-style:italic;font-size:11px;">Date de dernière mise à jour
                            :
                            <span id="last_update"
                                style="font-weight:bolder; font-size:11px;">{{$data->last_update}}</span>
                        </span>
                    </table>
                    <span class="pull-left" style="font-style:italic;font-size:11px;">Genéré par SIGAT le {{ date('d-m-Y')}}
                    </span>

                </div>
                {{-- <button type="submit" id="btnP" class="btn btn-primary"> Imprimer le PDF </button> --}}
            </div>
        </div>


        <script type="text/javascript" src="frontend/js/jquery.min.js"></script>
        <script type="text/javascript" src="frontend/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="frontend/js/jquery-ui.js"></script>
        <script type="text/javascript" src="frontend/js/app.backend.js"></script>
        <script type="text/javascript" src="frontend/js/livicons-1.4.min.js"></script>
</body>

</html>