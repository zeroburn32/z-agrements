<li class="{{ Request::is('regions*') ? 'active' : '' }}">
    <a href="{!! route('regions.index') !!}"><i class="fa fa-edit"></i><span>Regions</span></a>
</li>

<li class="{{ Request::is('provinces*') ? 'active' : '' }}">
    <a href="{!! route('provinces.index') !!}"><i class="fa fa-edit"></i><span>Provinces</span></a>
</li>

<li class="{{ Request::is('communes*') ? 'active' : '' }}">
    <a href="{!! route('communes.index') !!}"><i class="fa fa-edit"></i><span>Communes</span></a>
</li>

<li class="{{ Request::is('agrements*') ? 'active' : '' }}">
    <a href="{!! route('agrements.index') !!}"><i class="fa fa-edit"></i><span>Agrements</span></a>
</li>

<li class="{{ Request::is('agrements*') ? 'active' : '' }}">
    <a href="{!! route('agrements.index') !!}"><i class="fa fa-edit"></i><span>Agrements</span></a>
</li>

<li class="{{ Request::is('categorieAgrements*') ? 'active' : '' }}">
    <a href="{!! route('categorieAgrements.index') !!}"><i class="fa fa-edit"></i><span>Categorie Agrements</span></a>
</li>

<li class="{{ Request::is('categories*') ? 'active' : '' }}">
    <a href="{!! route('categories.index') !!}"><i class="fa fa-edit"></i><span>Categories</span></a>
</li>

<li class="{{ Request::is('domaines*') ? 'active' : '' }}">
    <a href="{!! route('domaines.index') !!}"><i class="fa fa-edit"></i><span>Domaines</span></a>
</li>

<li class="{{ Request::is('entreprises*') ? 'active' : '' }}">
    <a href="{!! route('entreprises.index') !!}"><i class="fa fa-edit"></i><span>Entreprises</span></a>
</li>

<li class="{{ Request::is('statutAgrements*') ? 'active' : '' }}">
    <a href="{!! route('statutAgrements.index') !!}"><i class="fa fa-edit"></i><span>Statut Agrements</span></a>
</li>

<li class="{{ Request::is('statuts*') ? 'active' : '' }}">
    <a href="{!! route('statuts.index') !!}"><i class="fa fa-edit"></i><span>Statuts</span></a>
</li>

<li class="{{ Request::is('typeTravauxes*') ? 'active' : '' }}">
    <a href="{!! route('typeTravauxes.index') !!}"><i class="fa fa-edit"></i><span>Type Travauxes</span></a>
</li>

