<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $regions->id !!}</p>
</div>

<!-- Code Region Field -->
<div class="form-group">
    {!! Form::label('code_region', 'Code Region:') !!}
    <p>{!! $regions->code_region !!}</p>
</div>

<!-- Libelle Region Field -->
<div class="form-group">
    {!! Form::label('libelle_region', 'Libelle Region:') !!}
    <p>{!! $regions->libelle_region !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $regions->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $regions->updated_at !!}</p>
</div>

