<table class="table table-responsive" id="tabl">
    <thead>
        <tr>
            <th>Code Region</th>
        <th>Libelle Region</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($regions as $regions)
        <tr>
            <td>{!! $regions->code_region !!}</td>
            <td>{!! $regions->libelle_region !!}</td>
            <td>
                {!! Form::open(['route' => ['regions.destroy', $regions->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('regions.show', [$regions->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('regions.edit', [$regions->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Êtes-vous sûr ?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>