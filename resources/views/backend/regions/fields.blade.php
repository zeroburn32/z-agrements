<!-- Code Region Field -->
<div class="form-group col-sm-6">
    {!! Form::label('code_region', 'Code Region:') !!}
    {!! Form::text('code_region', null, ['class' => 'form-control']) !!}
</div>

<!-- Libelle Region Field -->
<div class="form-group col-sm-6">
    {!! Form::label('libelle_region', 'Libelle Region:') !!}
    {!! Form::text('libelle_region', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('regions.index') !!}" class="btn btn-default">Annuler</a>
</div>
