<table class="table table-responsive" id="tabl">
    <thead>
        <tr>
            <th>Sigle Domaine</th>
        <th>Libelle Domaine</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($domaines as $domaines)
        <tr>
            <td>{!! $domaines->libelle_court !!}</td>
            <td>{!! $domaines->libelle_long !!}</td>
            <td>
                {!! Form::open(['route' => ['domaines.destroy', $domaines->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    {{-- <i href="{!! route('domaines.show', [$domaines->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></i> --}}
                    <a href="{!! route('domaines.edit', [$domaines->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                   {{--  {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Êtes-vous sûr ?')"]) !!} --}}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>