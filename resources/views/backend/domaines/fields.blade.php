<!-- Sigle Domaine Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sigle_domaine', 'Sigle Domaine:') !!}
    {!! Form::text('libelle_court', null, ['class' => 'form-control', 'readonly']) !!}
</div>

<!-- Libelle Domaine Field -->
<div class="form-group col-sm-6">
    {!! Form::label('libelle_domaine', 'Libelle Domaine:') !!}
    {!! Form::text('libelle_long', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('domaines.index') !!}" class="btn btn-default">Annuler</a>
</div>
