@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Modifier un Domaine
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($domaines, ['route' => ['domaines.update', $domaines->id], 'method' => 'patch']) !!}

                        @include('backend.domaines.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection