<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $domaines->id !!}</p>
</div>

<!-- Sigle Domaine Field -->
<div class="form-group">
    {!! Form::label('sigle_domaine', 'Sigle Domaine:') !!}
    <p>{!! $domaines->sigle_domaine !!}</p>
</div>

<!-- Libelle Domaine Field -->
<div class="form-group">
    {!! Form::label('libelle_domaine', 'Libelle Domaine:') !!}
    <p>{!! $domaines->libelle_domaine !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $domaines->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $domaines->updated_at !!}</p>
</div>

