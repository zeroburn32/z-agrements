<table class="table table-responsive" id="tabl">
    <thead>
        <tr>
            <th>Region</th>
        <th>Code Province</th>
        <th>Libelle Province</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($provinces as $provinces)
        <tr>
            <td>{!! $provinces->Reg_id !!}</td>
            <td>{!! $provinces->code_province !!}</td>
            <td>{!! $provinces->libelle_province !!}</td>
            <td>
                {!! Form::open(['route' => ['provinces.destroy', $provinces->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('provinces.show', [$provinces->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('provinces.edit', [$provinces->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Êtes-vous sûr ?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>