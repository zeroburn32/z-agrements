<!-- Region Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Reg_id', 'Région') !!}
    {!! Form::number('Reg_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Code Province Field -->
<div class="form-group col-sm-6">
    {!! Form::label('code_province', 'Code Province:') !!}
    {!! Form::text('code_province', null, ['class' => 'form-control']) !!}
</div>

<!-- Libelle Province Field -->
<div class="form-group col-sm-6">
    {!! Form::label('libelle_province', 'Libelle Province:') !!}
    {!! Form::text('libelle_province', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('provinces.index') !!}" class="btn btn-default">Annuler</a>
</div>
