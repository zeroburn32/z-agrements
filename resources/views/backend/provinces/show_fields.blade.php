<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $provinces->id !!}</p>
</div>

<!-- Region Field -->
<div class="form-group">
    {!! Form::label('Reg_id', 'Région') !!}
    <p>{!! $provinces->Reg_id !!}</p>
</div>

<!-- Code Province Field -->
<div class="form-group">
    {!! Form::label('code_province', 'Code Province:') !!}
    <p>{!! $provinces->code_province !!}</p>
</div>

<!-- Libelle Province Field -->
<div class="form-group">
    {!! Form::label('libelle_province', 'Libelle Province:') !!}
    <p>{!! $provinces->libelle_province !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $provinces->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $provinces->updated_at !!}</p>
</div>

