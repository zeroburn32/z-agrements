Bonjour Mr. <span style="font-weight: bold; font-size: 12px">{!! $nom !!} !
<br>
Votre compte d'accès à la plateforme SIGAT a été réinitialisé.
<br>
<span style="font-weight: bold; font-size: 12px">{!! $contenu !!}</span>
<br>
<div>Vous serez invité à changer le mot de passe dès la première connexion.</div>
<br>
<div><a href="www.agrement.eaubf.net" type="btn btn-primary">Accedez à SIGAT </a></div>
<br>
<div>Cordialement !</div>
<div>Le comité de gestion des agréments techniques</div>