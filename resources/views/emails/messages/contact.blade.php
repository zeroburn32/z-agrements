<div class="col col-8" style="padding-right:30%;padding-left;20%;">
Bonjour cher gestionnaire des agrements techniques.</br>
Vous avez réçu un nouveau mail de :</br>
    <hr/>
    <b>NOM     : </b> {{ $name }} <br/>
    <b>Prénoms : </b> {{ $prenom }}<br/>
    <b>E-mail  : </b> {{ $email }}<br/>
    
    <hr />
    <b>Objet : </b> {{ $objet }}<br />
    <b>Message : </b> <br/>
<div class="row offset-sm-4" style="background-color:lightgray;">
{{ $contenu }}
</div>
<hr />
<br />
<br />
<span style="color:blue;" > <a href="https://www.agrement.eaubf.net"> Plateforme SIGAT </a></span>  <br/>
</div>