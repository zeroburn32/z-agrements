<div class="form-group{{ $errors->has('key') ? 'has-error' : ''}} col-sm-12">
    {{-- {!! Form::label('key', 'Key', ['class' => 'control-label']) !!} --}}
    {!! Form::hidden('key', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control', 'readonly']) !!}
</div>
<div class="form-group{{ $errors->has('value') ? 'has-error' : ''}} col-sm-12">
    {!! Form::label('value', 'Entrer la nouvelle valeur ', ['class' => 'control-label']) !!}
    {!! Form::text('value', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
</div>

<div class="form-group" col-sm-12 style="padding-left:1em; padding-top:1em; ">
    {!! Form::submit($formMode === 'edit' ? 'Modifier' : 'Create', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('settings.index') !!}" class="btn btn-danger">Annuler</a>
</div>
