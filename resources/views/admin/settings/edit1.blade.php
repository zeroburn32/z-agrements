@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Modifier le paramètre système
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                        {!! Form::model($setting, [
                            'method' => 'PATCH',
                            'url' => ['/admin/settings', $setting->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('admin.settings.form1', ['formMode' => 'edit'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
