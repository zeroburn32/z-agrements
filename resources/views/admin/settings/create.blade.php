@extends('layouts.app')

@section('content')
   {{-- <section class="content-header">
        <h1>
             Nouveau paramètre système 
        </h1>
    </section> --}}
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                        {!! Form::open(['url' => '/admin/settings', 'class' => 'form-horizontal', 'files' => true]) !!}

                          @include ('admin.settings.form', ['formMode' => 'create']) 

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
    </div>
@endsection
