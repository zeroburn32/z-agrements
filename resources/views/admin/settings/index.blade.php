@extends('layouts.app')
@section('title')
<title>  - Les preférences </title>
@endsection
@section('content')
    <section class="content-header">
        <h1 class="pull-left">
                <i class="livicon" data-name="user" data-size="30" data-c="#0072ff" data-hc="#0072ff" data-loop="true"></i>
                Paramètres systèmes</h1>
        {{--<h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{ url('/admin/settings/create') }}">Ajouter</a> 
        </h1>--}}
    </section>

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message') 

        <div class="clearfix"></div>
        <div class="portlet box primary">
            <div class="portlet-title">
                <h5>
                    <i class="livicon" data-name="responsive" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    <strong id="btitle">Préférences du systèmes </strong>
                </h5>
            </div>
        
            <div class="portlet-body flip-scroll">
                
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="table-responsive" style="width:70%;">
                            <table class="table" id="tabl00">
                                <thead>
                                    <tr>
        
                                        <th>Paramètre</th>
                                        <th>Valeur</th>
                                        {{-- <th>Usage</th> --}}
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($settings as $item)
                                    <tr>
                                        {{--<td>{{ $item->key }}</td> --}}
                                        <td>{{ $item->label }} :</td>
                                        <td>{{ $item->value }}</td>
                                        {{-- <td><code>setting('{{ $item->key }}')</code></td> --}}
                                        <td>
                                            {{--<a href="{{ url('/admin/settings/' . $item->id) }}" title="View Setting"><button
                                                class="btn btn-info btn-sm"><i class="fa fa-eye"
                                                    aria-hidden="true"></i></button></a>
                                            --}}
                                            <a href="{{ url('/admin/settings/' . $item->id . '/edit') }}"
                                                title="Modifier"><button class="btn btn-primary btn-sm"><i
                                                        class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {{--
                                                            {!! Form::open([
                                                                'method' => 'DELETE',
                                                                'url' => ['/admin/settings', $item->id],
                                                                'style' => 'display:inline'
                                                            ]) !!}
                                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                                        'type' => 'submit',
                                                                        'class' => 'btn btn-danger btn-sm',
                                                                        'title' => 'Delete Setting',
                                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                                )) !!}
                                                            {!! Form::close() !!}
                                                            --}}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $settings->appends(['search' =>
                                Request::get('search')])->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- 
        <div class="box box-primary">
            <div class="box-body">
                <div class="table-responsive" style="width:70%;">
                            <table class="table" id="tabl00" >
                                <thead>
                                    <tr>
                                        
                                        <th>Paramètre</th>
                                        <th>Valeur</th>
                                        <th>Usage</th> 
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($settings as $item)
                                    <tr>
                                        {{--<td>{{ $item->key }}</td> 
                                        <td>{{ $item->key }} :</td>
                                        <td>{{ $item->value }}</td>
                                        <td><code>setting('{{ $item->key }}')</code></td> 
                                        <td>
                                            {{--<a href="{{ url('/admin/settings/' . $item->id) }}" title="View Setting"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a> 
                                            <a href="{{ url('/admin/settings/' . $item->id . '/edit') }}" title="Modifier"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {{--
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/settings', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete Setting',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                           
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $settings->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div> --}}
@endsection
