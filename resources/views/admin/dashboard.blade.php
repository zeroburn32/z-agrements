{{--@extends('backend.layouts.backend')--}}
{{--@extends('back.templates.default')--}}
@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            {{--@include('admin.sidebar')--}}
            <div class="col-md-6">
                @foreach($laravelAdminMenus->menus as $section)
                    @if($section->items)
                        <div class="card">
                            <div class="card-header">
                                {{ $section->section }}
                            </div>

                            <div class="card-body">
                                <ul class="nav flex-column" role="tablist">
                                    @foreach($section->items as $menu)
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link" href="{{ url($menu->url) }}">
                                                {{ $menu->title }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <br/>
                    @endif
                @endforeach
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        Your application's dashboard.
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
