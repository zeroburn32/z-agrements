@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1>
        Ajouter un utilisateur
    </h1>
</section>
<div class="content">
    @include('adminlte-templates::common.errors')
    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
                {!! Form::open(['url' => '/admin/users', 'class' => 'form-horizontal']) !!}

                @include ('admin.users.form', ['formMode' => 'create'])

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection