@extends('layouts.app')
@section('title')
    Utilisateurs
    @parent
@stop
@section('header_styles')
    {{-- <link rel="stylesheet" href="{{ asset('backend/css/dataTables.bootstrap4.css') }}"/>
    <link href="{{ asset('backend/css/tables.css') }}" rel="stylesheet" type="text/css"/>--}}
    <link rel="stylesheet" href="{{ asset('backend/css/sweetalert2.min.css') }}"/> 
@stop
@section('content')
    <section class="content-header">
        <h1 class="pull-left">Liste des utilisateurs</h1>
        <h1 class="pull-right">
            <i class="livicon" data-name="users" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
            <span class="pull-right">
                <a class="btn btn-success" href="{{ route('users.create') }}" title="Ajouter un utilisateur">
                    <i class="livicon" data-name="plus" data-size="20" data-c="#fff" data-hc="#fff" data-loop="true">Ajouter</i>
                </a>
            </span>
        </h1>
    </section>
    <div class="clearfix"></div>
    
    @include('flash::message')
    
    <div class="clearfix"></div>
    <section class="content">
          
                    <div class="box-body  ">
                        <table class="table table-bordered table-striped table-condensed table-responsive-sm" id="tabl">
                            <thead> 
                            <tr>
                                <th>Nom</th>
                                <th>Email</th>
                                <th>Profil</th>
                                <th>Statut</th>
                                <th class="no-sort" >Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $item)
                                <tr>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ $item->user_role }}</td>
                                    <td>
                                        @if ($item->active == false)
                                            <span class="label label-danger">Désactivé</span>
                                        @else
                                            <span class="label label-success">Actif</span>
                                        @endif
                                    </td>
                                    <td class="text-center" >
                                        <div class='btn-group'>
                                            <button type="button" class="btn btn-xs btn-primary dropdown-toggle"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Actions
                                            </button>
                                            <div class="dropdown-menu">
                                                <a href="{!! route('users.show', [$item->id]) !!}" class='btn btn-default dropdown-item'
                                                   data-toggle="tooltip" data-placement="top" title="Voir détail"><i
                                                            class="fa fa-eye"></i> Voir
                                                    détail
                                                </a>
                                                @if ($item->active == true)
                                                    @if (Auth::user()->id == $item->id)
                                                        <span data-toggle="popover" data-placement="top"
                                                              data-content="Vous ne pouvez pas changer votre profil en étant connecté !">
                                <button class='dropdown-item' disabled style="pointer-events: none;">
                                    <i class="fa fa-edit"></i> Changer de profil
                                </button>
                            </span>
                                                        <span data-toggle="popover" data-placement="top"
                                                              data-content="Vous ne pouvez pas désactiver votre compte en étant connecté !">
                                <button type="submit" class="dropdown-item" disabled style="pointer-events: none;">
                                    <i class="fa fa-lock"></i> Désactiver
                                </button>
                            </span>
                                                        <span data-toggle="popover" data-placement="top"
                                                              data-content="Vous ne pouvez pas réinitialiser votre compte en étant connecté !">
                                <button type="submit" class="dropdown-item" disabled style="pointer-events: none;">
                                    <i class="fa fa-refresh"></i> Réinitialiser
                                </button>
                            </span>
                                                    @else
                                                        <a href="{!! route('users.edit', [$item->id]) !!}"
                                                           class="btn btn-default dropdown-item"
                                                           title="Changer de profil"
                                                           data-toggle="tooltip" data-placement="top">
                                                            <i class="fa fa-edit"></i> Changer de profil
                                                        </a>
                                                        <button onclick="desactiver({{$item->id}},'{{$item->name}}')"
                                                                class="dropdown-item" data-toggle="tooltip"
                                                                data-placement="top"
                                                                title="Désactivé le compte">
                                                            <i class="fa fa-lock"></i> Désactivé
                                                        </button>
                                                        <button onclick="reinitialiser({{$item->id}},'{{$item->name}}')"
                                                                class="dropdown-item" data-toggle="tooltip"
                                                                data-placement="top"
                                                                title="Réinitialisé le mot de passe">
                                                            <i class="fa fa-refresh"></i> Réinitialiser
                                                        </button>
                                                    @endif
                                                @elseif($item->active == false)
                                                    <a href="{!! route('users.edit', [$item->id]) !!}"
                                                       class="btn btn-default dropdown-item"
                                                       title="Changer de profil"
                                                       data-toggle="tooltip" data-placement="top">
                                                        <i class="fa fa-edit"></i> Changer de profil
                                                    </a>
                                                    <button onclick="activer({{$item->id}},'{{$item->name}}')"
                                                            class="dropdown-item"
                                                            @if (Auth::user()->id == $item->id) disabled @endif
                                                            data-toggle="tooltip" data-placement="top"
                                                            title="Activé le compte">
                                                        <i class="fa fa-unlock"></i> Activé
                                                    </button>
                                                    <button onclick="reinitialiser({{$item->id}},'{{$item->name}}')"
                                                            class="dropdown-item" data-toggle="tooltip"
                                                            data-placement="top"
                                                            title="Réinitialisé le mot de passe">
                                                        <i class="fa fa-refresh"></i> Réinitialiser
                                                    </button>
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </section>
@endsection
@section('scripts')
    {{-- <script type="text/javascript" src="{{ asset('backend/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/dataTables.bootstrap4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/table-responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/bootstrap-colorpicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/color-picker.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('backend/js/jasny-bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/sweetalert2.all.min.js') }}"></script>--}}
{{-- <script type="text/javascript" src="{{ asset('backend/js/user_management.js') }}"></script> --}}
<script type="text/javascript" src="{{ asset('backend/js/sweetalert2.all.min.js') }}"></script>
<script>

function activer(id, login) {
    swal({
        title: 'Êtes-vous sûre?',
        text: "De vouloir activé le compte de " + login + " ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Oui, Activer',
        confirmButtonColor: '#08C832',
        cancelButtonText: 'Non, Annuler',
        cancelButtonColor: '#d33',
        reverseButtons: true,
        showLoaderOnConfirm: true,
        preConfirm: function () {
            return new Promise(function (resolve) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{url('/lockAndUnlockAccount')}}/" + id,
                    type: 'PATCH',
                    data: {id: id, '_token': '{{csrf_token()}}'},
                    success: function (response) {
                        swal({
                            title: "Activation !",
                            text: "Compte activer avec succès !",
                            type: "success",
                            button: "OK"
                        })
                        window.location.href = "users";
                    },
                    error: function () {
                        swal({
                            title: "Activation !",
                            text: "Une erreur est survenue lors de l'activation !",
                            type: "error",
                            button: "OK"
                        });
                    }
                });
            });
        },
        allowOutsideClick: false
    });
}

function desactiver(id, login) {
    swal({
        title: 'Êtes-vous sûre?',
        text: "De vouloir désactivé le compte de " + login + " ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Oui, Désactiver',
        confirmButtonColor: '#08C832',
        cancelButtonText: 'Non, Annuler',
        cancelButtonColor: '#d33',
        reverseButtons: true,
        showLoaderOnConfirm: true,
        preConfirm: function () {
            return new Promise(function (resolve) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{url('/lockAndUnlockAccount')}}/" + id,
                    type: 'PATCH',
                    data: {id: id, '_token': '{{csrf_token()}}'},
                    success: function (response) {
                        swal({
                            title: "Désactivation !",
                            text: "Compte désactiver avec succès !",
                            type: "success",
                            button: "OK"
                        });
                        window.location.href = "users";
                    },
                    error: function () {
                        swal({
                            title: "Désactivation !",
                            text: "Une erreur est survenue lors de la désactivation !",
                            type: "error",
                            button: "OK"
                        });
                    }
                });
            });
        },
        allowOutsideClick: false
    });
}

function reinitialiser(id, login) {
    swal({
        title: 'Êtes-vous sûre ?',
        text: "De vouloir réinitialiser le mot de passe du compte de " + login + " ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Oui, Réinitialiser',
        confirmButtonColor: '#08C832',
        cancelButtonText: 'Non, Annuler',
        cancelButtonColor: '#d33',
        reverseButtons: true,
        showLoaderOnConfirm: true,
        preConfirm: function () {
            return new Promise(function (resolve) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{url('/reinitialiserMotDePasse')}}/" + id,
                    type: 'GET',
                    success: function (data) {
                        swal({
                            title: "Opération reussie !",
                            text: "Votre nouveau mot de passe est : " + data,
                            type: "success",
                            button: "OK"
                        });
                    },
                    error: function (data) {
                        swal({
                            title: "Réinitialisation !",
                            text: "Une erreur est survenue lors de l'envoi du mot de passe par mail. Votre nouveau mot de passe est : " + data,
                            type: "error",
                            button: "OK"
                        });
                    }
                });
            });
        },
        allowOutsideClick: false
    });
}
</script>
@stop