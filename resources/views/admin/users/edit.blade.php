@extends('layouts.app')
@section('title')
    Utlisateurs
    @parent
@stop
@section('content')
<section class="content-header">
    <h1>
        Compte de {{$user->name}}
    </h1>
</section>
<div class="content">
    <div class="portlet box ">
        <div class="portlet-title primary">
            <div>
                <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                <strong id="btitle"> Changer de profil</strong>
                <span class="pull-right">
                    {{-- <a class="btn btn-success" href="{{ route('roles.create') }}" title="Ajouter un rôle">
                    <i class="livicon" data-name="plus" data-size="20" data-c="#fff" data-hc="#fff"
                        data-loop="true">Ajouter</i>
                    </a> --}}
                </span>
            </div>
        </div>
        <div class="portlet-body flip-scroll ">

            {!! Form::model($user, [
            'method' => 'PATCH',
            'url' => ['/admin/users', $user->id],
            'class' => 'form-horizontal'
            ]) !!}
            
            @include ('admin.users.form_edit', ['formMode' => 'edit'])
            
            {!! Form::close() !!}

        </div>
    </div>
</div>
@endsection
                        

        