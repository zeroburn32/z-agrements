<div class="row">
    <div class="col-2"></div>
    <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}} col-sm-8">
        {!! Form::label('name', 'Nom: ', ['class' => 'control-label']) !!}
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'readonly'=>'readonly']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-2"></div>
    <div class="col-2"></div>
    <div class="form-group{{ $errors->has('email') ? ' has-error' : ''}} col-sm-8">
        {!! Form::label('email', 'Email: ', ['class' => 'control-label']) !!}
        {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required', 'readonly'=>'readonly']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-2"></div>
    <div class="col-2"></div>
    <div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}} col-sm-8">
        {!! Form::label('role', 'Profile: ', ['class' => 'control-label']) !!}
        {!! Form::select('roles[]', $roles, isset($user_roles) ? $user_roles : [], ['class' => 'form-control', 'multiple' => false]) !!}
    </div>
    <div class="col-2"></div>
    <div class="col-2"></div>
    <div class="form-group col-sm-8" style="padding-left:1em; padding-top:1em; ">
        {!! Form::submit($formMode === 'edit' ? 'Valider' : 'Enregistrer', ['class' => 'btn btn-primary']) !!}
        <a href="{!! url('/admin/users') !!}" class="btn btn-danger">Annuler</a>
    </div>
</div>
{{--
<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}} col-sm-9">
    {!! Form::label('name', 'Nom: ', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}} col-sm-9">
    {!! Form::label('email', 'Email: ', ['class' => 'control-label']) !!}
    {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required', 'readonly'=>'readonly']) !!}
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}} col-sm-9">
    {!! Form::label('role', 'Profile: ', ['class' => 'control-label']) !!}
    {!! Form::select('roles[]', $roles, isset($user_roles) ? $user_roles : [], ['class' => 'form-control', 'multiple' => false, 'readonly'=>'readonly', 'disabled'=>'disabled']) !!}
</div>

<div class="form-group col-sm-9" style="padding-left:1em; padding-top:1em; ">
    {!! Form::submit($formMode === 'edit' ? 'Valider' : 'Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{!! url('/admin/users') !!}" class="btn btn-danger">Annuler</a>
</div>
--}}
