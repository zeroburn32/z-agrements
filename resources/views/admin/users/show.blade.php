@extends('layouts.app')
@section('title')
    Utlisateurs
    @parent
@stop
@section('content')
    <section class="content-header">
        <!--section starts-->
        <h1>Gestion des comptes utilisateurs</h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="livicon" data-name="home" data-size="18" data-loop="true"></i>
                    Accueil
                </a>
            </li>
            <li class="active">Détails d'un compte</li>
        </ol>
    </section>
    <div class="content">
        <div class="portlet box default" style="margin-top: 10px">
            <div class="portlet-title" style="height: 50px">
                <div class="caption">
                    <h5>
                        <i class="livicon" data-name="responsive" data-size="16" data-loop="true" data-c="#fff"
                           data-hc="white"></i>Détails du compte
                    </h5>
                </div>
            </div>
            <div class="portlet-body flip-scroll">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <div class="table-responsive-lg table-responsive-sm table-responsive-md table-responsive">
                            <table class="table table-sm table-bordered table-striped"
                                   id="users">
                                <tr>
                                    <td>Nom & Prénom</td>
                                    <td>
                                        <p class="user_name_max">{{ $user->name }}</p>
                                    </td>

                                </tr>
                                <tr>
                                    <td>E-mail</td>
                                    <td>
                                        {{ $user->email }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Profil</td>
                                    <td>
                                        {{ $user->user_role }}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="flip-scroll table-responsive">
                    <h3>Liste des droits de l'utilisateur</h3>
                    <table class="table table-bordered table-striped table-condensed flip-content example">
                        <thead>
                        <tr>
                            <th>Droit</th>
                            <th>Description</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($roles as $item)
                            <tr>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->label }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <a class="btn btn-warning btn-sm" href="{{ url('/admin/users') }}" title="Retour" style="width: 100%">
            <i class="fa fa-arrow-left" aria-hidden="true"></i> Retour
        </a>
    </div>
@endsection