@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1>
        Modifier un profil
    </h1>
</section>
<div class="content">
    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
            {!! Form::model($role, [
            'method' => 'PATCH',
            'url' => ['/admin/roles', $role->id],
            'class' => 'form-horizontal'
            ]) !!}
            
            @include ('admin.roles.form', ['formMode' => 'edit'])
            
            {!! Form::close() !!}

        </div>
        </div>
        </div>
        </div>
@endsection
            