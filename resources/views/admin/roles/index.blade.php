@extends('layouts.app')
@section('title')
    Utilisateurs
    @parent
@stop
@section('header_styles')
    <link rel="stylesheet" href="{{ asset('backend/css/dataTables.bootstrap4.css') }}"/>
    <link href="{{ asset('backend/css/tables.css') }}" rel="stylesheet" type="text/css"/>
@stop
@section('content') 
<section class="content-header">
    <h1 class="pull-left">Liste des profils</h1>
    <h1 class="pull-right">
        {{-- <i class="livicon" data-name="users" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> --}}
        <span class="pull-right">
            <a class="btn btn-success" href="{{ route('roles.create') }}" title="Ajouter un profil">
                <i class="livicon" data-name="plus" data-size="20" data-c="#fff" data-hc="#fff"
                    data-loop="true">Ajouter</i>
            </a>
        </span>
    </h1>
</section>
    {{-- <section class="content-header">
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="livicon" data-name="users" data-size="18" data-loop="true"></i>
                    Administration
                </a>
            </li>
            <li class="active">
                Profiles
            </li>
        </ol>
        <div class="clearfix"></div>

        @include('flashy::message')

        <div class="clearfix"></div>
    </section> --}}
    <section class="content">
                <div class="box-body  ">
                        <table class="table table-bordered table-striped table-condensed table-responsive-sm" id="tabl"
                               style="width: 100%;">
                            <thead>
                            <tr>
                                {{--<th>ID</th>--}}
                                <th>Profile</th>
                                <th>Descirption</th>
                                <th class="no-sort" style="width: 30px">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($roles as $item)
                                <tr>
                                    {{--<td>{{ $item->id }}</td>--}}
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->label }}</td>
                                    <td>
                                        <a href="{{ url('/admin/roles/' . $item->id . '/edit') }}"
                                           title="Editer le profile">
                                            <button
                                                    class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                                                                      aria-hidden="true"></i></button>
                                        </a>
                                        {{--{!! Form::open([
                                        'method' => 'DELETE',
                                        'url' => ['/admin/roles', $item->id],
                                        'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i>', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-sm',
                                        'title' => 'Supprimer le profile',
                                        'onclick'=>'return confirm("Voulez-vous supprimer ce profile ?")'
                                        )) !!}
                                        {!! Form::close() !!}--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                     </div>
                    </div>
                    </div>
    </section>
@endsection
@section('footer_scripts')
    {{-- <script type="text/javascript" src="{{ asset('backend/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/dataTables.bootstrap4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/table-responsive.js') }}"></script> --}}
@stop