<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}} col-sm-10">
    {!! Form::label('name', 'Profil: ', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>
<div class="form-group{{ $errors->has('label') ? ' has-error' : ''}} col-sm-10">
    {!! Form::label('label', 'Dscription: ', ['class' => 'control-label']) !!}
    {!! Form::text('label', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group{{ $errors->has('label') ? ' has-error' : ''}} col-sm-10">
    {!! Form::label('label', 'Selectionnez les Droits: ', ['class' => 'control-label']) !!}
    {!! Form::select('permissions[]', $permissions, isset($role) ? $role->permissions->pluck('name') : [], ['class' => 'form-control', 'multiple' => true]) !!}
</div>
<div class="form-group col-sm-12" style="padding-left:1em; padding-top:1em; ">
    {!! Form::submit($formMode === 'edit' ? 'Valider' : 'Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('roles.index') !!}" class="btn btn-danger">Annuler</a>
</div>
