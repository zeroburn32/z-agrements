@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1>
        &nbsp;
    </h1>
</section>
<div class="content">
    <div class="portlet box ">
        <div class="portlet-title primary">
            <div>
                <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                <strong id="btitle"> Modifier un droit</strong>
                <span class="pull-right">
                    {{-- <a class="btn btn-success" href="{{ route('roles.create') }}" title="Ajouter un rôle">
                    <i class="livicon" data-name="plus" data-size="20" data-c="#fff" data-hc="#fff"
                        data-loop="true">Ajouter</i>
                    </a> --}}
                </span>
            </div>
        </div>
        <div class="portlet-body flip-scroll ">
            {!! Form::model($permission, [
            'method' => 'PATCH',
            'url' => ['/admin/permissions', $permission->id],
            'class' => 'form-horizontal'
            ]) !!}
            
            @include ('admin.permissions.form', ['formMode' => 'edit'])
            
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection

            