@extends('layouts.app')
@section('title')
    Permissions
    @parent
@stop
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/dataTables.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/tables.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/jasny-bootstrap.css') }}">
@stop
@section('content')
    <section class="content-header">
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="livicon" data-name="users" data-size="18" data-loop="true"></i>
                    Administration
                </a>
            </li>
            <li class="active">
                Droits
            </li>
        </ol>
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box ">
                    <div class="portlet-title primary">
                        <div>
                            <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff"
                               data-hc="white"></i>
                            <strong id="btitle"> Liste des droits</strong>
                            <span class="pull-right">
                        </span>
                        </div>
                    </div>
                    <div class="portlet-body flip-scroll table-responsive">
                        <table class="table table-bordered table-striped table-condensed flip-content example">
                            <thead>
                            <tr>
                                <th>Droit</th>
                                <th>Description</th>
                                <th class="no-sort" style="width: 30px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($permissions as $item)
                                <tr>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->label }}</td>
                                    <td>
                                        <a href="{{ url('/admin/permissions/' . $item->id . '/edit') }}"
                                           title="Modifier la description">
                                            <button class="btn btn-primary btn-sm"><i
                                                        class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('backend/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/dataTables.bootstrap4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/table-responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/bootstrap-colorpicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/color-picker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/jasny-bootstrap.js') }}"></script>
@stop