<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>
        @section('title')
            Open election - Admin  
        @show
    </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <script src="{{ asset('backend/js/html5shiv.js') }}"></script>
    <script src="{{ asset('backend/js/respond.min.js') }}"></script>
    <link href="{{ asset('backend/css/app.backend.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('backend/css/syle-backend.css') }}" rel="stylesheet"/>
    {{-- <link href="{{ asset('backend/css/form2.css') }}" rel="stylesheet"/>
    <link href="{{ asset('backend/css/form3.css') }}" rel="stylesheet"/> --}}
    <link href="{{ asset('backend/css/all.css') }}" rel="stylesheet"/>
    <link href="{{asset('backend/css/fontawesome/css/all.min.css')}}" rel="stylesheet">
    <link href="{{asset('backend/css/style-backend.css')}}" rel="stylesheet">    
    <link rel="stylesheet" href="{{asset('backend/css/select2.min.css')}}">
    <link href="{{ asset('backend/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet"
        type="text/css" />
    <style>
        .colorpicker-right:after {
            top: -16px;
        }
    </style>
    @yield('header_styles')

    <body class="skin-josh">
<header class="header">
    <a href="{{ url('/admin') }}" class="logo">
        <img src="{{ asset('backend/img/open_elec1.png') }}" alt="logo" style="width:140px;">
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
        <div>
            <a href="#sidebar-toggle" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                <div class="responsive_nav"></div>
            </a>
        </div>
        <div class="navbar-right toggle">
            <ul class="nav navbar-nav  list-inline">
                <li class=" nav-item dropdown user user-menu">
                    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                        <img src="{{ asset('backend/img/no_avatar.jpg') }}" alt="img" height="35px" width="35px"
                             class="rounded-circle img-fluid float-left"/>
                        <div class="riot">
                            <div>
                                <p class="user_name_max">{{Auth::user()->name}}</p>
                                <span>
                                        <i class="caret"></i>
                                    </span>
                            </div>
                        </div>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header bg-light-blue">
                            <img src="{{ asset('backend/img/no_avatar.jpg') }}" alt="img" height="35px" width="35px"
                                 class="rounded-circle img-fluid float-left"/>
                        </li>
                        <!-- Menu Body -->
                        <li>
                            <a href="#">
                                <i class="livicon" data-name="user" data-s="18"></i>
                                Mon profil
                            </a>
                        </li>
                        <li role="presentation"></li>
                        <li>
                            <a href="#">
                                <i class="livicon" data-name="gears" data-s="18"></i>
                                Paramètre du compte
                            </a>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#">
                                    <i class="livicon" data-name="lock" data-size="16" data-c="#555555"
                                       data-hc="#555555" data-loop="true"></i>
                                    Verr.
                                </a>
                            </div>
                            <div class="pull-right">
                                <a href="logout">
                                    <i class="livicon" data-name="sign-out" data-s="18"></i>
                                    Deconnexion
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="wrapper ">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side ">
        <section class="sidebar ">
            <div class="page-sidebar  sidebar-nav">
                <ul id="menu" class="page-sidebar-menu">
                @include('backend.menu')
                </ul>
            </div>
        </section>
    </aside>
    <aside class="right-side">
        <!-- Content -->
        <section class="container ">
               @yield('content')
        </section>
    </aside>
    
</div>
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Retour en haut de la page"
   data-toggle="tooltip" data-placement="left">
    <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
</a>
<script src="{{ asset('backend/js/app.backend.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/js/validation.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/js/jasny-bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/js/icheck.js') }}"></script>
<script src="{{ asset('backend/js/bootstrap-colorpicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/js/pages/color-picker.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/js/select2.min.js')}}" ></script> 
<script src="{{ asset('backend/js/ajaxScript-all.js') }}"></script>
<script> 
$('select').select2({
    placeholder: 'Choisir un element ...',
    allowClear: true,
    minimumResultsForSearch: 3
});
</script> 
<script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!}
    console.log(APP_URL);
</script>
@yield('footer_scripts')
<!-- end page level js -->
</body>
</html>
