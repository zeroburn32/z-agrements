@extends('layouts.app')
@section('title')
<title>  - Liste des logs </title>
@endsection
@section('content')
    <section class="content-header">
        <h1 class="pull-left">
                <i class="livicon" data-name="user" data-size="30" data-c="#0072ff" data-hc="#0072ff" data-loop="true"></i>
                Opérations sur le système</h1>
       {{-- <h1 class="pull-right">
           <a class="btn btn-primary pull-right disable"  style="margin-top: -10px;margin-bottom: 5px" href="#">Ajouter</a>
        </h1> --}}
    </section>

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message') 

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <div class="table-responsive" >
                            <table class="table" id="tabl00" >
                                <thead>
                                    <tr>
                                        <th>ID</th><th>Activité</th><th>Acteur</th><th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($activitylogs as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->description }}</td>
                                        <td>
                                            @if ($item->causer)
                                                <a href="{{ url('/admin/users/' . $item->causer->id) }}">{{ $item->causer->name }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>{{ $item->created_at }}</td>
                                        {{--
                                        <td>
                                            <a href="{{ url('/admin/activitylogs/' . $item->id) }}" title="Voir Activité"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/activitylogs', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Supprimer Activité',
                                                        'onclick'=>'return confirm("Confirmer suppression?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td> --}}
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{-- <div class="pagination"> {!! $activitylogs->appends(['search' => Request::get('search')])->render() !!} </div> --}}
                        </div>
                    </div>
                </div>
    </div>
@endsection
