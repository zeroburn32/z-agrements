<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="CategorieAgrements",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="Cat_id",
 *          description="Cat_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="Agr_id",
 *          description="Agr_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class CategorieAgrements extends Model
{

    public $table = 'CategorieAgrements';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'Cat_id',
        'Agr_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'Cat_id' => 'integer',
        'Agr_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function agr()
    {
        return $this->belongsTo(\App\Models\Agrement::class, 'Agr_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function cat()
    {
        return $this->belongsTo(\App\Models\Category::class, 'Cat_id');
    }
}
