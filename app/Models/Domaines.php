<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Domaines",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="libelle_court",
 *          description="libelle_court",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="libelle_long",
 *          description="libelle_long",
 *          type="string"
 *      )
 * )
 */
class Domaines extends Model
{

    public $table = 'Domaines';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'libelle_court',
        'libelle_long'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'libelle_court' => 'string',
        'libelle_long' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function agrements()
    {
        return $this->hasMany(\App\Models\Agrement::class);
    }
}
