<?php

namespace App\Models;

use Eloquent as Model;
use Carbon\Carbon; 

/**
 * @SWG\Definition(
 *      definition="Agrements",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="Ent_id",
 *          description="Ent_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="Typ_id",
 *          description="Typ_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="Dom_id",
 *          description="Dom_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="numero_agrement",
 *          description="numero_agrement",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="numero_arrete",
 *          description="numero_arrete",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="observation",
 *          description="observation",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="est_expire",
 *          description="est_expire",
 *          type="boolean"
 *      )
 * )
 */
class Agrements extends Model
{

    public $table = 'Agrements';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'Ent_id',
        'Typ_id',
        'Dom_id',
        'numero_agrement',
        'est_actif',
        'numero_arrete',
        'date_arrete',
        'observation',
        'est_expire',
        'date_expiration'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'Ent_id' => 'integer',
        'Typ_id' => 'integer',
        'Dom_id' => 'integer',
        'numero_agrement' => 'string',
        'numero_arrete' => 'string',
        'observation' => 'string',
        'est_expire' => 'boolean',
        'est_actif' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function typ()
    {
        return $this->belongsTo(\App\Models\TypeTravaux::class, 'Typ_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function dom()
    {
        return $this->belongsTo(\App\Models\Domaine::class, 'Dom_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function ent()
    {
        return $this->belongsTo(\App\Models\Entreprise::class, 'Ent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function categorieAgrements()
    {
        return $this->hasMany(\App\Models\CategorieAgrements::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function statutAgrements()
    {
        return $this->hasMany(\App\Models\StatutAgrements::class);
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $date_arrete = new Carbon($model->date_arrete);
            $model->date_expiration = $date_arrete->addYears(setting('validite_agrement'));
            $model->numero_agrement = preg_replace('/\s+/', '', $model->numero_agrement);

        });
        static::updating(function ($model) {
            $date_arrete = new Carbon($model->date_arrete);
            $model->date_expiration = $date_arrete->addYears(setting('validite_agrement'));
            $model->numero_agrement = preg_replace('/\s+/', '', $model->numero_agrement);

        });
        /* static::deleting(function ($model) { // before delete() method call this
            $model->statutAgrements()->delete();
            $model->categorieAgrements()->delete();
            // do the rest of the cleanup...
        }); */
    }
}