<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Provinces",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="Reg_id",
 *          description="Reg_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="code_province",
 *          description="code_province",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="libelle_province",
 *          description="libelle_province",
 *          type="string"
 *      )
 * )
 */
class Provinces extends Model
{

    public $table = 'Provinces';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'Reg_id',
        'code_province',
        'libelle_province'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'Reg_id' => 'integer',
        'code_province' => 'string',
        'libelle_province' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function reg()
    {
        return $this->belongsTo(\App\Models\Region::class, 'Reg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function communes()
    {
        return $this->hasMany(\App\Models\Commune::class);
    }
}
