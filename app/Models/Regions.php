<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Regions",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="code_region",
 *          description="code_region",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="libelle_region",
 *          description="libelle_region",
 *          type="string"
 *      )
 * )
 */
class Regions extends Model
{

    public $table = 'Regions';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'code_region',
        'libelle_region'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'code_region' => 'string',
        'libelle_region' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function entreprises()
    {
        return $this->hasMany(\App\Models\Entreprise::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function provinces()
    {
        return $this->hasMany(\App\Models\Province::class);
    }
}
