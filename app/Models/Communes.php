<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Communes",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="Pro_id",
 *          description="Pro_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="code_commune",
 *          description="code_commune",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="libelle_commune",
 *          description="libelle_commune",
 *          type="string"
 *      )
 * )
 */
class Communes extends Model
{

    public $table = 'Communes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'Pro_id',
        'code_commune',
        'libelle_commune'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'Pro_id' => 'integer',
        'code_commune' => 'string',
        'libelle_commune' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function pro()
    {
        return $this->belongsTo(\App\Models\Province::class, 'Pro_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function entreprises()
    {
        return $this->hasMany(\App\Models\Entreprise::class);
    }
}
