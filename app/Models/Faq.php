<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    //
    protected  $fillable = ['titre', 'contenu', 'date_create', 'date_modif'];
}
