<?php

/**
 * Created by PhpStorm.
 * User: abdou
 * Date: 06/04/2020
 * Time: 12:52
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Doctrine\DBAL\Query\QueryException;
use Maatwebsite\Excel\Concerns\ToCollection;

class ImportAgrements implements ToCollection
{
    public function collection(Collection $rows)
    {
        file_put_contents(public_path() . '/errors/erreurs_importation.log', '');
        Log::channel('donneesLog')->info('Fichier ouvert : importation des agrements du ' . new Carbon() . ' !');
        Log::channel('importsLog')->info('###################################################################################################"');
        
        if (!empty($rows) && $rows->count()) {

            $user = auth()->user();
            $lignes = '';
            $nbre = 1;
            $updated_row = 0;
            $created_row = 0;
            $unsed_row = 0;

            foreach ($rows as $index => $row) {
                /** Récupération des valeurs */
                $ligne_vide = $row[6];
                if (!is_null($ligne_vide) && $index > 0) {
                    $personne_resp = $row[1];
                    $raison_sociale = $row[2];
                    $adresse = $row[3];
                    $siege_social = $row[4];
                    $rccm = $row[5];
                    $ifu = preg_replace('/\s+/', '', $row[6]);
                    //$ifu = $row[6];
                    $cnss = $row[7];
                    $numero_agrement = preg_replace('/\s+/', '', $row[8]);
                    //$numero_agrement = $row[8];
                    $categories = $row[9];
                    $numero_arrete = $row[10];
                    $date_arrete = $row[11];

                    $domaine = $row[12];
                    $type_travaux = $row[13];
                    $ville = $row[14];
                    Log::channel('importsLog')->info("############ Importation de la ligne n° " . $row[0] . " ; IFU: " . $ifu. " ############");
                    if (!$this->checkAgrementEntreprise($ifu, $numero_agrement)) {
                        Log::channel('importsLog')->info("L'agrément pour cette entreprise n'existe pas. IFU: " . $ifu);
                        if (!$this->checkEntreprise($ifu)) {
                            Log::channel('importsLog')->info("L'entreprise n'existe pas. Création....'. IFU: " . $ifu);
                            DB::beginTransaction();
                            try {
                                $entreprise = new Entreprises();
                                $entreprise->raison_sociale = $raison_sociale;
                                $entreprise->siege_social = $siege_social;
                                $entreprise->adresse = $adresse;
                                $entreprise->registre_commerce = $rccm;
                                $entreprise->numero_ifu = $ifu;
                                $entreprise->personne_responsable = $personne_resp;
                                $entreprise->numero_cnss = $cnss;
                                $entreprise->ville = $ville;
                                $entreprise->save();
                                Log::channel('importsLog')->info("L'entreprise a été importée'. IFU: " . $ifu);

                                $agrement = new Agrements();
                                $agrement->Ent_id = $entreprise->id;
                                $agrement->Dom_id = $this->getDomaineId($domaine);
                                $agrement->Typ_id = $this->getTypeTravauxId($type_travaux);
                                $agrement->numero_agrement = $numero_agrement;
                                $agrement->numero_arrete = $numero_arrete;
                                if (is_numeric($date_arrete)) {
                                    $agrement->date_arrete = Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date_arrete));
                                } else {
                                    $agrement->date_arrete = Carbon::createFromFormat('d/m/Y', $date_arrete);
                                }
                                //$agrement->date_arrete = Carbon::createFromFormat('d/m/Y', $date_arrete);

                                $agrement->est_actif = 1;
                                $agrement->save();

                                // Mise à jour du statut
                                $statut = new StatutAgrements();
                                $statut->Agr_id = $agrement->id;
                                $statut->Sta_id = 1;
                                $statut->est_actif = 1;
                                $statut->observation = 'en cours de validité';
                                $statut->save();

                                // Enregistrement des catégories de l'agrement
                                $categories = $this->saveCategories($categories, $agrement->Dom_id, $agrement->Typ_id);

                                foreach ($categories as $key => $value) {
                                    $categorieAgrement = new CategorieAgrements();
                                    $categorieAgrement->Agr_id = $agrement->id;
                                    $categorieAgrement->Cat_id = $value->id;
                                    $categorieAgrement->save();
                                }

                                // Si l'agrement est expiré, mette à jour le statut
                                if ($agrement->date_expiration < Carbon::today()) {
                                    app('App\Http\Controllers\AgrementsController')->expireAgrement($agrement->id);
                                }
                                Log::channel('importsLog')->info("L'agrément a été importée'. N° agrément: " . $numero_agrement);
                                $created_row++;
                                DB::commit();
                            } catch (Exception $e) {
                                DB::rollback();
                                Log::channel('importsLog')->info("Une erreur est survenue dans la transaction. Ligne N° ".$row[0]." Message : " . $e.message);
                            }
                        } else {
                            Log::channel('importsLog')->info("L'entreprise existe dans la base. IFU: " . $ifu);
                            $entreprise = Entreprises::whereNumero_ifu($ifu)->first();
                            if (!$this->checkAgrement($numero_agrement) && !empty($entreprise)) {
                                DB::beginTransaction();
                                try {
                                    $agrement = new Agrements();
                                    $agrement->Ent_id = $entreprise->id;
                                    $agrement->Dom_id = $this->getDomaineId($domaine);
                                    $agrement->Typ_id = $this->getTypeTravauxId($type_travaux);
                                    $agrement->numero_agrement = $numero_agrement;
                                    $agrement->numero_arrete = $numero_arrete;
                                    if (is_numeric($date_arrete)) {
                                        $agrement->date_arrete = Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date_arrete));
                                    } else {
                                        $agrement->date_arrete = Carbon::createFromFormat('d/m/Y', $date_arrete);
                                    }
                                    $agrement->est_actif = 1;
                                    $agrement->save();


                                    // Mise à jour du statut
                                    $statut = new StatutAgrements();
                                    $statut->Agr_id = $agrement->id;
                                    $statut->Sta_id = 1;
                                    $statut->est_actif = 1;
                                    $statut->observation = 'en cours de validité';
                                    $statut->save();

                                    // Enregistrement des catégories de l'agrement
                                    $categories = $this->saveCategories($categories, $agrement->Dom_id, $agrement->Typ_id);

                                    foreach ($categories as $key => $value) {
                                        $categorieAgrement = new CategorieAgrements();
                                        $categorieAgrement->Agr_id = $agrement->id;
                                        $categorieAgrement->Cat_id = $value->id;
                                        $categorieAgrement->save();
                                    }

                                    // Si l'agrement est expiré, mette à jour le statut
                                    if ($agrement->date_expiration < Carbon::today()) {
                                        app('App\Http\Controllers\AgrementsController')->expireAgrement($agrement->id);
                                    }

                                    $created_row++;
                                    DB::commit();
                                    Log::channel('importsLog')->info("L'agrément a été importée'. N° agrément: " . $numero_agrement);
                                } catch (Exception $e) {
                                    DB::rollback();
                                    Log::channel('importsLog')->info("Une erreur est survenue dans la transaction. Ligne N° " . $row[0] . " Message : " . $e.message);
                                }
                            } else {
                                Log::channel('importsLog')->info('Cet agrement existe dejà dans la base!');
                                //Log::channel('importsLog')->info('###############################################################################################################################"');
                            }
                        }
                    } else {
                        Log::channel('importsLog')->info('Cet agrement existe dejà dans la base!');
                        //Log::channel('importsLog')->info('###############################################################################################################################"');
                    }
                } else {
                    Log::channel('importsLog')->info('Fin des données');
                }
            }
            //Log::channel('importsLog')->info('###############################################################################################################################');
            Log::channel('importsLog')->info("Nombre d'agréments ajoutés : " . $created_row);
        } else {
            Log::channel('importsLog')->info('Fichier non ouvert !');
            //Log::channel('importsLog')->info('###############################################################################################################################"');
        }
    }


    public static function checkEntreprise($ifu)
    {
        $result = DB::table('Entreprises')->where([
            ['numero_ifu', '=', $ifu]
        ])->first();
        $response = null;
        empty($result) ? $response = null : $response = $result;
        return $response;
    }
    public static function checkAgrement($numero_agrement)
    {
        $result = DB::table('Agrements')->where([
            ['numero_agrement', '=', $numero_agrement]
        ])->first();
        $response = null;
        empty($result) ? $response = null : $response = $result;
        return $response;
    }
    public static function checkAgrementEntreprise($ifu, $numero_agrement)
    {
        $result = DB::table('Agrements')
            ->join('Entreprises', 'Entreprises.id', '=', 'Agrements.Ent_id')
            ->where([
            ['numero_ifu', '=', $ifu],
            ['numero_agrement', '=', $numero_agrement]
        ])->first();
        $response = null;
        empty($result) ? $response = null : $response = $result;
        return $response;
    }
    public static function getDomaineId($code)
    {
        $result = DB::table('Domaines')->where([
            ['libelle_court', '=', strtoupper($code)]
        ])->value('id');
        $response = null;
        empty($result) ? $response = null : $response = $result;
        return $response;
    }
    public static function getTypeTravauxId($code)
    {
        $result = DB::table('TypeTravaux')->where([
            ['libelle_type', '=', strtolower($code)]
        ])->value('id');
        $response = null;
        empty($result) ? $response = null : $response = $result;
        return $response;
    }
    public static function saveCategories($categories, $domaine, $type)
    {
        $stripped = str_replace(' ', '', $categories); // supprime les espaces inutiles
        //$categories = \explode(';', $stripped);
        $categories = explode("-", str_replace(
            array("\n", "\t", "\r", "\a", ",", ";", ".", "-"),"-",
            $stripped
        ));
        $categories = array_filter($categories);

        $result = DB::table('Categories')
            ->whereIn('libelle_court', $categories)
            ->pluck('libelle_court');  // retournes les categories dans existant dans la base

        // recupération des agrements non presents dans la base
        $newCategories =  array_udiff($categories, $result->toArray(), 'strcasecmp'); // Nouvelles catégories
        // enregistrement des nouveaux agrements
        if(! empty($newCategories)) {
            foreach ($newCategories as $key => $value) {
                $categorie = new Categories();
                $categorie->Dom_id = $domaine;
                $categorie->Typ_id = $type;
                $categorie->libelle_court = $value;
                $categorie->est_actif = 1;
                $categorie->save();
            }            
        }
        $result = DB::table('Categories')
            ->whereIn('libelle_court', $categories)
            ->get();

        $response = null;
        empty($result) ? $response = null : $response = $result;
        return $response;
    }
}
