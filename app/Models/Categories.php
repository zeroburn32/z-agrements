<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Categories",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="Cat_id",
 *          description="Cat_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="Dom_id",
 *          description="Dom_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="Typ_id",
 *          description="Type de travaux -etudes ou Travaux",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="libelle_court",
 *          description="libelle_court",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="libelle_long",
 *          description="libelle_long",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      )
 * )
 */
class Categories extends Model
{

    public $table = 'Categories';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'Cat_id',
        'Dom_id',
        'Typ_id',
        'libelle_court',
        'libelle_long',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'Cat_id' => 'integer',
        'Dom_id' => 'integer',
        'Typ_id' => 'integer',
        'libelle_court' => 'string',
        'libelle_long' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'libelle_court' => 'required',
        'libelle_long' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function dom()
    {
        return $this->belongsTo(\App\Models\Domaine::class, 'Dom_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function typ()
    {
        return $this->belongsTo(\App\Models\TypeTravaux::class, 'Typ_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function categorieAgrements()
    {
        return $this->hasMany(\App\Models\CategorieAgrement::class);
    }
}
