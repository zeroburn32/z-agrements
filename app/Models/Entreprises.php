<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Entreprises",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="Com_id",
 *          description="Com_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="Reg_id",
 *          description="Reg_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="raison_socaile",
 *          description="raison_socaile",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="siege_social",
 *          description="siege_social",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="adresse",
 *          description="adresse",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="telephone",
 *          description="telephone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="registre_commerce",
 *          description="registre_commerce",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="numero_ifu",
 *          description="numero_ifu",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="personne_responsable",
 *          description="personne_responsable",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="numero_cnss",
 *          description="numero_cnss",
 *          type="string"
 *      )
 * )
 */
class Entreprises extends Model
{

    public $table = 'Entreprises';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'Com_id',
        'Pro_id',
        'Reg_id',
        'ville',
        'raison_sociale',
        'siege_social',
        'adresse',
        'telephone',
        'registre_commerce',
        'numero_ifu',
        'personne_responsable',
        'numero_cnss'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'Com_id' => 'integer',
        'Pro_id' => 'integer',
        'Reg_id' => 'integer',
        'ville' => 'string',
        'raison_socaile' => 'string',
        'siege_social' => 'string',
        'adresse' => 'string',
        'telephone' => 'string',
        'registre_commerce' => 'string',
        'numero_ifu' => 'string',
        'personne_responsable' => 'string',
        'numero_cnss' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function com()
    {
        return $this->belongsTo(\App\Models\Commune::class, 'Com_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function reg()
    {
        return $this->belongsTo(\App\Models\Region::class, 'Reg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function agrements()
    {
        return $this->hasMany(\App\Models\Agrement::class);
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->numero_ifu = preg_replace('/\s+/', '', $model->numero_ifu);
        });
        static::updating(function ($model) {
            $model->numero_ifu = preg_replace('/\s+/', '', $model->numero_ifu);
        });
    }
}
