<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="StatutAgrements",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="Agr_id",
 *          description="Agr_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="Sta_id",
 *          description="Sta_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="observation",
 *          description="observation",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="est_actif",
 *          description="est_actif",
 *          type="boolean"
 *      )
 * )
 */
class StatutAgrements extends Model
{

    public $table = 'StatutAgrements';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'Agr_id',
        'Sta_id',
        'date_debut_statut',
        'date_fin_statut',
        'observation',
        'est_actif'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'Agr_id' => 'integer',
        'Sta_id' => 'integer',
        'observation' => 'string',
        'est_actif' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function sta()
    {
        return $this->belongsTo(\App\Models\Statut::class, 'Sta_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function agr()
    {
        return $this->belongsTo(\App\Models\Agrement::class, 'Agr_id');
    }
}
