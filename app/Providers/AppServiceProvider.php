<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $anneeActuelle = Carbon::now()->year;
        $annees = array();   // tableaux des années
        //$annees= range(2000, $annee); 
         for ($i = $anneeActuelle; $i >= 2016; $i--) {
            array_push($annees, $i);
        }
        $annees = array_combine($annees,  $annees);
        
        Schema::defaultStringLength(191);

        View::share('annees', $annees);
    }
}
