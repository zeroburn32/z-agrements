<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ExpireAgrement extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expire:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Expiration des agréments ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */ 
    public function handle()
    {
        $agrements_expires = DB::table('Agrements')
            ->where([
                ['date_expiration', '<', today()],
                ['est_expire', '=', 0],
                ['est_actif', '=', 1],
            ])
            ->get(); 

        foreach ($agrements_expires as $agrement) {
            app('App\Http\Controllers\AgrementsController')->expireAgrement($agrement->id);
        }
        $this->info('Expire:Cron Cummand Run successfully!');
    }
}
