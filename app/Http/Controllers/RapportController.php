<?php

namespace App\Http\Controllers;

use App\Models\Regions;
use App\Models\Statuts;
use App\Models\Communes;
use App\Models\Domaines;
use App\Models\Agrements;
use App\Models\Provinces;
use App\Models\TypeTravaux;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RapportController extends Controller
{
    //
    public function index(Request $request) {
       
        $typeTravaux = TypeTravaux::where('est_actif', '=', 1)->pluck('libelle_type', 'id')->toArray();
        $domaines = Domaines::where('est_actif', '=', 1)->pluck('libelle_long', 'id')->toArray();
        $statuts = Statuts::where('est_actif', '=', 1)->pluck('libelle_statut', 'id')->toArray();

        $regions =  Regions::pluck('libelle_region', 'id')->toArray();
        $provinces =  Provinces::pluck('libelle_province', 'id')->toArray();
        $communes =  Communes::pluck('libelle_commune', 'id')->toArray();

        $statut = $request->Sta_id;
        $domaine = $request->Dom_id;
        $type = $request->Typ_id;
        $categorie = $request->Cat_id;
        $annee = $request->Ann_id;

        if($statut && !$domaine && !$type) {
            $agrements = DB::table('Agrements')
                ->join('Domaines', 'Domaines.id', '=', 'Agrements.Dom_id')
                ->join('TypeTravaux', 'TypeTravaux.id', '=', 'Agrements.Typ_id')
                ->join('Entreprises', 'Entreprises.id', '=', 'Agrements.Ent_id')
                ->join('StatutAgrements', 'StatutAgrements.Agr_id', '=', 'Agrements.id')
                ->where([
                    ['Agrements.est_actif', '=', 1],
                    ['StatutAgrements.Sta_id', '=', $statut],
                ])
                ->Select('Agrements.id', 'Agrements.numero_agrement', 'Agrements.date_arrete', 'Entreprises.numero_ifu', 'Entreprises.raison_sociale', 'Domaines.libelle_court', 'TypeTravaux.libelle_type')
                ->get();
        }else if(!$statut && $domaine && !$type){
            $agrements = DB::table('Agrements')
                ->join('Domaines', 'Domaines.id', '=', 'Agrements.Dom_id')
                ->join('TypeTravaux', 'TypeTravaux.id', '=', 'Agrements.Typ_id')
                ->join('Entreprises', 'Entreprises.id', '=', 'Agrements.Ent_id')
                ->join('StatutAgrements', 'StatutAgrements.Agr_id', '=', 'Agrements.id')
                ->where([
                    ['Agrements.est_actif', '=', 1],
                    ['Agrements.Dom_id', '=', $domaine],
                ])
                ->Select('Agrements.id', 'Agrements.numero_agrement', 'Agrements.date_arrete', 'Entreprises.numero_ifu', 'Entreprises.raison_sociale', 'Domaines.libelle_court', 'TypeTravaux.libelle_type')
                ->get();

        }else if (!$statut && !$domaine && $type){
            $agrements = DB::table('Agrements')
                ->join('Domaines', 'Domaines.id', '=', 'Agrements.Dom_id')
                ->join('TypeTravaux', 'TypeTravaux.id', '=', 'Agrements.Typ_id')
                ->join('Entreprises', 'Entreprises.id', '=', 'Agrements.Ent_id')
                ->join('StatutAgrements', 'StatutAgrements.Agr_id', '=', 'Agrements.id')
                ->where([
                    ['Agrements.est_actif', '=', 1],
                    ['Agrements.Typ_id', '=', $type],
                ])
                ->Select('Agrements.id', 'Agrements.numero_agrement', 'Agrements.date_arrete', 'Entreprises.numero_ifu', 'Entreprises.raison_sociale', 'Domaines.libelle_court', 'TypeTravaux.libelle_type')
                ->get();
        } else if ($statut && $domaine && !$type) {
            $agrements = DB::table('Agrements')
                ->join('Domaines', 'Domaines.id', '=', 'Agrements.Dom_id')
                ->join('TypeTravaux', 'TypeTravaux.id', '=', 'Agrements.Typ_id')
                ->join('Entreprises', 'Entreprises.id', '=', 'Agrements.Ent_id')
                ->join('StatutAgrements', 'StatutAgrements.Agr_id', '=', 'Agrements.id')
                ->where([
                    ['Agrements.est_actif', '=', 1],
                    ['StatutAgrements.Sta_id', '=', $statut],
                    ['Agrements.Dom_id', '=', $domaine],
                ])
                ->Select('Agrements.id', 'Agrements.numero_agrement', 'Agrements.date_arrete', 'Entreprises.numero_ifu', 'Entreprises.raison_sociale', 'Domaines.libelle_court', 'TypeTravaux.libelle_type')
                ->get();
        } else if ($statut && !$domaine && $type) {
            $agrements = DB::table('Agrements')
                ->join('Domaines', 'Domaines.id', '=', 'Agrements.Dom_id')
                ->join('TypeTravaux', 'TypeTravaux.id', '=', 'Agrements.Typ_id')
                ->join('Entreprises', 'Entreprises.id', '=', 'Agrements.Ent_id')
                ->join('StatutAgrements', 'StatutAgrements.Agr_id', '=', 'Agrements.id')
                ->where([
                    ['Agrements.est_actif', '=', 1],
                    ['StatutAgrements.Sta_id', '=', $statut],
                    ['Agrements.Typ_id', '=', $type],
                ])
                ->Select('Agrements.id', 'Agrements.numero_agrement', 'Agrements.date_arrete', 'Entreprises.numero_ifu', 'Entreprises.raison_sociale', 'Domaines.libelle_court', 'TypeTravaux.libelle_type')
                ->get();
        } else if (!$statut && $domaine && $type && $categorie) {
            $agrements = DB::table('Agrements')
                ->join('Domaines', 'Domaines.id', '=', 'Agrements.Dom_id')
                ->join('TypeTravaux', 'TypeTravaux.id', '=', 'Agrements.Typ_id')
                ->join('Entreprises', 'Entreprises.id', '=', 'Agrements.Ent_id')
                ->join('StatutAgrements', 'StatutAgrements.Agr_id', '=', 'Agrements.id')
                ->join('CategorieAgrements', 'CategorieAgrements.Agr_id', '=', 'Agrements.id')
                ->where([
                    ['Agrements.est_actif', '=', 1],
                    ['Agrements.Dom_id', '=', $domaine],
                    ['Agrements.Typ_id', '=', $type],
                    ['CategorieAgrements.Cat_id', '=', $categorie],
                ])
                ->Select('Agrements.id', 'Agrements.numero_agrement', 'Agrements.date_arrete', 'Entreprises.numero_ifu', 'Entreprises.raison_sociale', 'Domaines.libelle_court', 'TypeTravaux.libelle_type')
                ->distinct('Agrements.numero_agrement')
                ->get();

        } else if (!$statut && $domaine && $type && !$categorie) {
            $agrements = DB::table('Agrements')
                ->join('Domaines', 'Domaines.id', '=', 'Agrements.Dom_id')
                ->join('TypeTravaux', 'TypeTravaux.id', '=', 'Agrements.Typ_id')
                ->join('Entreprises', 'Entreprises.id', '=', 'Agrements.Ent_id')
                ->join('StatutAgrements', 'StatutAgrements.Agr_id', '=', 'Agrements.id')
                ->where([
                    ['Agrements.est_actif', '=', 1],
                    ['Agrements.Dom_id', '=', $domaine],
                    ['Agrements.Typ_id', '=', $type],
                ])
                ->Select('Agrements.id', 'Agrements.numero_agrement', 'Agrements.date_arrete', 'Entreprises.numero_ifu', 'Entreprises.raison_sociale', 'Domaines.libelle_court', 'TypeTravaux.libelle_type')
                ->get();
        } else if ($statut && $domaine && $type && $categorie) {
            $agrements = DB::table('Agrements')
                ->join('Domaines', 'Domaines.id', '=', 'Agrements.Dom_id')
                ->join('TypeTravaux', 'TypeTravaux.id', '=', 'Agrements.Typ_id')
                ->join('Entreprises', 'Entreprises.id', '=', 'Agrements.Ent_id')
                ->join('StatutAgrements', 'StatutAgrements.Agr_id', '=', 'Agrements.id')
                ->join('CategorieAgrements', 'CategorieAgrements.Agr_id', '=', 'Agrements.id')
                ->where([
                    ['Agrements.est_actif', '=', 1],
                    ['StatutAgrements.Sta_id', '=', $statut],
                    ['Agrements.Dom_id', '=', $domaine],
                    ['Agrements.Typ_id', '=', $type],
                    ['CategorieAgrements.Cat_id', '=', $categorie],
                ])
                ->Select('Agrements.id', 'Agrements.numero_agrement', 'Agrements.date_arrete', 'Entreprises.numero_ifu', 'Entreprises.raison_sociale', 'Domaines.libelle_court', 'TypeTravaux.libelle_type')
                ->distinct('Agrements.numero_agrement')
                ->get();
        }else if ($statut && $domaine && $type && !$categorie) {
            $agrements = DB::table('Agrements')
                ->join('Domaines', 'Domaines.id', '=', 'Agrements.Dom_id')
                ->join('TypeTravaux', 'TypeTravaux.id', '=', 'Agrements.Typ_id')
                ->join('Entreprises', 'Entreprises.id', '=', 'Agrements.Ent_id')
                ->join('StatutAgrements', 'StatutAgrements.Agr_id', '=', 'Agrements.id')
                ->where([
                    ['Agrements.est_actif', '=', 1],
                    ['StatutAgrements.Sta_id', '=', $statut],
                    ['Agrements.Dom_id', '=', $domaine],
                    ['Agrements.Typ_id', '=', $type],
                ])
                ->Select('Agrements.id', 'Agrements.numero_agrement', 'Agrements.date_arrete', 'Entreprises.numero_ifu', 'Entreprises.raison_sociale', 'Domaines.libelle_court', 'TypeTravaux.libelle_type')
                ->get();
        }

        if (isset($agrements)) {
                $agrements->map(function ($agrement) {
                    $agrement->categories = '';
                    $categories = DB::table('Categories')
                        ->join('CategorieAgrements', 'CategorieAgrements.Cat_id', '=', 'Categories.id')
                        ->where('CategorieAgrements.Agr_id', '=', $agrement->id)
                        ->select('Categories.libelle_court')
                        ->get()->toArray();
                    foreach ($categories as $item) {
                        $agrement->categories = $agrement->categories . ' ' . $item->libelle_court;
                    }
                    return $agrement;
                });
        }

        $code_statut = DB::table('Statuts')->where('id','=',$statut)->value('code_statut');
        $libelle_domaine = DB::table('Domaines')->where('id', '=', $domaine)->value('libelle_court');
        $libelle_type = DB::table('TypeTravaux')->where('id', '=', $type)->value('libelle_type');
        $libelle_categorie = DB::table('Categories')->where('id', '=', $categorie)->value('libelle_court');
           
         return view('backend.rapports.index', compact('agrements','typeTravaux','statuts', 'domaines',
            'regions', 'communes','provinces','code_statut',
            'libelle_domaine',
            'libelle_type',
            'libelle_categorie' ));
    }
}
