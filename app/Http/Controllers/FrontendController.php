<?php

namespace App\Http\Controllers;


use App\Http\Controllers\AppBaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Flash;
use Flashy;
use Response;
use App\Models\Agrements;
use DB;
use PDF;

class FrontendController extends AppBaseController
{

    public function export_pdf(Request $request) {
        // Fetch all customers from database
        $input = $request->all();
        $input_ifu = $input['ifu1'];
        $input_agrement =  $input['agrement1'];
        $data = $this->recherche($input_agrement, $input_ifu);
        
        $pdf = PDF::loadView('backend.agrements.print_result', compact('data'));
        // If you want to store the generated pdf to the server then you can use the store function
        $pdf->save(storage_path() . '_filename.pdf');
        // Finally, you can download the file using download function
        return $pdf->download('resultats.pdf');
        //return view('backend.agrements.print_result', compact('data'));
        //return $data;
    }

    public function findAgrement(Request $request)
    {
        $input = $request->all();
        $input_ifu = $input['ifu'];
        $input_agrement =  $input['agrement'];
            
        $agrements = $this->recherche($input_agrement, $input_ifu);
        //sleep(10);

        if($request->ajax()){
            return  Response::json($agrements);
        }
        return redirect(route('/'));
        
    }

    public function recherche($input_agrement, $input_ifu)
    {
            $input_ifu = preg_replace('/\s+/', '', $input_ifu);
            $input_agrement = preg_replace('/\s+/', '', $input_agrement);

            $agrements = DB::table('Agrements')
                    ->join('Domaines','Domaines.id','=','Agrements.Dom_id')
                    ->join('TypeTravaux','TypeTravaux.id','=','Agrements.Typ_id')
                    ->join('Entreprises','Entreprises.id','=','Agrements.Ent_id')
                    ->Select('Agrements.*','Entreprises.raison_sociale', 'Entreprises.numero_ifu', 'Entreprises.registre_commerce', 'Domaines.libelle_court','TypeTravaux.libelle_type')
                    ->where ([
                            ['Agrements.numero_agrement', '=', $input_agrement],
                            ['Entreprises.numero_ifu', '=', $input_ifu],
                            ['Agrements.est_actif', '=', 1]
                            ])
                    ->first();

            if(!empty($agrements)) {
            $categories = DB::table('Categories')
                ->join('CategorieAgrements', 'CategorieAgrements.Cat_id', '=', 'Categories.id')
                ->where('CategorieAgrements.Agr_id', '=', $agrements->id)
                ->select('Categories.libelle_court')
                ->get()->toArray();

            $statut = DB::table('Statuts')
                ->join('StatutAgrements', 'StatutAgrements.Sta_id', '=', 'Statuts.id')
                ->where([
                    ['StatutAgrements.Agr_id', '=', $agrements->id],
                    ['StatutAgrements.est_actif', '=', 1]
                ])
                ->select('Statuts.libelle_statut','Statuts.code_statut')
                ->first();

            $last_update = Agrements::where('Agrements.est_actif', '=', 1)->latest()->value('created_at');

            $agrements->expiration = Carbon::parse($agrements->date_expiration)->diffForHumans();
            $agrements->date_expiration = date("d-m-Y", strtotime($agrements->date_expiration));
            $agrements->date_arrete = date("d-m-Y", strtotime($agrements->date_arrete));
            $agrements->categories = $categories;
            $agrements->statut = $statut->libelle_statut;
            $agrements->codeStatut = $statut->code_statut;
            $agrements->last_update = date("d-m-Y", strtotime($last_update));

            return $agrements;
        } else {
            $agrements = [];
            return $agrements;
        }
    }
}
