<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRegionsRequest;
use App\Http\Requests\UpdateRegionsRequest;
use App\Repositories\RegionsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flashy;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RegionsController extends AppBaseController
{
    /** @var  RegionsRepository */
    private $regionsRepository;

    public function __construct(RegionsRepository $regionsRepo)
    {
        $this->regionsRepository = $regionsRepo;
    }

    /**
     * Display a listing of the Regions.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->regionsRepository->pushCriteria(new RequestCriteria($request));
        $regions = $this->regionsRepository->all();

        return view('backend.regions.index')
            ->with('regions', $regions);
    }

    /**
     * Show the form for creating a new Regions.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.regions.create');
    }

    /**
     * Store a newly created Regions in storage.
     *
     * @param CreateRegionsRequest $request
     *
     * @return Response
     */
    public function store(CreateRegionsRequest $request)
    {
        $input = $request->all();

        $regions = $this->regionsRepository->create($input);

        Flashy::success('Regions enregistré avec succès.');

        return redirect(route('regions.index'));
    }

    /**
     * Display the specified Regions.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $regions = $this->regionsRepository->findWithoutFail($id);

        if (empty($regions)) {
            Flashy::error('Regions non trouvé');

            return redirect(route('regions.index'));
        }

        return view('backend.regions.show')->with('regions', $regions);
    }

    /**
     * Show the form for editing the specified Regions.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $regions = $this->regionsRepository->findWithoutFail($id);

        if (empty($regions)) {
            Flashy::error('Regions non trouvé');

            return redirect(route('regions.index'));
        }

        return view('backend.regions.edit')->with('regions', $regions);
    }

    /**
     * Update the specified Regions in storage.
     *
     * @param  int              $id
     * @param UpdateRegionsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRegionsRequest $request)
    {
        $regions = $this->regionsRepository->findWithoutFail($id);

        if (empty($regions)) {
            Flashy::error('Regions non trouvé');

            return redirect(route('regions.index'));
        }

        $regions = $this->regionsRepository->update($request->all(), $id);

        Flashy::success('Regions mis à jour avec succès.');

        return redirect(route('regions.index'));
    }

    /**
     * Remove the specified Regions from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $regions = $this->regionsRepository->findWithoutFail($id);

        if (empty($regions)) {
            Flashy::error('Regions non trouvé');

            return redirect(route('regions.index'));
        }

        $this->regionsRepository->delete($id);

        Flashy::success('Regions supprimé avec succès.');

        return redirect(route('regions.index'));
    }
}
