<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Http\Request;
use App\Mail\ContactMessageCreated;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\ContactsRequest;
use \PDF;
use Response;




class ContactsController  extends AppBaseController
{
    /* public function create()
    {
      return view('contacts.create');
    } */


    /* public function enregister(ContactsRequest $request)
    {
        $mailable = new contactMessageCreated($request->name, $request->prenom, $request->objet, $request->email, $request->message);
        Mail::to(config('mailsupport.admin_support_email'))->send($mailable);

       // flashy('Nous vous répondrons dans les plus brefs délais!!');

       // return view('welcome');

    } */
    public function enregister(Request $request)
    {
    $code = $request->input('CaptchaCode');
    $isHuman = captcha_validate($code);
    if ($isHuman) {
        $data = $request->all(); // This will get all the request data.

       //dd($data); // This will dump and die
       $name = $request->input('name');
       $prenom = $request->input('prenom');
       $objet = $request->input('objet');
       $email = $request->input('email');
       $contenu = $request->input('message'); 
    
      $data = ['name' => $name, 'prenom' => $prenom, "email" => $email, "objet" => $objet, "contenu" => $contenu];
      try {
      $retour = Mail::send('emails.messages.contact', $data, function ($message) use ($objet, $email) {
          $message->to($email)->subject($objet);
        }); 
      } catch (\Swift_TransportException $e) {
        $retour =1;
      }
        return  $retour;
      } else {
        $retour=2;
        //Flashy::error('Echec de verification du captcha. Reessayez!');
        return Response::json($retour);
      } 
      
  }
}
