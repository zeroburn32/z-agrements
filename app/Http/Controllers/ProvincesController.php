<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProvincesRequest;
use App\Http\Requests\UpdateProvincesRequest;
use App\Repositories\ProvincesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flashy;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ProvincesController extends AppBaseController
{
    /** @var  ProvincesRepository */
    private $provincesRepository;

    public function __construct(ProvincesRepository $provincesRepo)
    {
        $this->provincesRepository = $provincesRepo;
    }

    /**
     * Display a listing of the Provinces.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->provincesRepository->pushCriteria(new RequestCriteria($request));
        $provinces = $this->provincesRepository->all();

        return view('backend.provinces.index')
            ->with('provinces', $provinces);
    }

    /**
     * Show the form for creating a new Provinces.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.provinces.create');
    }

    /**
     * Store a newly created Provinces in storage.
     *
     * @param CreateProvincesRequest $request
     *
     * @return Response
     */
    public function store(CreateProvincesRequest $request)
    {
        $input = $request->all();

        $provinces = $this->provincesRepository->create($input);

        Flashy::success('Provinces enregistré avec succès.');

        return redirect(route('provinces.index'));
    }

    /**
     * Display the specified Provinces.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $provinces = $this->provincesRepository->findWithoutFail($id);

        if (empty($provinces)) {
            Flashy::error('Provinces non trouvé');

            return redirect(route('provinces.index'));
        }

        return view('backend.provinces.show')->with('provinces', $provinces);
    }

    /**
     * Show the form for editing the specified Provinces.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $provinces = $this->provincesRepository->findWithoutFail($id);

        if (empty($provinces)) {
            Flashy::error('Provinces non trouvé');

            return redirect(route('provinces.index'));
        }

        return view('backend.provinces.edit')->with('provinces', $provinces);
    }

    /**
     * Update the specified Provinces in storage.
     *
     * @param  int              $id
     * @param UpdateProvincesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProvincesRequest $request)
    {
        $provinces = $this->provincesRepository->findWithoutFail($id);

        if (empty($provinces)) {
            Flashy::error('Provinces non trouvé');

            return redirect(route('provinces.index'));
        }

        $provinces = $this->provincesRepository->update($request->all(), $id);

        Flashy::success('Provinces mis à jour avec succès.');

        return redirect(route('provinces.index'));
    }

    /**
     * Remove the specified Provinces from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $provinces = $this->provincesRepository->findWithoutFail($id);

        if (empty($provinces)) {
            Flashy::error('Provinces non trouvé');

            return redirect(route('provinces.index'));
        }

        $this->provincesRepository->delete($id);

        Flashy::success('Provinces supprimé avec succès.');

        return redirect(route('provinces.index'));
    }
}
