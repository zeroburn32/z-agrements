<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;

class LocalitesController extends Controller
{
    public function getRegions($id)
    {
        $results =  DB::table('Regions')
            ->select('Regions.id', 'Regions.libelle_region')
            ->get();
        return Response::json($results);
    }
    public function getProvinces($id)
    {
        $results =  DB::table('Provinces')
            ->join('Regions', 'Regions.id', '=', 'Provinces.Reg_id')
            ->select('Provinces.id', 'Provinces.libelle_province')
            ->Where([
                ['Provinces.Reg_id', '=', $id]
            ])
            ->get();
        return Response::json($results);
    }
    public function getCommunes($id)
    {
        $results =  DB::table('Communes')
            ->join('Provinces', 'Provinces.id', '=', 'Communes.Pro_id')
            ->select('Communes.id', 'Communes.libelle_commune')
            ->Where([
                ['Communes.Pro_id', '=', $id]
            ])
            ->get();
        return Response::json($results);
    }
}
