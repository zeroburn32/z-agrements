<?php

namespace App\Http\Controllers;

use Flashy;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Repositories\StatutsRepository;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreateStatutsRequest;
use App\Http\Requests\UpdateStatutsRequest;
use Prettus\Repository\Criteria\RequestCriteria;

class StatutsController extends AppBaseController
{
    /** @var  StatutsRepository */
    private $statutsRepository;

    public function __construct(StatutsRepository $statutsRepo)
    {
        $this->statutsRepository = $statutsRepo;
    }

    /**
     * Display a listing of the Statuts.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->statutsRepository->pushCriteria(new RequestCriteria($request));
        $statuts = $this->statutsRepository->all();

        return view('backend.statuts.index')
            ->with('statuts', $statuts);
    }

    /**
     * Show the form for creating a new Statuts.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.statuts.create');
    }

    /**
     * Store a newly created Statuts in storage.
     *
     * @param CreateStatutsRequest $request
     *
     * @return Response
     */
    public function store(CreateStatutsRequest $request)
    {
        $input = $request->all();

        $statuts = $this->statutsRepository->create($input);

        Flashy::success('Statuts enregistré avec succès.');

        return redirect(route('statuts.index'));
    }

    /**
     * Display the specified Statuts.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $statuts = $this->statutsRepository->findWithoutFail($id);

        if (empty($statuts)) {
            Flashy::error('Statuts non trouvé');

            return redirect(route('statuts.index'));
        }

        return view('backend.statuts.show')->with('statuts', $statuts);
    }

    /**
     * Show the form for editing the specified Statuts.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $statuts = $this->statutsRepository->findWithoutFail($id);

        if (empty($statuts)) {
            Flashy::error('Statuts non trouvé');

            return redirect(route('statuts.index'));
        }

        return view('backend.statuts.edit')->with('statuts', $statuts);
    }

    /**
     * Update the specified Statuts in storage.
     *
     * @param  int              $id
     * @param UpdateStatutsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStatutsRequest $request)
    {
        $statuts = $this->statutsRepository->findWithoutFail($id);

        if (empty($statuts)) {
            Flashy::error('Statuts non trouvé');

            return redirect(route('statuts.index'));
        }

        $statuts = $this->statutsRepository->update($request->all(), $id);

        Flashy::success('Statuts mis à jour avec succès.');

        return redirect(route('statuts.index'));
    }

    /**
     * Remove the specified Statuts from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $statuts = $this->statutsRepository->findWithoutFail($id);

        if (empty($statuts)) {
            Flashy::error('Statuts non trouvé');

            return redirect(route('statuts.index'));
        }

        $this->statutsRepository->delete($id);

        Flashy::success('Statuts supprimé avec succès.');

        return redirect(route('statuts.index'));
    }
}
