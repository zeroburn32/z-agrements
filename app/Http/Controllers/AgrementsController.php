<?php

namespace App\Http\Controllers;

use DB;
use Flashy;
use Response;
use Carbon\Carbon;
use App\Models\Regions;
use App\Models\Communes;
use App\Models\Domaines;
use App\Models\Agrements;
use App\Models\Provinces;
use App\Models\Entreprises;
use App\Models\TypeTravaux;
use Illuminate\Http\Request;
use App\Models\ImportAgrements;
use App\Models\CategorieAgrements;
use Maatwebsite\Excel\Facades\Excel;
use App\Repositories\AgrementsRepository;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreateAgrementsRequest;
use App\Http\Requests\UpdateAgrementsRequest;
use App\Models\StatutAgrements;
use App\Repositories\StatutAgrementsRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\CategorieAgrementsRepository;

class AgrementsController extends AppBaseController
{
    /** @var  AgrementsRepository */
    private $agrementsRepository;
    private $statutRepository;
    private $categorieRepository;

    public function __construct(AgrementsRepository $agrementsRepo, StatutAgrementsRepository $statAgr, CategorieAgrementsRepository $catAgr)
    {
        $this->agrementsRepository = $agrementsRepo;
        $this->statutRepository = $statAgr;
        $this->categorieRepository = $catAgr;
    }

    /**
     * Display a listing of the Agrements.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $agrements = DB::table('Agrements')
        ->join('Domaines','Domaines.id','=','Agrements.Dom_id')
        ->join('TypeTravaux','TypeTravaux.id','=','Agrements.Typ_id')
        ->join('Entreprises','Entreprises.id','=','Agrements.Ent_id')
        ->Select('Agrements.*','Entreprises.raison_sociale', 'Domaines.libelle_court','TypeTravaux.libelle_type')
        ->get();

        $agrements->map(function ($agrement) {
            $categories = DB::table('Categories')
                ->join('CategorieAgrements', 'CategorieAgrements.Cat_id', '=', 'Categories.id')
                ->where('CategorieAgrements.Agr_id', '=', $agrement->id)
                ->select('Categories.libelle_court')
                ->get()->toArray();
            $agrement->categories = $categories;
            return $agrement;
        });
        //dd($agrements);

        $entreprises = Entreprises::pluck('raison_sociale', 'id')->toArray();
        $typeTravaux = TypeTravaux::where('est_actif','=',1)->pluck('libelle_type', 'id')->toArray();
        $domaines = Domaines::where('est_actif', '=', 1)->pluck('libelle_long', 'id')->toArray();

        $regions =  Regions::pluck('libelle_region', 'id')->toArray();
        $provinces =  Provinces::pluck('libelle_province', 'id')->toArray();
        $communes =  Communes::pluck('libelle_commune', 'id')->toArray();

        return view('backend.agrements.index', compact('entreprises', 'typeTravaux', 'domaines',
            'regions', 'communes','provinces' ))
            ->with('agrements', $agrements);
    }

    public function getAgrementsByEtat($etat)
    {
        $agrements =null;
        if($etat == "1") {  // ACTIFS
                $agrements = DB::table('Agrements')
                ->join('Domaines','Domaines.id','=','Agrements.Dom_id')
                ->join('TypeTravaux','TypeTravaux.id','=','Agrements.Typ_id')
                ->join('Entreprises','Entreprises.id','=','Agrements.Ent_id')
                ->where('Agrements.est_actif', '=', 1)
                ->Select('Agrements.id','Agrements.numero_agrement','Agrements.date_arrete', 'Entreprises.raison_sociale', 'Entreprises.numero_ifu', 'Domaines.libelle_court','TypeTravaux.libelle_type')
                ->get();

        } elseif ($etat == "2") {  // RETIRE = 2
            $agrements = DB::table('Agrements')
                ->join('Domaines', 'Domaines.id', '=', 'Agrements.Dom_id')
                ->join('TypeTravaux', 'TypeTravaux.id', '=', 'Agrements.Typ_id')
                ->join('Entreprises', 'Entreprises.id', '=', 'Agrements.Ent_id')
                ->join('StatutAgrements', 'StatutAgrements.Agr_id', '=', 'Agrements.id')
                ->where([
                    ['Agrements.est_actif', '=', 1], 
                    ['StatutAgrements.est_actif', '=', 1],
                    ['StatutAgrements.Sta_id', '=', 2]  // RETIRE = 2
                    ])
                ->Select('Agrements.id', 'Agrements.numero_agrement', 'Agrements.date_arrete', 'Entreprises.raison_sociale', 'Entreprises.numero_ifu', 'Domaines.libelle_court', 'TypeTravaux.libelle_type')
                ->get();
        } elseif ($etat == "3") {
            $agrements = DB::table('Agrements')
                ->join('Domaines', 'Domaines.id', '=', 'Agrements.Dom_id')
                ->join('TypeTravaux', 'TypeTravaux.id', '=', 'Agrements.Typ_id')
                ->join('Entreprises', 'Entreprises.id', '=', 'Agrements.Ent_id')
                ->join('StatutAgrements', 'StatutAgrements.Agr_id', '=', 'Agrements.id')
                ->where([
                    ['Agrements.est_actif', '=', 1],
                    ['Agrements.est_expire', '=', 1],
                    ['StatutAgrements.est_actif', '=', 1],
                    ['StatutAgrements.Sta_id', '=', 4] // EXPIRE 
                ])
                ->Select('Agrements.id', 'Agrements.numero_agrement', 'Agrements.date_arrete', 'Entreprises.raison_sociale', 'Entreprises.numero_ifu', 'Domaines.libelle_court', 'TypeTravaux.libelle_type')
                ->get();
        }elseif ($etat == "4") {   // Agrements valides
            $agrements = DB::table('Agrements')
                ->join('Domaines', 'Domaines.id', '=', 'Agrements.Dom_id')
                ->join('TypeTravaux', 'TypeTravaux.id', '=', 'Agrements.Typ_id')
                ->join('Entreprises', 'Entreprises.id', '=', 'Agrements.Ent_id')
                ->join('StatutAgrements', 'StatutAgrements.Agr_id', '=', 'Agrements.id')
                ->where([
                    ['Agrements.est_actif', '=', 1],
                    ['StatutAgrements.est_actif', '=', 1],
                    ['StatutAgrements.Sta_id', '=', 1],
                ])
                ->Select('Agrements.id', 'Agrements.numero_agrement', 'Agrements.date_arrete', 'Entreprises.raison_sociale', 'Entreprises.numero_ifu', 'Domaines.libelle_court', 'TypeTravaux.libelle_type')
                ->get();
        } else {   // Agrements inactifs 
            $agrements = DB::table('Agrements')
                ->join('Domaines', 'Domaines.id', '=', 'Agrements.Dom_id')
                ->join('TypeTravaux', 'TypeTravaux.id', '=', 'Agrements.Typ_id')
                ->join('Entreprises', 'Entreprises.id', '=', 'Agrements.Ent_id')
                ->where('Agrements.est_actif', '=', 0)
                ->Select('Agrements.id','Agrements.numero_agrement', 'Agrements.date_arrete', 'Entreprises.raison_sociale', 'Entreprises.numero_ifu', 'Domaines.libelle_court', 'TypeTravaux.libelle_type')
                ->get();
        }


        $agrements->map(function ($agrement) {
            $agrement->categories ='';
            $categories = DB::table('Categories')
                ->join('CategorieAgrements', 'CategorieAgrements.Cat_id', '=', 'Categories.id')
                ->where('CategorieAgrements.Agr_id', '=', $agrement->id)
                ->select('Categories.libelle_court')
                ->get()->toArray();
            
            foreach ($categories as $item) {
            $agrement->categories = $agrement->categories . ' ' . $item->libelle_court;
            }

            return $agrement;
        });

        if ($etat == "1") {  // ACTIFS
        return datatables()->of($agrements)
            ->addColumn('action', 'backend.agrements.actif_button')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }elseif ($etat == "2") {  // RETIRE
            return datatables()->of($agrements)
                ->addColumn('action', 'backend.agrements.retrait_button')
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        } elseif ($etat == "3") {   // EXPIRE
            return datatables()->of($agrements)
                ->addColumn('action', 'backend.agrements.retrait_button')
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        } elseif ($etat == "4") {  // VALIDE
            return datatables()->of($agrements)
                ->addColumn('action', 'backend.agrements.retrait_button')
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        } else { // INACTIF
        return datatables()->of($agrements)
            ->addColumn('action', 'backend.agrements.inactif_button')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
    }

    public function getCategorie($domaine, $type_travaux) {
        $categories = DB::table('Categories')
            ->where([
                ['Categories.Dom_id','=',$domaine],
                ['Categories.Typ_id','=',$type_travaux],
                ['Categories.est_actif','=',1]
                ])
            ->get();

        return Response::json($categories);
    }
    public function getAgrement($id) {
        $agrement = DB::table('Agrements')
            ->where('Agrements.id', '=', $id)
            ->Select('Agrements.*')
            ->first();

        return Response::json($agrement);
    }
    public function getCategoriesByAgrement($id) {
        $categories = DB::table('Categories')
            ->join('CategorieAgrements', 'CategorieAgrements.Cat_id', '=', 'Categories.id')
            ->where('CategorieAgrements.Agr_id', '=', $id)
            ->select('Categories.libelle_court', 'Categories.id')
            ->get();

        return Response::json($categories);
    }

    /**
     * Show the form for creating a new Agrements.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.agrements.create');
    }

    /**
     * Store a newly created Agrements in storage.
     *
     * @param CreateAgrementsRequest $request
     *
     * @return Response
     */
    public function store(CreateAgrementsRequest $request, UpdateAgrementsRequest $requestUpdate)
    {
        $input = $request->all();
        if($input['formMode'] == 'create') {
            DB::beginTransaction() ;
            try {
                $agrements = $this->agrementsRepository->create($input);
                foreach ($input['categories'] as $item) {
                    $categorieAgrement = new CategorieAgrements();
                    $categorieAgrement->Cat_id = $item;
                    $categorieAgrement->Agr_id = $agrements->id;
                    $categorieAgrement->save();
                }
                
                DB::commit();
                activity()->log('Nouvel agrement ajouté');
                Flashy::success('Agrement enregistré avec succès.');
            } catch (Exception $e) {
                DB::rollback();
            }
        } else {
            DB::beginTransaction();
            try {
            app('App\Http\Controllers\AgrementsController')->update($input['agr_id'], $requestUpdate);
            Flashy::success('Agrement mis à jour avec succès.');
            activity()->log('Agrement modifié ');
                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
            }        
        }
        return redirect(route('agrements.index'));
    }

    public function expireAgrement ($id) {
        DB::beginTransaction();
        try {
            $statutAgrement = StatutAgrements::whereAgr_id($id)->first();
            if (! empty($statutAgrement)) {
                $statutAgrement->est_actif = 0;
                $statutAgrement->save();
            }

            $statutAgrement = StatutAgrements::updateOrCreate(
                [
                    'Agr_id' => $id,
                    'Sta_id' => 4
                ],
                [
                    'est_actif' => 1,
                    'observation' => 'EXPIRE',
                    'date_debut_statut' => Carbon::now(),
                ]
            );
            $agrement = Agrements::find($id);
            if (!empty($agrement)) {
                $agrement->est_expire = 1;
                $agrement->save();
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }   
    }

    public function retirer(Request $request)
    {
        $input = $request->all();
        $id = $input['id'];
        DB::beginTransaction();
        try {
            $statutAgrement = StatutAgrements::whereAgr_id($id)->update(['est_actif' => 0]);
            $statutAgrement = StatutAgrements::updateOrCreate(
                [
                    'Agr_id' => $id,
                    'Sta_id' => 2
                ],
                [
                    'est_actif' => 1,
                    'observation' => 'RETRAIT',
                    'date_debut_statut' => Carbon::now(),
                ]
            );
            DB::commit();
            activity()->log('Agrement retiré');
            return Response::json($statutAgrement);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(array(
                'rollback' => 1,
            ));
        }
    }
    
    public function toggle(Request $request)
    {
        $input = $request->all();
        $tableName = $input['tableName'];
        $attribute = $input['attribute'];
        $id = $input['id'];
        $value = $input['value'];
        $agrement = Agrements::whereId($id)->update(['est_actif' => $value]);
        if($value ==1) {
        DB::beginTransaction();
        try {
            $statutAgrement = StatutAgrements::whereAgr_id($id)->first();
            if (!empty($statutAgrement)) {
                $statutAgrement->est_actif = 0;
                $statutAgrement->save();
            }
            $statutAgrement = StatutAgrements::updateOrCreate(
                [
                    'Agr_id' => $id,
                    'Sta_id' => 1
                ],
                [
                    'est_actif' => 1,
                    'observation' => 'VALIDE',
                    'date_debut_statut' => Carbon::now(),
                ]
            );
                // Expirer l'agrément si la date expiration est arrivé 
            $agrement = Agrements::whereId($id)->first();
        if($agrement->date_expiration < Carbon::now()) {
                app('App\Http\Controllers\AgrementsController')->expireAgrement($id);
        }         

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
         }
        } else {
            DB::beginTransaction();
            try {
                $statutAgrement = StatutAgrements::whereAgr_id($id)->delete();
                // Expirer l'agrément si la date expiration est arrivé 
                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
            }
        }
       
        activity()->log('Changement de statut d\'un agrement');
        return Response::json('$results');
    }

    

    public function importer(Request $request)
    {
        if ($request->hasFile('fichier')) {
            $ext = $request->fichier->getClientOriginalExtension();
            $file = $request->file('fichier');
            if ($file->getSize() < intval(config('app.taille_max_upload'))) {
                if (strtolower($ext) == 'xlsx') {
                    $import = new ImportAgrements();
                    //$import->onlySheets(0);
                    Excel::import($import, $file); 
                    activity('Agrements')->log('Importation');

                    $chemin = public_path() . "/imports/";
                    $name = 'import_du_'. Carbon::now()->format('d_M_Y_H_i_s') . '.' . $ext;
                    $file->move($chemin, $name);

                    return ['status' => 'success'];
                } else {
                    return ['status' => 'ext-no-valide'];
                }
            } else {
                return ['status' => 'taille-no-valide'];
            }
        } else {
            return ['status' => 'no-file'];
        }
    }


    /**
     * Display the specified Agrements.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $agrements = $this->agrementsRepository->findWithoutFail($id);

        if (empty($agrements)) {
            Flashy::error('Agrements non trouvé');

            return redirect(route('agrements.index'));
        }

        return view('backend.agrements.show')->with('agrements', $agrements);
    }

    /**
     * Show the form for editing the specified Agrements.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        $agrements = $this->agrementsRepository->findWithoutFail($id);


        $entreprises = Entreprises::pluck('raison_sociale', 'id');
        $typeTravaux = TypeTravaux::pluck('libelle_type', 'id');
        $domaines = Domaines::pluck('libelle_long', 'id');

        if (empty($agrements)) {
            Flashy::error('Agrements non trouvé');

            return redirect(route('agrements.index'));
        }

        if($request->ajax()){
            return response()->json(array(
                'agrements' => $agrements,
                'entreprises' => $entreprises,

            ));
        }

        return view('backend.agrements.edit', compact('entreprises', 'typeTravaux', 'domaines' ))->with('agrements', $agrements);
    }

    /**
     * Update the specified Agrements in storage.
     *
     * @param  int              $id
     * @param UpdateAgrementsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAgrementsRequest $request)
    {
        $agrements = $this->agrementsRepository->findWithoutFail($id);

        if (empty($agrements)) {
            Flashy::error('Agrements non trouvé');

            return redirect(route('agrements.index'));
        }

        $agrements = $this->agrementsRepository->update($request->all(), $id);

        $input = $request->all();
        $affectedRows = CategorieAgrements::where('Agr_id', '=', $id)->delete();
        foreach ($input['categories'] as $item) {
            DB::insert('insert into CategorieAgrements (Cat_id, Agr_id) values (?, ?)', [$item, $agrements->id]);
        }

        Flashy::success('Agrements mis à jour avec succès.');

        return redirect(route('agrements.index'));
    }

    /**
     * Remove the specified Agrements from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $agrements = $this->agrementsRepository->findWithoutFail($id);
        
        if (empty($agrements)) {
            Flashy::error('Agrements non trouvé');

            return redirect(route('agrements.index'));
        }

        DB::beginTransaction();
        try {

            DB::table('StatutAgrements')->where('Agr_id','=', $id)->delete();
            DB::table('CategorieAgrements')->where('Agr_id','=', $id)->delete();
            $this->agrementsRepository->delete($id);

        Flashy::success('Agrements supprimé avec succès.');
            DB::commit();
        } catch (Exception $e) {
            Flashy::success('Une erreur est survenue lors de la suppression.');
        }
        return redirect(route('agrements.index'));
    }
}
