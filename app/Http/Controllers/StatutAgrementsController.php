<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateStatutAgrementsRequest;
use App\Http\Requests\UpdateStatutAgrementsRequest;
use App\Repositories\StatutAgrementsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flashy;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class StatutAgrementsController extends AppBaseController
{
    /** @var  StatutAgrementsRepository */
    private $statutAgrementsRepository;

    public function __construct(StatutAgrementsRepository $statutAgrementsRepo)
    {
        $this->statutAgrementsRepository = $statutAgrementsRepo;
    }

    /**
     * Display a listing of the StatutAgrements.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->statutAgrementsRepository->pushCriteria(new RequestCriteria($request));
        $statutAgrements = $this->statutAgrementsRepository->all();

        return view('backend.statut_agrements.index')
            ->with('statutAgrements', $statutAgrements);
    }

    /**
     * Show the form for creating a new StatutAgrements.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.statut_agrements.create');
    }

    /**
     * Store a newly created StatutAgrements in storage.
     *
     * @param CreateStatutAgrementsRequest $request
     *
     * @return Response
     */
    public function store(CreateStatutAgrementsRequest $request)
    {
        $input = $request->all();

        $statutAgrements = $this->statutAgrementsRepository->create($input);

        Flashy::success('Statut Agrements enregistré avec succès.');

        return redirect(route('statutAgrements.index'));
    }

    /**
     * Display the specified StatutAgrements.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $statutAgrements = $this->statutAgrementsRepository->findWithoutFail($id);

        if (empty($statutAgrements)) {
            Flashy::error('Statut Agrements non trouvé');

            return redirect(route('statutAgrements.index'));
        }

        return view('backend.statut_agrements.show')->with('statutAgrements', $statutAgrements);
    }

    /**
     * Show the form for editing the specified StatutAgrements.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $statutAgrements = $this->statutAgrementsRepository->findWithoutFail($id);

        if (empty($statutAgrements)) {
            Flashy::error('Statut Agrements non trouvé');

            return redirect(route('statutAgrements.index'));
        }

        return view('backend.statut_agrements.edit')->with('statutAgrements', $statutAgrements);
    }

    /**
     * Update the specified StatutAgrements in storage.
     *
     * @param  int              $id
     * @param UpdateStatutAgrementsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStatutAgrementsRequest $request)
    {
        $statutAgrements = $this->statutAgrementsRepository->findWithoutFail($id);

        if (empty($statutAgrements)) {
            Flashy::error('Statut Agrements non trouvé');

            return redirect(route('statutAgrements.index'));
        }

        $statutAgrements = $this->statutAgrementsRepository->update($request->all(), $id);

        Flashy::success('Statut Agrements mis à jour avec succès.');

        return redirect(route('statutAgrements.index'));
    }

    /**
     * Remove the specified StatutAgrements from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $statutAgrements = $this->statutAgrementsRepository->findWithoutFail($id);

        if (empty($statutAgrements)) {
            Flashy::error('Statut Agrements non trouvé');

            return redirect(route('statutAgrements.index'));
        }

        $this->statutAgrementsRepository->delete($id);

        Flashy::success('Statut Agrements supprimé avec succès.');

        return redirect(route('statutAgrements.index'));
    }
}
