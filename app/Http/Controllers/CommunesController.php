<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCommunesRequest;
use App\Http\Requests\UpdateCommunesRequest;
use App\Repositories\CommunesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flashy;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class CommunesController extends AppBaseController
{
    /** @var  CommunesRepository */
    private $communesRepository;

    public function __construct(CommunesRepository $communesRepo)
    {
        $this->communesRepository = $communesRepo;
    }

    /**
     * Display a listing of the Communes.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->communesRepository->pushCriteria(new RequestCriteria($request));
        $communes = $this->communesRepository->all();

        return view('backend.communes.index')
            ->with('communes', $communes);
    }

    /**
     * Show the form for creating a new Communes.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.communes.create');
    }

    /**
     * Store a newly created Communes in storage.
     *
     * @param CreateCommunesRequest $request
     *
     * @return Response
     */
    public function store(CreateCommunesRequest $request)
    {
        $input = $request->all();

        $communes = $this->communesRepository->create($input);

        Flashy::success('Communes enregistré avec succès.');

        return redirect(route('communes.index'));
    }

    /**
     * Display the specified Communes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $communes = $this->communesRepository->findWithoutFail($id);

        if (empty($communes)) {
            Flashy::error('Communes non trouvé');

            return redirect(route('communes.index'));
        }

        return view('backend.communes.show')->with('communes', $communes);
    }

    /**
     * Show the form for editing the specified Communes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $communes = $this->communesRepository->findWithoutFail($id);

        if (empty($communes)) {
            Flashy::error('Communes non trouvé');

            return redirect(route('communes.index'));
        }

        return view('backend.communes.edit')->with('communes', $communes);
    }

    /**
     * Update the specified Communes in storage.
     *
     * @param  int              $id
     * @param UpdateCommunesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCommunesRequest $request)
    {
        $communes = $this->communesRepository->findWithoutFail($id);

        if (empty($communes)) {
            Flashy::error('Communes non trouvé');

            return redirect(route('communes.index'));
        }

        $communes = $this->communesRepository->update($request->all(), $id);

        Flashy::success('Communes mis à jour avec succès.');

        return redirect(route('communes.index'));
    }

    /**
     * Remove the specified Communes from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $communes = $this->communesRepository->findWithoutFail($id);

        if (empty($communes)) {
            Flashy::error('Communes non trouvé');

            return redirect(route('communes.index'));
        }

        $this->communesRepository->delete($id);

        Flashy::success('Communes supprimé avec succès.');

        return redirect(route('communes.index'));
    }
}
