<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTypeTravauxRequest;
use App\Http\Requests\UpdateTypeTravauxRequest;
use App\Repositories\TypeTravauxRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flashy;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TypeTravauxController extends AppBaseController
{
    /** @var  TypeTravauxRepository */
    private $typeTravauxRepository;

    public function __construct(TypeTravauxRepository $typeTravauxRepo)
    {
        $this->typeTravauxRepository = $typeTravauxRepo;
    }

    /**
     * Display a listing of the TypeTravaux.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->typeTravauxRepository->pushCriteria(new RequestCriteria($request));
        $typeTravauxes = $this->typeTravauxRepository->all();

        return view('backend.type_travauxes.index')
            ->with('typeTravauxes', $typeTravauxes);
    }

    /**
     * Show the form for creating a new TypeTravaux.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.type_travauxes.create');
    }

    /**
     * Store a newly created TypeTravaux in storage.
     *
     * @param CreateTypeTravauxRequest $request
     *
     * @return Response
     */
    public function store(CreateTypeTravauxRequest $request)
    {
        $input = $request->all();

        $typeTravaux = $this->typeTravauxRepository->create($input);

        Flashy::success('Type Travaux enregistré avec succès.');

        return redirect(route('typeTravauxes.index'));
    }

    /**
     * Display the specified TypeTravaux.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $typeTravaux = $this->typeTravauxRepository->findWithoutFail($id);

        if (empty($typeTravaux)) {
            Flashy::error('Type Travaux non trouvé');

            return redirect(route('typeTravauxes.index'));
        }

        return view('backend.type_travauxes.show')->with('typeTravaux', $typeTravaux);
    }

    /**
     * Show the form for editing the specified TypeTravaux.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $typeTravaux = $this->typeTravauxRepository->findWithoutFail($id);

        if (empty($typeTravaux)) {
            Flashy::error('Type Travaux non trouvé');

            return redirect(route('typeTravauxes.index'));
        }

        return view('backend.type_travauxes.edit')->with('typeTravaux', $typeTravaux);
    }

    /**
     * Update the specified TypeTravaux in storage.
     *
     * @param  int              $id
     * @param UpdateTypeTravauxRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTypeTravauxRequest $request)
    {
        $typeTravaux = $this->typeTravauxRepository->findWithoutFail($id);

        if (empty($typeTravaux)) {
            Flashy::error('Type Travaux non trouvé');

            return redirect(route('typeTravauxes.index'));
        }

        $typeTravaux = $this->typeTravauxRepository->update($request->all(), $id);

        Flashy::success('Type Travaux mis à jour avec succès.');

        return redirect(route('typeTravauxes.index'));
    }

    /**
     * Remove the specified TypeTravaux from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $typeTravaux = $this->typeTravauxRepository->findWithoutFail($id);

        if (empty($typeTravaux)) {
            Flashy::error('Type Travaux non trouvé');

            return redirect(route('typeTravauxes.index'));
        }

        $this->typeTravauxRepository->delete($id);

        Flashy::success('Type Travaux supprimé avec succès.');

        return redirect(route('typeTravauxes.index'));
    }
}
