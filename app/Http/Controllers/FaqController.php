<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class FaqController extends AppBaseController
 {   //

        public function recupDonnees()
        {
            $faq = DB::table('faqs')->get(['titre', 'contenu']);
            $faqobj = json_encode($faq);
            //dd($faqobj);
            return  $faqobj;
        }

 }
