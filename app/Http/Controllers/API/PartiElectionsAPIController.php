<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePartiElectionsAPIRequest;
use App\Http\Requests\API\UpdatePartiElectionsAPIRequest;
use App\Models\PartiElections;
use App\Repositories\PartiElectionsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PartiElectionsController
 * @package App\Http\Controllers\API
 */

class PartiElectionsAPIController extends AppBaseController
{
    /** @var  PartiElectionsRepository */
    private $partiElectionsRepository;

    public function __construct(PartiElectionsRepository $partiElectionsRepo)
    {
        $this->partiElectionsRepository = $partiElectionsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/partiElections",
     *      summary="Get a listing of the PartiElections.",
     *      tags={"PartiElections"},
     *      description="Get all PartiElections",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/PartiElections")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->partiElectionsRepository->pushCriteria(new RequestCriteria($request));
        $this->partiElectionsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $partiElections = $this->partiElectionsRepository->all();

        return $this->sendResponse($partiElections->toArray(), 'Parti Elections retrieved successfully');
    }

    /**
     * @param CreatePartiElectionsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/partiElections",
     *      summary="Store a newly created PartiElections in storage",
     *      tags={"PartiElections"},
     *      description="Store PartiElections",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="PartiElections that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/PartiElections")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PartiElections"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePartiElectionsAPIRequest $request)
    {
        $input = $request->all();

        $partiElections = $this->partiElectionsRepository->create($input);

        return $this->sendResponse($partiElections->toArray(), 'Parti Elections enregistré avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/partiElections/{id}",
     *      summary="Display the specified PartiElections",
     *      tags={"PartiElections"},
     *      description="Get PartiElections",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of PartiElections",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PartiElections"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var PartiElections $partiElections */
        $partiElections = $this->partiElectionsRepository->findWithoutFail($id);

        if (empty($partiElections)) {
            return $this->sendError('Parti Elections non trouvé');
        }

        return $this->sendResponse($partiElections->toArray(), 'Parti Elections retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePartiElectionsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/partiElections/{id}",
     *      summary="Update the specified PartiElections in storage",
     *      tags={"PartiElections"},
     *      description="Update PartiElections",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of PartiElections",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="PartiElections that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/PartiElections")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PartiElections"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePartiElectionsAPIRequest $request)
    {
        $input = $request->all();

        /** @var PartiElections $partiElections */
        $partiElections = $this->partiElectionsRepository->findWithoutFail($id);

        if (empty($partiElections)) {
            return $this->sendError('Parti Elections non trouvé');
        }

        $partiElections = $this->partiElectionsRepository->update($input, $id);

        return $this->sendResponse($partiElections->toArray(), 'PartiElections modifié avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/partiElections/{id}",
     *      summary="Remove the specified PartiElections from storage",
     *      tags={"PartiElections"},
     *      description="Delete PartiElections",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of PartiElections",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var PartiElections $partiElections */
        $partiElections = $this->partiElectionsRepository->findWithoutFail($id);

        if (empty($partiElections)) {
            return $this->sendError('Parti Elections non trouvé');
        }

        $partiElections->delete();

        return $this->sendResponse($id, 'Parti Elections supprimé avec succès');
    }
}
