<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateProvincesAPIRequest;
use App\Http\Requests\API\UpdateProvincesAPIRequest;
use App\Models\Provinces;
use App\Repositories\ProvincesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ProvincesController
 * @package App\Http\Controllers\API
 */

class ProvincesAPIController extends AppBaseController
{
    /** @var  ProvincesRepository */
    private $provincesRepository;

    public function __construct(ProvincesRepository $provincesRepo)
    {
        $this->provincesRepository = $provincesRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/provinces",
     *      summary="Get a listing of the Provinces.",
     *      tags={"Provinces"},
     *      description="Get all Provinces",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Provinces")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->provincesRepository->pushCriteria(new RequestCriteria($request));
        $this->provincesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $provinces = $this->provincesRepository->all();

        return $this->sendResponse($provinces->toArray(), 'Provinces retrieved successfully');
    }

    /**
     * @param CreateProvincesAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/provinces",
     *      summary="Store a newly created Provinces in storage",
     *      tags={"Provinces"},
     *      description="Store Provinces",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Provinces that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Provinces")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Provinces"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateProvincesAPIRequest $request)
    {
        $input = $request->all();

        $provinces = $this->provincesRepository->create($input);

        return $this->sendResponse($provinces->toArray(), 'Provinces enregistré avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/provinces/{id}",
     *      summary="Display the specified Provinces",
     *      tags={"Provinces"},
     *      description="Get Provinces",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Provinces",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Provinces"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Provinces $provinces */
        $provinces = $this->provincesRepository->findWithoutFail($id);

        if (empty($provinces)) {
            return $this->sendError('Provinces non trouvé');
        }

        return $this->sendResponse($provinces->toArray(), 'Provinces retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateProvincesAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/provinces/{id}",
     *      summary="Update the specified Provinces in storage",
     *      tags={"Provinces"},
     *      description="Update Provinces",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Provinces",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Provinces that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Provinces")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Provinces"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateProvincesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Provinces $provinces */
        $provinces = $this->provincesRepository->findWithoutFail($id);

        if (empty($provinces)) {
            return $this->sendError('Provinces non trouvé');
        }

        $provinces = $this->provincesRepository->update($input, $id);

        return $this->sendResponse($provinces->toArray(), 'Provinces mis à jour avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/provinces/{id}",
     *      summary="Remove the specified Provinces from storage",
     *      tags={"Provinces"},
     *      description="Delete Provinces",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Provinces",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Provinces $provinces */
        $provinces = $this->provincesRepository->findWithoutFail($id);

        if (empty($provinces)) {
            return $this->sendError('Provinces non trouvé');
        }

        $provinces->delete();

        return $this->sendResponse($id, 'Provinces supprimé avec succès');
    }
}
