<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAgrementsAPIRequest;
use App\Http\Requests\API\UpdateAgrementsAPIRequest;
use App\Models\Agrements;
use App\Repositories\AgrementsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class AgrementsController
 * @package App\Http\Controllers\API
 */

class AgrementsAPIController extends AppBaseController
{
    /** @var  AgrementsRepository */
    private $agrementsRepository;

    public function __construct(AgrementsRepository $agrementsRepo)
    {
        $this->agrementsRepository = $agrementsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/agrements",
     *      summary="Get a listing of the Agrements.",
     *      tags={"Agrements"},
     *      description="Get all Agrements",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Agrements")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->agrementsRepository->pushCriteria(new RequestCriteria($request));
        $this->agrementsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $agrements = $this->agrementsRepository->all();

        return $this->sendResponse($agrements->toArray(), 'Agrements retrieved successfully');
    }

    /**
     * @param CreateAgrementsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/agrements",
     *      summary="Store a newly created Agrements in storage",
     *      tags={"Agrements"},
     *      description="Store Agrements",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Agrements that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Agrements")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Agrements"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateAgrementsAPIRequest $request)
    {
        $input = $request->all();

        $agrements = $this->agrementsRepository->create($input);

        return $this->sendResponse($agrements->toArray(), 'Agrements enregistré avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/agrements/{id}",
     *      summary="Display the specified Agrements",
     *      tags={"Agrements"},
     *      description="Get Agrements",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Agrements",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Agrements"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Agrements $agrements */
        $agrements = $this->agrementsRepository->findWithoutFail($id);

        if (empty($agrements)) {
            return $this->sendError('Agrements non trouvé');
        }

        return $this->sendResponse($agrements->toArray(), 'Agrements retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateAgrementsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/agrements/{id}",
     *      summary="Update the specified Agrements in storage",
     *      tags={"Agrements"},
     *      description="Update Agrements",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Agrements",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Agrements that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Agrements")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Agrements"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateAgrementsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Agrements $agrements */
        $agrements = $this->agrementsRepository->findWithoutFail($id);

        if (empty($agrements)) {
            return $this->sendError('Agrements non trouvé');
        }

        $agrements = $this->agrementsRepository->update($input, $id);

        return $this->sendResponse($agrements->toArray(), 'Agrements mis à jour avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/agrements/{id}",
     *      summary="Remove the specified Agrements from storage",
     *      tags={"Agrements"},
     *      description="Delete Agrements",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Agrements",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Agrements $agrements */
        $agrements = $this->agrementsRepository->findWithoutFail($id);

        if (empty($agrements)) {
            return $this->sendError('Agrements non trouvé');
        }

        $agrements->delete();

        return $this->sendResponse($id, 'Agrements supprimé avec succès');
    }
}
