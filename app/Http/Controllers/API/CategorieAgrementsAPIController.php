<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCategorieAgrementsAPIRequest;
use App\Http\Requests\API\UpdateCategorieAgrementsAPIRequest;
use App\Models\CategorieAgrements;
use App\Repositories\CategorieAgrementsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CategorieAgrementsController
 * @package App\Http\Controllers\API
 */

class CategorieAgrementsAPIController extends AppBaseController
{
    /** @var  CategorieAgrementsRepository */
    private $categorieAgrementsRepository;

    public function __construct(CategorieAgrementsRepository $categorieAgrementsRepo)
    {
        $this->categorieAgrementsRepository = $categorieAgrementsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/categorieAgrements",
     *      summary="Get a listing of the CategorieAgrements.",
     *      tags={"CategorieAgrements"},
     *      description="Get all CategorieAgrements",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/CategorieAgrements")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->categorieAgrementsRepository->pushCriteria(new RequestCriteria($request));
        $this->categorieAgrementsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $categorieAgrements = $this->categorieAgrementsRepository->all();

        return $this->sendResponse($categorieAgrements->toArray(), 'Categorie Agrements retrieved successfully');
    }

    /**
     * @param CreateCategorieAgrementsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/categorieAgrements",
     *      summary="Store a newly created CategorieAgrements in storage",
     *      tags={"CategorieAgrements"},
     *      description="Store CategorieAgrements",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CategorieAgrements that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CategorieAgrements")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CategorieAgrements"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCategorieAgrementsAPIRequest $request)
    {
        $input = $request->all();

        $categorieAgrements = $this->categorieAgrementsRepository->create($input);

        return $this->sendResponse($categorieAgrements->toArray(), 'Categorie Agrements enregistré avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/categorieAgrements/{id}",
     *      summary="Display the specified CategorieAgrements",
     *      tags={"CategorieAgrements"},
     *      description="Get CategorieAgrements",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CategorieAgrements",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CategorieAgrements"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var CategorieAgrements $categorieAgrements */
        $categorieAgrements = $this->categorieAgrementsRepository->findWithoutFail($id);

        if (empty($categorieAgrements)) {
            return $this->sendError('Categorie Agrements non trouvé');
        }

        return $this->sendResponse($categorieAgrements->toArray(), 'Categorie Agrements retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCategorieAgrementsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/categorieAgrements/{id}",
     *      summary="Update the specified CategorieAgrements in storage",
     *      tags={"CategorieAgrements"},
     *      description="Update CategorieAgrements",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CategorieAgrements",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CategorieAgrements that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CategorieAgrements")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CategorieAgrements"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCategorieAgrementsAPIRequest $request)
    {
        $input = $request->all();

        /** @var CategorieAgrements $categorieAgrements */
        $categorieAgrements = $this->categorieAgrementsRepository->findWithoutFail($id);

        if (empty($categorieAgrements)) {
            return $this->sendError('Categorie Agrements non trouvé');
        }

        $categorieAgrements = $this->categorieAgrementsRepository->update($input, $id);

        return $this->sendResponse($categorieAgrements->toArray(), 'CategorieAgrements mis à jour avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/categorieAgrements/{id}",
     *      summary="Remove the specified CategorieAgrements from storage",
     *      tags={"CategorieAgrements"},
     *      description="Delete CategorieAgrements",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CategorieAgrements",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var CategorieAgrements $categorieAgrements */
        $categorieAgrements = $this->categorieAgrementsRepository->findWithoutFail($id);

        if (empty($categorieAgrements)) {
            return $this->sendError('Categorie Agrements non trouvé');
        }

        $categorieAgrements->delete();

        return $this->sendResponse($id, 'Categorie Agrements supprimé avec succès');
    }
}
