<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateResultatsAPIRequest;
use App\Http\Requests\API\UpdateResultatsAPIRequest;
use App\Models\Resultats;
use App\Repositories\ResultatsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ResultatsController
 * @package App\Http\Controllers\API
 */

class ResultatsAPIController extends AppBaseController
{
    /** @var  ResultatsRepository */
    private $resultatsRepository;

    public function __construct(ResultatsRepository $resultatsRepo)
    {
        $this->resultatsRepository = $resultatsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/resultats",
     *      summary="Get a listing of the Resultats.",
     *      tags={"Resultats"},
     *      description="Get all Resultats",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Resultats")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->resultatsRepository->pushCriteria(new RequestCriteria($request));
        $this->resultatsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $resultats = $this->resultatsRepository->all();

        return $this->sendResponse($resultats->toArray(), 'Resultats retrieved successfully');
    }

    /**
     * @param CreateResultatsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/resultats",
     *      summary="Store a newly created Resultats in storage",
     *      tags={"Resultats"},
     *      description="Store Resultats",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Resultats that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Resultats")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Resultats"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateResultatsAPIRequest $request)
    {
        $input = $request->all();

        $resultats = $this->resultatsRepository->create($input);

        return $this->sendResponse($resultats->toArray(), 'Resultats enregistré avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/resultats/{id}",
     *      summary="Display the specified Resultats",
     *      tags={"Resultats"},
     *      description="Get Resultats",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Resultats",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Resultats"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Resultats $resultats */
        $resultats = $this->resultatsRepository->findWithoutFail($id);

        if (empty($resultats)) {
            return $this->sendError('Resultats non trouvé');
        }

        return $this->sendResponse($resultats->toArray(), 'Resultats retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateResultatsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/resultats/{id}",
     *      summary="Update the specified Resultats in storage",
     *      tags={"Resultats"},
     *      description="Update Resultats",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Resultats",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Resultats that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Resultats")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Resultats"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateResultatsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Resultats $resultats */
        $resultats = $this->resultatsRepository->findWithoutFail($id);

        if (empty($resultats)) {
            return $this->sendError('Resultats non trouvé');
        }

        $resultats = $this->resultatsRepository->update($input, $id);

        return $this->sendResponse($resultats->toArray(), 'Resultats modifié avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/resultats/{id}",
     *      summary="Remove the specified Resultats from storage",
     *      tags={"Resultats"},
     *      description="Delete Resultats",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Resultats",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Resultats $resultats */
        $resultats = $this->resultatsRepository->findWithoutFail($id);

        if (empty($resultats)) {
            return $this->sendError('Resultats non trouvé');
        }

        $resultats->delete();

        return $this->sendResponse($id, 'Resultats supprimé avec succès');
    }
}
