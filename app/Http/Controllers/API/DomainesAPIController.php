<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDomainesAPIRequest;
use App\Http\Requests\API\UpdateDomainesAPIRequest;
use App\Models\Domaines;
use App\Repositories\DomainesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class DomainesController
 * @package App\Http\Controllers\API
 */

class DomainesAPIController extends AppBaseController
{
    /** @var  DomainesRepository */
    private $domainesRepository;

    public function __construct(DomainesRepository $domainesRepo)
    {
        $this->domainesRepository = $domainesRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/domaines",
     *      summary="Get a listing of the Domaines.",
     *      tags={"Domaines"},
     *      description="Get all Domaines",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Domaines")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->domainesRepository->pushCriteria(new RequestCriteria($request));
        $this->domainesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $domaines = $this->domainesRepository->all();

        return $this->sendResponse($domaines->toArray(), 'Domaines retrieved successfully');
    }

    /**
     * @param CreateDomainesAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/domaines",
     *      summary="Store a newly created Domaines in storage",
     *      tags={"Domaines"},
     *      description="Store Domaines",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Domaines that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Domaines")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Domaines"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateDomainesAPIRequest $request)
    {
        $input = $request->all();

        $domaines = $this->domainesRepository->create($input);

        return $this->sendResponse($domaines->toArray(), 'Domaines enregistré avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/domaines/{id}",
     *      summary="Display the specified Domaines",
     *      tags={"Domaines"},
     *      description="Get Domaines",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Domaines",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Domaines"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Domaines $domaines */
        $domaines = $this->domainesRepository->findWithoutFail($id);

        if (empty($domaines)) {
            return $this->sendError('Domaines non trouvé');
        }

        return $this->sendResponse($domaines->toArray(), 'Domaines retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateDomainesAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/domaines/{id}",
     *      summary="Update the specified Domaines in storage",
     *      tags={"Domaines"},
     *      description="Update Domaines",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Domaines",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Domaines that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Domaines")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Domaines"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateDomainesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Domaines $domaines */
        $domaines = $this->domainesRepository->findWithoutFail($id);

        if (empty($domaines)) {
            return $this->sendError('Domaines non trouvé');
        }

        $domaines = $this->domainesRepository->update($input, $id);

        return $this->sendResponse($domaines->toArray(), 'Domaines mis à jour avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/domaines/{id}",
     *      summary="Remove the specified Domaines from storage",
     *      tags={"Domaines"},
     *      description="Delete Domaines",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Domaines",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Domaines $domaines */
        $domaines = $this->domainesRepository->findWithoutFail($id);

        if (empty($domaines)) {
            return $this->sendError('Domaines non trouvé');
        }

        $domaines->delete();

        return $this->sendResponse($id, 'Domaines supprimé avec succès');
    }
}
