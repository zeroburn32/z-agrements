<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTypeTravauxAPIRequest;
use App\Http\Requests\API\UpdateTypeTravauxAPIRequest;
use App\Models\TypeTravaux;
use App\Repositories\TypeTravauxRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class TypeTravauxController
 * @package App\Http\Controllers\API
 */

class TypeTravauxAPIController extends AppBaseController
{
    /** @var  TypeTravauxRepository */
    private $typeTravauxRepository;

    public function __construct(TypeTravauxRepository $typeTravauxRepo)
    {
        $this->typeTravauxRepository = $typeTravauxRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/typeTravauxes",
     *      summary="Get a listing of the TypeTravauxes.",
     *      tags={"TypeTravaux"},
     *      description="Get all TypeTravauxes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/TypeTravaux")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->typeTravauxRepository->pushCriteria(new RequestCriteria($request));
        $this->typeTravauxRepository->pushCriteria(new LimitOffsetCriteria($request));
        $typeTravauxes = $this->typeTravauxRepository->all();

        return $this->sendResponse($typeTravauxes->toArray(), 'Type Travauxes retrieved successfully');
    }

    /**
     * @param CreateTypeTravauxAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/typeTravauxes",
     *      summary="Store a newly created TypeTravaux in storage",
     *      tags={"TypeTravaux"},
     *      description="Store TypeTravaux",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TypeTravaux that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TypeTravaux")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TypeTravaux"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateTypeTravauxAPIRequest $request)
    {
        $input = $request->all();

        $typeTravaux = $this->typeTravauxRepository->create($input);

        return $this->sendResponse($typeTravaux->toArray(), 'Type Travaux enregistré avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/typeTravauxes/{id}",
     *      summary="Display the specified TypeTravaux",
     *      tags={"TypeTravaux"},
     *      description="Get TypeTravaux",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TypeTravaux",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TypeTravaux"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var TypeTravaux $typeTravaux */
        $typeTravaux = $this->typeTravauxRepository->findWithoutFail($id);

        if (empty($typeTravaux)) {
            return $this->sendError('Type Travaux non trouvé');
        }

        return $this->sendResponse($typeTravaux->toArray(), 'Type Travaux retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateTypeTravauxAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/typeTravauxes/{id}",
     *      summary="Update the specified TypeTravaux in storage",
     *      tags={"TypeTravaux"},
     *      description="Update TypeTravaux",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TypeTravaux",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TypeTravaux that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TypeTravaux")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TypeTravaux"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateTypeTravauxAPIRequest $request)
    {
        $input = $request->all();

        /** @var TypeTravaux $typeTravaux */
        $typeTravaux = $this->typeTravauxRepository->findWithoutFail($id);

        if (empty($typeTravaux)) {
            return $this->sendError('Type Travaux non trouvé');
        }

        $typeTravaux = $this->typeTravauxRepository->update($input, $id);

        return $this->sendResponse($typeTravaux->toArray(), 'TypeTravaux mis à jour avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/typeTravauxes/{id}",
     *      summary="Remove the specified TypeTravaux from storage",
     *      tags={"TypeTravaux"},
     *      description="Delete TypeTravaux",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TypeTravaux",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var TypeTravaux $typeTravaux */
        $typeTravaux = $this->typeTravauxRepository->findWithoutFail($id);

        if (empty($typeTravaux)) {
            return $this->sendError('Type Travaux non trouvé');
        }

        $typeTravaux->delete();

        return $this->sendResponse($id, 'Type Travaux supprimé avec succès');
    }
}
