<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCommunesAPIRequest;
use App\Http\Requests\API\UpdateCommunesAPIRequest;
use App\Models\Communes;
use App\Repositories\CommunesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CommunesController
 * @package App\Http\Controllers\API
 */

class CommunesAPIController extends AppBaseController
{
    /** @var  CommunesRepository */
    private $communesRepository;

    public function __construct(CommunesRepository $communesRepo)
    {
        $this->communesRepository = $communesRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/communes",
     *      summary="Get a listing of the Communes.",
     *      tags={"Communes"},
     *      description="Get all Communes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Communes")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->communesRepository->pushCriteria(new RequestCriteria($request));
        $this->communesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $communes = $this->communesRepository->all();

        return $this->sendResponse($communes->toArray(), 'Communes retrieved successfully');
    }

    /**
     * @param CreateCommunesAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/communes",
     *      summary="Store a newly created Communes in storage",
     *      tags={"Communes"},
     *      description="Store Communes",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Communes that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Communes")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Communes"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCommunesAPIRequest $request)
    {
        $input = $request->all();

        $communes = $this->communesRepository->create($input);

        return $this->sendResponse($communes->toArray(), 'Communes enregistré avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/communes/{id}",
     *      summary="Display the specified Communes",
     *      tags={"Communes"},
     *      description="Get Communes",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Communes",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Communes"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Communes $communes */
        $communes = $this->communesRepository->findWithoutFail($id);

        if (empty($communes)) {
            return $this->sendError('Communes non trouvé');
        }

        return $this->sendResponse($communes->toArray(), 'Communes retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCommunesAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/communes/{id}",
     *      summary="Update the specified Communes in storage",
     *      tags={"Communes"},
     *      description="Update Communes",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Communes",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Communes that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Communes")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Communes"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCommunesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Communes $communes */
        $communes = $this->communesRepository->findWithoutFail($id);

        if (empty($communes)) {
            return $this->sendError('Communes non trouvé');
        }

        $communes = $this->communesRepository->update($input, $id);

        return $this->sendResponse($communes->toArray(), 'Communes mis à jour avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/communes/{id}",
     *      summary="Remove the specified Communes from storage",
     *      tags={"Communes"},
     *      description="Delete Communes",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Communes",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Communes $communes */
        $communes = $this->communesRepository->findWithoutFail($id);

        if (empty($communes)) {
            return $this->sendError('Communes non trouvé');
        }

        $communes->delete();

        return $this->sendResponse($id, 'Communes supprimé avec succès');
    }
}
