<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStatutResultatsAPIRequest;
use App\Http\Requests\API\UpdateStatutResultatsAPIRequest;
use App\Models\StatutResultats;
use App\Repositories\StatutResultatsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class StatutResultatsController
 * @package App\Http\Controllers\API
 */

class StatutResultatsAPIController extends AppBaseController
{
    /** @var  StatutResultatsRepository */
    private $statutResultatsRepository;

    public function __construct(StatutResultatsRepository $statutResultatsRepo)
    {
        $this->statutResultatsRepository = $statutResultatsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/statutResultats",
     *      summary="Get a listing of the StatutResultats.",
     *      tags={"StatutResultats"},
     *      description="Get all StatutResultats",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/StatutResultats")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->statutResultatsRepository->pushCriteria(new RequestCriteria($request));
        $this->statutResultatsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $statutResultats = $this->statutResultatsRepository->all();

        return $this->sendResponse($statutResultats->toArray(), 'Statut Resultats retrieved successfully');
    }

    /**
     * @param CreateStatutResultatsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/statutResultats",
     *      summary="Store a newly created StatutResultats in storage",
     *      tags={"StatutResultats"},
     *      description="Store StatutResultats",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="StatutResultats that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/StatutResultats")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/StatutResultats"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateStatutResultatsAPIRequest $request)
    {
        $input = $request->all();

        $statutResultats = $this->statutResultatsRepository->create($input);

        return $this->sendResponse($statutResultats->toArray(), 'Statut Resultats enregistré avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/statutResultats/{id}",
     *      summary="Display the specified StatutResultats",
     *      tags={"StatutResultats"},
     *      description="Get StatutResultats",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of StatutResultats",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/StatutResultats"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var StatutResultats $statutResultats */
        $statutResultats = $this->statutResultatsRepository->findWithoutFail($id);

        if (empty($statutResultats)) {
            return $this->sendError('Statut Resultats non trouvé');
        }

        return $this->sendResponse($statutResultats->toArray(), 'Statut Resultats retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateStatutResultatsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/statutResultats/{id}",
     *      summary="Update the specified StatutResultats in storage",
     *      tags={"StatutResultats"},
     *      description="Update StatutResultats",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of StatutResultats",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="StatutResultats that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/StatutResultats")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/StatutResultats"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateStatutResultatsAPIRequest $request)
    {
        $input = $request->all();

        /** @var StatutResultats $statutResultats */
        $statutResultats = $this->statutResultatsRepository->findWithoutFail($id);

        if (empty($statutResultats)) {
            return $this->sendError('Statut Resultats non trouvé');
        }

        $statutResultats = $this->statutResultatsRepository->update($input, $id);

        return $this->sendResponse($statutResultats->toArray(), 'StatutResultats modifié avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/statutResultats/{id}",
     *      summary="Remove the specified StatutResultats from storage",
     *      tags={"StatutResultats"},
     *      description="Delete StatutResultats",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of StatutResultats",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var StatutResultats $statutResultats */
        $statutResultats = $this->statutResultatsRepository->findWithoutFail($id);

        if (empty($statutResultats)) {
            return $this->sendError('Statut Resultats non trouvé');
        }

        $statutResultats->delete();

        return $this->sendResponse($id, 'Statut Resultats supprimé avec succès');
    }
}
