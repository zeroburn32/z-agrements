<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCandidatElectionsAPIRequest;
use App\Http\Requests\API\UpdateCandidatElectionsAPIRequest;
use App\Models\CandidatElections;
use App\Repositories\CandidatElectionsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CandidatElectionsController
 * @package App\Http\Controllers\API
 */

class CandidatElectionsAPIController extends AppBaseController
{
    /** @var  CandidatElectionsRepository */
    private $candidatElectionsRepository;

    public function __construct(CandidatElectionsRepository $candidatElectionsRepo)
    {
        $this->candidatElectionsRepository = $candidatElectionsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/candidatElections",
     *      summary="Get a listing of the CandidatElections.",
     *      tags={"CandidatElections"},
     *      description="Get all CandidatElections",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/CandidatElections")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->candidatElectionsRepository->pushCriteria(new RequestCriteria($request));
        $this->candidatElectionsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $candidatElections = $this->candidatElectionsRepository->all();

        return $this->sendResponse($candidatElections->toArray(), 'Candidat Elections retrieved successfully');
    }

    /**
     * @param CreateCandidatElectionsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/candidatElections",
     *      summary="Store a newly created CandidatElections in storage",
     *      tags={"CandidatElections"},
     *      description="Store CandidatElections",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CandidatElections that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CandidatElections")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CandidatElections"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCandidatElectionsAPIRequest $request)
    {
        $input = $request->all();

        $candidatElections = $this->candidatElectionsRepository->create($input);

        return $this->sendResponse($candidatElections->toArray(), 'Candidat Elections enregistré avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/candidatElections/{id}",
     *      summary="Display the specified CandidatElections",
     *      tags={"CandidatElections"},
     *      description="Get CandidatElections",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CandidatElections",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CandidatElections"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var CandidatElections $candidatElections */
        $candidatElections = $this->candidatElectionsRepository->findWithoutFail($id);

        if (empty($candidatElections)) {
            return $this->sendError('Candidat Elections non trouvé');
        }

        return $this->sendResponse($candidatElections->toArray(), 'Candidat Elections retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCandidatElectionsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/candidatElections/{id}",
     *      summary="Update the specified CandidatElections in storage",
     *      tags={"CandidatElections"},
     *      description="Update CandidatElections",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CandidatElections",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CandidatElections that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CandidatElections")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CandidatElections"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCandidatElectionsAPIRequest $request)
    {
        $input = $request->all();

        /** @var CandidatElections $candidatElections */
        $candidatElections = $this->candidatElectionsRepository->findWithoutFail($id);

        if (empty($candidatElections)) {
            return $this->sendError('Candidat Elections non trouvé');
        }

        $candidatElections = $this->candidatElectionsRepository->update($input, $id);

        return $this->sendResponse($candidatElections->toArray(), 'CandidatElections modifié avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/candidatElections/{id}",
     *      summary="Remove the specified CandidatElections from storage",
     *      tags={"CandidatElections"},
     *      description="Delete CandidatElections",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CandidatElections",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var CandidatElections $candidatElections */
        $candidatElections = $this->candidatElectionsRepository->findWithoutFail($id);

        if (empty($candidatElections)) {
            return $this->sendError('Candidat Elections non trouvé');
        }

        $candidatElections->delete();

        return $this->sendResponse($id, 'Candidat Elections supprimé avec succès');
    }
}
