<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStatutAgrementsAPIRequest;
use App\Http\Requests\API\UpdateStatutAgrementsAPIRequest;
use App\Models\StatutAgrements;
use App\Repositories\StatutAgrementsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class StatutAgrementsController
 * @package App\Http\Controllers\API
 */

class StatutAgrementsAPIController extends AppBaseController
{
    /** @var  StatutAgrementsRepository */
    private $statutAgrementsRepository;

    public function __construct(StatutAgrementsRepository $statutAgrementsRepo)
    {
        $this->statutAgrementsRepository = $statutAgrementsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/statutAgrements",
     *      summary="Get a listing of the StatutAgrements.",
     *      tags={"StatutAgrements"},
     *      description="Get all StatutAgrements",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/StatutAgrements")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->statutAgrementsRepository->pushCriteria(new RequestCriteria($request));
        $this->statutAgrementsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $statutAgrements = $this->statutAgrementsRepository->all();

        return $this->sendResponse($statutAgrements->toArray(), 'Statut Agrements retrieved successfully');
    }

    /**
     * @param CreateStatutAgrementsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/statutAgrements",
     *      summary="Store a newly created StatutAgrements in storage",
     *      tags={"StatutAgrements"},
     *      description="Store StatutAgrements",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="StatutAgrements that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/StatutAgrements")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/StatutAgrements"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateStatutAgrementsAPIRequest $request)
    {
        $input = $request->all();

        $statutAgrements = $this->statutAgrementsRepository->create($input);

        return $this->sendResponse($statutAgrements->toArray(), 'Statut Agrements enregistré avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/statutAgrements/{id}",
     *      summary="Display the specified StatutAgrements",
     *      tags={"StatutAgrements"},
     *      description="Get StatutAgrements",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of StatutAgrements",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/StatutAgrements"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var StatutAgrements $statutAgrements */
        $statutAgrements = $this->statutAgrementsRepository->findWithoutFail($id);

        if (empty($statutAgrements)) {
            return $this->sendError('Statut Agrements non trouvé');
        }

        return $this->sendResponse($statutAgrements->toArray(), 'Statut Agrements retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateStatutAgrementsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/statutAgrements/{id}",
     *      summary="Update the specified StatutAgrements in storage",
     *      tags={"StatutAgrements"},
     *      description="Update StatutAgrements",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of StatutAgrements",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="StatutAgrements that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/StatutAgrements")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/StatutAgrements"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateStatutAgrementsAPIRequest $request)
    {
        $input = $request->all();

        /** @var StatutAgrements $statutAgrements */
        $statutAgrements = $this->statutAgrementsRepository->findWithoutFail($id);

        if (empty($statutAgrements)) {
            return $this->sendError('Statut Agrements non trouvé');
        }

        $statutAgrements = $this->statutAgrementsRepository->update($input, $id);

        return $this->sendResponse($statutAgrements->toArray(), 'StatutAgrements mis à jour avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/statutAgrements/{id}",
     *      summary="Remove the specified StatutAgrements from storage",
     *      tags={"StatutAgrements"},
     *      description="Delete StatutAgrements",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of StatutAgrements",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var StatutAgrements $statutAgrements */
        $statutAgrements = $this->statutAgrementsRepository->findWithoutFail($id);

        if (empty($statutAgrements)) {
            return $this->sendError('Statut Agrements non trouvé');
        }

        $statutAgrements->delete();

        return $this->sendResponse($id, 'Statut Agrements supprimé avec succès');
    }
}
