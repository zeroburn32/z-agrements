<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStatutsAPIRequest;
use App\Http\Requests\API\UpdateStatutsAPIRequest;
use App\Models\Statuts;
use App\Repositories\StatutsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class StatutsController
 * @package App\Http\Controllers\API
 */

class StatutsAPIController extends AppBaseController
{
    /** @var  StatutsRepository */
    private $statutsRepository;

    public function __construct(StatutsRepository $statutsRepo)
    {
        $this->statutsRepository = $statutsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/statuts",
     *      summary="Get a listing of the Statuts.",
     *      tags={"Statuts"},
     *      description="Get all Statuts",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Statuts")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->statutsRepository->pushCriteria(new RequestCriteria($request));
        $this->statutsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $statuts = $this->statutsRepository->all();

        return $this->sendResponse($statuts->toArray(), 'Statuts retrieved successfully');
    }

    /**
     * @param CreateStatutsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/statuts",
     *      summary="Store a newly created Statuts in storage",
     *      tags={"Statuts"},
     *      description="Store Statuts",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Statuts that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Statuts")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Statuts"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateStatutsAPIRequest $request)
    {
        $input = $request->all();

        $statuts = $this->statutsRepository->create($input);

        return $this->sendResponse($statuts->toArray(), 'Statuts enregistré avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/statuts/{id}",
     *      summary="Display the specified Statuts",
     *      tags={"Statuts"},
     *      description="Get Statuts",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Statuts",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Statuts"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Statuts $statuts */
        $statuts = $this->statutsRepository->findWithoutFail($id);

        if (empty($statuts)) {
            return $this->sendError('Statuts non trouvé');
        }

        return $this->sendResponse($statuts->toArray(), 'Statuts retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateStatutsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/statuts/{id}",
     *      summary="Update the specified Statuts in storage",
     *      tags={"Statuts"},
     *      description="Update Statuts",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Statuts",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Statuts that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Statuts")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Statuts"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateStatutsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Statuts $statuts */
        $statuts = $this->statutsRepository->findWithoutFail($id);

        if (empty($statuts)) {
            return $this->sendError('Statuts non trouvé');
        }

        $statuts = $this->statutsRepository->update($input, $id);

        return $this->sendResponse($statuts->toArray(), 'Statuts mis à jour avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/statuts/{id}",
     *      summary="Remove the specified Statuts from storage",
     *      tags={"Statuts"},
     *      description="Delete Statuts",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Statuts",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Statuts $statuts */
        $statuts = $this->statutsRepository->findWithoutFail($id);

        if (empty($statuts)) {
            return $this->sendError('Statuts non trouvé');
        }

        $statuts->delete();

        return $this->sendResponse($id, 'Statuts supprimé avec succès');
    }
}
