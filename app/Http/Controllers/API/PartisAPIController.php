<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePartisAPIRequest;
use App\Http\Requests\API\UpdatePartisAPIRequest;
use App\Models\Partis;
use App\Repositories\PartisRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PartisController
 * @package App\Http\Controllers\API
 */

class PartisAPIController extends AppBaseController
{
    /** @var  PartisRepository */
    private $partisRepository;

    public function __construct(PartisRepository $partisRepo)
    {
        $this->partisRepository = $partisRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/partis",
     *      summary="Get a listing of the Partis.",
     *      tags={"Partis"},
     *      description="Get all Partis",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Partis")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->partisRepository->pushCriteria(new RequestCriteria($request));
        $this->partisRepository->pushCriteria(new LimitOffsetCriteria($request));
        $partis = $this->partisRepository->all();

        return $this->sendResponse($partis->toArray(), 'Partis retrieved successfully');
    }

    /**
     * @param CreatePartisAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/partis",
     *      summary="Store a newly created Partis in storage",
     *      tags={"Partis"},
     *      description="Store Partis",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Partis that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Partis")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Partis"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePartisAPIRequest $request)
    {
        $input = $request->all();

        $partis = $this->partisRepository->create($input);

        return $this->sendResponse($partis->toArray(), 'Partis enregistré avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/partis/{id}",
     *      summary="Display the specified Partis",
     *      tags={"Partis"},
     *      description="Get Partis",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Partis",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Partis"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Partis $partis */
        $partis = $this->partisRepository->findWithoutFail($id);

        if (empty($partis)) {
            return $this->sendError('Partis non trouvé');
        }

        return $this->sendResponse($partis->toArray(), 'Partis retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePartisAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/partis/{id}",
     *      summary="Update the specified Partis in storage",
     *      tags={"Partis"},
     *      description="Update Partis",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Partis",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Partis that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Partis")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Partis"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePartisAPIRequest $request)
    {
        $input = $request->all();

        /** @var Partis $partis */
        $partis = $this->partisRepository->findWithoutFail($id);

        if (empty($partis)) {
            return $this->sendError('Partis non trouvé');
        }

        $partis = $this->partisRepository->update($input, $id);

        return $this->sendResponse($partis->toArray(), 'Partis modifié avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/partis/{id}",
     *      summary="Remove the specified Partis from storage",
     *      tags={"Partis"},
     *      description="Delete Partis",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Partis",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Partis $partis */
        $partis = $this->partisRepository->findWithoutFail($id);

        if (empty($partis)) {
            return $this->sendError('Partis non trouvé');
        }

        $partis->delete();

        return $this->sendResponse($id, 'Partis supprimé avec succès');
    }
}
