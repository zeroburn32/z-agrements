<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateElectionsAPIRequest;
use App\Http\Requests\API\UpdateElectionsAPIRequest;
use App\Models\Elections;
use App\Repositories\ElectionsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ElectionsController
 * @package App\Http\Controllers\API
 */

class ElectionsAPIController extends AppBaseController
{
    /** @var  ElectionsRepository */
    private $electionsRepository;

    public function __construct(ElectionsRepository $electionsRepo)
    {
        $this->electionsRepository = $electionsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/elections",
     *      summary="Get a listing of the Elections.",
     *      tags={"Elections"},
     *      description="Get all Elections",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Elections")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->electionsRepository->pushCriteria(new RequestCriteria($request));
        $this->electionsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $elections = $this->electionsRepository->all();

        return $this->sendResponse($elections->toArray(), 'Elections retrieved successfully');
    }

    /**
     * @param CreateElectionsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/elections",
     *      summary="Store a newly created Elections in storage",
     *      tags={"Elections"},
     *      description="Store Elections",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Elections that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Elections")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Elections"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateElectionsAPIRequest $request)
    {
        $input = $request->all();

        $elections = $this->electionsRepository->create($input);

        return $this->sendResponse($elections->toArray(), 'Elections enregistré avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/elections/{id}",
     *      summary="Display the specified Elections",
     *      tags={"Elections"},
     *      description="Get Elections",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Elections",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Elections"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Elections $elections */
        $elections = $this->electionsRepository->findWithoutFail($id);

        if (empty($elections)) {
            return $this->sendError('Elections non trouvé');
        }

        return $this->sendResponse($elections->toArray(), 'Elections retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateElectionsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/elections/{id}",
     *      summary="Update the specified Elections in storage",
     *      tags={"Elections"},
     *      description="Update Elections",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Elections",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Elections that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Elections")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Elections"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateElectionsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Elections $elections */
        $elections = $this->electionsRepository->findWithoutFail($id);

        if (empty($elections)) {
            return $this->sendError('Elections non trouvé');
        }

        $elections = $this->electionsRepository->update($input, $id);

        return $this->sendResponse($elections->toArray(), 'Elections modifié avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/elections/{id}",
     *      summary="Remove the specified Elections from storage",
     *      tags={"Elections"},
     *      description="Delete Elections",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Elections",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Elections $elections */
        $elections = $this->electionsRepository->findWithoutFail($id);

        if (empty($elections)) {
            return $this->sendError('Elections non trouvé');
        }

        $elections->delete();

        return $this->sendResponse($id, 'Elections supprimé avec succès');
    }
}
