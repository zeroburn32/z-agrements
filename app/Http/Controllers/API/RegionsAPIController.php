<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRegionsAPIRequest;
use App\Http\Requests\API\UpdateRegionsAPIRequest;
use App\Models\Regions;
use App\Repositories\RegionsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class RegionsController
 * @package App\Http\Controllers\API
 */

class RegionsAPIController extends AppBaseController
{
    /** @var  RegionsRepository */
    private $regionsRepository;

    public function __construct(RegionsRepository $regionsRepo)
    {
        $this->regionsRepository = $regionsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/regions",
     *      summary="Get a listing of the Regions.",
     *      tags={"Regions"},
     *      description="Get all Regions",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Regions")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->regionsRepository->pushCriteria(new RequestCriteria($request));
        $this->regionsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $regions = $this->regionsRepository->all();

        return $this->sendResponse($regions->toArray(), 'Regions retrieved successfully');
    }

    /**
     * @param CreateRegionsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/regions",
     *      summary="Store a newly created Regions in storage",
     *      tags={"Regions"},
     *      description="Store Regions",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Regions that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Regions")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Regions"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateRegionsAPIRequest $request)
    {
        $input = $request->all();

        $regions = $this->regionsRepository->create($input);

        return $this->sendResponse($regions->toArray(), 'Regions enregistré avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/regions/{id}",
     *      summary="Display the specified Regions",
     *      tags={"Regions"},
     *      description="Get Regions",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Regions",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Regions"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Regions $regions */
        $regions = $this->regionsRepository->findWithoutFail($id);

        if (empty($regions)) {
            return $this->sendError('Regions non trouvé');
        }

        return $this->sendResponse($regions->toArray(), 'Regions retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateRegionsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/regions/{id}",
     *      summary="Update the specified Regions in storage",
     *      tags={"Regions"},
     *      description="Update Regions",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Regions",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Regions that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Regions")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Regions"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateRegionsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Regions $regions */
        $regions = $this->regionsRepository->findWithoutFail($id);

        if (empty($regions)) {
            return $this->sendError('Regions non trouvé');
        }

        $regions = $this->regionsRepository->update($input, $id);

        return $this->sendResponse($regions->toArray(), 'Regions mis à jour avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/regions/{id}",
     *      summary="Remove the specified Regions from storage",
     *      tags={"Regions"},
     *      description="Delete Regions",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Regions",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Regions $regions */
        $regions = $this->regionsRepository->findWithoutFail($id);

        if (empty($regions)) {
            return $this->sendError('Regions non trouvé');
        }

        $regions->delete();

        return $this->sendResponse($id, 'Regions supprimé avec succès');
    }
}
