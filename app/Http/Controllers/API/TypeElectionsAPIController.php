<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTypeElectionsAPIRequest;
use App\Http\Requests\API\UpdateTypeElectionsAPIRequest;
use App\Models\TypeElections;
use App\Repositories\TypeElectionsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class TypeElectionsController
 * @package App\Http\Controllers\API
 */

class TypeElectionsAPIController extends AppBaseController
{
    /** @var  TypeElectionsRepository */
    private $typeElectionsRepository;

    public function __construct(TypeElectionsRepository $typeElectionsRepo)
    {
        $this->typeElectionsRepository = $typeElectionsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/typeElections",
     *      summary="Get a listing of the TypeElections.",
     *      tags={"TypeElections"},
     *      description="Get all TypeElections",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/TypeElections")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->typeElectionsRepository->pushCriteria(new RequestCriteria($request));
        $this->typeElectionsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $typeElections = $this->typeElectionsRepository->all();

        return $this->sendResponse($typeElections->toArray(), 'Type Elections retrieved successfully');
    }

    /**
     * @param CreateTypeElectionsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/typeElections",
     *      summary="Store a newly created TypeElections in storage",
     *      tags={"TypeElections"},
     *      description="Store TypeElections",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TypeElections that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TypeElections")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TypeElections"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateTypeElectionsAPIRequest $request)
    {
        $input = $request->all();

        $typeElections = $this->typeElectionsRepository->create($input);

        return $this->sendResponse($typeElections->toArray(), 'Type Elections enregistré avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/typeElections/{id}",
     *      summary="Display the specified TypeElections",
     *      tags={"TypeElections"},
     *      description="Get TypeElections",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TypeElections",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TypeElections"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var TypeElections $typeElections */
        $typeElections = $this->typeElectionsRepository->findWithoutFail($id);

        if (empty($typeElections)) {
            return $this->sendError('Type Elections non trouvé');
        }

        return $this->sendResponse($typeElections->toArray(), 'Type Elections retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateTypeElectionsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/typeElections/{id}",
     *      summary="Update the specified TypeElections in storage",
     *      tags={"TypeElections"},
     *      description="Update TypeElections",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TypeElections",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TypeElections that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TypeElections")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TypeElections"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateTypeElectionsAPIRequest $request)
    {
        $input = $request->all();

        /** @var TypeElections $typeElections */
        $typeElections = $this->typeElectionsRepository->findWithoutFail($id);

        if (empty($typeElections)) {
            return $this->sendError('Type Elections non trouvé');
        }

        $typeElections = $this->typeElectionsRepository->update($input, $id);

        return $this->sendResponse($typeElections->toArray(), 'TypeElections modifié avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/typeElections/{id}",
     *      summary="Remove the specified TypeElections from storage",
     *      tags={"TypeElections"},
     *      description="Delete TypeElections",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TypeElections",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var TypeElections $typeElections */
        $typeElections = $this->typeElectionsRepository->findWithoutFail($id);

        if (empty($typeElections)) {
            return $this->sendError('Type Elections non trouvé');
        }

        $typeElections->delete();

        return $this->sendResponse($id, 'Type Elections supprimé avec succès');
    }
}
