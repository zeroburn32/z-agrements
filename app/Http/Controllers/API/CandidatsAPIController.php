<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCandidatsAPIRequest;
use App\Http\Requests\API\UpdateCandidatsAPIRequest;
use App\Models\Candidats;
use App\Repositories\CandidatsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CandidatsController
 * @package App\Http\Controllers\API
 */

class CandidatsAPIController extends AppBaseController
{
    /** @var  CandidatsRepository */
    private $candidatsRepository;

    public function __construct(CandidatsRepository $candidatsRepo)
    {
        $this->candidatsRepository = $candidatsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/candidats",
     *      summary="Get a listing of the Candidats.",
     *      tags={"Candidats"},
     *      description="Get all Candidats",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Candidats")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->candidatsRepository->pushCriteria(new RequestCriteria($request));
        $this->candidatsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $candidats = $this->candidatsRepository->all();

        return $this->sendResponse($candidats->toArray(), 'Candidats retrieved successfully');
    }

    /**
     * @param CreateCandidatsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/candidats",
     *      summary="Store a newly created Candidats in storage",
     *      tags={"Candidats"},
     *      description="Store Candidats",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Candidats that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Candidats")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Candidats"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCandidatsAPIRequest $request)
    {
        $input = $request->all();

        $candidats = $this->candidatsRepository->create($input);

        return $this->sendResponse($candidats->toArray(), 'Candidats enregistré avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/candidats/{id}",
     *      summary="Display the specified Candidats",
     *      tags={"Candidats"},
     *      description="Get Candidats",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Candidats",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Candidats"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Candidats $candidats */
        $candidats = $this->candidatsRepository->findWithoutFail($id);

        if (empty($candidats)) {
            return $this->sendError('Candidats non trouvé');
        }

        return $this->sendResponse($candidats->toArray(), 'Candidats retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCandidatsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/candidats/{id}",
     *      summary="Update the specified Candidats in storage",
     *      tags={"Candidats"},
     *      description="Update Candidats",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Candidats",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Candidats that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Candidats")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Candidats"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCandidatsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Candidats $candidats */
        $candidats = $this->candidatsRepository->findWithoutFail($id);

        if (empty($candidats)) {
            return $this->sendError('Candidats non trouvé');
        }

        $candidats = $this->candidatsRepository->update($input, $id);

        return $this->sendResponse($candidats->toArray(), 'Candidats modifié avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/candidats/{id}",
     *      summary="Remove the specified Candidats from storage",
     *      tags={"Candidats"},
     *      description="Delete Candidats",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Candidats",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Candidats $candidats */
        $candidats = $this->candidatsRepository->findWithoutFail($id);

        if (empty($candidats)) {
            return $this->sendError('Candidats non trouvé');
        }

        $candidats->delete();

        return $this->sendResponse($id, 'Candidats supprimé avec succès');
    }
}
