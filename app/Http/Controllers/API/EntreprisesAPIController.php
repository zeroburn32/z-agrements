<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEntreprisesAPIRequest;
use App\Http\Requests\API\UpdateEntreprisesAPIRequest;
use App\Models\Entreprises;
use App\Repositories\EntreprisesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class EntreprisesController
 * @package App\Http\Controllers\API
 */

class EntreprisesAPIController extends AppBaseController
{
    /** @var  EntreprisesRepository */
    private $entreprisesRepository;

    public function __construct(EntreprisesRepository $entreprisesRepo)
    {
        $this->entreprisesRepository = $entreprisesRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/entreprises",
     *      summary="Get a listing of the Entreprises.",
     *      tags={"Entreprises"},
     *      description="Get all Entreprises",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Entreprises")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->entreprisesRepository->pushCriteria(new RequestCriteria($request));
        $this->entreprisesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $entreprises = $this->entreprisesRepository->all();

        return $this->sendResponse($entreprises->toArray(), 'Entreprises retrieved successfully');
    }

    /**
     * @param CreateEntreprisesAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/entreprises",
     *      summary="Store a newly created Entreprises in storage",
     *      tags={"Entreprises"},
     *      description="Store Entreprises",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Entreprises that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Entreprises")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Entreprises"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateEntreprisesAPIRequest $request)
    {
        $input = $request->all();

        $entreprises = $this->entreprisesRepository->create($input);

        return $this->sendResponse($entreprises->toArray(), 'Entreprises enregistré avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/entreprises/{id}",
     *      summary="Display the specified Entreprises",
     *      tags={"Entreprises"},
     *      description="Get Entreprises",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Entreprises",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Entreprises"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Entreprises $entreprises */
        $entreprises = $this->entreprisesRepository->findWithoutFail($id);

        if (empty($entreprises)) {
            return $this->sendError('Entreprises non trouvé');
        }

        return $this->sendResponse($entreprises->toArray(), 'Entreprises retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateEntreprisesAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/entreprises/{id}",
     *      summary="Update the specified Entreprises in storage",
     *      tags={"Entreprises"},
     *      description="Update Entreprises",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Entreprises",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Entreprises that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Entreprises")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Entreprises"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateEntreprisesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Entreprises $entreprises */
        $entreprises = $this->entreprisesRepository->findWithoutFail($id);

        if (empty($entreprises)) {
            return $this->sendError('Entreprises non trouvé');
        }

        $entreprises = $this->entreprisesRepository->update($input, $id);

        return $this->sendResponse($entreprises->toArray(), 'Entreprises mis à jour avec succès');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/entreprises/{id}",
     *      summary="Remove the specified Entreprises from storage",
     *      tags={"Entreprises"},
     *      description="Delete Entreprises",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Entreprises",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Entreprises $entreprises */
        $entreprises = $this->entreprisesRepository->findWithoutFail($id);

        if (empty($entreprises)) {
            return $this->sendError('Entreprises non trouvé');
        }

        $entreprises->delete();

        return $this->sendResponse($id, 'Entreprises supprimé avec succès');
    }
}
