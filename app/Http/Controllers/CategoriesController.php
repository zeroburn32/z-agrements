<?php

namespace App\Http\Controllers;

use DB;
use Flashy;
use Response;
use App\Models\Domaines;
use App\Models\Categories;
use App\Models\TypeTravaux;
use Illuminate\Http\Request;
use App\Repositories\CategoriesRepository;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreateCategoriesRequest;
use App\Http\Requests\UpdateCategoriesRequest;
use Prettus\Repository\Criteria\RequestCriteria;

class CategoriesController extends AppBaseController
{
    /** @var  CategoriesRepository */
    private $categoriesRepository;

    public function __construct(CategoriesRepository $categoriesRepo)
    {
        $this->categoriesRepository = $categoriesRepo;
    }

    /**
     * Display a listing of the Categories.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->categoriesRepository->pushCriteria(new RequestCriteria($request));
        $categories = $this->categoriesRepository->all();

        $categories = DB::table('Categories')
            ->join('Domaines', 'Domaines.id','=','Categories.Dom_id')
            ->join('TypeTravaux', 'TypeTravaux.id','=','Categories.Typ_id')
            ->select('Categories.*','Domaines.libelle_court as dom_court', 'Domaines.libelle_long as dom_long', 'TypeTravaux.libelle_type')
            ->get();

        return view('backend.categories.index')
            ->with('categories', $categories);
    }

    /**
     * Show the form for creating a new Categories.
     *
     * @return Response
     */
    public function create()
    {
        $categorieParentes = Categories::pluck('Categories.libelle_court', 'Categories.id');
        $domaines = Domaines::pluck('Domaines.libelle_court', 'Domaines.id');
        $typetravaux = TypeTravaux::pluck('TypeTravaux.libelle_type', 'TypeTravaux.id');
        return view('backend.categories.create',compact('domaines','typetravaux'))->with('categorieParentes', $categorieParentes);
    }

    /**
     * Store a newly created Categories in storage.
     *
     * @param CreateCategoriesRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoriesRequest $request)
    {
        $input = $request->all();

        $categories = $this->categoriesRepository->create($input);

        Flashy::success('Categories enregistré avec succès.');

        return redirect(route('categories.index'));
    }

    /**
     * Display the specified Categories.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $categories = $this->categoriesRepository->findWithoutFail($id);

        if (empty($categories)) {
            Flashy::error('Categories non trouvé');

            return redirect(route('categories.index'));
        }
        
        return view('backend.categories.show')->with('categories', $categories);
    }

    /**
     * Show the form for editing the specified Categories.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $categories = $this->categoriesRepository->findWithoutFail($id);

        if (empty($categories)) {
            Flashy::error('Categories non trouvé');

            return redirect(route('categories.index'));
        }
        $categorieParentes = Categories::pluck('Categories.libelle_court', 'Categories.id');
        $domaines = Domaines::pluck('Domaines.libelle_court', 'Domaines.id');
        $typetravaux = TypeTravaux::pluck('TypeTravaux.libelle_type', 'TypeTravaux.id');
        return view('backend.categories.edit', compact('domaines', 'typetravaux', 'categorieParentes'))->with('categories', $categories);
    }

    /**
     * Update the specified Categories in storage.
     *
     * @param  int              $id
     * @param UpdateCategoriesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoriesRequest $request)
    {
        $categories = $this->categoriesRepository->findWithoutFail($id);

        if (empty($categories)) {
            Flashy::error('Categories non trouvé');

            return redirect(route('categories.index'));
        }

        $categories = $this->categoriesRepository->update($request->all(), $id);

        Flashy::success('Categories mis à jour avec succès.');

        return redirect(route('categories.index'));
    }

    /**
     * Remove the specified Categories from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $categories = $this->categoriesRepository->findWithoutFail($id);

        if (empty($categories)) {
            Flashy::error('Categories non trouvé');

            return redirect(route('categories.index'));
        }

        $this->categoriesRepository->delete($id);

        Flashy::success('Categories supprimé avec succès.');

        return redirect(route('categories.index'));
    }
}
