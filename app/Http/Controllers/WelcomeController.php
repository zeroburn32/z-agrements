<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WelcomeController extends AppBaseController
{
    //
    public function index() {

        $faqs = DB::table('faqs')->orderby('created_at','asc')->get();
        
        return view('welcome',compact('faqs'));
    }
}
