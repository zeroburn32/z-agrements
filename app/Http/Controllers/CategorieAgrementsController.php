<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCategorieAgrementsRequest;
use App\Http\Requests\UpdateCategorieAgrementsRequest;
use App\Repositories\CategorieAgrementsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flashy;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class CategorieAgrementsController extends AppBaseController
{
    /** @var  CategorieAgrementsRepository */
    private $categorieAgrementsRepository;

    public function __construct(CategorieAgrementsRepository $categorieAgrementsRepo)
    {
        $this->categorieAgrementsRepository = $categorieAgrementsRepo;
    }

    /**
     * Display a listing of the CategorieAgrements.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->categorieAgrementsRepository->pushCriteria(new RequestCriteria($request));
        $categorieAgrements = $this->categorieAgrementsRepository->all();

        return view('backend.categorie_agrements.index')
            ->with('categorieAgrements', $categorieAgrements);
    }

    /**
     * Show the form for creating a new CategorieAgrements.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.categorie_agrements.create');
    }

    /**
     * Store a newly created CategorieAgrements in storage.
     *
     * @param CreateCategorieAgrementsRequest $request
     *
     * @return Response
     */
    public function store(CreateCategorieAgrementsRequest $request)
    {
        $input = $request->all();

        $categorieAgrements = $this->categorieAgrementsRepository->create($input);

        Flashy::success('Categorie Agrements enregistré avec succès.');

        return redirect(route('categorieAgrements.index'));
    }

    /**
     * Display the specified CategorieAgrements.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $categorieAgrements = $this->categorieAgrementsRepository->findWithoutFail($id);

        if (empty($categorieAgrements)) {
            Flashy::error('Categorie Agrements non trouvé');

            return redirect(route('categorieAgrements.index'));
        }

        return view('backend.categorie_agrements.show')->with('categorieAgrements', $categorieAgrements);
    }

    /**
     * Show the form for editing the specified CategorieAgrements.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $categorieAgrements = $this->categorieAgrementsRepository->findWithoutFail($id);

        if (empty($categorieAgrements)) {
            Flashy::error('Categorie Agrements non trouvé');

            return redirect(route('categorieAgrements.index'));
        }

        return view('backend.categorie_agrements.edit')->with('categorieAgrements', $categorieAgrements);
    }

    /**
     * Update the specified CategorieAgrements in storage.
     *
     * @param  int              $id
     * @param UpdateCategorieAgrementsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategorieAgrementsRequest $request)
    {
        $categorieAgrements = $this->categorieAgrementsRepository->findWithoutFail($id);

        if (empty($categorieAgrements)) {
            Flashy::error('Categorie Agrements non trouvé');

            return redirect(route('categorieAgrements.index'));
        }

        $categorieAgrements = $this->categorieAgrementsRepository->update($request->all(), $id);

        Flashy::success('Categorie Agrements mis à jour avec succès.');

        return redirect(route('categorieAgrements.index'));
    }

    /**
     * Remove the specified CategorieAgrements from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $categorieAgrements = $this->categorieAgrementsRepository->findWithoutFail($id);

        if (empty($categorieAgrements)) {
            Flashy::error('Categorie Agrements non trouvé');

            return redirect(route('categorieAgrements.index'));
        }

        $this->categorieAgrementsRepository->delete($id);

        Flashy::success('Categorie Agrements supprimé avec succès.');

        return redirect(route('categorieAgrements.index'));
    }
}
