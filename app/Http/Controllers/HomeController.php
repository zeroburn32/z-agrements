<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //Nombre d'agrements valides par domaine
        $agrementAh = DB::select('select count(DISTINCT(Agrements.id)) AS valeur from Agrements, StatutAgrements where Agrements.id = StatutAgrements.Agr_id AND Agrements.est_actif= 1 AND StatutAgrements.est_actif= 1 AND Sta_id=1 AND Dom_id = 3', []);
        $agrementAeue = DB::select('select count(DISTINCT(Agrements.id)) AS valeur from Agrements, StatutAgrements where Agrements.id = StatutAgrements.Agr_id AND Agrements.est_actif= 1 AND StatutAgrements.est_actif= 1 AND Sta_id=1 AND Dom_id = 2', []);
        $agrementAep = DB::select('select count(DISTINCT(Agrements.id)) AS valeur from Agrements, StatutAgrements where Agrements.id = StatutAgrements.Agr_id AND Agrements.est_actif= 1 AND StatutAgrements.est_actif= 1 AND Sta_id=1 AND Dom_id = 1', []);

        //Nombre d'agrements expirés par domaine
        $agrementAh_exp = DB::select('select count(DISTINCT(Agrements.id)) AS valeur from Agrements, StatutAgrements where Agrements.id = StatutAgrements.Agr_id AND Agrements.est_actif= 1 AND StatutAgrements.est_actif= 1 AND Sta_id=4 AND Dom_id = 3', []);
        $agrementAeue_exp = DB::select('select count(DISTINCT(Agrements.id)) AS valeur from Agrements, StatutAgrements where Agrements.id = StatutAgrements.Agr_id AND Agrements.est_actif= 1 AND StatutAgrements.est_actif= 1 AND Sta_id=4 AND Dom_id = 2', []);
        $agrementAep_exp = DB::select('select count(DISTINCT(Agrements.id)) AS valeur from Agrements, StatutAgrements where Agrements.id = StatutAgrements.Agr_id AND Agrements.est_actif= 1 AND StatutAgrements.est_actif= 1 AND Sta_id=4 AND Dom_id = 1', []);

        //Nombre d'agrements retirés par domaine
        $agrementAh_ret = DB::select('select count(DISTINCT(Agrements.id)) AS valeur from Agrements, StatutAgrements where Agrements.id = StatutAgrements.Agr_id AND Agrements.est_actif= 1 AND StatutAgrements.est_actif= 1 AND Sta_id=2 AND Dom_id = 3', []);
        $agrementAeue_ret = DB::select('select count(DISTINCT(Agrements.id)) AS valeur from Agrements, StatutAgrements where Agrements.id = StatutAgrements.Agr_id AND Agrements.est_actif= 1 AND StatutAgrements.est_actif= 1 AND Sta_id=2 AND Dom_id = 2', []);
        $agrementAep_ret = DB::select('select count(DISTINCT(Agrements.id)) AS valeur from Agrements, StatutAgrements where Agrements.id = StatutAgrements.Agr_id AND Agrements.est_actif= 1 AND StatutAgrements.est_actif= 1 AND Sta_id=2 AND Dom_id = 1', []);
        
        //Nombre total d'agréments actifs dans le système
        $nb_agrements = DB::table('Agrements')->whereEst_actif(1)->count();

        //Nombre total d'agréments valides dans le système
        $nb_agrements_valide = DB::table('Agrements')
            ->join('StatutAgrements', 'StatutAgrements.Agr_id', '=', 'Agrements.id')
            ->where([
                ['Agrements.est_actif', '=', 1],
                ['StatutAgrements.est_actif', '=', 1],
                ['StatutAgrements.Sta_id', '=', 1]
            ])
            ->count();

        //Nombre total d'agréments expirés dans le système
        $nb_agrements_expire = DB::table('Agrements')
            ->join('StatutAgrements', 'StatutAgrements.Agr_id', '=', 'Agrements.id')
            ->where([
                ['Agrements.est_actif', '=', 1],
                ['StatutAgrements.est_actif', '=', 1],
                ['StatutAgrements.Sta_id', '=', 4]
            ])
            ->count();

        //Nombre total d'agréments retirés dans le système
        $nb_agrements_retire =  DB::table('Agrements')
            ->join('StatutAgrements', 'StatutAgrements.Agr_id', '=', 'Agrements.id')
            ->where([
                ['Agrements.est_actif', '=', 1],
                ['StatutAgrements.est_actif', '=', 1],
                ['StatutAgrements.Sta_id', '=', 2]
            ])
            ->count();

        $nb_agrements_by_annee = DB::table('Agrements')
            ->join('Domaines', 'Domaines.id', '=', 'Agrements.Dom_id')
            ->select( DB::raw('year(date_arrete) as annee'), DB::raw('count(Agrements.id) as nombre'))
            ->where([
                ['Agrements.est_actif', '=', 1],
            ])
            ->groupby('annee')
            ->get();

            foreach ($nb_agrements_by_annee as $key => $value) {
            $nb_agrements_by_annee[$key]->nb_aep = DB::table('Agrements')
                ->join('Domaines', 'Domaines.id', '=', 'Agrements.Dom_id')
                ->select(DB::raw('count(Agrements.id) as nombre'))
                ->where([
                    ['Agrements.est_actif', '=', 1],
                    ['Domaines.libelle_court', '=', 'AEP'],
                    [DB::raw('year(date_arrete)'), '=', $value->annee],
                ])
                ->value('nombre');

            $nb_agrements_by_annee[$key]->nb_aeue = DB::table('Agrements')
                ->join('Domaines', 'Domaines.id', '=', 'Agrements.Dom_id')
                ->select(DB::raw('count(Agrements.id) as nombre'))
                ->where([
                    ['Agrements.est_actif', '=', 1],
                    ['Domaines.libelle_court', '=', 'AEUE'],
                    [DB::raw('year(date_arrete)'), '=', $value->annee],
                ])
                ->value('nombre');

            $nb_agrements_by_annee[$key]->nb_ah = DB::table('Agrements')
                ->join('Domaines', 'Domaines.id', '=', 'Agrements.Dom_id')
                ->select(DB::raw('count(Agrements.id) as nombre'))
                ->where([
                    ['Agrements.est_actif', '=', 1],
                    ['Domaines.libelle_court', '=', 'AH'],
                    [DB::raw('year(date_arrete)'), '=', $value->annee],
                ])
                ->value('nombre');;
            }
        
        return view('home', 
        compact('nb_agrements','nb_agrements_retire', 'nb_agrements_expire', 'nb_agrements_valide',
                'agrementAh', 'agrementAeue', 'agrementAep',
                'agrementAh_exp','agrementAeue_exp','agrementAep_exp',
                'agrementAh_ret','agrementAeue_ret','agrementAep_ret', 'nb_agrements_by_annee'
    ));
    }
}
