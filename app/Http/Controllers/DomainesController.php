<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDomainesRequest;
use App\Http\Requests\UpdateDomainesRequest;
use App\Repositories\DomainesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flashy;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class DomainesController extends AppBaseController
{
    /** @var  DomainesRepository */
    private $domainesRepository;

    public function __construct(DomainesRepository $domainesRepo)
    {
        $this->domainesRepository = $domainesRepo;
    }

    /**
     * Display a listing of the Domaines.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->domainesRepository->pushCriteria(new RequestCriteria($request));
        $domaines = $this->domainesRepository->all();

        return view('backend.domaines.index')
            ->with('domaines', $domaines);
    }

    /**
     * Show the form for creating a new Domaines.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.domaines.create');
    }

    /**
     * Store a newly created Domaines in storage.
     *
     * @param CreateDomainesRequest $request
     *
     * @return Response
     */
    public function store(CreateDomainesRequest $request)
    {
        $input = $request->all();

        $domaines = $this->domainesRepository->create($input);

        Flashy::success('Domaines enregistré avec succès.');

        return redirect(route('domaines.index'));
    }

    /**
     * Display the specified Domaines.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $domaines = $this->domainesRepository->findWithoutFail($id);

        if (empty($domaines)) {
            Flashy::error('Domaines non trouvé');

            return redirect(route('domaines.index'));
        }

        return view('backend.domaines.show')->with('domaines', $domaines);
    }

    /**
     * Show the form for editing the specified Domaines.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $domaines = $this->domainesRepository->findWithoutFail($id);

        if (empty($domaines)) {
            Flashy::error('Domaines non trouvé');

            return redirect(route('domaines.index'));
        }

        return view('backend.domaines.edit')->with('domaines', $domaines);
    }

    /**
     * Update the specified Domaines in storage.
     *
     * @param  int              $id
     * @param UpdateDomainesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDomainesRequest $request)
    {
        $domaines = $this->domainesRepository->findWithoutFail($id);

        if (empty($domaines)) {
            Flashy::error('Domaines non trouvé');

            return redirect(route('domaines.index'));
        }

        $domaines = $this->domainesRepository->update($request->all(), $id);

        Flashy::success('Domaines mis à jour avec succès.');

        return redirect(route('domaines.index'));
    }

    /**
     * Remove the specified Domaines from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $domaines = $this->domainesRepository->findWithoutFail($id);

        if (empty($domaines)) {
            Flashy::error('Domaines non trouvé');

            return redirect(route('domaines.index'));
        }

        $this->domainesRepository->delete($id);

        Flashy::success('Domaines supprimé avec succès.');

        return redirect(route('domaines.index'));
    }
}
