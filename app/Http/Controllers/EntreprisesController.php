<?php

namespace App\Http\Controllers;

use DB;
use Flashy;
use Response;
use App\Models\Regions;
use App\Models\Communes;
use App\Models\Provinces;
use App\Models\Entreprises;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Repositories\EntreprisesRepository;
use App\Http\Requests\CreateEntreprisesRequest;
use App\Http\Requests\UpdateEntreprisesRequest;
use Prettus\Repository\Criteria\RequestCriteria;

class EntreprisesController extends AppBaseController
{
    /** @var  EntreprisesRepository */
    private $entreprisesRepository;

    public function __construct(EntreprisesRepository $entreprisesRepo)
    {
        $this->entreprisesRepository = $entreprisesRepo;
    }

    /**
     * Display a listing of the Entreprises.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->entreprisesRepository->pushCriteria(new RequestCriteria($request));
        $entreprises = $this->entreprisesRepository->all();

        $entreprises = DB::table('Entreprises')
        ->leftjoin('Regions','Regions.id','=','Entreprises.Reg_id')
        ->leftjoin('Communes','Communes.id','=','Entreprises.Com_id')
        ->select('Entreprises.*','Communes.libelle_commune','Regions.libelle_region')
        ->get();

        $regions =  Regions::pluck('libelle_region', 'id')->toArray();
        $provinces =  Provinces::pluck('libelle_province', 'id')->toArray();
        $communes =  Communes::pluck('libelle_commune', 'id')->toArray(); 

        return view('backend.entreprises.index', compact('regions','communes','provinces'))
            ->with('entreprises', $entreprises);
    }

    public function getEntreprise($id)
    {
        $entreprise = DB::table('Entreprises')
            ->where('Entreprises.id', '=', $id)
            ->Select('Entreprises.*')
            ->first();

        return Response::json($entreprise);
    }

    /**
     * Show the form for creating a new Entreprises.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.entreprises.create');
    }

    /**
     * Store a newly created Entreprises in storage.
     *
     * @param CreateEntreprisesRequest $request
     *
     * @return Response
     */
    public function store(CreateEntreprisesRequest $request, UpdateEntreprisesRequest $requestUpdate)
    {
        $input = $request->all();
        if($input['formMode'] == 'create') {
        $entreprises = $this->entreprisesRepository->create($input);

            if ($request->ajax()) {
                return Response::json($entreprises);
            }

        activity()->log('Nouvelle entreprise ajoutée');
        Flashy::success('Entreprises enregistré avec succès.'); 
        } else {
            app('App\Http\Controllers\EntreprisesController')->update($input['ent_id'], $requestUpdate);
            activity()->log('Entreprise modifiée ');
            Flashy::success('Entreprises mise à jour avec succès.');
        }

        return redirect(route('entreprises.index'));
    }

    /**
     * Display the specified Entreprises.
     *
     * @param  int $id col-sm-6
     *
     * @return Response
     */
    public function show($id)
    {
        $entreprises = $this->entreprisesRepository->findWithoutFail($id);

        if (empty($entreprises)) {
            Flashy::error('Entreprises non trouvé');

            return redirect(route('entreprises.index'));
        }

        return view('backend.entreprises.show')->with('entreprises', $entreprises);
    }

    /**
     * Show the form for editing the specified Entreprises.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $entreprises = $this->entreprisesRepository->findWithoutFail($id);

        if (empty($entreprises)) {
            Flashy::error('Entreprises non trouvé');

            return redirect(route('entreprises.index'));
        }

        return view('backend.entreprises.edit')->with('entreprises', $entreprises);
    }

    /**
     * Update the specified Entreprises in storage.
     *
     * @param  int              $id
     * @param UpdateEntreprisesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEntreprisesRequest $request)
    {
        $entreprises = $this->entreprisesRepository->findWithoutFail($id);

        if (empty($entreprises)) {
            Flashy::error('Entreprises non trouvé');

            return redirect(route('entreprises.index'));
        }

        $entreprises = $this->entreprisesRepository->update($request->all(), $id);

        Flashy::success('Entreprises mis à jour avec succès.');

        return redirect(route('entreprises.index'));
    }

    /**
     * Remove the specified Entreprises from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $entreprises = $this->entreprisesRepository->findWithoutFail($id);

        if (empty($entreprises)) {
            Flashy::error('Entreprises non trouvé');

            return redirect(route('entreprises.index'));
        }

        $this->entreprisesRepository->delete($id);

        Flashy::success('Entreprises supprimé avec succès.');

        return redirect(route('entreprises.index'));
    }
}
