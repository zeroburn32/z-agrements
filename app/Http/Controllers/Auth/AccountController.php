<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Auth;
use Flashy;
use App\User;
use Hash;
use Illuminate\Support\Facades\Mail;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('guest')->except('logout');
    }

    public function formPassword()
    {
        $user = Auth::user();
        return view('auth.changePwd')->with('user', $user);
    }

    /** Permet d'enregistrer le changement de password
     * @return redirection sur le dashboard
     */
    public function changePassword()
    {
        if (!(Hash::check(request('old_password'), Auth::user()->password))) {
            Flashy::error('L\'ancien mot de passe est incorrect !');
            return back();
        } else {
            if (strcmp(request('old_password'), request('password')) == 0) {
                Flashy::error('Le nouveau mot de passe ne peut pas être identique à l\'ancien !');
                return back();
            } else {
                if (strcmp(request('password'), request('confirm_password')) == 0) {
                    if (preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}$#', request('password'))) {
                        $user = Auth::user();
                        $user->password = bcrypt(request('password'));
                        $user->save();
                        Flashy::success('Mot de passe modifié avec succès !');
                        return redirect(route('logout'));
                    } else {
                        Flashy::error('Le mot de passe ne répond aux criteres de complexité réquis !');
                        return back();
                    }
                } else {
                    Flashy::error('Le nouveau mot de passe n\'est pas identique à la confirmation !');
                    return back();
                }

            }
        }
    }

    public function lockAndUnlock($id)
    {
        $userConnect = auth()->user();
        $user = User::findOrFail($id);
        if (empty($user)) {
            Flashy::error('Compte non trouvé !');
            return redirect(route('admin/users'));
        }
        if ($user->active == true) {
            $user->active = false;
        } else {
            $user->active = true;
        }
        if ($user->id == $userConnect->id) {
            Flashy::error('Désolé vous ne pouvez pas désactivé vous même votre compte !');
            return redirect(route('admin/users'));
        }
        $user->save();
        Flashy::success('Autorisation du compte modifié avec succès !');
    }

    public function reinitialiserMotDePasse($id)
    {
        // on recherche l'utilisateur
        $user = User::findOrFail($id);
        // on genere et attribue le nouveau mot de passe
        $newPassword = "";
        if (!empty($user)) {
            $newPassword = $this->genereMotDePasse(8);
            $user->password = bcrypt($newPassword);
            $user->firstconnect = true;
            // on enregistre le changement
            $user->save();
        }
        // on envoi un mail si l'utilisateur possede une adresse mail
        if (($user->email != null || !empty($user->email)) && $newPassword != "") {
            $contenu = "Votre nouveau mot de passe est : " . $newPassword;
            $objet = "Réinitialisation de mot de passe";
            $email = $user->email;
            $data = ['nom' => $user->name, "email" => $user->email, "contenu" => $contenu];
            try {
                Mail::send('emails.messages.compte', $data, function ($message) use ($objet, $email) {
                    $message->to($email)->subject($objet);
                });
            } catch (\Swift_TransportException $e) {
                return $newPassword . " ! Echec d'envoi par mail";
            }
        } else {
            return $newPassword . " ! Echec d'envoi par email (l'utilisateur ne possède pas d'email)";
        }
        return $newPassword;
    }

    /** Permet de generer un mot de passe aleatoire de longueur : $taille
     * @param $longueur
     * @return string
     */
    public function genereMotDePasse($longueur)
    {
        // chaine de caractères qui sera mis dans le désordre:
        $Chaine = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"; // 62 caractères au total
        // on mélange la chaine
        $Chaine = str_shuffle($Chaine);
        // ensuite on coupe à la longueur voulue
        $Chaine = substr($Chaine, 0, $longueur);
        // ensuite on retourne notre mot de passe
        return $Chaine;
    }
}
