<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;
use Flashy;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('login');
    }

    public function authenticate(Request $request)
    {
        /* $code = $request->input('CaptchaCode');
        $isHuman = captcha_validate($code); */
        $isHuman=1;
        if ($isHuman) {
            $user = DB::table('users')->where('email', $request->email)->first();
            if (empty($user)) {
                Flashy::error('Echec de connexion, verifier vos identifiants de connexion ou veuillez contacter l\'administrateur !');
                return redirect('login');
            } else {
                if ($user->active == true) {
                    $userdata = array(
                        'email' => request('email'),
                        'password' => request('password')
                    );
                    if (Auth::attempt($userdata)) {
                        if ($user->firstconnect == false) {
                            return redirect()->intended('home');
                        } else {
                            Auth::logout();
//                        Flashy::warning('Nous vous prions de changer votre mot de passe par mésure de confidentialité!');
                            return view('auth.firstConnect')->with('user', $user);
                        }
                    } else {
                        Flashy::error('Echec de connexion, verifier vos identifiants de connexion ou veuillez contacter l\'administrateur !');
                        return redirect('login');
                    }
                } else {
                    Flashy::error('Echec de connexion, verifier vos identifiants de connexion ou veuillez contacter l\'administrateur !');
                    return redirect('login');
                }
            }
        } else {
            Flashy::error('Echec de verification du captcha. Reessayez!');
            return redirect('login');
        }
    }

    /** Permet d'enregistrer le changement de password à la première connexion
     * @param $id
     * @return redirection sur la page de connexion
     */
    public function firstConnect($id)
    {
//        $user = $this->usersRepository->findWithoutFail($id);
        $user = DB::table('users')->where('id', $id)->get()->first();
        if (strcmp(request('newPassword'), request('confirmPassword')) == 0) {
            if (preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}$#', request('newPassword'))) {
                DB::table('users')->where('id', $id)->update(['password' => bcrypt(request('newPassword')), 'firstconnect' => false, 'updated_at' => new Carbon()]);
                Flashy::success('Mot de passe modifié avec succès !');
                return redirect(route('home'));
            } else {
                Flashy::error('Le mot de passe ne répond aux criteres de complexité réquis !');
                return view('auth.firstConnect')->with('user', $user);
            }
        } else {
            Flashy::error('Le nouveau mot de passe n\'est pas identique à la confirmation !');
            return view('auth.firstConnect')->with('user', $user);
        }
    }
}
