<?php

namespace App\Repositories;

use App\Models\Entreprises;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class EntreprisesRepository
 * @package App\Repositories
 * @version September 6, 2019, 10:39 am UTC
 *
 * @method Entreprises findWithoutFail($id, $columns = ['*'])
 * @method Entreprises find($id, $columns = ['*'])
 * @method Entreprises first($columns = ['*'])
*/
class EntreprisesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'Com_id',
        'Reg_id',
        'raison_socaile',
        'siege_social',
        'adresse',
        'telephone',
        'registre_commerce',
        'numero_ifu',
        'personne_responsable',
        'numero_cnss'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Entreprises::class;
    }
}
