<?php

namespace App\Repositories;

use App\Models\Statuts;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class StatutsRepository
 * @package App\Repositories
 * @version September 6, 2019, 10:39 am UTC
 *
 * @method Statuts findWithoutFail($id, $columns = ['*'])
 * @method Statuts find($id, $columns = ['*'])
 * @method Statuts first($columns = ['*'])
*/
class StatutsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'libelle_statut',
        'observation'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Statuts::class;
    }
}
