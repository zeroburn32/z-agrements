<?php

namespace App\Repositories;

use App\Models\Communes;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CommunesRepository
 * @package App\Repositories
 * @version September 6, 2019, 10:39 am UTC
 *
 * @method Communes findWithoutFail($id, $columns = ['*'])
 * @method Communes find($id, $columns = ['*'])
 * @method Communes first($columns = ['*'])
*/
class CommunesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'Pro_id',
        'code_commune',
        'libelle_commune'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Communes::class;
    }
}
