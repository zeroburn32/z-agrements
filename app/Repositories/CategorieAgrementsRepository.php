<?php

namespace App\Repositories;

use App\Models\CategorieAgrements;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CategorieAgrementsRepository
 * @package App\Repositories
 * @version September 6, 2019, 10:39 am UTC
 *
 * @method CategorieAgrements findWithoutFail($id, $columns = ['*'])
 * @method CategorieAgrements find($id, $columns = ['*'])
 * @method CategorieAgrements first($columns = ['*'])
*/
class CategorieAgrementsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'Cat_id',
        'Agr_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CategorieAgrements::class;
    }
}
