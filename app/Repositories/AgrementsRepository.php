<?php

namespace App\Repositories;

use App\Models\Agrements;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AgrementsRepository
 * @package App\Repositories
 * @version September 6, 2019, 10:39 am UTC
 *
 * @method Agrements findWithoutFail($id, $columns = ['*'])
 * @method Agrements find($id, $columns = ['*'])
 * @method Agrements first($columns = ['*'])
*/
class AgrementsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'Ent_id',
        'Typ_id',
        'Dom_id',
        'numero_agrement',
        'numero_arrete',
        'observation',
        'est_expire',
        'date_expiration'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Agrements::class;
    }
}
