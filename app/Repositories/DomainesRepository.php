<?php

namespace App\Repositories;

use App\Models\Domaines;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class DomainesRepository
 * @package App\Repositories
 * @version September 6, 2019, 10:39 am UTC
 *
 * @method Domaines findWithoutFail($id, $columns = ['*'])
 * @method Domaines find($id, $columns = ['*'])
 * @method Domaines first($columns = ['*'])
*/
class DomainesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'libelle_court',
        'libelle_long'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Domaines::class;
    }
}
