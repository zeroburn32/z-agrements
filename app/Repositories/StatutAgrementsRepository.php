<?php

namespace App\Repositories;

use App\Models\StatutAgrements;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class StatutAgrementsRepository
 * @package App\Repositories
 * @version September 6, 2019, 10:39 am UTC
 *
 * @method StatutAgrements findWithoutFail($id, $columns = ['*'])
 * @method StatutAgrements find($id, $columns = ['*'])
 * @method StatutAgrements first($columns = ['*'])
*/
class StatutAgrementsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'Agr_id',
        'Sta_id',
        'date',
        'observation',
        'est_actif'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StatutAgrements::class;
    }
}
