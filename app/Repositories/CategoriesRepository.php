<?php

namespace App\Repositories;

use App\Models\Categories;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CategoriesRepository
 * @package App\Repositories
 * @version October 3, 2019, 9:04 am UTC
 *
 * @method Categories findWithoutFail($id, $columns = ['*'])
 * @method Categories find($id, $columns = ['*'])
 * @method Categories first($columns = ['*'])
*/
class CategoriesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'Cat_id',
        'Dom_id',
        'Typ_id',
        'libelle_court',
        'libelle_long',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Categories::class;
    }
}
