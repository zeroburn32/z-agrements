<?php

namespace App\Repositories;

use App\Models\Regions;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RegionsRepository
 * @package App\Repositories
 * @version September 6, 2019, 10:34 am UTC
 *
 * @method Regions findWithoutFail($id, $columns = ['*'])
 * @method Regions find($id, $columns = ['*'])
 * @method Regions first($columns = ['*'])
*/
class RegionsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code_region',
        'libelle_region'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Regions::class;
    }
}
