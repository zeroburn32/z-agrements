<?php

namespace App\Repositories;

use App\Models\TypeTravaux;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TypeTravauxRepository
 * @package App\Repositories
 * @version September 6, 2019, 10:39 am UTC
 *
 * @method TypeTravaux findWithoutFail($id, $columns = ['*'])
 * @method TypeTravaux find($id, $columns = ['*'])
 * @method TypeTravaux first($columns = ['*'])
*/
class TypeTravauxRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'libelle_type',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TypeTravaux::class;
    }
}
