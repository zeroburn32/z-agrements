<?php

namespace App\Repositories;

use App\Models\Provinces;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ProvincesRepository
 * @package App\Repositories
 * @version September 6, 2019, 10:35 am UTC
 *
 * @method Provinces findWithoutFail($id, $columns = ['*'])
 * @method Provinces find($id, $columns = ['*'])
 * @method Provinces first($columns = ['*'])
*/
class ProvincesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'Reg_id',
        'code_province',
        'libelle_province'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Provinces::class;
    }
}
